/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <atomic>
#include <cstddef>
#include <cstdint>
#include <mutex>
#include <stdexcept>

#include "buffercontextcache.hh"
#include "filedescriptor.hh"

namespace larod {

/**
 * @brief Class representing a buffer backed by a file descriptor.
 */
class Buffer {
    // Used for giving access to private function isSaved().
    friend class BufferMessenger;

public:
    Buffer(FileDescriptor&& fd, const size_t maxSize, const int64_t startOffset,
           const uint32_t fdProps);

    /**
     * @brief Destructor.
     *
     * Destructs the @c Buffer by removing any cached memory mappings and closes
     * the underlying file descriptor.
     *
     * All instances of @c BufferContextsCache that have been supplied as an
     * argument to addObserver(), will be notified through their respective @c
     * BufferContextCache::deleteBufferContext(). In these methods, all
     * context-based data associated to this @c Buffer should be deleted.
     */
    virtual ~Buffer();

    Buffer(const Buffer&) = delete;
    Buffer& operator=(const Buffer&) = delete;

    /**
     * @brief Get fd.
     *
     * @return Fd backing this buffer.
     */
    const FileDescriptor& getFd() const { return fd; }

    /**
     * @brief Get maximum size.
     *
     * A value of zero means that there is no explicit maximum size.
     *
     * @return The maxiumum size.
     */
    size_t getMaxSize() const { return maxSize; }

    /**
     * @brief Get start offset.
     *
     * @return The start offset for the backing fd.
     */
    int64_t getStartOffset() const { return startOffset; }

    /**
     * @brief Get fd properties.
     *
     * @return The fd properties.
     */
    uint32_t getFdProps() const { return fdProps; }

    /**
     * @brief Read from @c fd.
     *
     * Fd offset will be set to @c startOffset and @p sz bytes will be read. In
     * case of errors, @c FileDescriptorError will be thrown.
     *
     * @param buf Buffer to store read bytes.
     * @param sz Number of bytes to read.
     */
    void read(const span<uint8_t>& buf, const size_t sz) const;

    /**
     * @brief Read from @c fd.
     *
     * Fd offset will be set to @c startOffset and @c buf.size() bytes will be
     * read. In case of errors, @c FileDescriptorError will be thrown.
     *
     * @param buf Buffer to store read bytes.
     */
    void read(const span<uint8_t>& buf) const;

    /**
     * @brief Write to @c fd.
     *
     * Fd offset will be set to @c startOffset and @p sz bytes will be written.
     * In case of errors, @c FileDescriptorError will be thrown.
     *
     * @param buf Buffer that stores the data that will be written.
     * @param sz Specifies how many bytes will be written.
     */
    void write(const span<uint8_t>& buf, const size_t sz) const;

    /**
     * @brief Write to @c fd.
     *
     * Fd offset will be set to @c startOffset and @c buf.size() bytes will be
     * written. In case of errors, @c FileDescriptorError will be thrown.
     *
     * @param buf Buffer that stores the data that will be written.
     */
    void write(const span<uint8_t>& buf) const;

    /**
     * @brief Get a mapping of @c fd that's at least @p sz bytes large.
     *
     * This is a convenience function that handles the logic of creating a new
     * mapping of @c fd only when it's necessary. The mapping is valid as long
     * as this object's lifetime.
     *
     * In case of errors, @c FileDescriptorError will be thrown. In particular
     * this will happen if there already is a mapping of @c fd that's strictly
     * smaller than @p sz bytes or the existing mapping does not fill the
     * requirements of @p reqMmapProt.
     *
     * If a new mapping is made, PROT_READ and PROT_WRITE bits set in the @p
     * reqMmapProt bitmask will act as requirements on the mapping and sanity
     * checked against the file access rights of @c fd.
     *
     * If a new mapping is made it will have reading rights if and only if the
     * file access rights of @c fd permit it no matter what is specified in @p
     * reqMmapProt, and the same goes for writing rights.
     *
     * @param sz Speficies a minimum size of the requested mapping of @c fd.
     * @param reqMmapProt Minimum mmap prot required for mapping. Only PROT_READ
     * and PROT_WRITE are valid bits, and either or both must be set.
     * @return A mapping of @c fd at least @p sz bytes large. Do not manipulate
     * this pointer! E.g. don't try to free, realloc or unmap it!
     */
    uint8_t* getMapping(const size_t sz, const int reqMmapProt);

    /**
     * @brief Add @p cache as an observer of this @c Buffer.
     *
     * See documentation for @c BufferContextCache for more information.
     *
     * @p cache will be notified through @c
     * BufferContextCache::deleteBufferContext() before this @c Buffer is
     * destructed.
     */
    void addObserver(BufferContextCache* cache);

    /**
     * @brief Get the unique cookie.
     *
     * @return The unique cookie value for this @c Buffer.
     */
    uint64_t getCookie() const { return COOKIE; }

    /**
     * @brief Check if this buffer is saved.
     *
     * A saved buffer means that there is an owner that has intended to save
     * this buffer for an extended period of time, i.e. it will most likely not
     * destruct in the near future. This can be used to do additonal decisions
     * for this buffer, e.g. setup a long-lived @c BufferContextCache through
     * addObserver().
     *
     * @return True if this buffer has been saved, otherwise false.
     */
    bool isSaved() const { return saved; }

    /**
     * @brief Get supportedFdProps as a string.
     *
     * @return a comma separated string representing the supportedFdProps
     * bitmask.
     */
    static std::string getPropNames(uint32_t props);

    /**
     * @brief Get maximum access rights.
     *
     * Returns a bitmask consisting of as many access rights as possible for a
     * file descriptor to use for mmap for example. Throws FileDescriptorError
     * if the reqMapProt access rights are missing.
     *
     * @param fd File descriptor to get access rights to.
     * @param reqMmapProt Bitmask of minimum access rights.
     * @return Access rights, e.g. PROT_READ | PROT_WRITE.
     */
    static int getMaxProt(int fd, const int reqMmapProt);

protected:
    FileDescriptor fd;
    size_t maxSize = 0; // Zero means there is no explicit max size.
    int64_t startOffset = 0;
    uint32_t fdProps = 0;

private:
    static inline std::string THIS_NAMESPACE = "Buffer::";
    /// Incrementing counter that specifies the next @c Buffer's cookie.
    static inline std::atomic<uint64_t> nextCookie = 1;

    /// Set containing all observers that have been supplied as an argument to
    /// addObserver() at least once.
    std::unordered_set<BufferContextCache*> bufferObservers;

    mutable std::mutex mtxFileOffset; ///< Mutex for file offset.
    std::mutex mtxMapping;            ///< Mutex for mapping.
    std::mutex mtxObservers;          ///< Mutex for @c bufferObservers

    /// Address to mapping of @c fd if it has been mapped, nullptr otherwise.
    std::shared_ptr<uint8_t[]> mappedAddr = nullptr;
    /// Address to part of mapping of @c fd that may be accessed by the service.
    /// Set to nullptr if there is no mapping.
    uint8_t* mappedAccessAddr = nullptr;
    /// Number of mapped bytes accessible to the service at @c mappedAccessAddr.
    size_t mappedAccessSz = 0;
    /// The prot bitmask that was used to map if mapping valid, -1 otherwise.
    int mappedMmapProt = -1;

    const uint64_t COOKIE; ///< A global unique identifier of this @c Buffer.

    /// Indication if this buffer has been saved by someone (c.f. isSaved());
    std::atomic<bool> saved = false;

    /**
     * @brief Map @c fd.
     *
     * Maps @c fd in such a way that @p sz bytes will be accessible to the
     * service at @c startOffset.
     *
     * PROT_READ and PROT_WRITE bits set in the @p reqMmapProt bitmask will act
     * as requirements on the mapping and sanity checked against the file access
     * rights of @c fd.
     *
     * The mapping will have reading rights if and only if the file access
     * rights of @c fd permit it no matter what is specified in @p reqMmapProt,
     * and the same goes for writing rights.
     *
     * @param sz Number of bytes to map at @c startOffset of @c fd.
     * @param reqMmapProt Minimum mmap prot required for mapping.
     */
    void map(const size_t sz, const int reqMmapProt);

    /**
     * @brief Set saved status.
     *
     * @c BufferMessenger is used to grant permission for calling this function.
     *
     * @param saved True if it has been saved, otherwise false.
     */
    void isSaved(const bool saved) { this->saved = saved; }
};

struct FileDescriptorError : std::runtime_error {
    FileDescriptorError(const std::string& msg) : std::runtime_error(msg) {}
};

} // namespace larod
