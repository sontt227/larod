/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include "larod.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    DMA_INVALID = 0,
    DMA_CAVALRY = 1,
    NUM_BUFFER_TYPES = 3,
} BufferType;

/**
 * @brief A dma-buffer struct that is used in the convenience functions below.
 */
typedef struct DmaBuffer {
    int fd;
    size_t size;
    void* data;
    BufferType type;
} DmaBuffer;

/**
 * @brief Allocate dma buffer.
 *
 * Allocates a dma buffer and maps it into virtual memory. Release the
 * returned buffer with freeDmaBuffer(). The allocated buffer may be slightly
 * larger than @p size due to platform specific requirements.
 * @c DmaBuffer#size contains the actually allocated buffer size.
 *
 * @param size Byte size of buffer.
 * @param chip Specifies what kind of dma buffer that will be allocated.
 * @return Pointer to allocated dma buffer and NULL in case of failure.
 */
DmaBuffer* allocDmaBuffer(size_t size, larodChip chip);

/**
 * @brief Free buffer.
 *
 * @param buffer Dma buffer.
 * @return true if successful; false otherwise.
 */
bool freeDmaBuffer(DmaBuffer* buffer);

/**
 * @brief Copy data into dma buffer.
 *
 * Copies the data pointed to by @p data into dma buffer allocated with e.g.
 * @c allocDmaBuffer().
 *
 * @param data Pointer to data to be copied into buffer.
 * @param size Byte size of data to be copied.
 * @param buffer Pointer to dma buffer to be filled with data.
 * @return true if successful; false otherwise.
 */
bool copyDataToBuffer(void* data, size_t size, DmaBuffer* buffer);

/**
 * @brief Copy data into generic dmabuf.
 *
 * Copies the data pointed to by @p data into a dmabuf identified by @p fd.
 * Corresponding ioctl:s will be called to mark CPU access start and end.
 *
 * @param data Pointer to data to be copied into the dmabuf.
 * @param size Byte size of data to be copied.
 * @param fd Destination dmabuf file descriptor.
 * @return true if successful; false otherwise.
 */
bool copyDataToDmabuf(void* data, size_t size, int fd);

/**
 * @brief Read data from generic dmabuf.
 *
 * Copies data from dmabuf @p fd into the memory buffer buffer @p data.
 * Corresponding ioctl:s will be called to mark CPU access start and end.
 *
 * @param fd Source dmabuf file descriptor.
 * @param data Pointer to destination mem buffer.
 * @param size Byte size of data to be copied.
 * @return true if successful; false otherwise.
 */
bool copyDataFromDmabuf(int fd, void* data, size_t size);

/**
 * @brief Copy data from file into dma buffer.
 *
 * Opens the given file, maps it into memory, allocates dma buffer and calls
 * copyDataToDmaBuf() to copy the file's data into the dma buffer.
 *
 * @param filePath Path to file.
 * @param chip Specifies what kind of dma buffer that will be allocated.
 * @return Pointer to allocateed DmaBuffer, NULL in case of error.
 */
DmaBuffer* allocDmaBufferFromFile(const char* filePath, larodChip chip);

#ifdef __cplusplus
}
#endif
