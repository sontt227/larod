/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "linkedlist.h"

#include <assert.h>
#include <errno.h>
#include <stdlib.h>

struct LinkedList {
    Node* first;
    Node* last;
    uint64_t size; ///< Number of elements.
};

static int newNode(Node** node, void* data) {
    assert(node);
    assert(data);

    *node = malloc(sizeof(Node));
    if (!(*node)) {
        return ENOMEM;
    }

    (*node)->data = data;
    (*node)->next = NULL;

    return 0;
}

int listConstruct(LinkedList** list) {
    if (!list) {
        return EINVAL;
    }

    *list = malloc(sizeof(LinkedList));
    if (!(*list)) {
        return ENOMEM;
    }

    (*list)->first = NULL;
    (*list)->last = NULL;
    (*list)->size = 0;

    return 0;
}

void listDestruct(LinkedList** list) {
    if (!list || !(*list)) {
        return;
    }

    if (!(*list)->first) {
        // Empty list.
        goto end;
    }

    Node* node = (*list)->first;
    Node* next;
    while (node) {
        next = node->next;
        free(node);
        node = next;
    }

end:
    free(*list);
    *list = NULL;
}

int listPushBack(LinkedList* list, void* data) {
    if (!list || !data) {
        return EINVAL;
    }

    Node* node;
    int ret = newNode(&node, data);
    if (ret) {
        return ret;
    }

    if (!list->first) {
        // Empty list, add first.
        list->first = node;
    } else {
        list->last->next = node;
    }

    list->last = node;
    ++list->size;

    return 0;
}

int listErase(LinkedList* list, void* data) {
    if (!list || !data) {
        return EINVAL;
    }

    if (!list->first) {
        return ENODATA;
    }

    Node* node = list->first;
    if (node->data == data) {
        // Erasing at the beginning.
        list->first = node->next;

        goto end;
    }

    Node* prev = node;
    node = node->next;
    while (node) {
        if (node->data == data) {
            prev->next = node->next;

            if (node == list->last) {
                // Erasing last element. We need to update last reference.
                list->last = prev;
            }

            goto end;
        }

        prev = node;
        node = node->next;
    }

    return ENODATA;

end:
    free(node);
    --list->size;

    return 0;
}

int listGetSize(LinkedList* list, uint64_t* size) {
    if (!list || !size) {
        return EINVAL;
    }

    *size = list->size;

    return 0;
}

int listGetFirstNode(LinkedList* list, const Node** firstNode) {
    if (!list || !firstNode) {
        return EINVAL;
    }

    *firstNode = list->first;

    return 0;
}
