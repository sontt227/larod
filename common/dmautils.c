/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "dmautils.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/dma-buf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "dmacavalry.h"
#include "dmaoperations.h"
#include "log.h"

// Map of buffer type to set of corresponding functions.
static const FdOps* fdOps[NUM_BUFFER_TYPES] = {NULL};

// Number of buffers allocated.
static size_t numBuffers[NUM_BUFFER_TYPES] = {0};

static bool accessDmabuf(void* data, size_t size, int dmaBufFd, bool doWrite);

/**
 * @brief Initializes the dma-buffer frameworks if necessary.
 *
 * @return False if there is no dma framework implemented for the given type or
 * in case of initialization failure; true otherwise.
 */
static bool checkBufferCounter(BufferType type) {
    assert(type >= DMA_INVALID);
    assert(type < NUM_BUFFER_TYPES);

    // Setting up function pointers to all the frameworks.
    static bool initialized = false;
    if (!initialized) {
#ifdef LAROD_USE_CVFLOW
        fdOps[DMA_CAVALRY] = &cavalryOps;
#endif
        initialized = true;
    }

    if (!fdOps[type]) {
        logError("No dma framework implemented for buffer type %d", type);

        return false;
    }

    if (numBuffers[type] == 0) {
        return fdOps[type]->initFramework();
    }

    return true;
}

/**
 * @brief Decrements the buffer counter and exits framework if it is the last
 * one.
 */
void decrementBufferCounter(BufferType type) {
    if (--numBuffers[type] == 0) {
        // Clean up if it is the last one.
        fdOps[type]->exitFramework();
    }
}

/**
 * @brief Sets appropriate dma-buffer type given a larodChip.
 */
static bool setDmaBufferType(DmaBuffer* buffer, larodChip chip) {
    assert(buffer);

    switch (chip) {
    case LAROD_CHIP_CVFLOW_NN:
    case LAROD_CHIP_CVFLOW_PROC: {
        buffer->type = DMA_CAVALRY;
        return true;
    }
#ifdef LAROD_USE_CVFLOW
    case LAROD_CHIP_TFLITE_CPU:
    case LAROD_CHIP_LIBYUV:
        buffer->type = DMA_CAVALRY;
        return true;
#endif
    default: {
        logError("No dma type available for larodChip %d", chip);
        buffer->type = DMA_INVALID;
        return false;
    }
    }
}

DmaBuffer* allocDmaBuffer(size_t size, larodChip chip) {
    DmaBuffer* buffer = malloc(sizeof(DmaBuffer));
    if (!buffer) {
        logError("Could not allocate DmaBuffer: %s", strerror(errno));
        goto error;
    }
    buffer->fd = -1;
    buffer->size = 0;
    buffer->data = NULL;
    if (!setDmaBufferType(buffer, chip)) {
        goto error;
    }

    if (!checkBufferCounter(buffer->type)) {
        goto error;
    }

    if (!fdOps[buffer->type]->allocFd(size, buffer)) {
        goto error;
    }

    numBuffers[buffer->type]++;

    return buffer;
error:
    free(buffer);

    return NULL;
}

bool freeDmaBuffer(DmaBuffer* buffer) {
    if (!buffer) {
        return false;
    }

    if (!fdOps[buffer->type]->freeFd(buffer)) {
        logWarning("Could not free DmaBuffer");
        return false;
    }

    decrementBufferCounter(buffer->type);

    free(buffer);

    return true;
}

bool copyDataToBuffer(void* data, size_t size, DmaBuffer* buffer) {
    assert(data);
    assert(buffer);

    // TODO: Rework this once we have full support for DMA_BUF_IOCTL_SYNC for
    // Ambarella dmabufs. copyDataToDmabuf() should be enough for anyone...
    return fdOps[buffer->type]->writeData(data, size, buffer);
}

bool accessDmabuf(void* data, size_t size, int dmaBufFd, bool doWrite) {
    assert(data);
    bool res = false;
    int prot = doWrite ? PROT_WRITE : PROT_READ;

    void* mapPtr = mmap(NULL, size, prot, MAP_SHARED, dmaBufFd, 0);
    if (mapPtr == MAP_FAILED) {
        logError("Failed mmap() on dmabuf: %s", strerror(errno));
        return false;
    }

    // TODO: Re-enable this once all dmabuf implementations support
    // DMA_BUF_IOCTL_SYNC and larod backends are updated to move cache
    // management responsibility to applications.
    // struct dma_buf_sync sync_args = {0};
    // sync_args.flags = DMA_BUF_SYNC_START;
    // sync_args.flags |= (doWrite ? DMA_BUF_SYNC_WRITE : DMA_BUF_SYNC_READ);
    // int ret = ioctl(dmaBufFd, DMA_BUF_IOCTL_SYNC, sync_args);
    // if (ret) {
    //     logError("Failed DMA_BUF_SYNC_START: %d", ret);
    //     goto end;
    // }

    if (doWrite) {
        // Copy contents from pointer to dmabuf.
        memcpy(mapPtr, data, size);
    } else {
        // Read contents from dmabuf to the supplied buffer pointer.
        memcpy(data, mapPtr, size);
    }

    // TODO: Re-enable this when backend cache management is updated.
    // sync_args.flags = DMA_BUF_SYNC_END;
    // sync_args.flags |= (doWrite ? DMA_BUF_SYNC_WRITE : DMA_BUF_SYNC_READ);
    // ret = ioctl(dmaBufFd, DMA_BUF_IOCTL_SYNC, sync_args);
    // if (ret) {
    //     logError("Failed DMA_BUF_SYNC_END: %d", ret);
    //     goto end;
    // }

    res = true;

    // end:
    if (munmap(mapPtr, size)) {
        logWarning("Failed munmap() on dmabuf: %s", strerror(errno));
    }
    return res;
}

bool copyDataToDmabuf(void* data, size_t size, int fd) {
    return accessDmabuf(data, size, fd, true);
}

bool copyDataFromDmabuf(int fd, void* data, size_t size) {
    return accessDmabuf(data, size, fd, false);
}

DmaBuffer* allocDmaBufferFromFile(const char* filePath, larodChip chip) {
    assert(filePath);

    void* fileData = MAP_FAILED;
    size_t fileSize = 0;
    DmaBuffer* buffer = NULL;

    // Read file.
    int fileFd = open(filePath, O_RDONLY);
    if (fileFd < 0) {
        logError("Could not open file: %s: %s", filePath, strerror(errno));

        goto error;
    }

    // Get file size.
    off_t filePos = lseek(fileFd, 0, SEEK_END);
    if (filePos < 0) {
        logError("Could not get file size of fd: %s", strerror(errno));

        goto error;
    }
    fileSize = (size_t) filePos;

    fileData =
        mmap(NULL, fileSize, PROT_READ, MAP_PRIVATE | MAP_POPULATE, fileFd, 0);
    if (fileData == MAP_FAILED) {
        logError("Could not mmap fd=%d, size=%zu: %s", fileFd, fileSize,
                 strerror(errno));

        goto error;
    }

    buffer = allocDmaBuffer(fileSize, chip);
    if (!buffer || !copyDataToBuffer(fileData, fileSize, buffer)) {
        goto error;
    }

    goto end;

error:
    freeDmaBuffer(buffer);
    buffer = NULL;

end:
    if (fileFd >= 0) {
        close(fileFd);
    }

    if (fileData != MAP_FAILED) {
        munmap(fileData, fileSize);
    }

    return buffer;
}
