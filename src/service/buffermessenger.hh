/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include "buffer.hh"

namespace larod {

/**
 * @brief Proxy object for selectively grant member access to @c Buffer.
 *
 * Only @c Session can use @c isSaved(bool) to tell @c Buffer that it has been
 * saved.
 */
class BufferMessenger {
    friend class Session;

private:
    static void isSaved(Buffer& buf, const bool saved) { buf.isSaved(saved); }
};

} // namespace larod
