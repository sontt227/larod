/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "opencl.hh"

#include <algorithm>
#include <iterator>
#include <thread>

#include "openclnv12torgb.hh"
#include "openclscale.hh"
#include "openclscalenv12torgb.hh"

using namespace std;

namespace larod {
namespace backendunit {

OpenCL::Context::Context(
    const ParamsMap& map,
    const vector<shared_ptr<DeviceContext>>& deviceContexts) {
    ParamsMap tmpMap = map;

    tie(inputMetaData, outputMetaData) =
        imageparams::extractInputOutputInfo(tmpMap, numeric_limits<int>::max());

    // Look for a device specifier. If it is not found, we take the first device
    // as default.
    auto it = tmpMap.find("device");
    if (it != tmpMap.end()) {
        string deviceName;
        try {
            deviceName = get<string>(it->second);
        } catch (const bad_variant_access&) {
            throw invalid_argument("Device specification is not a string");
        }

        for (auto& devCtx : deviceContexts) {
            if (devCtx->NAME.find(deviceName) != string::npos) {
                deviceContext = devCtx;
                tmpMap.erase(it);
                break;
            }
        }
        if (!deviceContext) {
            throw invalid_argument(
                "Could not find any OpenCL device that matches \"" +
                deviceName + "\"");
        }
    } else {
        deviceContext = deviceContexts.front();
    }

    imageparams::checkIfMapIsEmpty(tmpMap);
}

OpenCL::OpenCL() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

OpenCL::~OpenCL() {
    stopProcessingQueues();

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed");
}

void OpenCL::initializeDevices() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Getting devices...");
    try {
        cl::Platform::get(&platform);

        platform.getDevices(CL_DEVICE_TYPE_DEFAULT, &devices);
        if (devices.empty()) {
            throw runtime_error(
                string("Could not construct OpenCL backend: No device found"));
        }

        context = cl::Context(devices);

        cl::Program::Sources kernelSources;
        kernelSources.emplace_back(openCLnv12torgbSource.c_str(),
                                   openCLnv12torgbSource.length());
        kernelSources.emplace_back(openCLscaleSource.c_str(),
                                   openCLscaleSource.length());
        kernelSources.emplace_back(openCLscalenv12torgbSource.c_str(),
                                   openCLscalenv12torgbSource.length());

        program = cl::Program(context, kernelSources, nullptr);
    } catch (cl::Error& e) {
        throw runtime_error(string("Could not construct OpenCL backend: ") +
                            e.what() + " (" + to_string(e.err()) + ")");
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Building kernels...");
    try {
        program.build(devices);
    } catch (cl::Error& e) {
        for (auto& dev : devices) {
            string log = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(dev);
            LOG.error("Failed to build OpenCL program:\n" + log);
        }
        throw runtime_error(string("Could not build OpenCL program: ") +
                            e.what() + " (" + to_string(e.err()) + ")");
    }

    try {
        kernelScaleConvert = cl::Kernel(program, "scaleToRgb");
        kernelY = cl::Kernel(program, "scaleY");
        kernelCbCr = cl::Kernel(program, "scaleCbCr");
        kernelConvert = cl::Kernel(program, "nv12ToRgb");
    } catch (cl::Error& e) {
        throw runtime_error(string("Could not construct OpenCL kernels: ") +
                            e.what() + " (" + to_string(e.err()) + ")");
    }

    // Construct contexts for each device.
    for (auto& dev : devices) {
        string name(dev.getInfo<CL_DEVICE_NAME>());
        LOG.debug(THIS_NAMESPACE + __func__ + "(): Creating context for \"" +
                  name + "\"");
        size_t maxWorkGroupSize = dev.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
        deviceContexts.emplace_back(
            make_shared<DeviceContext>(name, context, dev, maxWorkGroupSize,
                                       getMinWorkGroupSizeMultiple(dev)));
    }
}

vector<TensorMetadata>
    OpenCL::createTensorsFromMetaData(const imageparams::MetaData& metaData) {
    size_t byteSize = imageparams::getByteSize(metaData);

    switch (metaData.format) {
    case imageparams::Format::NV12: {
        const size_t pitch =
            metaData.pitchSpecified ? metaData.rowPitch : metaData.width;
        return {TensorMetadata(
            LAROD_TENSOR_DATA_TYPE_UINT8, LAROD_TENSOR_LAYOUT_420SP,
            {3, metaData.height, metaData.width},
            {byteSize, metaData.height * pitch, pitch}, byteSize, "YUV420SP")};
    }
    case imageparams::Format::RGB_PLANAR: {
        const size_t pitch =
            metaData.pitchSpecified ? metaData.rowPitch : 3 * metaData.width;
        return {TensorMetadata(
            LAROD_TENSOR_DATA_TYPE_UINT8, LAROD_TENSOR_LAYOUT_NCHW,
            {1, 3, metaData.height, metaData.width},
            {byteSize, byteSize, metaData.height * pitch, pitch}, byteSize,
            "RGB_PLANAR")};
    }
    case imageparams::Format::RGB_INTERLEAVED: {
        const size_t pitch =
            metaData.pitchSpecified ? metaData.rowPitch : 3 * metaData.width;
        return {TensorMetadata(LAROD_TENSOR_DATA_TYPE_UINT8,
                               LAROD_TENSOR_LAYOUT_NHWC,
                               {1, metaData.height, metaData.width, 3},
                               {byteSize, metaData.height * pitch, pitch, 3},
                               byteSize, "RGB_INTERLEAVED")};
    }
    default:
        throw invalid_argument(
            "Image format is not supported by OpenCL backend");
    }
}

pair<vector<TensorMetadata>, vector<TensorMetadata>>
    OpenCL::loadModelVirtual(const span<uint8_t>& data, const uint64_t modelId,
                             const ParamsMap& params) {
    if (data.size() != 0) {
        throw invalid_argument("OpenCL does not use model data");
    }

    {
        lock_guard<mutex> lockGuard(mtxDevices);
        if (devices.empty()) {
            initializeDevices();
        }
    }
    shared_ptr<Context> ctx = make_shared<Context>(params, deviceContexts);

    try {
        ctx->input = cl::Buffer(context, CL_MEM_READ_ONLY,
                                getByteSize(ctx->inputMetaData));
        ctx->output = cl::Buffer(context, CL_MEM_WRITE_ONLY,
                                 getByteSize(ctx->outputMetaData));
    } catch (cl::Error& e) {
        throw runtime_error("Failed to create OpenCL buffers: " +
                            string(e.what()));
    }

    lock_guard<mutex> lockGuard(mtxContexts);
    if (!contexts
             .emplace(piecewise_construct, forward_as_tuple(modelId),
                      forward_as_tuple(ctx))
             .second) {
        // Should never happen...
        throw runtime_error("Could not save context");
    }

    auto inputs = createTensorsFromMetaData(ctx->inputMetaData);
    auto outputs = createTensorsFromMetaData(ctx->outputMetaData);

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Loaded model onto \"" +
              ctx->deviceContext->NAME + "\"");

    return {inputs, outputs};
}

void OpenCL::deleteModelVirtual(const uint64_t modelId) {
    lock_guard<mutex> lockGuard(mtxContexts);
    auto it = contexts.find(modelId);
    if (it == contexts.end()) {
        // Should never happen...
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }
    contexts.erase(it);
}

void OpenCL::runJobVirtual(const uint64_t modelId, vector<Tensor>& inputTensors,
                           vector<Tensor>& outputTensors,
                           const ParamsMap& params) {
    assert(inputTensors.size() == 1);
    assert(outputTensors.size() == 1);

    const Buffer& inputBuffer = inputTensors[0].getBuffer();
    if (!(inputBuffer.getFdProps() & LAROD_FD_PROP_READWRITE)) {
        throw invalid_argument(
            "Input tensor fd property not supported by this chip; "
            "supported: LAROD_FD_PROP_READWRITE");
    }

    const Buffer& outputBuffer = outputTensors[0].getBuffer();
    if (!(outputBuffer.getFdProps() & LAROD_FD_PROP_READWRITE)) {
        throw invalid_argument(
            "Output tensor fd property not supported by this chip; "
            "supported: LAROD_FD_PROP_READWRITE");
    }

    shared_ptr<Context> ctx;
    {
        lock_guard<mutex> contextsLockGuard(mtxContexts);
        auto it = contexts.find(modelId);
        if (it == contexts.end()) {
            throw ModelNotFound("Model " + to_string(modelId) + " not found");
        }
        ctx = it->second;
    }
    DeviceContext* devCtx = ctx->deviceContext.get();

    vector<uint8_t> tmpInput(imageparams::getByteSize(ctx->inputMetaData));
    inputBuffer.read(tmpInput);

    vector<uint8_t> tmpOutput(imageparams::getByteSize(ctx->outputMetaData));

    auto [doCrop, cropInfo] = imageparams::getCropInfo(
        params, ctx->inputMetaData.width, ctx->inputMetaData.height,
        numeric_limits<int>::max());
    const bool doCropScale =
        ctx->inputMetaData.width != ctx->outputMetaData.width ||
        ctx->inputMetaData.height != ctx->outputMetaData.height || doCrop;
    const bool doConvertFormat =
        ctx->inputMetaData.format != ctx->outputMetaData.format;

    try {
        devCtx->queue.enqueueWriteBuffer(
            ctx->input, CL_TRUE, 0,
            imageparams::getByteSize(ctx->inputMetaData), tmpInput.data());
    } catch (cl::Error& e) {
        throw runtime_error("Failed to copy input into OpenCL buffer: " +
                            string(e.what()));
    }

    if (doCropScale && doConvertFormat) {
        cropScaleConvert(devCtx, ctx->input, ctx->output, ctx->inputMetaData,
                         ctx->outputMetaData, cropInfo);
    } else if (doCropScale) {
        cropScale(devCtx, ctx->input, ctx->output, ctx->inputMetaData,
                  ctx->outputMetaData, cropInfo);
    } else if (doConvertFormat) {
        colorConvert(devCtx, ctx->input, ctx->output, ctx->inputMetaData,
                     ctx->outputMetaData);
    } else {
        throw invalid_argument("No operation to be done");
    }

    try {
        devCtx->queue.enqueueReadBuffer(
            ctx->output, CL_TRUE, 0,
            imageparams::getByteSize(ctx->outputMetaData), tmpOutput.data());
    } catch (cl::Error& e) {
        throw runtime_error("Failed to read output from OpenCL kernel: " +
                            string(e.what()));
    }

    outputBuffer.write(tmpOutput);
}

inline size_t OpenCL::roundDown(size_t x, size_t mult) {
    return (x / mult) * mult;
}

template<typename... Args>
void setKernelArgsImp(cl::Kernel& kernel, cl_uint& index, Args... args);

template<typename Arg, typename... Args>
void setKernelArgsImp(cl::Kernel& kernel, cl_uint& index, Arg arg,
                      Args... rest) {
    kernel.setArg(index++, arg);
    setKernelArgsImp(kernel, index, rest...);
}

template<> void setKernelArgsImp(cl::Kernel&, cl_uint&) {
}

// This is just a quality-of-life function.
// Instead of having to write:
//     kernel.setArg(0, arg0);
//     kernel.setArg(1, arg1);
//     kernel.setArg(2, arg2);
//     ...
// you can just do:
//     setKernelArgs(kernel, arg0, arg1, arg2, ...);
template<typename... Args>
void OpenCL::setKernelArgs(cl::Kernel& kernel, Args... args) {
    cl_uint index = 0;
    setKernelArgsImp(kernel, index, args...);
}

size_t OpenCL::getMinWorkGroupSizeMultiple(const cl::Device& dev) {
    const size_t kernelScaleConvertPrefWorkMult =
        kernelScaleConvert
            .getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(
                dev);
    const size_t kernelYPrefWorkMult =
        kernelY.getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(
            dev);
    const size_t kernelCbCrPrefWorkMult =
        kernelCbCr
            .getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(
                dev);
    const size_t kernelConvertPrefWorkMult =
        kernelConvert
            .getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(
                dev);

    return min({kernelScaleConvertPrefWorkMult, kernelYPrefWorkMult,
                kernelCbCrPrefWorkMult, kernelConvertPrefWorkMult});
}

tuple<size_t, size_t> OpenCL::calcWorkSizes(DeviceContext* devCtx,
                                            size_t totalWorkItems) {
    // Given the total number of work items we compute the global worksize by
    // first capping the work-items so that it is no larger than
    // MAX_WORK_GROUP_SIZE (we can't go past that), and then we round it down to
    // a multiple of DEFAULT_NUM_WORK_GROUPS * PREFERRED_WORK_GROUP_SIZE. This
    // is to ensure that
    //     a) worksize is evenly divisible with DEFAULT_NUM_WORK_GROUPS, if it
    //     is not we get an error when running the kernel, b) the localsize is a
    //     multiple of PREFERRED_WORK_GROUP_SIZE, which helps the kernel go
    //     faster.
    //
    // Finally, we calculate the local work group size, which is just the
    // worksize  / DEFAULT_NUM_WORK_GROUPS.
    size_t globalWorkSize =
        roundDown(std::min(devCtx->MAX_WORK_GROUP_SIZE, totalWorkItems),
                  DEFAULT_NUM_WORK_GROUPS * devCtx->PREFERRED_WORK_GROUP_SIZE);
    size_t localWorkSize = globalWorkSize / DEFAULT_NUM_WORK_GROUPS;

    return {globalWorkSize, localWorkSize};
}

void OpenCL::cropScaleConvert(DeviceContext* devCtx, cl::Buffer& input,
                              cl::Buffer& output,
                              const imageparams::MetaData& inputMetaData,
                              const imageparams::MetaData& outputMetaData,
                              const imageparams::CropInfo& cropInfo) {
    if (inputMetaData.format != imageparams::Format::NV12) {
        throw invalid_argument(
            "Crop-scale-convert for the specified input format is not "
            "supported by opencl backend");
    }
    int inputPitch = static_cast<int>(inputMetaData.rowPitch);
    int inputCbCrOffset =
        static_cast<int>(inputMetaData.rowPitch * inputMetaData.height);
    int outputPitch = static_cast<int>(outputMetaData.rowPitch);

    int cropX = static_cast<int>(cropInfo.x);
    int cropY = static_cast<int>(cropInfo.y);
    int cropWidth = static_cast<int>(cropInfo.w);
    int cropHeight = static_cast<int>(cropInfo.h);

    float scaleX = static_cast<float>(cropWidth - 1) /
                   static_cast<float>(outputMetaData.width - 1);
    float scaleY = static_cast<float>(cropHeight - 1) /
                   static_cast<float>(outputMetaData.height - 1);

    auto [workSize, localSize] = calcWorkSizes(devCtx, outputMetaData.height);

    try {
        setKernelArgs(kernelScaleConvert, input, inputCbCrOffset, cropX, cropY,
                      cropWidth, inputPitch, cropHeight, output,
                      outputMetaData.width, outputPitch, outputMetaData.height,
                      scaleX, scaleY);

        devCtx->queue.enqueueNDRangeKernel(kernelScaleConvert, cl::NullRange,
                                           cl::NDRange(workSize),
                                           cl::NDRange(localSize));

        devCtx->queue.finish();
    } catch (cl::Error& e) {
        throw runtime_error("Failed to run OpenCL crop-scale-convert: " +
                            string(e.what()));
    }
}

void OpenCL::cropScale(DeviceContext* devCtx, cl::Buffer& input,
                       cl::Buffer& output,
                       const imageparams::MetaData& inputMetaData,
                       const imageparams::MetaData& outputMetaData,
                       const imageparams::CropInfo& cropInfo) {
    if (inputMetaData.format != imageparams::Format::NV12) {
        throw invalid_argument(
            "Crop-scale for the specified input format is not "
            "supported by opencl backend");
    }
    int inputPitch = static_cast<int>(inputMetaData.rowPitch);
    int inputCbCrOffset =
        static_cast<int>(inputMetaData.rowPitch * inputMetaData.height);
    int outputPitch = static_cast<int>(outputMetaData.rowPitch);

    int cropX = static_cast<int>(cropInfo.x);
    int cropY = static_cast<int>(cropInfo.y);
    int cropWidth = static_cast<int>(cropInfo.w);
    int cropHeight = static_cast<int>(cropInfo.h);

    float scaleX = static_cast<float>(cropWidth - 1) /
                   static_cast<float>(outputMetaData.width - 1);
    float scaleY = static_cast<float>(cropHeight - 1) /
                   static_cast<float>(outputMetaData.height - 1);

    auto [workSizeY, localSizeY] = calcWorkSizes(devCtx, outputMetaData.height);

    auto [workSizeCbCr, localSizeCbCr] =
        calcWorkSizes(devCtx, (outputMetaData.height + 1) / 2);

    try {
        setKernelArgs(kernelY, input, cropX, cropY, cropWidth, inputPitch,
                      cropHeight, output, outputMetaData.width, outputPitch,
                      outputMetaData.height, scaleX, scaleY);
        setKernelArgs(kernelCbCr, input, inputCbCrOffset, cropX, cropY,
                      cropWidth, inputPitch, cropHeight, output,
                      outputMetaData.width, outputPitch, outputMetaData.height,
                      scaleX, scaleY);

        devCtx->queue.enqueueNDRangeKernel(kernelY, cl::NullRange,
                                           cl::NDRange(workSizeY),
                                           cl::NDRange(localSizeY));

        devCtx->queue.enqueueNDRangeKernel(kernelCbCr, cl::NullRange,
                                           cl::NDRange(workSizeCbCr),
                                           cl::NDRange(localSizeCbCr));

        devCtx->queue.finish();
    } catch (cl::Error& e) {
        throw runtime_error("Failed to run OpenCL crop-scale: " +
                            string(e.what()));
    }
}

void OpenCL::colorConvert(DeviceContext* devCtx, cl::Buffer& input,
                          cl::Buffer& output,
                          const imageparams::MetaData& inputMetaData,
                          const imageparams::MetaData& outputMetaData) {
    if (inputMetaData.format != imageparams::Format::NV12 ||
        outputMetaData.format != imageparams::Format::RGB_INTERLEAVED) {
        throw invalid_argument("This backend only supports color conversion "
                               "from NV12 to RGB interleaved");
    }

    int inputPitch =
        static_cast<int>(inputMetaData.pitchSpecified ? inputMetaData.rowPitch :
                                                        inputMetaData.width);

    auto [workSize, localSize] =
        calcWorkSizes(devCtx, outputMetaData.height / 2);

    try {
        setKernelArgs(kernelConvert, input, inputMetaData.width, inputPitch,
                      inputMetaData.height, output, outputMetaData.rowPitch);
        devCtx->queue.enqueueNDRangeKernel(kernelConvert, cl::NullRange,
                                           cl::NDRange(workSize),
                                           cl::NDRange(localSize));
        devCtx->queue.finish();
    } catch (cl::Error& e) {
        throw runtime_error("Failed to run OpenCL format conversion: " +
                            string(e.what()));
    }
}

} // namespace backendunit
} // namespace larod
