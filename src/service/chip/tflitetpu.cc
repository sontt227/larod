/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "tflitetpu.hh"

#include <sys/mman.h>
#include <tensorflow/lite/interpreter.h>
#include <tensorflow/lite/kernels/register.h>
#include <tensorflow/lite/model.h>

#include "edgetpu.h"

using namespace std;

namespace larod {
namespace backendunit {

TFLiteTPU::TFLiteTPU() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Creating TPU context...");

    tpuContext = edgetpu::EdgeTpuManager::GetSingleton()->NewEdgeTpuContext();
    if (!tpuContext) {
        throw runtime_error("Could not initialize TPU");
    }

    dmaDelegate = {nullptr,
                   &TFLiteTPU::delegatePrepare,
                   &TFLiteTPU::delegateCopyFromBufferHandle,
                   &TFLiteTPU::delegateCopyToBufferHandle,
                   &TFLiteTPU::delegateFreeBufferHandle,
                   kTfLiteDelegateFlagsNone};

    supportedInputFdProps |= LAROD_FD_PROP_DMABUF;
    memFdAllocator = allocator::MemFd::getInstance();

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

TFLiteTPU::~TFLiteTPU() {
    // We own tpuContext and we must make sure that no job is running (and using
    // it) while we destroy it.
    stopProcessingQueues();

    // We must make sure no one uses the models that depend on tpuContext.
    unique_lock<mutex> lock(mtxInterpreters);
    interpreters.clear();
    lock.unlock();
}

unique_ptr<Buffer> TFLiteTPU::allocateInput(const size_t size,
                                            const uint32_t fdProps,
                                            const ParamsMap& params) const {
    if ((fdProps & LAROD_FD_PROP_DMABUF) &&
        (fdProps & LAROD_FD_PROP_READWRITE)) {
        throw invalid_argument(
            "Invalid combination of fd properties for input tensor: One "
            "cannot allocate a dma buffer with read/write access");
    }

    if (fdProps & LAROD_FD_PROP_DMABUF) {
        throw invalid_argument("No DMA buffer allocator available");
    }

    return memFdAllocator->allocate(size, fdProps, params);
}

unique_ptr<Buffer> TFLiteTPU::allocateOutput(const size_t size,
                                             const uint32_t fdProps,
                                             const ParamsMap& params) const {
    if (fdProps & LAROD_FD_PROP_DMABUF) {
        throw invalid_argument("Allocating dma buffers for output tensors is "
                               "not supported by LAROD_CHIP_TPU");
    }

    return memFdAllocator->allocate(size, fdProps, params);
}

unique_ptr<tflite::Interpreter>
    TFLiteTPU::buildInterpreter(const tflite::FlatBufferModel& model) {
    // Check that the model is compiled for EdgeTPU.
    if (model.GetModel()->operator_codes()) {
        bool hasEdgeTPUCustomOp = false;
        for (const auto& opcode : *model.GetModel()->operator_codes()) {
            if (opcode->builtin_code() == tflite::BuiltinOperator_CUSTOM &&
                opcode->custom_code()->str() == "edgetpu-custom-op") {
                hasEdgeTPUCustomOp = true;
                break;
            }
        }
        if (!hasEdgeTPUCustomOp) {
            throw invalid_argument("Model is not compiled for EdgeTPU");
        }
    }

    // Register TPU as a custom op handler.
    tflite::ops::builtin::BuiltinOpResolver resolver;
    resolver.AddCustom(edgetpu::kCustomOp, edgetpu::RegisterCustomOp());

    tflite::InterpreterBuilder builder(model, resolver);
    unique_ptr<tflite::Interpreter> interpreter;
    builder(&interpreter);

    // Bind TPU context.
    if (interpreter) {
        interpreter->SetExternalContext(kTfLiteEdgeTpuContext,
                                        tpuContext.get());
    }

    return interpreter;
}

TfLiteStatus TFLiteTPU::delegatePrepare(TfLiteContext*, TfLiteDelegate*) {
    return kTfLiteOk;
}

TfLiteStatus TFLiteTPU::delegateCopyFromBufferHandle(TfLiteContext*,
                                                     TfLiteDelegate*,
                                                     TfLiteBufferHandle,
                                                     TfLiteTensor*) {
    LOG.warning("Delegate copy from fallback not implemented");

    return kTfLiteOk;
}

TfLiteStatus TFLiteTPU::delegateCopyToBufferHandle(TfLiteContext*,
                                                   TfLiteDelegate*,
                                                   TfLiteBufferHandle,
                                                   TfLiteTensor*) {
    LOG.warning("Delegate copy to fallback not implemented");

    return kTfLiteOk;
}

void TFLiteTPU::delegateFreeBufferHandle(TfLiteContext*, TfLiteDelegate*,
                                         TfLiteBufferHandle*) {
    // This callback is called after the inference, when the delegator is done
    // with the buffer handle. Other parts of larod will handle closing the
    // dmabuf fd so we do nothing here.
}

void TFLiteTPU::setupInput(const vector<TfLiteTensor*>& tfInputTensors,
                           const vector<Tensor>& inputTensors,
                           tflite::Interpreter* interpreter) {
    for (size_t i = 0; i < tfInputTensors.size(); ++i) {
        Buffer& providedInput = inputTensors[i].getBuffer();
        auto fdProps = providedInput.getFdProps();

        if (fdProps & LAROD_FD_PROP_DMABUF) {
            if (providedInput.getStartOffset()) {
                throw invalid_argument(
                    "DMA buffer file descriptor must have "
                    "offset zero for this chip (got " +
                    to_string(providedInput.getStartOffset()) + ")");
            }

            interpreter->SetBufferHandle(interpreter->inputs()[i],
                                         providedInput.getFd(), &dmaDelegate);
        } else if (fdProps & LAROD_FD_PROP_MAP) {
            tfInputTensors[i]->data.uint8 =
                providedInput.getMapping(tfInputTensors[i]->bytes, PROT_READ);
        } else if (fdProps & LAROD_FD_PROP_READWRITE) {
            span<uint8_t> inputBuffer(tfInputTensors[i]->data.uint8,
                                      tfInputTensors[i]->bytes);
            providedInput.read(inputBuffer);
        } else {
            // Should never happen...
            assert(false);
            throw invalid_argument("Input tensor fd property is unknown; "
                                   "supported: " +
                                   Buffer::getPropNames(supportedInputFdProps));
        }
    }
}

} // namespace backendunit
} // namespace larod
