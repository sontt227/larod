/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <cstdint>
#include <map>
#include <string>
#include <utility>
#include <variant>
#include <vector>

#include "paramsmap.hh"

namespace larod {

/**
 * @brief Parameter helper functions for image processing backends.
 */
namespace imageparams {

/**
 * @brief Image formats.
 */
enum class Format { NV12, RGB_INTERLEAVED, RGB_PLANAR };

/**
 * @brief Image meta data specifications.
 */
struct MetaData {
    Format format;       /// < Image format.
    size_t width;        /// < Image width (pixels).
    size_t height;       /// < Image height (pixels).
    bool pitchSpecified; /// < Flag indicating whether pitch was specified by
                         /// the client. If the client did not specify ptich the
                         /// @c rowPitch member is calculated by best effort.
    size_t rowPitch;     /// < Row pitch (bytes).
};

/**
 * @brief Image crop area.
 */
struct CropInfo {
    size_t x;
    size_t y;
    size_t w;
    size_t h;
};

/**
 * @brief Checks whether a map is empty and throws exception if it is not.
 *
 * @param map A parameter key/value map.
 */
void checkIfMapIsEmpty(const ParamsMap& map);

/**
 * @brief Extracts image processing info from param map.
 *
 * Extracts mandatory and optional image information from a parameter map. If
 * any mandatory parameters for image processing backends are missing (or any
 * other problem occur) an exception will be thrown. Pitch info is optional and
 * if there is no pitch information in the map the pitch will be calculated for
 * a non-padded image with uint8 data. Input width/height are checked to be
 * larger than 0. Input width/height are also verified to be smaller than @p
 * maxDim or maximum size_t value, whichever is smaller. Each element in @c map
 * that is used is erased from the map.
 *
 * @param map A parameter key/value map.
 * @param maxDim Maximum width/height that backend supports.
 * @return A pair of @c MetaData objects that describe the input and
 * output images respectively.
 */
std::pair<MetaData, MetaData> extractInputOutputInfo(ParamsMap& map,
                                                     uint64_t maxDim);

/**
 * @brief Get image processing info from param map.
 *
 * Calls @c extractInputOutputInfo() on a copy of the map and if there are other
 * keys in the @p map that are not recognized by the parser an exception will be
 * thrown; otherwise returns the extracted input/output metadata.
 *
 * @param map A parameter key/value map.
 * @param maxDim Maximum width/height that backend supports.
 * @return A pair of @c MetaData objects that describe the input and
 * output images respectively.
 */
std::pair<MetaData, MetaData> getInputOutputInfo(const ParamsMap& map,
                                                 uint64_t maxDim);

/**
 * @brief Extract crop info from params map.
 *
 * Extract (optional) crop info for image processing backends from a
 * parameter map. The extracted crop region is sanity-checked towards the
 * image dimensions supplied. If image crop key is found but some error
 * occurs in the parsing or sanity fails an exception is thrown.
 * Crop values are checked to be smaller than @p maxDim or maximum size_t value,
 * whichever is smaller. Width/height of crop is verified to larger than 0.
 *
 * @param map A parameter key/value map.
 * @param imgWidth Image width for sanity checking.
 * @param imgHeight Image widht for sanity checking.
 * @param maxDim Maximum value for any crop value (x, y, w, h).
 * @return A pair of bool and @c CropInfo. If image crop is not found the
 * bool value will be set to false and the CropInfo set to cover the whole
 * @p imgWidth x @p imgHeight area. Otherwise the bool value will be true
 * and the CropInfo will have data based on the crop parameters in @p map.
 */
std::pair<bool, CropInfo> getCropInfo(const ParamsMap& map, size_t imgWidth,
                                      size_t imgHeight, uint64_t maxDim);

/**
 * @brief Convenience function to get size of data in bytes from the
 * corresponding MetaData.
 */
size_t getByteSize(const MetaData& metaData);

} // namespace imageparams
} // namespace larod
