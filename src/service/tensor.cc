/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "tensor.hh"

using namespace std;

namespace larod {

Tensor::Tensor(const larodTensorDataType dataType,
               const larodTensorLayout layout, const vector<size_t> dims,
               const vector<size_t> pitches, const size_t byteSize,
               const string name, const shared_ptr<Buffer> buffer)
    : TensorMetadata(dataType, layout, dims, pitches, byteSize, name),
      buffer(buffer) {
}

Tensor::Tensor(const int32_t dataType, const int32_t layout,
               const vector<uint64_t> dims, const vector<uint64_t> pitches,
               const size_t byteSize, const string name,
               const shared_ptr<Buffer> buffer)
    : Tensor(static_cast<larodTensorDataType>(dataType),
             static_cast<larodTensorLayout>(layout),
             castVector<uint64_t, size_t>(dims),
             castVector<uint64_t, size_t>(pitches), byteSize, name, buffer) {
}

template<typename S, typename D>
vector<D> Tensor::castVector(const vector<S>& s) {
    vector<D> d;
    d.resize(s.size());
    for (size_t i = 0; i < s.size(); ++i) {
        S element = s[i];
        if (element > numeric_limits<D>::max()) {
            throw invalid_argument("Element " + to_string(element) +
                                   " is too large");
        }

        d[i] = static_cast<D>(element);
    }

    return d;
}

} // namespace larod
