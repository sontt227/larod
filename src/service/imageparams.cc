/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "imageparams.hh"

#include <algorithm>
#include <limits>
#include <stdexcept>

using namespace std;

namespace {
using namespace larod::imageparams;

const pair<string, string> IMG_DIMS = {"image.", ".size"};
const pair<string, string> IMG_FORMAT = {"image.", ".format"};
const pair<string, string> IMG_PITCH = {"image.", ".row-pitch"};

const string INPUT_IMG_CROP = "image.input.crop";

const string FORMAT_NV12 = "nv12";
const string FORMAT_RGB_INTERLEAVED = "rgb-interleaved";
const string FORMAT_RGB_PLANAR = "rgb-planar";

Format formatFromString(const string& formatStr) {
    if (formatStr == FORMAT_NV12) {
        return Format::NV12;
    }
    if (formatStr == FORMAT_RGB_INTERLEAVED) {
        return Format::RGB_INTERLEAVED;
    }
    if (formatStr == FORMAT_RGB_PLANAR) {
        return Format::RGB_PLANAR;
    }

    throw invalid_argument("Unsupported img format (" + formatStr + ")");
}

MetaData metaDataFromMap(larod::ParamsMap& map, const string& type,
                         uint64_t maxDim) {
    uint64_t maxNumValue =
        min(maxDim, static_cast<uint64_t>(numeric_limits<size_t>::max()));
    MetaData metaData;
    vector<int64_t> imageDims;
    string formatStr;
    int64_t rowPitch = -1;

    try {
        string imgDimsKey = IMG_DIMS.first + type + IMG_DIMS.second;
        auto dimsVariant = map.at(imgDimsKey);
        imageDims = get<vector<int64_t>>(dimsVariant);
        map.erase(imgDimsKey);
    } catch (const bad_variant_access&) {
        throw invalid_argument(type + " image size is not an int array");
    } catch (const out_of_range&) {
        throw invalid_argument(
            type + " image size key not found in the parameter map");
    }

    if (imageDims.size() != 2) {
        throw invalid_argument(type +
                               " image size must be int array with size 2");
    }
    if (imageDims[0] <= 0 ||
        static_cast<uint64_t>(imageDims[0]) >= maxNumValue) {
        throw invalid_argument(
            type + " image width must be positive integer smaller than " +
            to_string(maxNumValue));
    }
    if (imageDims[1] <= 0 ||
        static_cast<uint64_t>(imageDims[1]) >= maxNumValue) {
        throw invalid_argument(type +
                               " image height must be positive integer "
                               "that smaller than " +
                               to_string(maxNumValue));
    }

    metaData.width = static_cast<size_t>(imageDims[0]);
    metaData.height = static_cast<size_t>(imageDims[1]);

    try {
        string imgFormatKey = IMG_FORMAT.first + type + IMG_FORMAT.second;
        auto formatVariant = map.at(imgFormatKey);
        formatStr = get<string>(formatVariant);
        map.erase(imgFormatKey);
    } catch (const bad_variant_access&) {
        throw invalid_argument(type + " image format is not a string");
    } catch (const out_of_range&) {
        throw invalid_argument(type +
                               " image format key not found in parameter map");
    }
    metaData.format = formatFromString(formatStr);

    try {
        string imgPitchKey = IMG_PITCH.first + type + IMG_PITCH.second;
        auto it = map.find(imgPitchKey);
        if (it != map.end()) {
            auto pitchVariant = it->second;
            rowPitch = get<int64_t>(pitchVariant);
            metaData.pitchSpecified = true;
            map.erase(imgPitchKey);
        } else {
            metaData.pitchSpecified = false;
        }
    } catch (const bad_variant_access&) {
        throw invalid_argument(type + " row-pitch is not an int");
    }

    if (metaData.pitchSpecified) {
        if (rowPitch <= 0 || static_cast<uint64_t>(rowPitch) >= maxNumValue ||
            static_cast<size_t>(rowPitch) < metaData.width) {
            throw invalid_argument(type +
                                   " image row pitch must be positive integer, "
                                   "larger than image width and smaller than " +
                                   to_string(maxNumValue));
        }
        metaData.rowPitch = static_cast<size_t>(rowPitch);
    } else {
        // If pitch was not specified (from the parameters map) we default to
        // 8 bits per pixel and no padding.
        switch (metaData.format) {
        case Format::NV12:
        case Format::RGB_PLANAR:
            metaData.rowPitch = metaData.width;
            break;
        case Format::RGB_INTERLEAVED:
            metaData.rowPitch = 3 * metaData.width;
            break;
        default:
            throw invalid_argument("Unknown image format");
            break;
        }
    }

    return metaData;
}
} // namespace

namespace larod {
namespace imageparams {

size_t getByteSize(const MetaData& metaData) {
    switch (metaData.format) {
    case Format::NV12:
        return 3 * metaData.height * metaData.rowPitch / 2;
    case Format::RGB_PLANAR:
        return 3 * metaData.height * metaData.rowPitch;
    case Format::RGB_INTERLEAVED:
        return metaData.height * metaData.rowPitch;
    default:
        throw invalid_argument("Unknown image format");
    }
}

void checkIfMapIsEmpty(const ParamsMap& map) {
    if (map.size() > 0) {
        auto it = map.begin();
        string firstKey = it->first;
        throw invalid_argument("There are keys in the parameter map that this "
                               "backend does not recognize (e.g. \"" +
                               firstKey +
                               "\"). Please refer to the documentation for "
                               "this particular backend.");
    }
}

pair<MetaData, MetaData> extractInputOutputInfo(ParamsMap& map,
                                                uint64_t maxDim) {
    return {metaDataFromMap(map, "input", maxDim),
            metaDataFromMap(map, "output", maxDim)};
}

pair<MetaData, MetaData> getInputOutputInfo(const ParamsMap& map,
                                            uint64_t maxDim) {
    ParamsMap workMap = map;

    auto [inputMetaData, outputMetaData] =
        extractInputOutputInfo(workMap, maxDim);

    checkIfMapIsEmpty(workMap);

    return {inputMetaData, outputMetaData};
}

pair<bool, CropInfo> getCropInfo(const ParamsMap& map, size_t imgWidth,
                                 size_t imgHeight, uint64_t maxDim) {
    uint64_t maxNumValue =
        min(maxDim, static_cast<uint64_t>(numeric_limits<size_t>::max()));
    CropInfo cropInfo = {0, 0, imgWidth, imgHeight};
    vector<int64_t> cropVector;
    bool cropInfoFound = false;

    try {
        auto it = map.find(INPUT_IMG_CROP);
        if (it != map.end()) {
            auto cropVariant = it->second;
            cropVector = get<vector<int64_t>>(cropVariant);
            cropInfoFound = true;
        }
    } catch (const bad_variant_access&) {
        throw invalid_argument("Crop info is not an int array");
    }

    // Check for unknown keys in the map.
    const size_t maxNumKeys = cropInfoFound ? 1 : 0;
    if (map.size() > maxNumKeys) {
        auto it = map.begin();
        string key = it->first;
        if (key == INPUT_IMG_CROP) {
            it++;
            key = it->first;
        }
        throw invalid_argument("There are keys in the parameter map that this "
                               "backend does not recognize (e.g. \"" +
                               key +
                               "\"). Please refer to the documentation for "
                               "this particular backend.");
    }

    if (!cropInfoFound) {
        return {cropInfoFound, cropInfo};
    }

    if (cropVector.size() != 4) {
        throw invalid_argument("Crop info must have 4 entries (x,y,w,h)");
    }

    // Sanity check and cast crop x coord.
    if (cropVector[0] < 0 ||
        static_cast<uint64_t>(cropVector[0]) >= maxNumValue ||
        static_cast<size_t>(cropVector[0]) >= imgWidth) {
        throw invalid_argument(
            "Crop x coord must be less than image width and smaller than " +
            to_string(maxNumValue));
    }
    cropInfo.x = static_cast<size_t>(cropVector[0]);

    // Sanity check and cast crop y coord.
    if (cropVector[1] < 0 ||
        static_cast<uint64_t>(cropVector[1]) >= maxNumValue ||
        static_cast<size_t>(cropVector[1]) >= imgHeight) {
        throw invalid_argument(

            "Crop y coord must be less than image height and smaller than " +
            to_string(maxNumValue));
    }
    cropInfo.y = static_cast<size_t>(cropVector[1]);

    // Crop width.
    if (cropVector[2] <= 0 ||
        static_cast<uint64_t>(cropVector[2]) >= maxNumValue ||
        static_cast<size_t>(cropVector[2]) > (imgWidth - cropInfo.x)) {
        throw invalid_argument("Crop width must be within image boundary");
    }
    cropInfo.w = static_cast<size_t>(cropVector[2]);

    // Crop height.
    if (cropVector[3] <= 0 ||
        static_cast<uint64_t>(cropVector[3]) >= maxNumValue ||
        static_cast<size_t>(cropVector[3]) > (imgHeight - cropInfo.y)) {
        throw invalid_argument("Crop height must be within image boundary");
    }
    cropInfo.h = static_cast<size_t>(cropVector[3]);

    return {cropInfoFound, cropInfo};
}

} // namespace imageparams
} // namespace larod
