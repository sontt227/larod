/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "allocator.hh"

#include <gtest/gtest.h>
#include <sys/mman.h>

#include "larod.h"
#include "memfd.hh"
#include "paramsmap.hh"

using namespace std;
using namespace larod;
using namespace testing;

class AllocatorTest : public Test {
public:
    shared_ptr<larod::allocator::MemFd> memFdAllocator =
        larod::allocator::MemFd::getInstance();
    ParamsMap validParamsMap;
    size_t validSize = 10;
    uint32_t validProps = LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP;
};

TEST_F(AllocatorTest, MemFdAllocate) {
    unique_ptr<Buffer> buf =
        memFdAllocator->allocate(validSize, validProps, validParamsMap);

    ASSERT_GE(buf->getFd(), 0);
    ASSERT_EQ(buf->getStartOffset(), 0);
    ASSERT_EQ(buf->getMaxSize(), validSize);
    ASSERT_EQ(buf->getFdProps(), validProps);
}

TEST_F(AllocatorTest, MemFdAllocateParamsMapInvalid) {
    ParamsMap paramsMap;
    paramsMap.insert({"key", static_cast<int>(1)});

    EXPECT_THROW(memFdAllocator->allocate(validSize, validProps, paramsMap),
                 invalid_argument);
}

TEST_F(AllocatorTest, MemFdAllocatePropsInvalid) {
    EXPECT_THROW(memFdAllocator->allocate(validSize, LAROD_FD_PROP_DMABUF,
                                          validParamsMap),
                 invalid_argument);
}
