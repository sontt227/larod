/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <cstdint>
#include <math.h>
#include <string_view>
#include <vector>

/**
 * @brief Generate a pseudo-random YCbCr image pattern.
 *
 * The size of the output vector is 3 * (@c height * @c pitch / 2) elements.
 *
 * @param The width of the image pattern.
 * @param The pitch/stride of the image pattern.
 * @param The height of the image pattern.
 * @param randomSeed The seed for underlying @c rand() calls.
 * @return A vector containing the generated pattern.
 */
std::vector<uint8_t> generateYcbcrTestPattern(size_t width, size_t pitch,
                                              size_t height,
                                              unsigned int randomSeed);

/**
 * @brief Convert an YCbCr image to an RGB image.
 *
 * This function expects that @ycbcrBuffer is at least
 * 3 * (@c height * @c pitch / 2) in size and that @rgbBuffer has room for at
 * least 3 * @c height * @c pitch bytes.
 *
 * @param ycbcrBuffer The input YCbCr data of the input image.
 * @param width The width of the YCbCr image.
 * @param pitch The pitch/stride of the YCbCr image.
 * @param height The height of the YCbCr image.
 * @param interleaved @c true if output is interleaved. @c false for planar.
 * @return An image buffer for the output RGB data.
 */
std::vector<uint8_t> convertYcbcrToRgb(const std::vector<uint8_t>& ycbcrBuffer,
                                       size_t width, size_t pitch,
                                       size_t height, bool interleaved);

/**
 * @brief Compares two image buffers.
 *
 * Compares the difference of two image buffers and returns the mean, as well as
 * the max, difference.
 *
 * @param width The width of the image.
 * @param height The height of the image.
 * @param pitch The pitch/stride of the image.
 * @param format Image format, i.e. "nv12", "rgb-planar" or "rgb-interleaved".
 * @return The difference as a pair: <"mean diff", "max diff">.
 */
std::pair<float, int> compareBuffers(size_t width, size_t height, size_t pitch,
                                     const uint8_t* buffeerOne,
                                     const uint8_t* bufferTwo,
                                     std::string_view format);

/**
 * @brief Calculates the buffer byte size from image meta data.
 *
 * This function throws an exception in the case of an invalid @p format.
 *
 * @param format The image format (nv12, rgb-interleaved or rgb-planar).
 * @param height The image height.
 * @param pitch The image row-pitch.
 * @return The buffer size in bytes.
 */
size_t calcBufferSize(std::string_view format, size_t height, size_t pitch);

/**
 * @brief Calculates an unpadded pitch.
 *
 * @param width The width of an image.
 * @param format The image format.
 * @return Image pitch.
 */
size_t calcPitch(size_t width, std::string_view format);

/**
 * @brief Cast int64_t to size_t.
 *
 * Throws exception if the casting cannot be done.
 *
 * @param num The number that will be cast.
 * @param name Name of variable to be used for error message.
 * @return @p num as a size_t.
 */
size_t castToSizeT(int64_t num, const std::string& name);
