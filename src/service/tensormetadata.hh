/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <functional>
#include <string>

#include "larod.h"

namespace larod {

/**
 * @brief Class to represent a tensor metadata. No actual byte data is stored in
 * this class (c.f. class Tensor).
 */
class TensorMetadata {
public:
    /**
     * @brief Create a tensor.
     *
     * This will throw if any of the attributes data type, layout, dimensions,
     * pitches and byte size are invalid or inconsistent with one another.
     */
    TensorMetadata(const larodTensorDataType dataType,
                   const larodTensorLayout layout,
                   const std::vector<size_t> dims,
                   const std::vector<size_t> pitches, const size_t byteSize,
                   const std::string name);

    larodTensorDataType getDataType() const { return DATA_TYPE; }
    larodTensorLayout getLayout() const { return LAYOUT; }
    std::vector<size_t> getDims() const { return DIMS; }
    std::vector<size_t> getPitches() const { return PITCHES; }
    std::string getName() const { return NAME; }
    size_t getByteSize() const { return BYTE_SIZE; }

    /**
     * @brief Check if tensors have equal attributes.
     *
     * Throws @c invalid_argument if @p lhs @c Tensor has an attribute value not
     * equal to @p rhs @c Tensor. Specifically, data type, layout, dimensions
     * and byte size will be compared. If any of these attributes are
     * unspecified (zero for byte size), that attribute will not be checked.
     *
     * @param lhs @c Tensor to check against @p rhs.
     * @param rhs @c Tensor that @lhs will be matched against.
     */
    static void assertEqual(const TensorMetadata& lhs,
                            const TensorMetadata& rhs);

protected:
    const larodTensorDataType DATA_TYPE;
    const larodTensorLayout LAYOUT;
    const std::vector<size_t> DIMS;
    const std::vector<size_t> PITCHES;
    const size_t BYTE_SIZE; // Can be zero, which then means it is undefined.
    const std::string NAME;

private:
    static std::string vectorToString(const std::vector<size_t>& vec);
};

} // namespace larod
