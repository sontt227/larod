#!/bin/bash

# Copyright 2021 Axis Communications
# SPDX-License-Identifier: Apache-2.0
#
# Run build-and-run-tests.sh for a number of versions of larod.
#
# Requirements:
#
# - dbus
#
# Example usage:
#
#   dbus-run-session -- \
#       ./tools/build-and-run-compat-tests/build-and-run-compat-tests.sh

# Abort script if any command fails.
set -e

# Print commands.
set -x

# Major versions to test ABI compatibility for.
MAJOR_VERSIONS="1 2"

# Get ID of current commit in git.
CURRENT_COMMIT=$(git rev-parse HEAD)

# Get tags for major versions from input.
for MAJOR_VERSION in $MAJOR_VERSIONS; do
    TAG=" $(git tag -l "R$MAJOR_VERSION.*" --sort=v:refname | tail -n 1)"
    ./tools/build-and-run-tests/build-and-run-tests.sh $CURRENT_COMMIT $TAG

    # Check out commit to run build-and-run-tests.sh script from. Has to be
    # done, since build-and-run-tests.sh does not return the git to the state
    # in which it started.
    git checkout $CURRENT_COMMIT
done
