/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "tflite.hh"

#include <algorithm>
#include <sys/mman.h>
#include <tensorflow/lite/kernels/register.h>

#include "model.hh"
#include "modelformat_generated.h"

using namespace std;

namespace larod {
namespace backendunit {

TFLite::TFLite()
    : supportedInputFdProps(LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP),
      supportedOutputFdProps(LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

TFLite::~TFLite() {
    stopProcessingQueues();

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed");
}

pair<vector<TensorMetadata>, vector<TensorMetadata>>
    TFLite::loadModelVirtual(const span<uint8_t>& data, const uint64_t modelId,
                             const ParamsMap& params) {
    // We don't support any additional parameters.
    if (params.size()) {
        throw runtime_error("Additional parameters are not supported");
    }

    // tflite::FlatBufferModel requires the model buffer to be kept alive.
    // Thus, we store a copy of the tflite model. We let Flatbuffers get
    // pointers into this copy.
    shared_ptr<vector<uint8_t>> nativeModelBuf;

    // Check model format. We first try to unpack the model data interpreted as
    // a .larod flatbuffer and if it fails, we use the raw model data.
    const modelformat::Model* fbModel = verifyFbModel(data);
    const modelformat::TFLite* fbTFLite =
        fbModel ? fbModel->backEndData_as_TFLite() : nullptr;
    if (fbTFLite) {
        const flatbuffers::Vector<uint8_t>* nativeData = fbTFLite->nativeData();
        nativeModelBuf = make_shared<vector<uint8_t>>(nativeData->begin(),
                                                      nativeData->end());
    } else {
        nativeModelBuf = make_shared<vector<uint8_t>>(data.begin(), data.end());
    }

    // Since flatbuffers only supports reading "byte" buffers as signed or
    // unsigned char, and VerifyAndBuildFromBuffer() requires char we need
    // to do a reinterpret cast here. NOTE: This may overflow on some
    // architectures.
    const char* tfLiteData =
        reinterpret_cast<const char*>(nativeModelBuf.get()->data());

    lock_guard<mutex> lockGuard(mtxInterpreters);

    // Load model
    shared_ptr<tflite::FlatBufferModel> model =
        tflite::FlatBufferModel::VerifyAndBuildFromBuffer(
            tfLiteData, nativeModelBuf->size(), nullptr, &errRepLog);
    if (!model) {
        throw runtime_error("Could not verify and build model from buffer");
    }

    // Build the interpreter.
    shared_ptr<tflite::Interpreter> interpreter = buildInterpreter(*model);
    if (!interpreter) {
        throw runtime_error("Could not build an interpreter of the model");
    }

    interpreter->SetNumThreads(LAROD_NBR_OF_THREADS);
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Set " +
              to_string(LAROD_NBR_OF_THREADS) + " thread(s)");

    // Allocate tensor buffers.
    if (interpreter->AllocateTensors() != kTfLiteOk) {
        throw runtime_error("Could not allocate tensors");
    }

    // Extract input and output tensor information.
    auto [inputTensors, outputTensors] = getTensors(*interpreter);

    // Save the interpreter.
    interpreters.insert(
        make_pair(modelId, make_tuple(std::move(interpreter), std::move(model),
                                      std::move(nativeModelBuf))));

    return {inputTensors, outputTensors};
}

void TFLite::deleteModelVirtual(const uint64_t modelId) {
    lock_guard<mutex> lockGuard(mtxInterpreters);

    auto it = interpreters.find(modelId);
    if (it == interpreters.end()) {
        // Should never happen...
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    interpreters.erase(it);
}

void TFLite::runJobVirtual(const uint64_t modelId, vector<Tensor>& inputTensors,
                           vector<Tensor>& outputTensors,
                           const ParamsMap& params) {
    // We don't support any additional parameters.
    if (params.size()) {
        throw invalid_argument("Additional parameters are not supported");
    }

    checkFdProps(inputTensors, outputTensors);

    // Get the FlatBufferModel and the associated Interpreter. We save local
    // references just in case someone wants to delete it while we run
    // inference.
    shared_ptr<tflite::Interpreter> interpreter;
    shared_ptr<tflite::FlatBufferModel> model;
    shared_ptr<vector<uint8_t>> modelBuf;
    {
        lock_guard<mutex> lockGuard(mtxInterpreters);
        auto it = interpreters.find(modelId);
        if (it == interpreters.end()) {
            throw ModelNotFound("Model " + to_string(modelId) + " not found");
        }

        interpreter = get<0>(it->second);
        model = get<1>(it->second);
        modelBuf = get<2>(it->second);
    }

    // Set up input tensors.
    vector<TfLiteTensor*> tfInputTensors;
    vector<const uint8_t*> oldTfInputTensorDataPtrs;
    for (size_t i = 0; i < interpreter->inputs().size(); ++i) {
        TfLiteTensor* tfInputTensor = interpreter->input_tensor(i);
        if (!tfInputTensor) {
            throw runtime_error("Invalid input tensor obtained from TFLite");
        }

        // TODO: For now we use this hack to temporarily save the data pointers
        // of the TFLite tensors so that we may overwrite them later with mapped
        // addresses and thus avoid copies.
        // This method will be replaced by a custom TFLite allocator in the
        // future.
        oldTfInputTensorDataPtrs.push_back(tfInputTensor->data.uint8);
        tfInputTensors.push_back(tfInputTensor);
    }

    setupInput(tfInputTensors, inputTensors, interpreter.get());

    // Set up output tensors.
    vector<TfLiteTensor*> tfOutputTensors;
    vector<const uint8_t*> oldTfOutputTensorDataPtrs;
    for (size_t i = 0; i < interpreter->outputs().size(); ++i) {
        TfLiteTensor* tfOutputTensor = interpreter->output_tensor(i);
        if (!tfOutputTensor) {
            throw runtime_error("Invalid output tensor obtained from TFLite");
        }

        // TODO: For now we use this hack to temporarily save the data pointers
        // of the TFLite tensors so that we may overwrite them later with mapped
        // addresses and thus avoid copies.
        // This method will be replaced by a custom TFLite allocator in the
        // future.
        oldTfOutputTensorDataPtrs.push_back(tfOutputTensor->data.uint8);
        tfOutputTensors.push_back(tfOutputTensor);
    }

    setupOutput(tfOutputTensors, outputTensors, interpreter.get());

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Starting job...");
    if (interpreter->Invoke() != kTfLiteOk) {
        throw runtime_error("Failure when invoking interpreter");
    }
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Job done");

    // Write output tensors when applicable.
    writeOutput(tfOutputTensors, outputTensors, interpreter.get());

    // Reset input tensors.
    for (size_t i = 0; i < oldTfInputTensorDataPtrs.size(); ++i) {
        tfInputTensors[i]->data.uint8 =
            const_cast<uint8_t*>(oldTfInputTensorDataPtrs[i]);
    }

    // Reset output tensors.
    for (size_t i = 0; i < oldTfOutputTensorDataPtrs.size(); ++i) {
        tfOutputTensors[i]->data.uint8 =
            const_cast<uint8_t*>(oldTfOutputTensorDataPtrs[i]);
    }
}

void TFLite::checkFdProps(const vector<Tensor>& inputTensors,
                          const vector<Tensor>& outputTensors) const {
    for (auto& tensor : inputTensors) {
        const Buffer& inputBuffer = tensor.getBuffer();
        if (!(inputBuffer.getFdProps() & supportedInputFdProps)) {
            throw invalid_argument(
                "Input tensor fd property not supported by this chip; "
                "supported: " +
                Buffer::getPropNames(supportedInputFdProps));
        }
    }

    for (auto& tensor : outputTensors) {
        const Buffer& outputBuffer = tensor.getBuffer();
        if (!(outputBuffer.getFdProps() & supportedOutputFdProps)) {
            throw invalid_argument(
                "Output tensor fd property not supported by this chip; "
                "supported: " +
                Buffer::getPropNames(supportedOutputFdProps));
        }
    }
}

void TFLite::setupInput(const vector<TfLiteTensor*>& tfInputTensors,
                        const vector<Tensor>& inputTensors,
                        tflite::Interpreter*) {
    for (size_t i = 0; i < tfInputTensors.size(); ++i) {
        Buffer& providedInput = inputTensors[i].getBuffer();

        if (providedInput.getFdProps() & LAROD_FD_PROP_MAP) {
            tfInputTensors[i]->data.uint8 =
                providedInput.getMapping(tfInputTensors[i]->bytes, PROT_READ);
        } else if (providedInput.getFdProps() & LAROD_FD_PROP_READWRITE) {
            span<uint8_t> inputBuffer(tfInputTensors[i]->data.uint8,
                                      tfInputTensors[i]->bytes);
            providedInput.read(inputBuffer);
        } else {
            // Should never happen...
            assert(false);
            throw invalid_argument(
                "Input tensor fd property not supported by this chip; "
                "supported: LAROD_FD_PROP_READWRITE, LAROD_FD_PROP_MAP");
        }
    }
}

void TFLite::setupOutput(const vector<TfLiteTensor*>& tfOutputTensors,
                         const vector<Tensor>& outputTensors,
                         tflite::Interpreter*) {
    for (size_t i = 0; i < tfOutputTensors.size(); ++i) {
        Buffer& providedOutput = outputTensors[i].getBuffer();

        if (providedOutput.getFdProps() & LAROD_FD_PROP_MAP) {
            tfOutputTensors[i]->data.uint8 = providedOutput.getMapping(
                tfOutputTensors[i]->bytes, PROT_WRITE);
        }
    }
}

void TFLite::writeOutput(const vector<TfLiteTensor*>& tfOutputTensors,
                         const vector<Tensor>& outputTensors,
                         tflite::Interpreter*) {
    for (size_t i = 0; i < tfOutputTensors.size(); ++i) {
        Buffer& providedOutput = outputTensors[i].getBuffer();

        if (providedOutput.getFdProps() & LAROD_FD_PROP_MAP) {
            continue;
        }

        assert(providedOutput.getFdProps() & LAROD_FD_PROP_READWRITE);

        span<uint8_t> outputBuffer(tfOutputTensors[i]->data.uint8,
                                   tfOutputTensors[i]->bytes);
        providedOutput.write(outputBuffer);
    }
}

TensorMetadata TFLite::parseTensor(const TfLiteTensor* tensor) {
    auto it = DATA_TYPES.find(tensor->type);
    larodTensorDataType larodType;
    if (it != DATA_TYPES.end()) {
        larodType = it->second;
    } else {
        larodType = LAROD_TENSOR_DATA_TYPE_UNSPECIFIED;
    }

    // Convert the TFLite int dims array to a size_t array.
    vector<size_t> larodDims;
    for_each(
        tensor->dims->data, tensor->dims->data + tensor->dims->size,
        [&](const int& dim) { larodDims.push_back(static_cast<size_t>(dim)); });

    // Calculate pitches.
    size_t typeSize = 0;
    if (tflite::GetSizeOfType(nullptr, tensor->type, &typeSize) != kTfLiteOk) {
        throw runtime_error("Invalid tensor type.");
    }
    size_t dimsLen = larodDims.size();
    vector<size_t> pitches(dimsLen);
    if (dimsLen > 0) {
        pitches[dimsLen - 1] = larodDims[dimsLen - 1] * typeSize;
        for (size_t i = 1; i < dimsLen; ++i) {
            pitches[dimsLen - 1 - i] =
                pitches[dimsLen - i] * larodDims[dimsLen - 1 - i];
        }
    }

    // As of yet there is no notion of "layout" in tensorflow lite.
    return TensorMetadata(larodType, LAROD_TENSOR_LAYOUT_UNSPECIFIED, larodDims,
                          pitches, tensor->bytes, tensor->name);
}

pair<vector<TensorMetadata>, vector<TensorMetadata>>
    TFLite::getTensors(const tflite::Interpreter& interpreter) {
    vector<TensorMetadata> inputTensors;
    for (size_t i = 0; i < interpreter.inputs().size(); ++i) {
        const TfLiteTensor* inputTensor = interpreter.input_tensor(i);
        if (!inputTensor) {
            throw runtime_error("Invalid input tensor");
        }

        inputTensors.emplace_back(parseTensor(inputTensor));
    }

    vector<TensorMetadata> outputTensors;
    for (size_t i = 0; i < interpreter.outputs().size(); ++i) {
        const TfLiteTensor* outputTensor = interpreter.output_tensor(i);
        if (!outputTensor) {
            throw runtime_error("Invalid output tensor");
        }

        outputTensors.emplace_back(parseTensor(outputTensor));
    }

    return {inputTensors, outputTensors};
}

} // namespace backendunit
} // namespace larod
