/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "session.hh"

#include <cassert>
#include <poll.h>
#include <sys/eventfd.h>
#include <sys/socket.h>
#include <unistd.h>
#include <utility>

#include "buffermessenger.hh"
#include "joborder.hh"
#include "log.hh"
#include "message.h"
#include "model.hh"
#include "tensor.h"

using namespace std;

namespace larod {

Session::Session(sdbusplus::bus::bus& bus, const string path,
                 const vector<unique_ptr<BackEndUnit>>& units)
    : stopBusProc(false), hasHandshaked(false), ID(nextId),
      ID_STR(to_string(ID)), UNITS(units) {
    // Create sd-bus interface server.
    sdbusServer = make_unique<SessionSdbusServer>(
        bus, path, bind(&Session::getSessionId, this),
        bind(&Session::getModels, this));

    assert(!UNITS.empty());

    // Create job request priority queues for each unit.
    for (size_t i = 0; i < UNITS.size(); ++i) {
        auto [ignore, ret] = jobReqs.emplace(
            piecewise_construct, forward_as_tuple(i), forward_as_tuple());
        if (!ret) {
            throw runtime_error("Could not create job request priority queues");
        }
    }

    // Initialize list of private models.
    privateModels.resize(UNITS.size());

    // Create asynchronous buses
    int serverReadClientWriteFds[2]; // 0 is our read and 1 is client's write.
    int serverWriteClientReadFds[2]; // 0 is our write and 1 is client's read.
    if (socketpair(AF_UNIX, SOCK_STREAM, 0, serverReadClientWriteFds) ||
        socketpair(AF_UNIX, SOCK_STREAM, 0, serverWriteClientReadFds)) {
        throw runtime_error(string("Could not create socket pairs: ") +
                            strerror(errno));
    }

    clientAsyncWriteFd = std::move(serverReadClientWriteFds[1]);
    clientAsyncReadFd = std::move(serverWriteClientReadFds[1]);

    asyncReadBus.reset(createAsyncBus(serverReadClientWriteFds[0]));
    asyncWriteBus.reset(createAsyncBus(serverWriteClientReadFds[0]));

    // Create eventfd for signaling asynchronous read bus processing thread
    busProcEventFd = eventfd(0, 0);
    if (busProcEventFd < 0) {
        throw runtime_error(string("Could not create eventfd: ") +
                            strerror(errno));
    }

    // Start asynchronous read bus processing
    busProcThread = thread(&Session::processAsyncReadBus, this);

    ++nextId;

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed " + ID_STR);
}

Session::~Session() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructing...");

    // Check that bus processing has been stopped.
    assert(busProcEventFd < 0);
    if (busProcEventFd >= 0) {
        LOG.error("Session is destructing without stopping the bus processing");
    }

    // Delete all private models.
    for (size_t i = 0; i < privateModels.size(); ++i) {
        for (auto it = privateModels[i].begin(); it != privateModels[i].end();
             ++it) {
            try {
                UNITS[i]->deleteModel(ID, *it);
                LOG.debug(THIS_NAMESPACE + __func__ +
                          "(): Deleted private model " + to_string(*it) +
                          " on chip " + to_string(i));
            } catch (exception& e) {
                LOG.error(e.what());
            }
        }
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed " + ID_STR);
}

void Session::stopBusProcessing() noexcept {
    if (busProcEventFd < 0) {
        // This is the only place where we close busProcEventFd after the
        // constructor and set it to -1 (from busProcEventFd.close() below). We
        // can thus use it as a flag to check if we have already called this
        // function before and stopped the bus processing already.
        return;
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopping bus processing...");

    // Delete the sd-bus interface server.
    sdbusServer.reset(nullptr);

    // Stop bus processing thread.
    stopBusProc = true;

    // Trigger the eventfd to release the thread blocking on ppoll().
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Writing to eventfd...");
    uint64_t value = 1;
    ssize_t writtenBytes = write(busProcEventFd, &value, sizeof(value));
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Wrote " +
              to_string(writtenBytes) + " bytes");

    if (static_cast<size_t>(writtenBytes) == sizeof(value)) {
        LOG.debug(THIS_NAMESPACE + __func__ + "(): Joining busProcThread...");
        busProcThread.join();
        LOG.debug(THIS_NAMESPACE + __func__ + "(): busProcThread joined");
    } else {
        // Forcibly destruct thread using pthreads. If thread is joinable
        // std::terminate() will be called upon thread destruction.
        // Therefore, we detach it first.
        busProcThread.detach();
        pthread_cancel(busProcThread.native_handle());
    }

    // Close bus file descriptors.
    // This function is naturally called just before ~Session(). We could assume
    // that and not explicitly close the bus file descriptors (they will then
    // automatically close in ~FileDescriptor()). However, it is better to be
    // safe and close here (and to signal that we have closed busProcEventFd).
    busProcEventFd.close();
    clientAsyncReadFd.close();
    clientAsyncWriteFd.close();
}

sd_bus* Session::createAsyncBus(int fd) {
    int ret;
    sd_bus* bus;
    if ((ret = sd_bus_new(&bus)) < 0) {
        throw runtime_error(string("Could not create a new bus: ") +
                            strerror(-ret));
    }

    if ((ret = sd_bus_set_fd(bus, fd, fd)) < 0) {
        throw runtime_error(string("Could not set file descriptor for bus: ") +
                            strerror(-ret));
    }

    sd_id128_t id;
    sd_id128_randomize(&id);
    if ((ret = sd_bus_set_server(bus, true, id)) < 0) {
        throw runtime_error(string("Could not set server setting for bus: ") +
                            strerror(-ret));
    }

    if ((ret = sd_bus_set_anonymous(bus, false)) < 0) {
        throw runtime_error(
            string("Could not set anonymous setting for bus: ") +
            strerror(-ret));
    }

    if ((ret = sd_bus_negotiate_fds(bus, true)) < 0) {
        throw runtime_error(
            string("Could not set negotiation setting for bus: ") +
            strerror(-ret));
    }

    if ((ret = sd_bus_start(bus)) < 0) {
        throw runtime_error(string("Could not start bus: ") + strerror(-ret));
    }

    return bus;
}

void Session::queueLoadModel(const FileDescriptor& fd, const uint64_t sz,
                             const size_t chipId, const int32_t access,
                             const string name, const uint64_t metaData,
                             const ParamsMap&& params) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": chipId = " + to_string(chipId));
    Model::Access modelAccess(static_cast<Model::Access>(access));
    if (modelAccess != Model::Access::PRIVATE &&
        modelAccess != Model::Access::PUBLIC) {
        throw invalid_argument("Invalid model access specifier");
    }

    if (chipId >= UNITS.size()) {
        throw invalid_argument("Invalid chip ID");
    }

    if (sz > SIZE_MAX) {
        string errMsg =
            to_string(sz) + " bytes is larger than " + to_string(SIZE_MAX);
        throw TypeSizeOverflow(errMsg);
    }
    size_t size = static_cast<size_t>(sz);

    if (!name.empty()) {
        LOG.debug("Session " + ID_STR + ": Queuing loading of model \"" + name +
                  "\"...");
    } else {
        LOG.debug("Session " + ID_STR + ": Queuing loading of model...");
    }

    vector<uint8_t> buf;
    try {
        buf.resize(size);
    } catch (bad_alloc& e) {
        throw runtime_error("Could not resize model vector to " +
                            to_string(size) + " bytes: " + e.what());
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Reading model file...");
    fd.read(buf, buf.size());
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Model file read");

    auto loadModelReq = make_shared<LoadModelRequest>(
        weak_from_this(), UNITS[chipId].get(), ID, chipId, std::move(buf),
        modelAccess, name, metaData, std::move(params));
    UNITS[chipId]->queueLoadModel(loadModelReq);

    if (!name.empty()) {
        LOG.debug("Session " + ID_STR + ": Queuing loading of model \"" + name +
                  "\" done");
    } else {
        LOG.debug("Session " + ID_STR + ": Queuing loading of model done");
    }
}

void Session::deleteModel(uint64_t modelId, const size_t chipId) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Deleting model...");

    UNITS[chipId]->deleteModel(ID, modelId);

    // Might need to clear from the Session's collection of private models as
    // well. This call works since all model IDs are unique and does nothing if
    // it was not found in the collection.
    {
        lock_guard<mutex> lockGuard(mtxPrivateModels);
        privateModels[chipId].erase(modelId);
    }

    LOG.debug("Session " + ID_STR + ": Model " + to_string(modelId) +
              " deleted");
}

void Session::queueJob(const uint64_t modelId, const size_t chipId,
                       vector<Tensor>&& inputTensors,
                       vector<Tensor>&& outputTensors, const uint8_t priority,
                       const uint64_t metaData, const ParamsMap&& params) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Queuing job on model " + to_string(modelId) + "...");

    auto jobReq = make_shared<JobRequest>(
        weak_from_this(), modelId, std::move(inputTensors),
        std::move(outputTensors), priority, metaData, std::move(params));
    try {
        // Add job request to relevant priority job queue.
        lock_guard<mutex> lockGuard(mtxJobReqs);
        jobReqs.at(chipId).push(jobReq);
    } catch (out_of_range& e) {
        // Should never happen...
        throw runtime_error("Could not find chip with index " +
                            to_string(chipId));
    }

    auto jobOrder = make_shared<JobOrder>(weak_from_this(), chipId);
    UNITS[chipId]->queueJob(jobOrder);

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Queued job request on model " + to_string(modelId));
}

Session::TensorMetadataMsg
    Session::createTensorMetadataMsg(const TensorMetadata& tensorMetadata) {
    vector<size_t> dims(tensorMetadata.getDims());
    vector<size_t> pitches(tensorMetadata.getPitches());

    // FIXME: Assuming SIZE_MAX <= UINT64_MAX always holds.
    return {tensorMetadata.getDataType(),
            tensorMetadata.getLayout(),
            static_cast<uint64_t>(tensorMetadata.getByteSize()),
            vector<uint64_t>(dims.begin(), dims.end()),
            vector<uint64_t>(pitches.begin(), pitches.end()),
            tensorMetadata.getName()};
}

vector<Session::TensorMetadataMsg> Session::createTensorMetadataMsg(
    const vector<TensorMetadata>& tensorMetadata) {
    vector<TensorMetadataMsg> msgs;
    for (const TensorMetadata& tensor : tensorMetadata) {
        msgs.push_back(createTensorMetadataMsg(tensor));
    }

    return msgs;
}

tuple<vector<Tensor>, vector<Tensor>>
    Session::readTensorMsg(QueueJobTensorsMsg inputsMsg,
                           QueueJobTensorsMsg outputsMsg) {
    if (inputsMsg.empty()) {
        throw invalid_argument("No input tensors provided");
    }

    if (outputsMsg.empty()) {
        throw invalid_argument("No output tensors provided");
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Reading input tensors...");

    // Read input tensors.
    vector<Tensor> inputTensors;
    for (size_t i = 0; i < inputsMsg.size(); ++i) {
        const tuple<int32_t,                     // Data type.
                    int32_t,                     // Layout.
                    uint64_t,                    // Byte size.
                    vector<uint64_t>,            // Dims.
                    vector<uint64_t>,            // Pitches.
                    sdbusplus::message::unix_fd, // File descriptor.
                    uint64_t,                    // Fd size.
                    int64_t,                     // Fd offset.
                    uint64_t,                    // Fd ID.
                    uint32_t                     // Fd props.
                    >& inputTensorMsg = inputsMsg[i];

        if (get<2>(inputTensorMsg) > numeric_limits<size_t>::max()) {
            throw TypeSizeOverflow("Input tensor " + to_string(i) +
                                   " byte size is too large (" +
                                   to_string(get<2>(inputTensorMsg)) + ")");
        }

        const size_t byteSize = static_cast<size_t>(get<2>(inputTensorMsg));

        if (get<6>(inputTensorMsg) > numeric_limits<size_t>::max()) {
            throw TypeSizeOverflow(
                "Input tensors's file descriptor size is too large (" +
                to_string(get<6>(inputTensorMsg)) + ")");
        }

        const size_t fdSize = static_cast<size_t>(get<6>(inputTensorMsg));

        // Copy file descriptors since sd-bus closes them when the message is
        // freed. These ones will be closed in ~JobRequest() (in ~Tensor()),
        // after output has been written (JobRequest::signal()).
        int tmpFd = dup(get<5>(inputTensorMsg));
        if (tmpFd < 0) {
            throw FileDescriptorError("Could not duplicate file descriptor: " +
                                      string(strerror(errno)));
        }

        try {
            const uint64_t id = get<8>(inputTensorMsg);
            shared_ptr<Buffer> buffer = make_shared<Buffer>(
                FileDescriptor(std::move(tmpFd)), fdSize,
                get<7>(inputTensorMsg), get<9>(inputTensorMsg));

            if (id > 0) {
                // Save buffer if needed, otherwise set ptr to the saved one.
                buffer = resolveBuffer(buffer, id);
            }

            inputTensors.emplace_back(
                get<0>(inputTensorMsg), get<1>(inputTensorMsg),
                get<3>(inputTensorMsg), get<4>(inputTensorMsg), byteSize, "",
                buffer);
        } catch (invalid_argument& e) {
            throw invalid_argument(string("Could not parse input tensors: ") +
                                   e.what());
        }
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Input tensors read");
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Reading output tensors...");

    // Read output tensors.
    vector<Tensor> outputTensors;
    for (size_t i = 0; i < outputsMsg.size(); ++i) {
        const auto& outputTensorMsg = outputsMsg[i];

        if (get<2>(outputTensorMsg) > numeric_limits<size_t>::max()) {
            throw TypeSizeOverflow("Output tensor " + to_string(i) +
                                   " byte size is too large (" +
                                   to_string(get<2>(outputTensorMsg)) + ")");
        }

        const size_t byteSize = static_cast<size_t>(get<2>(outputTensorMsg));

        if (get<6>(outputTensorMsg) > numeric_limits<size_t>::max()) {
            throw TypeSizeOverflow(
                "Input tensors's file descriptor size is too large (" +
                to_string(get<6>(outputTensorMsg)) + ")");
        }

        const size_t fdSize = static_cast<size_t>(get<6>(outputTensorMsg));

        // Copy file descriptors since sd-bus closes them when the message is
        // freed. These ones will be closed in ~JobRequest() (in ~Tensor()),
        // after output has been written (JobRequest::signal()).
        int tmpFd = dup(get<5>(outputTensorMsg));
        if (tmpFd < 0) {
            throw FileDescriptorError("Could not duplicate file descriptor: " +
                                      string(strerror(errno)));
        }

        try {
            const uint64_t id = get<8>(outputTensorMsg);
            shared_ptr<Buffer> buffer = make_shared<Buffer>(
                FileDescriptor(std::move(tmpFd)), fdSize,
                get<7>(outputTensorMsg), get<9>(outputTensorMsg));

            if (id > 0) {
                // Save buffer if needed, otherwise set ptr to the saved one.
                buffer = resolveBuffer(buffer, id);
            }

            outputTensors.emplace_back(
                get<0>(outputTensorMsg), get<1>(outputTensorMsg),
                get<3>(outputTensorMsg), get<4>(outputTensorMsg), byteSize, "",
                buffer);
        } catch (invalid_argument& e) {
            throw invalid_argument(string("Could not parse output tensors: ") +
                                   e.what());
        }
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Output tensors read");

    return {std::move(inputTensors), std::move(outputTensors)};
}

shared_ptr<Buffer> Session::resolveBuffer(shared_ptr<Buffer> buffer,
                                          const uint64_t id) {
    assert(buffer->getFd() > 0);
    assert(id > 0);

    if (!buffers.count(id)) {
        // Buffer not found. Check total number of saved buffers.
        if (buffers.size() >= MAX_BUFFERS) {
            throw runtime_error("Buffer limit reached");
        }

        // Save it.
        const auto [it, hasInserted] = buffers.insert({id, buffer});
        if (!hasInserted) {
            throw runtime_error("Could not save buffer");
        }

        BufferMessenger::isSaved(*it->second, true);

        return it->second;
    }

    // Verify buffer against the saved one.
    const auto& savedBuf = buffers.at(id);
    if (savedBuf->getMaxSize() != buffer->getMaxSize()) {
        throw invalid_argument("Buffer with ID " + to_string(id) +
                               " has size (" + to_string(buffer->getMaxSize()) +
                               ") unequal to saved counterpart (" +
                               to_string(savedBuf->getMaxSize()) + ")");
    } else if (savedBuf->getStartOffset() != buffer->getStartOffset()) {
        throw invalid_argument("Buffer with ID " + to_string(id) +
                               " has offset (" +
                               to_string(buffer->getStartOffset()) +
                               ") unequal to saved counterpart (" +
                               to_string(savedBuf->getStartOffset()) + ")");
    } else if (savedBuf->getFdProps() != buffer->getFdProps()) {
        throw invalid_argument("Buffer with ID " + to_string(id) +
                               " has buffer properties (" +
                               to_string(buffer->getFdProps()) +
                               ") unequal to saved counterpart (" +
                               to_string(savedBuf->getFdProps()) + ")");
    }

    return savedBuf;
}

vector<shared_ptr<Buffer>> Session::allocTensorBuffers(
    const BackEndUnit& unit, const uint8_t inputOutputSpecifier,
    const uint64_t modelId, const vector<uint64_t>& ids, const uint32_t fdProps,
    const ParamsMap& params) {
    // Check number of IDs.
    const auto [inputTensorMetadata, outputTensorMetadata] =
        unit.getModel(modelId).getTensorMetadata();
    if (inputOutputSpecifier == LAROD_MSG_ALLOC_TENSORS_SPECIFIER_INPUT) {
        if (inputTensorMetadata.size() != ids.size()) {
            throw invalid_argument("Number of IDs (" + to_string(ids.size()) +
                                   ") doesn't match number of input tensors (" +
                                   to_string(inputTensorMetadata.size()) + ")");
        }
    } else if (inputOutputSpecifier ==
               LAROD_MSG_ALLOC_TENSORS_SPECIFIER_OUTPUT) {
        if (outputTensorMetadata.size() != ids.size()) {
            throw invalid_argument(
                "Number of IDs (" + to_string(ids.size()) +
                ") doesn't match number of output tensors (" +
                to_string(outputTensorMetadata.size()) + ")");
        }
    } else {
        throw invalid_argument("Invalid input/output specifier (" +
                               to_string(inputOutputSpecifier) + ")");
    }

    // Check total number of saved buffers.
    if (buffers.size() + ids.size() > MAX_BUFFERS) {
        throw runtime_error("Buffer limit reached");
    }

    // Allocate buffers.
    vector<shared_ptr<Buffer>> bufs;
    for (size_t i = 0; i < ids.size(); ++i) {
        if (inputOutputSpecifier == LAROD_MSG_ALLOC_TENSORS_SPECIFIER_INPUT) {
            bufs.push_back(unit.allocateInput(
                inputTensorMetadata[i].getByteSize(), fdProps, params));
        } else if (inputOutputSpecifier ==
                   LAROD_MSG_ALLOC_TENSORS_SPECIFIER_OUTPUT) {
            bufs.push_back(unit.allocateOutput(
                outputTensorMetadata[i].getByteSize(), fdProps, params));
        } else {
            assert(0);
        }
    }

    // Save buffers.
    for (size_t i = 0; i < bufs.size(); ++i) {
        auto [it, hasInserted] = buffers.insert({ids[i], bufs[i]});
        if (!hasInserted) {
            // Clean up previously inserted buffers.
            for (size_t j = 0; j < i; ++j) {
                if (!buffers.erase(ids[j])) {
                    LOG.error(
                        "Could not clean up allocated saved buffer with ID " +
                        to_string(ids[j]));
                }
            }

            throw runtime_error("Could not save allocated buffer with ID " +
                                to_string(ids[i]));
        }

        BufferMessenger::isSaved(*it->second, true);
    }

    return bufs;
}

uint64_t Session::getNextId() {
    return nextId;
}

pair<int, int> Session::getClientAsyncFds() const {
    return {clientAsyncReadFd, clientAsyncWriteFd};
}

uint64_t Session::getSessionId() const {
    return ID;
}

Session::ModelsRetType Session::getModels() const {
    Session::ModelsRetType ret;
    for (size_t chipId = 0; chipId < UNITS.size(); ++chipId) {
        unordered_map<uint64_t, Model> models = UNITS[chipId]->getModels();
        for (const auto& model : models) {
            auto [inputTensorMetadata, outputTensorMetadata] =
                model.second.getTensorMetadata();
            ret.emplace_back(model.second.getSize(), model.second.getId(),
                             static_cast<uint64_t>(chipId),
                             static_cast<int32_t>(UNITS[chipId]->getChip()),
                             static_cast<int32_t>(model.second.getAccessType()),
                             model.second.getName(),
                             createTensorMetadataMsg(inputTensorMetadata),
                             createTensorMetadataMsg(outputTensorMetadata));

            LOG.debug(
                THIS_NAMESPACE + __func__ + "(): Session " + ID_STR + ": " +
                model.second.getName() + ", " +
                to_string(model.second.getId()) + ", " + to_string(chipId) +
                ", " + to_string(model.second.getSize()) + ", " +
                to_string(static_cast<int>(model.second.getAccessType())));
        }
    }

    return ret;
}

void Session::procHandshakeMsg(sd_bus_message* msg) {
    string clientName;
    try {
        sdbusplus::message::message(msg).read(clientName);
    } catch (sdbusplus::exception::SdBusError& e) {
        LOG.error("Session " + ID_STR +
                  ": Could not read message: " + e.what());

        return;
    }

    // We need to respond to the handshake in order for the bus to complete the
    // authentication phase. If we respond later (after BUS_AUTH_TIMEOUT) we
    // would get a ETIMEDOUT.
    sendMsg(LAROD_MSG_HANDSHAKE, ID);

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Handshaked with peer " +
              clientName);

    // Close our copies of the client's fds.
    clientAsyncReadFd.close();
    clientAsyncWriteFd.close();

    hasHandshaked = true;
}

void Session::procQueueLoadModelMsg(sd_bus_message* sdbusMsg) {
    sdbusplus::message::unix_fd msgFd = -1;
    uint64_t sz = 0;
    uint64_t chipId = 0;
    int32_t access = 0;
    std::string name;
    uint64_t metaData = 0;
    ParamsMap params;
    sdbusplus::message::message msg(sdbusMsg);
    try {
        msg.read(msgFd, sz, chipId, access, name, metaData);
        if (containsDict(msg)) {
            msg.read(params);
        }
    } catch (sdbusplus::exception::SdBusError& e) {
        LOG.error("Session " + ID_STR +
                  ": Could not read message: " + e.what());

        return;
    }

    if (chipId > UNITS.size()) {
        string errMsg = "Chip ID is larger than number of units";
        LOG.error("Session " + ID_STR + ": Could not load model: " + errMsg);
        sendLoadModelResultError(LAROD_ERROR_INVALID_CHIP_ID, errMsg, metaData);

        return;
    }

    // Copy file descriptor since sd-bus closes them when the message is freed.
    int tmpFd = dup(msgFd);
    if (tmpFd < 0) {
        string errMsg = "Could not duplicate file descriptor";
        string errnoMsg = strerror(errno);
        LOG.error("Session " + ID_STR + ": " + errMsg +
                  " for incoming message: " + errnoMsg);
        sendLoadModelResultError(LAROD_ERROR_FD, errMsg + ": " + errnoMsg,
                                 metaData);
        return;
    }

    FileDescriptor fd = std::move(tmpFd);
    try {
        queueLoadModel(fd, sz, static_cast<size_t>(chipId), access, name,
                       metaData, std::move(params));
    } catch (FileDescriptorError& e) {
        LOG.error("Session " + ID_STR + ": Could not load model: " + e.what());
        sendLoadModelResultError(LAROD_ERROR_FD, e.what(), metaData);
    } catch (TypeSizeOverflow& e) {
        LOG.error("Session " + ID_STR + ": Could not load model: " + e.what());
        sendLoadModelResultError(static_cast<larodErrorCode>(EOVERFLOW),
                                 e.what(), metaData);
    } catch (exception& e) {
        LOG.error("Session " + ID_STR + ": Could not load model: " + e.what());
        sendLoadModelResultError(LAROD_ERROR_LOAD_MODEL, e.what(), metaData);
    }
}

void Session::procDeleteModelMsg(sd_bus_message* msg) {
    uint64_t modelID = 0;
    uint64_t chipID = 0;
    uint64_t callbackPtr = 0;
    try {
        sdbusplus::message::message(msg).read(modelID, chipID, callbackPtr);
    } catch (sdbusplus::exception::SdBusError& e) {
        LOG.error("Session " + ID_STR +
                  ": Could not read message: " + e.what());

        return;
    }

    int32_t errorCode = LAROD_ERROR_NONE;
    string errorMsg;
    if (chipID > UNITS.size()) {
        errorMsg = "Chip ID is larger than number of units";
        LOG.error("Session " + ID_STR +
                  ": Could not delete model: " + errorMsg);

        errorCode = LAROD_ERROR_INVALID_CHIP_ID;
        try {
            lock_guard<recursive_mutex> lockGuard(mtxSend);
            sendMsg(LAROD_MSG_DELETE_MODEL, callbackPtr, errorCode, errorMsg);
        } catch (ConnectionError& e) {
            // Client unexpectedly disconnected. We just ignore it (it will be
            // handled in read bus process thread).
        }

        return;
    }

    try {
        deleteModel(modelID, static_cast<size_t>(chipID));
    } catch (BackEndUnit::ModelNotFound& e) {
        errorCode = LAROD_ERROR_MODEL_NOT_FOUND;
        errorMsg = string("Could not delete model: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errorMsg);
    } catch (BackEndUnit::PermissionDenied& e) {
        errorCode = LAROD_ERROR_PERMISSION;
        errorMsg = string("Could not delete model: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errorMsg);
    } catch (exception& e) {
        errorCode = LAROD_ERROR_DELETE_MODEL;
        errorMsg = string("Could not delete model: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errorMsg);
    }

    try {
        lock_guard<recursive_mutex> lockGuard(mtxSend);
        sendMsg(LAROD_MSG_DELETE_MODEL, callbackPtr, errorCode, errorMsg);
    } catch (ConnectionError& e) {
        // Client unexpectedly disconnected. We just ignore it (it will be
        // handled in read bus process thread).
    }
}

void Session::procQueueJobMsg(sd_bus_message* sdbusMsg) {
    uint64_t modelId;
    uint64_t chipId;
    uint8_t priority;
    uint64_t metaData;
    QueueJobTensorsMsg inputsMsg;
    QueueJobTensorsMsg outputsMsg;
    ParamsMap params;
    sdbusplus::message::message msg(sdbusMsg);
    try {
        msg.read(modelId, chipId, priority, metaData, inputsMsg, outputsMsg);
        if (containsDict(msg)) {
            msg.read(params);
        }
    } catch (sdbusplus::exception::SdBusError& e) {
        LOG.error("Session " + ID_STR +
                  ": Could not read message: " + e.what());

        return;
    }

    if (chipId > UNITS.size()) {
        string errMsg = "Chip ID is larger than number of units";
        LOG.error("Session " + ID_STR + ": Could not run job: " + errMsg);
        sendLoadModelResultError(LAROD_ERROR_INVALID_CHIP_ID, errMsg, metaData);

        return;
    }

    try {
        vector<Tensor> inputTensors;
        vector<Tensor> outputTensors;
        tie(inputTensors, outputTensors) = readTensorMsg(inputsMsg, outputsMsg);
        queueJob(modelId, static_cast<size_t>(chipId), std::move(inputTensors),
                 std::move(outputTensors), priority, metaData,
                 std::move(params));
    } catch (FileDescriptorError& e) {
        string errMsg = string("Could not queue job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errMsg);
        sendJobResultError(LAROD_ERROR_FD, errMsg, metaData);
    } catch (TypeSizeOverflow& e) {
        string errMsg = string("Could not queue job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errMsg);
        sendJobResultError(static_cast<larodErrorCode>(EOVERFLOW), errMsg,
                           metaData);
    } catch (BackEndUnit::ModelNotFound& e) {
        string errMsg = string("Could not queue job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errMsg);
        sendJobResultError(LAROD_ERROR_MODEL_NOT_FOUND, errMsg, metaData);
    } catch (exception& e) {
        string errMsg = string("Could not queue job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errMsg);
        sendJobResultError(LAROD_ERROR_JOB, errMsg, metaData);
    }
}

void Session::procAllocTensorsMsg(sd_bus_message* sdbusMsg) {
    uint64_t modelId;
    uint64_t chipId;
    vector<uint64_t> ids;
    uint8_t inputOutputSpecifier;
    uint32_t fdProps;
    uint64_t metaData;
    ParamsMap params;
    sdbusplus::message::message msg(sdbusMsg);
    try {
        msg.read(modelId, chipId, ids, inputOutputSpecifier, fdProps, metaData);
        if (containsDict(msg)) {
            msg.read(params);
        }
    } catch (sdbusplus::exception::SdBusError& e) {
        LOG.error("Session " + ID_STR +
                  ": Could not read message: " + e.what());

        return;
    }

    vector<sdbusplus::message::unix_fd> allocFds;
    vector<uint64_t> allocSizes;
    vector<uint32_t> allocFdProps;
    int32_t errCode = LAROD_ERROR_NONE;
    string errMsg;
    if (chipId > UNITS.size()) {
        string errMsg = "Chip ID is larger than number of units";
        LOG.error("Session " + ID_STR +
                  ": Could not allocate tensors: " + errMsg);

        errCode = LAROD_ERROR_INVALID_CHIP_ID;
        try {
            lock_guard<recursive_mutex> lockGuard(mtxSend);
            sendMsg(LAROD_MSG_ALLOC_TENSORS, allocFds, allocSizes, allocFdProps,
                    metaData, errCode, errMsg);
        } catch (ConnectionError& e) {
            // Client unexpectedly disconnected. We just ignore it (it will be
            // handled in read bus process thread).
        }

        return;
    }

    try {
        const auto& buffers = allocTensorBuffers(
            *UNITS[static_cast<size_t>(chipId)], inputOutputSpecifier, modelId,
            ids, fdProps, params);
        for (const auto& buf : buffers) {
            allocFds.push_back(static_cast<int>(buf->getFd()));
            allocSizes.push_back(buf->getMaxSize());
            allocFdProps.push_back(buf->getFdProps());
        }
    } catch (BackEndUnit::ModelNotFound& e) {
        errCode = LAROD_ERROR_MODEL_NOT_FOUND;
        errMsg = "Could not get model: " + string(e.what());
        LOG.error("Session " + ID_STR + ": " + errMsg);
    } catch (exception& e) {
        errCode = LAROD_ERROR_ALLOC;
        errMsg = "Could not allocate tensor buffers: " + string(e.what());
        LOG.error("Session " + ID_STR + ": " + errMsg);
    }

    try {
        lock_guard<recursive_mutex> lockGuard(mtxSend);
        sendMsg(LAROD_MSG_ALLOC_TENSORS, allocFds, allocSizes, allocFdProps,
                metaData, errCode, errMsg);
    } catch (ConnectionError& e) {
        // Client unexpectedly disconnected. We just ignore it (it will be
        // handled in read bus process thread).
    }
}

void Session::processAsyncReadBus() noexcept {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    // Initialize inputs to ppoll().
    static constexpr int NUM_FD = 2;
    struct pollfd pfd[NUM_FD] = {};
    pfd[0].fd = sd_bus_get_fd(asyncReadBus.get());
    pfd[1].fd = busProcEventFd;
    pfd[0].events = pfd[1].events = POLLIN | POLLPRI | POLLRDHUP | POLLERR;

    sd_bus_message* msg = nullptr;
    int ret;
    while (!stopBusProc) {
        ret = sd_bus_process(asyncReadBus.get(), &msg);
        if (ret < 0) {
            LOG.warning("Session " + ID_STR +
                        ": Could not process incoming asynchronous requests: " +
                        strerror(-ret));

            msg = nullptr;
            stopBusProc = true;
        }

        if (msg) {
            string msgMember = sd_bus_message_get_member(msg);
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Got message: " + msgMember);

            if (msgMember == LAROD_MSG_HANDSHAKE && !hasHandshaked) {
                procHandshakeMsg(msg);
            } else if (msgMember == LAROD_MSG_QUEUE_LOAD_MODEL &&
                       hasHandshaked) {
                procQueueLoadModelMsg(msg);
            } else if (msgMember == LAROD_MSG_QUEUE_JOB && hasHandshaked) {
                procQueueJobMsg(msg);
            } else if (msgMember == LAROD_MSG_DELETE_MODEL && hasHandshaked) {
                procDeleteModelMsg(msg);
            } else if (msgMember == LAROD_MSG_ALLOC_TENSORS && hasHandshaked) {
                procAllocTensorsMsg(msg);
            } else if (msgMember == LAROD_MSG_DISCONNECTED) {
                // This message is sent (by sd-bus itself) when connection is
                // reset by peer.
                stopBusProc = true;
            } else {
                LOG.error("Session " + ID_STR + ": Invalid message " +
                          msgMember + " from client");
            }
        }

        msg = sd_bus_message_unref(msg);

        if (ret == 0) {
            // No new messages, wait for some on the bus.
            pfd[0].revents = pfd[1].revents = 0;
            int pollRet = ppoll(pfd, NUM_FD, NULL, NULL);

            if (pollRet < 0) {
                LOG.error(
                    "Session " + ID_STR +
                    ": Could not process incoming asynchronous requests: " +
                    strerror(errno));
                stopBusProc = true;
            } else if (pfd[1].revents != 0) {
                // Something triggered the eventfd. Stop the processing.
                stopBusProc = true;
            }
        }
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}

shared_ptr<JobRequest> Session::popJobRequest(const size_t chipId) {
    lock_guard<mutex> lockGuard(mtxJobReqs);

    auto& queue = jobReqs.at(chipId);
    auto jobReq = queue.top();
    queue.pop();

    return jobReq;
}

template<typename... Args>
void Session::sendMsg(const string& msgType, Args&&... data) {
    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Sending message: msgType: " + msgType);

    sd_bus_message* tmp;
    int ret = sd_bus_message_new_method_call(
        asyncWriteBus.get(), &tmp, "com.axis.Larod1", "/", "com.axis.Larod1",
        msgType.c_str());
    if (ret < 0) {
        handleSendMsgError(ret, "Could not allocate method call");
    }

    unique_ptr<sd_bus_message, decltype(msgDeleter)*> msg{tmp,
                                                          Session::msgDeleter};
    sdbusplus::message::message(msg.get()).append(std::forward<Args>(data)...);

    ret = sd_bus_send(asyncWriteBus.get(), msg.get(), NULL);
    if (ret < 0) {
        handleSendMsgError(ret, "Could not send message");
    }

    ret = sd_bus_flush(asyncWriteBus.get());
    if (ret < 0) {
        handleSendMsgError(ret, "Could not flush bus");
    }
}

template<>
void Session::sendMsg(const string& msgType, const LoadModelRequest& req) {
    assert(msgType == LAROD_MSG_QUEUE_LOAD_MODEL);

    Session::LoadModelRetType msg;
    auto [inputTensorMetadata, outputTensorMetadata] = req.getTensorMetadata();

    // Fill model metadata.
    assert(req.CHIP_ID < UNITS.size());
    get<0>(msg) = {req.DATA.size(),
                   req.getModelId(),
                   req.CHIP_ID,
                   static_cast<int32_t>(
                       UNITS[static_cast<size_t>(req.CHIP_ID)]->getChip()),
                   static_cast<int32_t>(req.ACCESS),
                   req.NAME,
                   createTensorMetadataMsg(inputTensorMetadata),
                   createTensorMetadataMsg(outputTensorMetadata)};

    // Fill return information.
    get<1>(msg) = req.META_DATA;
    get<2>(msg) = LAROD_ERROR_NONE;
    get<3>(msg) = "";

    sendMsg(msgType, std::move(msg));
}

void Session::sendLoadModelResult(const LoadModelRequest* req) {
    lock_guard<recursive_mutex> lockGuard(mtxSend);

    // Check for error.
    exception_ptr eptr = req->getException();
    try {
        if (eptr) {
            rethrow_exception(eptr);
        }
    } catch (exception& e) {
        LOG.error("Session " + ID_STR + ": " + e.what());
        sendLoadModelResultError(LAROD_ERROR_LOAD_MODEL, e.what(),
                                 req->META_DATA);
        return;
    }

    // Update private models.
    if (req->ACCESS == Model::Access::PRIVATE) {
        lock_guard<mutex> lockGuard(mtxPrivateModels);
        size_t unitId = static_cast<size_t>(req->CHIP_ID);
        bool ret = privateModels[unitId].insert(req->getModelId()).second;
        if (!ret) {
            // Should never happen...
            throw runtime_error("Session " + ID_STR +
                                ": Could not insert private model " +
                                to_string(req->getModelId()) + " into set");
        }
    }

    if (!req->NAME.empty()) {
        LOG.debug("Session " + ID_STR + ": Model \"" + req->NAME + "\" loaded");
    } else {
        LOG.debug("Session " + ID_STR + ": Model loaded");
    }

    try {
        sendMsg(LAROD_MSG_QUEUE_LOAD_MODEL, *req);
    } catch (ConnectionError& e) {
        // Client unexpectedly disconnected. We just ignore it (it will be
        // handled in read bus process thread).
    }
}

void Session::sendLoadModelResultError(const larodErrorCode error,
                                       const string& errorMsg,
                                       const uint64_t metaData) {
    lock_guard<recursive_mutex> lockGuard(mtxSend);

    // Fill return information.
    LoadModelRetType msg;
    get<1>(msg) = metaData;
    get<2>(msg) = error;
    get<3>(msg) = errorMsg;

    try {
        sendMsg(LAROD_MSG_QUEUE_LOAD_MODEL, std::move(msg));
    } catch (ConnectionError& e) {
        // Client unexpectedly disconnected. We just ignore it (it will be
        // handled in sd_bus_process() in read bus process thread).
    }
}

void Session::sendJobResult(JobRequest* req) {
    lock_guard<recursive_mutex> lockGuard(mtxSend);

    // Check for error.
    exception_ptr eptr = req->getException();
    try {
        if (eptr) {
            rethrow_exception(eptr);
        }
    } catch (FileDescriptorError& e) {
        string errMsg = string("Could not run job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errMsg);
        sendJobResultError(LAROD_ERROR_FD, errMsg, req->META_DATA);
        return;
    } catch (BackEndUnit::ModelNotFound& e) {
        string errMsg = string("Could not run job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errMsg);
        sendJobResultError(LAROD_ERROR_MODEL_NOT_FOUND, errMsg, req->META_DATA);
        return;
    } catch (BackEndUnit::PermissionDenied& e) {
        string errMsg = string("Could not run job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errMsg);
        sendJobResultError(LAROD_ERROR_PERMISSION, errMsg, req->META_DATA);
        return;
    } catch (exception& e) {
        string errMsg = string("Could not run job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errMsg);
        sendJobResultError(LAROD_ERROR_JOB, errMsg, req->META_DATA);
        return;
    }

    try {
        sendMsg(LAROD_MSG_QUEUE_JOB, req->META_DATA,
                static_cast<int32_t>(LAROD_ERROR_NONE), "");
    } catch (ConnectionError& e) {
        // Client unexpectedly disconnected. We just ignore it (it will be
        // handled in read bus process thread).
    }
}

void Session::sendJobResultError(const larodErrorCode error,
                                 const string& errorMsg,
                                 const uint64_t metaData) {
    lock_guard<recursive_mutex> lockGuard(mtxSend);

    try {
        sendMsg(LAROD_MSG_QUEUE_JOB, metaData, static_cast<int32_t>(error),
                errorMsg);
    } catch (ConnectionError& e) {
        // Client unexpectedly disconnected. We just ignore it (it will be
        // handled in read bus process thread).
    }
}

void Session::handleSendMsgError(const int retCode, const string& msg) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): " + msg + ": " +
              strerror(-retCode));
    if (-retCode == ECONNRESET || -retCode == ENOTCONN) {
        throw ConnectionError(strerror(-retCode));
    }

    throw runtime_error("Could not send message");
}

} // namespace larod
