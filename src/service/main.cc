/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <sys/eventfd.h>
#include <unistd.h>

#include "argparse.hh"
#include "log.hh"
#include "service.hh"

using namespace std;
using namespace larod;

namespace {

const string SERVICE_NAME = "com.axis.Larod1";
const string SERVICE_OBJ_PATH = "/com/axis/Larod1";

int stopServiceEventFd = -1;

void signalHandler(int sig) {
    static volatile sig_atomic_t signalHandled = 0;
    if (signalHandled) {
        signal(sig, SIG_DFL);
        raise(sig);

        return;
    }

    signalHandled = 1;

    // Write to stopServiceEventFd to signal service to stop.
    uint64_t value = 1;
    ssize_t writtenBytes = write(stopServiceEventFd, &value, sizeof(value));
    if (static_cast<size_t>(writtenBytes) != sizeof(value)) {
        // Last resort...
        signal(sig, SIG_DFL);
        raise(sig);
    }
}

} // namespace

int main(int argc, char** argv) {
    // Parse arguments.
    args_t args;
    if (parseArgs(argc, argv, &args)) {
        return EXIT_FAILURE;
    }

    // Setup logger.
    LOG.setPrintToSyslog(args.printToSyslog);

    // Create and request D-Bus name.
#ifdef LAROD_USE_SESSION_BUS
    auto bus = sdbusplus::bus::new_user();
#else
    auto bus = sdbusplus::bus::new_system();
#endif
    sdbusplus::server::manager_t manager{bus, SERVICE_OBJ_PATH.c_str()};
    try {
        bus.request_name(SERVICE_NAME.c_str());
    } catch (sdbusplus::exception::SdBusError& e) {
        LOG.error(string("Could not request bus name: ") + e.what());

        return EXIT_FAILURE;
    }

    // Initialize list of chips to be created for service.
    vector<larodChip> chips;
#ifdef LAROD_USE_CVFLOW_NNCTRL
    chips.push_back(LAROD_CHIP_CVFLOW_NN);
#endif
#ifdef LAROD_USE_CVFLOW_VPROC
    chips.push_back(LAROD_CHIP_CVFLOW_PROC);
#endif
#ifdef LAROD_USE_TFLITE_CPU
    chips.push_back(LAROD_CHIP_TFLITE_CPU);
#endif
#ifdef LAROD_USE_TPU
    chips.push_back(LAROD_CHIP_TPU);
#endif
#ifdef LAROD_USE_GPU
    chips.push_back(LAROD_CHIP_TFLITE_GLGPU);
#endif
#ifdef LAROD_USE_LIBYUV
    chips.push_back(LAROD_CHIP_LIBYUV);
#endif
#ifdef LAROD_USE_OPENCL
    chips.push_back(LAROD_CHIP_OPENCL);
#endif
#if defined(DEBUG) || defined(LAROD_USE_DEBUG_CHIP)
    chips.push_back(LAROD_CHIP_DEBUG);
#endif
    if (chips.empty()) {
        // In case of no avaialable chips, use debug chip
        chips.push_back(LAROD_CHIP_DEBUG);
    }

    // Create service. This will setup the D-Bus interface as well, but no
    // processing will be done until we actually start the service.
    shared_ptr<Service> service;
    try {
        service =
            make_shared<Service>(SERVICE_NAME, SERVICE_OBJ_PATH, bus, chips);
    } catch (exception& e) {
        LOG.error(string("Could not create service: ") + e.what());

        return EXIT_FAILURE;
    }

    // Create eventfd to be able to signal the service to eventually stop.
    stopServiceEventFd = eventfd(0, 0);
    if (stopServiceEventFd < 0) {
        LOG.error("Could not create eventfd");

        return EXIT_FAILURE;
    }

    // Auto-close stopServiceEventFd.
    unique_ptr<int, function<void(int*)>> fd(&stopServiceEventFd,
                                             [](int* fd) { close(*fd); });

    // Register signal handler before starting the service.
    if (signal(SIGINT, signalHandler) == SIG_ERR ||
        signal(SIGTERM, signalHandler) == SIG_ERR) {
        LOG.error("Could not register signal handler");

        return EXIT_FAILURE;
    }

    try {
        service->start(stopServiceEventFd);
    } catch (exception& e) {
        LOG.error(string("Could not start service: ") + e.what());

        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
