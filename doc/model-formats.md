# Data formats in larod
This file describes data format of model files in larod.

## Binary-data models

The backends that run neural networks are loaded with a file descriptor pointing
to a blob of binary data. Usually, this would be a file produced by a toolchain
related to the backend in question and the format of this data is therefore
backend specific. See [nn-inference](nn-inference.md) for a list of all
neural-network backends and their specific model-format requirements.

### Deprecated larod format

For backwards compatibility, some backends support binary data packed in
`.larod` format, as produced by the
[larod-converter](../tools/model-converter/README.md) tool. This format is
simply backend specific data packed in a
[Flatbuffers](https://google.github.io/flatbuffers/) to define a model. NOTE:
The `.larod` format is DEPRECATED and will be removed in the future. One can now
skip the larod-convert step and send in models "as-is".

## Parameters-only models

Some models do not need any binary data; instead a set of parameters is enough
for these models. This is the case for some of the preprocessing models, where
no file descriptor is passed in the model load call; instead the model is
constructed from a given `larodMap`. The parameters associated with
preprocessing backends can be found in the [preprocessing](preprocessing.md)
document.
