/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "argparse.h"

#include <argp.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "log.h"
#include "map.h"
#include "utils.h"

#define KEY_USAGE (127)

/**
 * @brief Parses a string as an unsigned long long.
 *
 * @param arg String to parse.
 * @param i Pointer to the number being the result of parsing.
 * @return Positive errno style return code (zero means success).
 */
static int parsePosInt(char* arg, unsigned long long* i);

static int parseOpt(int key, char* arg, struct argp_state* state);

/**
 * @brief Increases the capacity of a char** if need be.
 *
 * @param strArr Array to expand.
 * @param neededLen The array's currently required length.
 * @param currentLen Pointer to the current length of the array.
 * @return False if any errors occur, otherwise true.
 */
static bool expandStrArr(char*** strArr, size_t neededLen, size_t* currentLen);

const struct argp_option opts[] = {
    {"asynchronous", 'a', NULL, 0,
     "Run jobs asynchronously. The default is synchronous calls.", 0},
    {"chip", 'c', "CHIP", 0,
     "Chooses chip CHIP to test load on, where CHIP is the enum type "
     "larodChip from the library. If not specified, LAROD_CHIP_TFLITE_CPU will "
     "be used.",
     0},
    {"debug", 'd', NULL, 0, "Enable debug output.", 0},
    {"dma-input", 'z', NULL, 0,
     "Use chip-specific dma buffers for input tensors. The input data will be "
     "copied into the corresponding buffers before inference.",
     0},
    {"dma-output", 'w', NULL, 0,
     "Use chip-specific dma buffers for output tensors. The output data "
     "will be copied from the buffers to file destinations after all "
     "inferences are done.",
     0},
    {"force-map-input", 'j', NULL, 0,
     "Set LAROD_FD_PROP_MAP as only fd property of input buffers, effectively "
     "forcing the backend to mmap the inputs. In particular if used together "
     "with --dma-input dma buffers will be used but the LAROD_FD_PROP_DMABUF "
     "will be removed from the input tensors' fd props.",
     0},
    {"force-map-output", 'k', NULL, 0,
     "Set LAROD_FD_PROP_MAP as only fd property of input buffers, effectively "
     "forcing the backend to mmap the outputs. In particular if used together "
     "with --dma-output dma buffers will be used but the LAROD_FD_PROP_DMABUF "
     "will be removed from the output tensors' fd props.",
     0},
    {"model-params", 'b', "KEY:VAL", 0,
     "Add additonal parameters when loading model (c.f. larodLoadModel()). "
     "KEY should be a string and VAL should either be a string, integer or an "
     "array of integers separated by \",\". For example "
     "\"image.input.format:rgb-interleaved\", \"image.input.row-pitch:224\" or "
     "\"image.input.size:224,224\".",
     0},
    {"job-params", 'e', "KEY:VAL", 0,
     "Add additional parameters when running a job (c.f. "
     "larodCreateJobRequest()). See option \"--model-params\" for a description"
     " of KEY and VAL.",
     0},
    {"input", 'i', "INPUT_FILE", 0,
     "Run job on INPUT_FILE. This option can be specified multiple times and "
     "will then correspond to each input tensor of the model. The order you "
     "add them will map to the order of the input tensors of the model.",
     0},
    {"model", 'm', "MODEL", 0,
     "Run job with MODEL. This option is optional since some backends do not "
     "use a dedicated model file to run jobs with.",
     0},
    {"name", 'n', "NAME", 0,
     "Optional model name. If not specified, the file name will be used.", 0},
    {"output", 'o', "OUTPUT_FILE", 0,
     "Write job result to OUTPUT_FILE. This option can be specified multiple "
     "times and will then correspond to each output tensor. The order you add "
     "them will map to the order of the output tensors of the model. If fewer "
     "files than the number of output tensors of the model are specified, then "
     "the remaining output tensors will be written to \"INPUT_FILE[0].outX\" "
     "where INPUT_FILE[0] refers to the first input file given, and X "
     "corresponds to the models's ordering of the output tensors.",
     0},
    {"private", 'p', NULL, 0,
     "Make the model private when loaded. The default is public.", 0},
    {"rounds", 'r', "N", 0,
     "Runs the job N rounds, or indefinitely if not specified.", 0},
    {"sleep", 's', "T", 0,
     "Sleep for T milliseconds between asynchronous jobs. Default is no sleep.",
     0},
    {"time", 't', NULL, 0,
     "Print the mean of the job time in milliseconds. This time "
     "measurement is only an approximation and the actual result might differ. "
     "For accurate measurements, make sure that this app is the only one "
     "executing on the corresponding chip.",
     0},
    {"track-inputs", 'f', NULL, 0,
     "Set unique IDs on input tensors which makes larod cache the tensors' "
     "buffers and operate on that cached data. This flag must be set if "
     "larod-allocated buffers are used since buffers allocated from larod are "
     "always tracked.",
     0},
    {"track-outputs", 'u', NULL, 0,
     "Set unique IDs on output tensors which makes larod cache the tensors' "
     "buffers and operate on that cached data. This flag must be set if "
     "larod-allocated buffers are used since buffers allocated from larod are "
     "always tracked.",
     0},
    {"limit", 'l', "T", 0,
     "Set T milliseconds as an upper limit for the mean of the job time. "
     "If the measured time is greater than this limit, the test will fail. "
     "This time measurement is only an approximation and the actual result "
     "might differ. For accurate measurements, make sure that this app is the "
     "only one executing on the corresponding chip.",
     0},
    {"csv", 'x', "FILE", 0, "Save job times (us) in CSV format to FILE.", 0},
    {"help", 'h', NULL, 0, "Print this help text and exit.", 0},
    {"usage", KEY_USAGE, NULL, 0, "Print short usage message and exit.", 0},
    {0}};
const struct argp argp = {opts,
                          parseOpt,
                          "(--model | --model-params [--model-params...]) -i "
                          "INPUT_FILE [-i INPUT_FILE...] [-o OUTPUT_FILE...]",
                          "Test load on larod by continuously running jobs",
                          NULL,
                          NULL,
                          NULL};

bool parseArgs(int argc, char** argv, args_t* args) {
    if (argp_parse(&argp, argc, argv, ARGP_NO_HELP, NULL, args)) {
        return false;
    }

    // Check option conditions

    // Model file or model parameters must be present.
    if (!args->modelFile && !args->hasModelParams) {
        fprintf(stderr, "%s: Model file or model parameters not specified\n",
                argv[0]);

        return false;
    }

    // At least one input file must be supplied.
    if (!args->numInputs) {
        fprintf(stderr, "%s: No input file specified\n", argv[0]);

        return false;
    }

    // Sleep time without asynchronous jobs.
    if (args->sleepTime && !args->async) {
        fprintf(stderr,
                "%s: Option \"--sleep\" only works together with option "
                "\"--async\"\n",
                argv[0]);

        return false;
    }

    return true;
}

int parseOpt(int key, char* arg, struct argp_state* state) {
    args_t* args = state->input;
    switch (key) {
    case 'a':
        args->async = true;
        break;
    case 'c': {
        unsigned long long chip;
        int ret = parsePosInt(arg, &chip);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid chip type");
        }
        // Make sure we don't overflow when casting to enum below.
        if (chip > INT_MAX) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ERANGE,
                         "chip type value too large");
        }
        args->chip = (larodChip) chip;
        break;
    }
    case 'd':
        args->debug = true;
        break;
    case 'z':
        args->useDmaInput = true;
        break;
    case 'w':
        args->useDmaOutput = true;
        break;
    case 'j':
        args->forceMapInput = true;
        break;
    case 'k':
        args->forceMapOutput = true;
        break;
    case 'f':
        args->trackInputs = true;
        break;
    case 'u':
        args->trackOutputs = true;
        break;
    case 'b': {
        int ret = addParam(args->modelParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to model parameters", arg);
            return ret;
        }
        args->hasModelParams = true;
        break;
    }
    case 'e': {
        int ret = addParam(args->jobParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to job parameters", arg);
            return ret;
        }
        break;
    }
    case 'i':
        args->numInputs++;
        if (!expandStrArr(&args->inputFiles, args->numInputs,
                          &args->inputsArrLen)) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "unable to reallocate input files array");
        }

        if (!strlen(arg)) {
            destroyArgs(args);
            argp_error(state, "input file name cannot be empty");
        }

        args->inputFiles[args->numInputs - 1] = arg;
        break;
    case 'm':
        args->modelFile = arg;
        break;
    case 'n':
        args->modelName = arg;
        break;
    case 'o':
        args->numOutputs++;
        if (!expandStrArr(&args->outputFiles, args->numOutputs,
                          &args->outputsArrLen)) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "unable to reallocate output files array");
        }
        args->outputFiles[args->numOutputs - 1] = arg;
        break;
    case 'p':
        args->modelAccess = LAROD_ACCESS_PRIVATE;
        break;
    case 'r': {
        int ret = parsePosInt(arg, &args->rounds);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid number of rounds");
        }
        break;
    }
    case 's': {
        unsigned long long tmp;
        int ret = parsePosInt(arg, &tmp);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid sleep time");
        } else if (tmp > UINT_MAX) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ERANGE,
                         "sleep time must be less than %u ms", UINT_MAX);
        }
        args->sleepTime = (unsigned int) tmp;
        break;
    }
    case 't':
        args->timeJob = true;
        break;
    case 'l': {
        unsigned long long tmp;
        int ret = parsePosInt(arg, &tmp);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid job time limit");
        }

        args->jobTimeLimit = tmp;
        break;
    }
    case 'x':
        args->csvPath = arg;
        break;
    case 'h':
        destroyArgs(args);
        argp_state_help(state, stdout, ARGP_HELP_STD_HELP);
        break;
    case KEY_USAGE:
        destroyArgs(args);
        argp_state_help(state, stdout, ARGP_HELP_USAGE | ARGP_HELP_EXIT_OK);
        break;
    case ARGP_KEY_ARG:
        destroyArgs(args);
        argp_error(state, "Too many arguments given");
        break;
    case ARGP_KEY_INIT:
        args->jobTimeLimit = ULONG_MAX;
        args->rounds = 0;
        args->modelFile = NULL;
        args->inputFiles = NULL;
        args->outputFiles = NULL;
        args->numInputs = 0;
        args->numOutputs = 0;
        args->modelName = NULL;
        args->csvPath = NULL;
        args->chip = LAROD_CHIP_TFLITE_CPU;
        args->modelAccess = LAROD_ACCESS_PUBLIC;
        args->sleepTime = 0;
        args->async = false;
        args->debug = false;
        args->useDmaInput = false;
        args->useDmaOutput = false;
        args->forceMapInput = false;
        args->forceMapOutput = false;
        args->trackInputs = false;
        args->trackOutputs = false;
        args->timeJob = false;
        args->hasModelParams = false;

        args->inputsArrLen = 1;
        args->inputFiles =
            calloc(args->inputsArrLen, sizeof(*(args->inputFiles)));
        if (!args->inputFiles) {
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "unable to allocate input files array");
        }

        args->outputsArrLen = 1;
        args->outputFiles =
            calloc(args->outputsArrLen, sizeof(*(args->outputFiles)));
        if (!args->outputFiles) {
            free(args->inputFiles);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "unable to allocate output files array");
        }

        args->modelParams = larodCreateMap(NULL);
        if (!args->modelParams) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not allocate model parameters");
        }

        args->jobParams = larodCreateMap(NULL);
        if (!args->jobParams) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not create larodMap");
        }
        break;
    case ARGP_KEY_END:
        if (state->arg_num != 0) {
            destroyArgs(args);
            argp_error(state, "Invalid number of arguments given");
        }
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static int parsePosInt(char* arg, unsigned long long* i) {
    char* endPtr;
    *i = strtoull(arg, &endPtr, 0);
    if (*endPtr != '\0') {
        return EINVAL;
    } else if (arg[0] == '-' || *i == 0) {
        return EINVAL;
    } else if (*i == ULLONG_MAX) {
        return ERANGE;
    }

    return 0;
}

void destroyArgs(args_t* args) {
    assert(args);

    free(args->inputFiles);
    free(args->outputFiles);
    larodDestroyMap(&args->modelParams);
    larodDestroyMap(&args->jobParams);
}

static bool expandStrArr(char*** strArr, size_t neededLen, size_t* currentLen) {
    assert(strArr);
    assert(currentLen);

    if (neededLen <= *currentLen) {
        return true;
    }
    *currentLen *= 2;

    char** newStrArr = realloc(*strArr, *currentLen * sizeof(*newStrArr));
    if (!newStrArr) {
        return false;
    }
    *strArr = newStrArr;

    return true;
}
