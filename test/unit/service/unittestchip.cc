/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "unittestchip.hh"

#include <numeric>

#include "log.hh"

using namespace std;

namespace larod {
namespace backendunit {

const string UnitTestChip::THIS_NAMESPACE = "UnitTestChip::";
const vector<size_t> UnitTestChip::INPUT_TENSOR_DIMS = {INPUT_TENSOR_BYTE_SIZE,
                                                        1, 1, 1};
const vector<size_t> UnitTestChip::OUTPUT_TENSOR_DIMS = {
    OUTPUT_TENSOR_BYTE_SIZE, 1, 1, 1};
const vector<size_t> UnitTestChip::INPUT_TENSOR_PITCHES = {};
const vector<size_t> UnitTestChip::OUTPUT_TENSOR_PITCHES = {};
const string UnitTestChip::INPUT_TENSOR_NAME = "UnitTestChipInput";
const string UnitTestChip::OUTPUT_TENSOR_NAME = "UnitTestChipOutput";
const vector<TensorMetadata> UnitTestChip::INPUT_TENSOR_METADATA{
    NBR_OF_INPUT_TENSORS,
    TensorMetadata(TENSOR_DATA_TYPE, INPUT_TENSOR_LAYOUT, INPUT_TENSOR_DIMS,
                   INPUT_TENSOR_PITCHES, INPUT_TENSOR_BYTE_SIZE,
                   INPUT_TENSOR_NAME)};
const vector<TensorMetadata> UnitTestChip::OUTPUT_TENSOR_METADATA{
    NBR_OF_OUTPUT_TENSORS,
    TensorMetadata(TENSOR_DATA_TYPE, OUTPUT_TENSOR_LAYOUT, OUTPUT_TENSOR_DIMS,
                   OUTPUT_TENSOR_PITCHES, OUTPUT_TENSOR_BYTE_SIZE,
                   OUTPUT_TENSOR_NAME)};

pair<vector<TensorMetadata>, vector<TensorMetadata>>
    UnitTestChip::loadModelVirtual(const span<uint8_t>& data, const uint64_t,
                                   const ParamsMap&) {
    verifyFbModel(data);

    return {INPUT_TENSOR_METADATA, OUTPUT_TENSOR_METADATA};
}

void UnitTestChip::deleteModelVirtual(const uint64_t) {
}

void UnitTestChip::runJobVirtual(const uint64_t, vector<Tensor>&,
                                 vector<Tensor>&, const ParamsMap&) {
}

std::shared_ptr<JobRequest> UnitTestChip::popJobQueueWait() {
    return BackEndUnit::popJobQueueWait();
}

std::shared_ptr<LoadModelRequest> UnitTestChip::popLoadModelQueueWait() {
    return BackEndUnit::popLoadModelQueueWait();
}

size_t UnitTestChip::getJobQueueSize() const {
    return BackEndUnit::getJobQueueSize();
}

size_t UnitTestChip::getLoadModelQueueSize() const {
    return BackEndUnit::getLoadModelQueueSize();
}

void UnitTestChip::startProcJobRequest() {
    jobInProgress = BackEndUnit::popJobQueueWait();
}

void UnitTestChip::startProcLoadModelRequest() {
    loadModelInProgress = BackEndUnit::popLoadModelQueueWait();
}

void UnitTestChip::finishJobRequest() {
    jobInProgress->signal();
    jobInProgress.reset();
}

void UnitTestChip::finishLoadModelRequest() {
    loadModelInProgress->signal();
    loadModelInProgress.reset();
}

} // namespace backendunit
} // namespace larod
