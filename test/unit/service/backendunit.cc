/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "backendunit.hh"

#include <gtest/gtest.h>

#include "debugchip.hh"
#include "model.hh"
#include "span.hh"
#include "testutils.hh"
#include "unittestchip.hh"

using namespace std;
using namespace larod;
using namespace larod::test;

/**
 * @brief Test fixture for BackEndUnit unit tests.
 */
class BackEndUnitTest : public ::testing::Test {
public:
    BackEndUnitTest() : MODEL_DATA(VALID_MODEL_BUF) {}

    backendunit::DebugChip debugChip;
    backendunit::UnitTestChip unitTestChip;
    const span<uint8_t> MODEL_DATA;
    // TODO: Generalize (DebugChip has only one input and output for now).
    vector<TensorMetadata> inputTensorMetadata =
        backendunit::DebugChip::INPUT_TENSOR_METADATA;
    vector<TensorMetadata> outputTensorMetadata =
        backendunit::DebugChip::OUTPUT_TENSOR_METADATA;
    static constexpr size_t allocSize = 1;
    static constexpr uint32_t allocFdProps =
        LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP;
    inline static const ParamsMap allocParamsMap;
};

TEST_F(BackEndUnitTest, LoadNonFlatBuffer) {
    vector<uint8_t> randomModelData = {1, 2, 3};
    larod::span<uint8_t> modelData(randomModelData);
    ASSERT_NO_THROW(
        unitTestChip.loadModel(0, modelData, Model::Access::PUBLIC, ""));
}

TEST_F(BackEndUnitTest, LoadModelVersion7OnMinVersion0) {
    // Recall that the version field is 7 for MODEL_DATA (see testmodel.h).
    ASSERT_NO_THROW(
        unitTestChip.loadModel(0, MODEL_DATA, Model::Access::PUBLIC, ""));
}

TEST_F(BackEndUnitTest, LoadModelVersion7OnMinVersion6) {
    // Recall that the version field is 7 for MODEL_DATA (see testmodel.h).
    unitTestChip.setMinVersion(6);
    ASSERT_NO_THROW(
        unitTestChip.loadModel(0, MODEL_DATA, Model::Access::PUBLIC, ""));
}

TEST_F(BackEndUnitTest, LoadModelVersion7OnMinVersion8) {
    // Recall that the version field is 7 for MODEL_DATA (see testmodel.h).
    unitTestChip.setMinVersion(8);
    ASSERT_THROW(
        unitTestChip.loadModel(0, MODEL_DATA, Model::Access::PUBLIC, ""),
        invalid_argument);
}

TEST_F(BackEndUnitTest, LoadPublicModel) {
    ASSERT_NO_THROW(
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PUBLIC, ""));
}

TEST_F(BackEndUnitTest, LoadPrivateModel) {
    ASSERT_NO_THROW(
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PRIVATE, ""));
}

TEST_F(BackEndUnitTest, LoadModelGivesUniqueIDs) {
    Model model1 =
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PRIVATE, "");
    Model model2 =
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PRIVATE, "");

    ASSERT_NE(model1.getId(), model2.getId());
}

TEST_F(BackEndUnitTest, DeletePrivateModel) {
    Model model =
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PRIVATE, "");
    ASSERT_NO_THROW(debugChip.deleteModel(0, model.getId()));
}

TEST_F(BackEndUnitTest, DeleteOthersPrivateModel) {
    Model model =
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PRIVATE, "");
    ASSERT_ANY_THROW(debugChip.deleteModel(1, model.getId()));
}

TEST_F(BackEndUnitTest, DeleteOthersPublicModel) {
    Model model = debugChip.loadModel(0, MODEL_DATA, Model::Access::PUBLIC, "");
    ASSERT_NO_THROW(debugChip.deleteModel(1, model.getId()));
}

TEST_F(BackEndUnitTest, DeleteNonExistentModel) {
    ASSERT_ANY_THROW(debugChip.deleteModel(0, numeric_limits<uint64_t>::max()));
}

TEST_F(BackEndUnitTest, AllocateInput) {
    unique_ptr<Buffer> buf;
    ASSERT_NO_THROW(buf = unitTestChip.allocateInput(allocSize, allocFdProps,
                                                     allocParamsMap));
    ASSERT_GE(buf->getFd(), 0);
    ASSERT_EQ(buf->getStartOffset(), 0);
    ASSERT_EQ(buf->getMaxSize(), allocSize);
    ASSERT_EQ(buf->getFdProps(), allocFdProps);
}

TEST_F(BackEndUnitTest, AllocateOutput) {
    unique_ptr<Buffer> buf;
    ASSERT_NO_THROW(buf = unitTestChip.allocateOutput(allocSize, allocFdProps,
                                                      allocParamsMap));
    ASSERT_GE(buf->getFd(), 0);
    ASSERT_EQ(buf->getStartOffset(), 0);
    ASSERT_EQ(buf->getMaxSize(), allocSize);
    ASSERT_EQ(buf->getFdProps(), allocFdProps);
}
