/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include "jobrequest.hh"
#include "msg.hh"

namespace larod {

/**
 * @brief Represents an order from a Session for running a job.
 *
 * This is associated with a Session. When the order is processed, the executor
 * (e.g. BackEndUnit) should call getJobRequest() to get an actual job request
 * from the Session to process.
 */
class JobOrder : public Msg {
public:
    JobOrder(std::weak_ptr<Session> session, const size_t chipId);

    /**
     * @brief Get the first job request in line.
     *
     * This will ask the @c Session, associated this message, for the first job
     * request in queue for the chip @c CHIP_ID.
     *
     * @return The first job request in line.
     */
    std::shared_ptr<JobRequest> getJobRequest();

    const size_t CHIP_ID;
};

} // namespace larod
