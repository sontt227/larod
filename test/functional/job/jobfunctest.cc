/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "jobfunctest.hh"

#include <cstdio>
#include <cstdlib>
#include <fcntl.h>
#include <gtest/gtest.h>
#include <iostream>
#include <stdexcept>

#include "testutils.hh"

using namespace std;
using namespace larod::test;

// Parameters from argparse.
string JobFuncTest::modelFile;
vector<string> JobFuncTest::inputFiles;
vector<string> JobFuncTest::verificationFiles;
bool JobFuncTest::debugPrints = false;
JobTestBufType JobFuncTest::inputBufType = BUF_TYPE_INVALID;
JobTestBufType JobFuncTest::outputBufType = BUF_TYPE_INVALID;
bool JobFuncTest::trackInputs = false;
bool JobFuncTest::trackOutputs = false;
bool JobFuncTest::forceMapInput = false;
bool JobFuncTest::forceMapOutput = false;
larodChip JobFuncTest::chip;
larodMap* JobFuncTest::modelParams = nullptr;
larodMap* JobFuncTest::jobParams = nullptr;

larodJobRequest* JobFuncTest::Connection::createJobReq() {
    larodError* error = nullptr;
    if (jobReq != nullptr) {
        larodDestroyJobRequest(&jobReq);
    }

    larodJobRequest* jobPtr =
        larodCreateJobRequest(modelPtr, inputTensors, numInputs, outputTensors,
                              numOutputs, jobParams, &error);
    if (!jobPtr) {
        throw runtime_error("Failed creating job request: " +
                            string(error->msg));
    }

    jobReq = jobPtr;
    return jobReq;
}

void JobFuncTest::Connection::disconnect() {
    larodDestroyTensors(&inputTensors, numInputs);
    larodDestroyTensors(&outputTensors, numOutputs);

    larodDestroyJobRequest(&jobReq);

    if (!larodDeleteModel(larodConn, modelPtr, nullptr)) {
        throw runtime_error("Failed deleting model!");
    }

    larodDestroyModel(&modelPtr);

    if (!larodDisconnect(&larodConn, nullptr)) {
        throw runtime_error("Failed closing larod connection!");
    }
}

JobFuncTest::JobFuncTest()
    : modelFd(-1), inputByteSizes(nullptr), outputByteSizes(nullptr) {
}

void JobFuncTest::eraseAllConnections() {
    for (auto& conn : larodConns) {
        conn->disconnect();
    }
    larodConns.clear();
}

void JobFuncTest::TearDown() {
    if (modelFd >= 0) {
        close(modelFd);
    }
    for (auto& fd : fdTracker) {
        close(fd);
    }
    // NOTE: DmaBuffer objects will be released when the dmaBufMap member is
    // destroyed.

    free(inputByteSizes);
    free(outputByteSizes);

    eraseAllConnections();

    // Call base GoogleTest Test::TearDown() for framework cleanup.
    Test::TearDown();
}

shared_ptr<JobFuncTest::Connection>
    JobFuncTest::addConnection(unsigned int chHandlerId, bool enableTracking) {
    larodConnection* conn = nullptr;
    larodError* error = nullptr;

    bool ret = larodConnect(&conn, &error);
    if (!ret || !conn) {
        throw runtime_error("Failed to open larod connection: " +
                            getErrMsgAndClear(&error));
    }

    // Get hold input model file if file name was supplied by the user on
    // command line.
    if (modelFile.size()) {
        modelFd = open(modelFile.c_str(), O_RDONLY);
        if (modelFd < 0) {
            throw runtime_error("Failed to open model file: " +
                                getErrMsgAndClear(&error));
        }
    } else if (chip == LAROD_CHIP_DEBUG) {
        const size_t dummyModelSize = 20; // Also debugchip need some data.
        modelFd = createTempFd(dummyModelSize, BUF_TYPE_EXT_DISKFD);
        fdTracker.push_back(modelFd);
    } else {
        // File descriptor is negative if no model file is specified.
        modelFd = -1;
    }

    larodModel* model =
        larodLoadModel(conn, modelFd, chip, LAROD_ACCESS_PRIVATE, "larod-model",
                       modelParams, &error);
    if (!model) {
        throw runtime_error("Failed loading model: " +
                            getErrMsgAndClear(&error));
    }

    // Load input files when first model is loaded. At this time we have access
    // to the model tensors, and we know that same model is opened every time.
    if (larodConns.empty()) {
        inputByteSizes = larodGetModelInputByteSizes(model, nullptr, nullptr);
        outputByteSizes = larodGetModelOutputByteSizes(model, nullptr, nullptr);

        if (!inputByteSizes || !outputByteSizes) {
            throw runtime_error("Failed getting tensor byte sizes");
        }
    }

    larodTensor** inputTensors = nullptr;
    larodTensor** outputTensors = nullptr;
    vector<int> inputFds;  // File descriptors corresponding to inputTensors.
    vector<int> outputFds; // File descriptors corresponding to outputTensors.

    // Input tensors.
    if ((inputBufType == BUF_TYPE_EXT_DMABUF) ||
        (inputBufType == BUF_TYPE_EXT_DISKFD)) {
        tie(inputTensors, inputFds) =
            initExternalInputTensors(conn, model, enableTracking);
    } else {
        tie(inputTensors, inputFds) = initLarodAllocInputTensors(conn, model);
    }

    // Output tensors.
    if ((outputBufType == BUF_TYPE_EXT_DMABUF) ||
        (outputBufType == BUF_TYPE_EXT_DISKFD)) {
        tie(outputTensors, outputFds) =
            initExternalOutputTensors(conn, model, enableTracking);
    } else {
        tie(outputTensors, outputFds) =
            initLarodAllocOutputTensors(conn, model);
    }

    // Adding larodClearError() mostly for the record: exceptions are thrown on
    // error thus error will always be NULL here...
    larodClearError(&error);

    // Read all verification files.
    for (const auto& fileName : verificationFiles) {
        int fd = open(fileName.c_str(), O_RDONLY);
        if (fd < 0) {
            throw runtime_error("Failed to open input file " + fileName);
        }
        verificationFileFds.push_back(fd);
        fdTracker.push_back(fd);
    }

    return larodConns.emplace_back(make_shared<JobFuncTest::Connection>(
        conn, model, inputTensors, outputTensors, std::move(inputFds),
        std::move(outputFds),
        make_shared<struct CallBackHandler>(chHandlerId)));
}

pair<larodTensor**, vector<int>> JobFuncTest::initExternalInputTensors(
    larodConnection* conn, larodModel* model, bool enableTracking) {
    size_t numInputs = 0;
    larodError* error = nullptr;

    larodTensor** inputTensors =
        larodCreateModelInputs(model, &numInputs, &error);
    if (inputTensors == NULL) {
        throw runtime_error("Failed fetching model inputs: " +
                            getErrMsgAndClear(&error));
    }

    vector<int> inputFds = initExtInputFileDescriptors(numInputs);

    for (size_t i = 0; i < numInputs; i++) {
        updateTensorInfo(conn, inputTensors[i], inputFds[i], inputByteSizes[i],
                         forceMapInput, (enableTracking && trackInputs));
    }

    return {inputTensors, std::move(inputFds)};
}

pair<larodTensor**, vector<int>> JobFuncTest::initExternalOutputTensors(
    larodConnection* conn, larodModel* model, bool enableTracking) {
    size_t numOutputs = 0;
    larodError* error = nullptr;

    larodTensor** outputTensors =
        larodCreateModelOutputs(model, &numOutputs, &error);
    if (outputTensors == NULL) {
        throw runtime_error("Failed fetching model outputs: " +
                            getErrMsgAndClear(&error));
    }

    // Create temp output files.
    vector<int> outputFds;
    for (size_t i = 0; i < numOutputs; i++) {
        int fd = createTempFd(outputByteSizes[i], outputBufType);
        outputFds.push_back(fd);
    }

    for (size_t i = 0; i < numOutputs; i++) {
        updateTensorInfo(conn, outputTensors[i], outputFds[i],
                         outputByteSizes[i], forceMapOutput,
                         (enableTracking && trackOutputs));
    }

    return {outputTensors, std::move(outputFds)};
}

pair<larodTensor**, vector<int>>
    JobFuncTest::initLarodAllocInputTensors(larodConnection* conn,
                                            larodModel* model) {
    size_t numInputs = 0;
    larodError* error = nullptr;
    larodMap* bufferAllocParams = nullptr;
    uint32_t fdPropFlags = LAROD_FD_PROP_READWRITE;

    if (forceMapInput) {
        fdPropFlags = LAROD_FD_PROP_MAP;
    }
    if (inputBufType == BUF_TYPE_LAROD_DMABUF) {
        fdPropFlags = LAROD_FD_PROP_DMABUF;
    }

    larodTensor** inputTensors = larodAllocModelInputs(
        conn, model, fdPropFlags, &numInputs, bufferAllocParams, &error);
    if (inputTensors == NULL) {
        throw runtime_error("Failed allocating model inputs: " +
                            getErrMsgAndClear(&error));
    }

    // Note: Buffers allocated by larod is automatically tracked.
    vector<int> inputFds =
        initLarodAllocInputFileDescriptors(numInputs, inputTensors);

    return {inputTensors, std::move(inputFds)};
}

pair<larodTensor**, vector<int>>
    JobFuncTest::initLarodAllocOutputTensors(larodConnection* conn,
                                             larodModel* model) {
    size_t numOutputs = 0;
    vector<int> outputFds;
    larodError* error = nullptr;
    larodMap* bufferAllocParams = nullptr;
    uint32_t fdPropFlags = LAROD_FD_PROP_READWRITE;

    if (forceMapOutput) {
        fdPropFlags = LAROD_FD_PROP_MAP;
    }
    if (outputBufType == BUF_TYPE_LAROD_DMABUF) {
        fdPropFlags = LAROD_FD_PROP_DMABUF;
    }

    larodTensor** outputTensors = larodAllocModelOutputs(
        conn, model, fdPropFlags, &numOutputs, bufferAllocParams, &error);
    if (outputTensors == NULL) {
        throw runtime_error("Failed allocating model outputs: " +
                            getErrMsgAndClear(&error));
    }

    for (size_t i = 0; i < numOutputs; i++) {
        int tensorFd = larodGetTensorFd(outputTensors[i], &error);
        if (tensorFd == LAROD_INVALID_FD) {
            throw runtime_error("Failed retrieving input tensor fd: " +
                                getErrMsgAndClear(&error));
        }
        outputFds.push_back(tensorFd);
    }

    return {outputTensors, std::move(outputFds)};
}

void JobFuncTest::gtestDebugPrint(string msg) {
    if (debugPrints) {
        gtestPrint(msg);
    }
}

void JobFuncTest::destroyDmaBuffer(DmaBuffer* buffer) {
    if (!buffer) {
        return;
    }

    if (!freeDmaBuffer(buffer)) {
        gtestPrint("Error in freeDmaBuffer()");
    }
}

int JobFuncTest::createTempFd(size_t size, JobTestBufType bufType) {
    int fd = -1;

    if (bufType == BUF_TYPE_EXT_DMABUF) {
        DmaBufPtr buffer(allocDmaBuffer(size, chip), destroyDmaBuffer);
        if (!buffer) {
            throw runtime_error("Could not allocate dma buffer");
        }
        fd = buffer->fd;
        auto [ignore, keyAdded] =
            dmaBufMap.try_emplace(buffer->fd, std::move(buffer));
        if (!keyAdded) {
            // Should never happen.
            throw runtime_error("Internal error: DmaBuffer fd value "
                                "already in the map");
        }
    } else if (bufType == BUF_TYPE_EXT_DISKFD) {
        fd = createTmpFile(size);
        fdTracker.push_back(fd);
    } else {
        // Should never happen.
        throw runtime_error("Internal error: Trying to create temp buffer with "
                            "larod-alloc type");
    }

    return fd;
}

vector<int> JobFuncTest::initExtInputFileDescriptors(size_t numInputs) {
    vector<int> inputFds;

    if (inputFiles.empty()) {
        // No input files provided by client. Create temp files or DmaBuffers
        // which are resized based on the tensor info.
        for (size_t i = 0; i < numInputs; i++) {
            int fd = createTempFd(inputByteSizes[i], inputBufType);
            inputFds.push_back(fd);
        }
    } else {
        // If user has provided input files then they must match the number of
        // input tensors.
        if (inputFiles.size() != numInputs) {
            throw runtime_error("User provided " +
                                to_string(inputFiles.size()) +
                                " files but model have " +
                                to_string(numInputs) + " input tensors");
        }

        for (string fileName : inputFiles) {
            size_t i = inputFds.size();
            if (inputBufType == BUF_TYPE_EXT_DMABUF) {
                DmaBufPtr buffer(allocDmaBufferFromFile(fileName.c_str(), chip),
                                 destroyDmaBuffer);
                if (!buffer) {
                    throw runtime_error("Could not copy file to dma buffer");
                }
                inputFds.push_back(buffer->fd);
                auto [ignore, keyAdded] =
                    dmaBufMap.try_emplace(buffer->fd, std::move(buffer));
                if (!keyAdded) {
                    // Should never happen.
                    throw runtime_error("Internal error: DmaBuffer fd value "
                                        "already in the map");
                }
            } else {
                // External 'disk-based' input fd.
                int fd = open(fileName.c_str(), O_RDONLY);
                if (fd < 0) {
                    throw runtime_error("Failed to open input file");
                }
                inputFds.push_back(fd);
                fdTracker.push_back(fd);

                // Figure out and update the input tensor sizes. The sizes we
                // get from larod can't be trusted when loading a pre-processing
                // model, since the tensor with dynamic parameters has size 0.
                off_t filePos = lseek(fd, 0, SEEK_END);
                if (filePos < 0) {
                    throw runtime_error("Failed to seek to end of input file");
                }
                if (i < numInputs) {
                    inputByteSizes[i] = (size_t) filePos;
                }
                if (lseek(fd, 0, SEEK_SET) != 0) {
                    throw runtime_error("Failed lseek() call");
                }
            }
        }
    }

    return inputFds;
}

vector<int>
    JobFuncTest::initLarodAllocInputFileDescriptors(size_t numInputs,
                                                    larodTensor** tensors) {
    vector<int> inputFds;
    larodError* error = nullptr;

    // If user has provided input files then they must match the number
    // of input tensors.
    if (!inputFiles.empty() && (inputFiles.size() != numInputs)) {
        throw runtime_error("User provided " + to_string(inputFiles.size()) +
                            " files but model have " + to_string(numInputs) +
                            " input tensors");
    }

    for (size_t i = 0; i < numInputs; i++) {
        int tensorFd = larodGetTensorFd(tensors[i], &error);
        if (tensorFd == LAROD_INVALID_FD) {
            throw runtime_error("Failed retrieving input tensor fd: " +
                                getErrMsgAndClear(&error));
        }
        inputFds.push_back(tensorFd);

        if (inputFiles.empty()) {
            continue;
        }
        string fileName = inputFiles[i];

        int diskFd = open(fileName.c_str(), O_RDONLY);
        if (diskFd < 0) {
            throw runtime_error("Failed to open input file " + fileName);
        }

        vector<uint8_t> fileBuffer = std::move(
            readFdToVector(diskFd, 0, AUTO_SIZE, BUF_TYPE_EXT_DISKFD));
        inputByteSizes[i] = fileBuffer.size();

        writeVectorToFd(fileBuffer, tensorFd, 0, inputBufType);

        close(diskFd);
    }

    return inputFds;
}

void JobFuncTest::updateTensorInfo(larodConnection* conn,
                                   larodTensor* const tensor, const int fd,
                                   const size_t modelReportedSize,
                                   bool forceMap, bool trackTensor) {
    larodError* error = nullptr;
    size_t fdSize = 0;
    uint32_t fdProps = 0;

    auto it = dmaBufMap.find(fd);
    DmaBuffer* dmaBuf = (it != dmaBufMap.end()) ? it->second.get() : nullptr;

    if (dmaBuf != nullptr) {
        // Restrict service to access as dmabuf even if dmabuf:s (almost?)
        // always support also FD_PROP_MAP. Restricting to one specific access
        // type makes e.g. mmap()-leak-testing much easier.
        fdSize = dmaBuf->size;
        fdProps = LAROD_FD_PROP_DMABUF;
    } else {
        // This is an ordinary "disk-type" fd that could be accessed through
        // read/write() or mmap(). We only set FD_PROP_READWRITE here to enforce
        // service using that access type. Client should use
        // force-map-input/force-map-output to use mmap() access type.
        fdSize = modelReportedSize;
        fdProps = LAROD_FD_PROP_READWRITE;
    }

    if (forceMap) {
        fdProps = LAROD_FD_PROP_MAP;
    }

    if (!larodSetTensorFd(tensor, fd, &error)) {
        throw runtime_error("Failed setting fd on tensor: " +
                            getErrMsgAndClear(&error));
    }

    if (!larodSetTensorFdSize(tensor, fdSize, &error)) {
        throw runtime_error("Failed setting fd on tensor: " +
                            getErrMsgAndClear(&error));
    }

    if (!larodSetTensorFdProps(tensor, fdProps, &error)) {
        throw std::runtime_error("Failed settings props on tensor: " +
                                 getErrMsgAndClear(&error));
    }
    if (trackTensor) {
        if (!larodTrackTensor(conn, tensor, &error)) {
            throw runtime_error("Could not track tensor: " +
                                getErrMsgAndClear(&error));
        }
    }
}

tuple<int, size_t> JobFuncTest::buildTempFdWithOffset(int inputFd, off_t offset,
                                                      JobTestBufType bufType) {
    assert(offset >= 0);

    int tempFdWithOffset = -1;
    size_t totalBytesWritten = 0;

    if (bufType == BUF_TYPE_EXT_DMABUF) {
        // We are dealing with external dmabuf!
        DmaBuffer* dmaBufSrc = nullptr;
        try {
            dmaBufSrc = dmaBufMap.at(inputFd).get();
        } catch (const out_of_range& oor) {
            throw runtime_error("Could not lookup dmabuffer/fd");
        }

        // We assume the cache to be synched here for dmaBufSrc, since this
        // is taken care of in utils lib and larod service.
        size_t newBufferSize = dmaBufSrc->size + static_cast<size_t>(offset);
        vector<uint8_t> dataVec(newBufferSize);
        copy(static_cast<uint8_t*>(dmaBufSrc->data),
             static_cast<uint8_t*>(dmaBufSrc->data) + dmaBufSrc->size,
             dataVec.data() + offset);

        DmaBufPtr bufferWithOffset(allocDmaBuffer(newBufferSize, chip),
                                   destroyDmaBuffer);
        if (!bufferWithOffset) {
            throw runtime_error("Could not allocate buffer with offset");
        }
        if (!copyDataToBuffer(dataVec.data(), newBufferSize,
                              bufferWithOffset.get())) {
            throw runtime_error("Could not copy data to dma buffer");
        }
        tempFdWithOffset = bufferWithOffset->fd;
        totalBytesWritten = dmaBufSrc->size;
        auto [ignore, keyAdded] = dmaBufMap.try_emplace(
            bufferWithOffset->fd, std::move(bufferWithOffset));
        if (!keyAdded) {
            // Should never happen.
            throw runtime_error("Internal error: DmaBuffer fd value "
                                "already in the map");
        }
    } else if (bufType == BUF_TYPE_EXT_DISKFD) {
        // Ordinary fd (e.g. from disk).
        tempFdWithOffset = createTempFd(static_cast<size_t>(offset), bufType);

        vector<uint8_t> fileContents = std::move(
            readFdToVector(inputFd, 0, AUTO_SIZE, BUF_TYPE_EXT_DISKFD));

        writeVectorToFd(fileContents, tempFdWithOffset, offset,
                        BUF_TYPE_EXT_DISKFD);

        totalBytesWritten = fileContents.size();
    } else {
        // Should never happen.
        throw runtime_error("Internal error: Creating temp fd based on"
                            "larod-allocation is not supported");
    }
    return {tempFdWithOffset, totalBytesWritten};
}

vector<uint8_t> JobFuncTest::readFdToVector(int fd, off_t offset, size_t size,
                                            JobTestBufType bufType) {
    vector<uint8_t> fdContents;

    if ((bufType == BUF_TYPE_EXT_DISKFD) ||
        (bufType == BUF_TYPE_LAROD_DISKFD)) {
        if (size == AUTO_SIZE) {
            struct stat buf;
            if (fstat(fd, &buf) != 0) {
                throw runtime_error("Failed to stat file");
            }
            size = static_cast<size_t>(buf.st_size);
            assert(offset >= 0);
            size -= static_cast<size_t>(offset);
        }

        if (lseek(fd, offset, SEEK_SET) != offset) {
            throw runtime_error("Failed lseek() call");
        }

        fdContents.resize(size);
        ssize_t readBytes = read(fd, fdContents.data(), size);
        if (readBytes < 0) {
            throw runtime_error("Failed read() call.");
        }

        if (static_cast<size_t>(readBytes) != size) {
            throw runtime_error("Failed to read complete file");
        }
    } else if (bufType == BUF_TYPE_EXT_DMABUF) {
        DmaBuffer* dmaBuf = nullptr;
        try {
            dmaBuf = dmaBufMap.at(fd).get();
        } catch (const out_of_range& oor) {
            throw runtime_error("Could not lookup dmabuffer/fd");
        }
        if (size == AUTO_SIZE) {
            size = dmaBuf->size;
        }
        fdContents.resize(size);
        // We assume the cache to be synched here for dmaBufSrc, since this
        // is taken care of in utils lib and larod service.
        memcpy(fdContents.data(), static_cast<uint8_t*>(dmaBuf->data) + offset,
               size);
    } else {
        if (offset != 0) {
            throw runtime_error("Offset != 0 not supported for larod-allocated "
                                "dmabufs since there will be no slack...");
        }
        if (size == AUTO_SIZE) {
            throw runtime_error("Size AUTO is not supported for reading "
                                "larod-allocated dmabufs");
        }

        fdContents.resize(size);
        if (!copyDataFromDmabuf(fd, fdContents.data(), size)) {
            throw runtime_error(
                "Failed reading contents from larod-alloc:ed dmabuf");
        }
    }

    return fdContents;
}

void JobFuncTest::writeVectorToFd(vector<uint8_t>& buffer, int fd,
                                  off_t fdOffset, JobTestBufType bufType) {
    switch (bufType) {
    case BUF_TYPE_EXT_DMABUF: {
        DmaBuffer* dmaBuffer = nullptr;
        try {
            dmaBuffer = dmaBufMap.at(fd).get();
        } catch (const out_of_range& oor) {
            throw runtime_error("Could not lookup destination dmabuffer");
        }
        if (!copyDataToBuffer(buffer.data(), buffer.size(), dmaBuffer)) {
            throw runtime_error("Failed writing to (external) dmabuffer");
        }
    } break;

    case BUF_TYPE_LAROD_DMABUF: {
        if (fdOffset != 0) {
            throw runtime_error("Writing buffer into larod-allocated dmabuf "
                                "does not support nonzero fdOffset");
        }
        if (!copyDataToDmabuf(buffer.data(), buffer.size(), fd)) {
            throw runtime_error(
                "Failed writing buffer to larod-allocated dmabuf");
        }
    } break;

    case BUF_TYPE_EXT_DISKFD:
    case BUF_TYPE_LAROD_DISKFD: {
        if (lseek(fd, fdOffset, SEEK_SET) != fdOffset) {
            throw runtime_error("Failed lseek() call");
        }

        size_t bytesWritten = 0;
        uint8_t* buf = buffer.data();
        while (bytesWritten < buffer.size()) {
            ssize_t writeRet =
                write(fd, buf + bytesWritten,
                      static_cast<size_t>(buffer.size() - bytesWritten));
            if (writeRet < 0) {
                throw runtime_error(string("write() call failed: ") +
                                    strerror(errno));
            }
            bytesWritten += static_cast<size_t>(writeRet);
        }
    } break;

    default:
        // Should never happen.
        throw runtime_error("Internal error: Unknown bufType");
    }
}
