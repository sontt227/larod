/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <gtest/gtest.h>

#include "larod.h"
#include "tensorattributes.hh"
#include "testmacros.h"

using namespace std;

TEST_F(TensorAttributes, SetAndGetDimensionAreEqual) {
    // Set dimension.
    const larodTensorDims setDims = {{1, 2, 3, 4}, 4};
    bool ret = larodSetTensorDims(tensors[0], &setDims, &error);
    ASSERT_TRUE(ret);
    EXPECT_EQ(error, nullptr);

    // Get dimension.
    const larodTensorDims* getDims = larodGetTensorDims(tensors[0], &error);
    ASSERT_NE(getDims, nullptr);
    EXPECT_EQ(error, nullptr);
    ASSERT_EQ(getDims->len, setDims.len);
    ASSERT_TRUE(equal(setDims.dims, setDims.dims + setDims.len, getDims->dims));
}

TEST_F(TensorAttributes, SetInvalidDims) {
    bool ret = larodSetTensorDims(tensors[0], nullptr, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetInvalidDimsLen) {
    const larodTensorDims setDims = {{0}, LAROD_TENSOR_MAX_LEN + 1};
    bool ret = larodSetTensorDims(tensors[0], &setDims, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetDimsOnInvalidTensor) {
    const larodTensorDims setDims = {};
    bool ret = larodSetTensorDims(nullptr, &setDims, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, GetDimsFromInvalidTensor) {
    const larodTensorDims* getDims = larodGetTensorDims(nullptr, &error);
    EXPECT_EQ(getDims, nullptr);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetAndGetPitchesAreEqual) {
    // Set pitches.
    const larodTensorPitches setPitches = {{1, 2, 3, 4}, 4};
    bool ret = larodSetTensorPitches(tensors[0], &setPitches, &error);
    ASSERT_TRUE(ret);
    EXPECT_EQ(error, nullptr);

    // Get pitches.
    const larodTensorPitches* getPitches =
        larodGetTensorPitches(tensors[0], &error);
    ASSERT_NE(getPitches, nullptr);
    EXPECT_EQ(error, nullptr);
    ASSERT_EQ(getPitches->len, setPitches.len);
    ASSERT_TRUE(equal(setPitches.pitches, setPitches.pitches + setPitches.len,
                      getPitches->pitches));
}

TEST_F(TensorAttributes, SetInvalidPitches) {
    bool ret = larodSetTensorPitches(tensors[0], nullptr, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetInvalidPitchesLen) {
    const larodTensorPitches setPitches = {{0}, LAROD_TENSOR_MAX_LEN + 1};
    bool ret = larodSetTensorPitches(tensors[0], &setPitches, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetPitchesOnInvalidTensor) {
    const larodTensorPitches setPitches = {};
    bool ret = larodSetTensorPitches(nullptr, &setPitches, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, GetPitchesFromInvalidTensor) {
    const larodTensorPitches* getPitches =
        larodGetTensorPitches(nullptr, &error);
    EXPECT_EQ(getPitches, nullptr);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetAndGetDataTypeAreEqual) {
    // Set data type.
    const larodTensorDataType setDataType = LAROD_TENSOR_DATA_TYPE_INT64;
    bool ret = larodSetTensorDataType(tensors[0], setDataType, &error);
    ASSERT_TRUE(ret);
    EXPECT_EQ(error, nullptr);

    // Get data type.
    const larodTensorDataType getDataType =
        larodGetTensorDataType(tensors[0], &error);
    EXPECT_EQ(setDataType, getDataType);
    EXPECT_EQ(error, nullptr);
}

TEST_F(TensorAttributes, SetInvalidDataType) {
    bool ret = larodSetTensorDataType(tensors[0],
                                      LAROD_TENSOR_DATA_TYPE_INVALID, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetDataTypeOnInvalidTensor) {
    bool ret =
        larodSetTensorDataType(nullptr, LAROD_TENSOR_DATA_TYPE_INT64, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, GetDataTypeFromInvalidTensor) {
    const larodTensorDataType getDataType =
        larodGetTensorDataType(nullptr, &error);
    EXPECT_EQ(getDataType, LAROD_TENSOR_DATA_TYPE_INVALID);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetAndGetLayoutAreEqual) {
    // Set layout.
    const larodTensorLayout setLayout = LAROD_TENSOR_LAYOUT_NHWC;
    bool ret = larodSetTensorLayout(tensors[0], setLayout, &error);
    ASSERT_TRUE(ret);
    EXPECT_EQ(error, nullptr);

    // Get layout.
    const larodTensorLayout getLayout =
        larodGetTensorLayout(tensors[0], &error);
    EXPECT_EQ(setLayout, getLayout);
    EXPECT_EQ(error, nullptr);
}

TEST_F(TensorAttributes, SetInvalidLayout) {
    bool ret =
        larodSetTensorLayout(tensors[0], LAROD_TENSOR_LAYOUT_INVALID, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetLayoutOnInvalidTensor) {
    bool ret =
        larodSetTensorLayout(nullptr, LAROD_TENSOR_LAYOUT_UNSPECIFIED, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, GetLayoutFromInvalidTensor) {
    const larodTensorLayout getLayout = larodGetTensorLayout(nullptr, &error);
    EXPECT_EQ(getLayout, LAROD_TENSOR_LAYOUT_INVALID);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetAndGetFdAreEqual) {
    // Set file descriptor.
    const int setFd = 2;
    bool ret = larodSetTensorFd(tensors[0], setFd, &error);
    ASSERT_TRUE(ret);
    EXPECT_EQ(error, nullptr);

    // Get file descriptor.
    const int getFd = larodGetTensorFd(tensors[0], &error);
    EXPECT_EQ(setFd, getFd);
    EXPECT_EQ(error, nullptr);
}

TEST_F(TensorAttributes, SetInvalidFd) {
    bool ret = larodSetTensorFd(tensors[0], -2, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetFdOnInvalidTensor) {
    bool ret = larodSetTensorFd(nullptr, 2, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, GetFdFromInvalidTensor) {
    const int getFd = larodGetTensorFd(nullptr, &error);
    EXPECT_EQ(getFd, LAROD_INVALID_FD);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetAndGetFdSizeAreEqual) {
    // Set file descriptor size.
    const size_t setFdSize = 2;
    bool ret = larodSetTensorFdSize(tensors[0], setFdSize, &error);
    ASSERT_TRUE(ret);
    EXPECT_EQ(error, nullptr);

    // Get file descriptor size.
    size_t getFdSize;
    ret = larodGetTensorFdSize(tensors[0], &getFdSize, &error);
    EXPECT_TRUE(ret);
    EXPECT_EQ(setFdSize, getFdSize);
    EXPECT_EQ(error, nullptr);
}

TEST_F(TensorAttributes, SetZeroFdSize) {
    bool ret = larodSetTensorFdSize(tensors[0], 0, &error);
    EXPECT_TRUE(ret);
    EXPECT_EQ(error, nullptr);
}

TEST_F(TensorAttributes, SetFdSizeOnInvalidTensor) {
    bool ret = larodSetTensorFdSize(nullptr, 2, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, GetFdSizeFromInvalidTensor) {
    size_t getFdSize;
    bool ret = larodGetTensorFdSize(nullptr, &getFdSize, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, GetFdSizeWithInvalidSizePtr) {
    bool ret = larodGetTensorFdSize(tensors[0], nullptr, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetAndGetFdOffsetAreEqual) {
    // Set file descriptor offset.
    const int64_t setFdOffset = 2;
    bool ret = larodSetTensorFdOffset(tensors[0], setFdOffset, &error);
    ASSERT_TRUE(ret);
    EXPECT_EQ(error, nullptr);

    // Get file descriptor offset.
    const int64_t getFd = larodGetTensorFdOffset(tensors[0], &error);
    EXPECT_EQ(setFdOffset, getFd);
    EXPECT_EQ(error, nullptr);
}

TEST_F(TensorAttributes, SetInvalidFdOffset) {
    bool ret = larodSetTensorFdOffset(tensors[0], -2, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, SetFdOffsetOnInvalidTensor) {
    bool ret = larodSetTensorFdOffset(nullptr, 2, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
}

TEST_F(TensorAttributes, GetFdOffsetFromInvalidTensor) {
    const int64_t getFdOffset = larodGetTensorFdOffset(nullptr, &error);
    EXPECT_LT(getFdOffset, 0);
    ASSERT_LAROD_ERROR(error);
}
