Architecture
============

larod is intended to run as a `systemd` service and to be interfaced through a
`C` [library](../lib/larod.h) (from now on _larod_ will refer to the service, if
not stated otherwise). A program that uses this library, to connect to larod,
will be referred to as a _client_. larod supports multiple clients at the same
time.

Each client can load neural networks or other processing models (we will refer
to these as just _model_ in the future), run jobs and more to a specific chip
designed for accelerating these operations (including CPU's or on-chip IP
blocks). If multiple chips are present, clients can choose between these using
the API provided by the library.

Below we will describe different parts of larod in more detail. This can
therefore get technical and can be skipped if you are not intending to work
directly with the source code of larod.

## Service

When clients connects to larod, using the library, an initial handshake is
initiated over the D-Bus protocol with the _`Service`_. The `Service` manages
the back end chips available for accelerating computation. It also manages
_`Session`s_, which we will explain further in the next section.

Moreover, it exposes a D-Bus interface for listing available chips and creating
`Session`s. However, the library should be used to connect to the `Service` (it
has some more functionality than just calling the `Service`'s D-Bus interface).

### Session

After the initial handshake upon connecting to larod, the `Service` object
creates an unique _`Session`_. A `Session` is an object which has exactly one
client associated to it. If a client disconnects from larod, the associated
`Session` gets destroyed as well (and all data specific to just that `Session`).

Using `Session`s, clients can run various _commands_. Examples of commands are
loading and deleting models, running jobs and more. `Session`s can use all chips
provided by `Service` during the construction of `Session` and a chip is
represented in larod as a _`BackEndUnit`_ object.

### BackEndUnit

A _`BackEndUnit`_ is an abstract class representing a general chip for
accelerating computation of a model. It declares some abstract functions for the
unit such as loading and deleting a model, running job, and more. A concrete
class representing an actual computational unit will thus sub-class
`BackEndUnit` and override the previously stated abstract functions.

`BackEndUnit` also provides asynchronous loading of models and running of jobs.
It does so by queuing these requests and letting the sub-class process these
queues in its implementation-defined way. Furthermore, it maintains a list of
`Model`s loaded to it and their respective creators (models can be private or
public, meaning that only the creator can modify it or anyone, respectively.)

### The big picture

The figure below summarizes the different parts in larod and their relation to
each other. Note that `--<>` in the figure represents so called _aggregation_ in
[UML class diagrams][1], i.e. "has a" relationship. While `--<x>` represents a
_composition_ relationship and `A` symbolizes an empty triangle, i.e.
inheritance ("is a" relationship).

```
          +--------+         +----------+
          |        |         |          |
          | Client |-------<>| liblarod |<--------------------+
          |        |         |          |                     |
          +--------+         +----------+                     |
                                   ^                          |
                                   |                          |
                                   | D-Bus                    |
                                   |                          | D-Bus
                                   V                          |
                             +-----------+                    |
                             |           |                    |
                     +-------+  Service  +------+             |
                     |       |           |      |             |
                     |       +-----------+      |             |
                     |                          |             V
                     |                          |        +---------+
                     |                          |        |         |
                     |                          +-----<x>| Session |
                     |                                   |         |
                     |                                   +----+----+
                     |      +-------------+                   |
                     |      |             |                   |
                     +---<x>| BackEndUnit |<>-----------------+
                            |             |
                            +-------------+
                               A   A   A
                               |   |   |
                  +------------+   |   +------------+
                  |                |                |
                  |                |                |
                  |                |                |
              +---+----+       +---+----+       +---+----+
              |        |       |        |       |        |
              | Chip X |       | Chip Y |       | Chip Z |
              |        |       |        |       |        |
              +--------+       +--------+       +--------+
```

Beginning from the upper left box, a client communicates with the `Service` and
its `Session` through the library. The client can request a list of available
chips (`BackEndUnit`s), the number of active `Session`s and such from the
`Service`. Clients can also call commands specified in `Session` through the
library. For example loading models, running jobs and more.

The `Service` has a list of `BackEndUnit`s that it creates and manages. The
exact types of `BackEndUnit`s are decided at compile time and corresponds to
what is actual available on the hardware. Moreover, it manages the `Session`s
for all connected clients. Whenever a client disconnects, `Service` makes sure
the corresponding `Session` destructs as well.

`Session` has references to the `BackEndUnit`s available. Note however that it
is the `Service` that is responsible for the `BackEndUnit`s lifetime, i.e.
several `Session`s can use the same chip in and it is not deallocated when the
last `Session` having a reference to it destructs.

Since several `Session`s can use the same `BackEndUnit`, all relevant
synchronization happens in the `BackEndUnit` class. Chip `X`, `Y` and `Z` are
(made-up) examples of real chips that may sub-class `BackEndUnit`. The actual
functionality of loading models and running jobs resides in these respective
classes.

[1]: https://en.wikipedia.org/wiki/Class_diagram
