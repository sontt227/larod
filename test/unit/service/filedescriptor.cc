/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "filedescriptor.hh"

#include <gtest/gtest.h>
#include <sys/mman.h>

#include "syscallmock.hh"

using namespace std;
using namespace larod;
using namespace testing;

class FileDescriptorTest : public Test {
protected:
    test::SyscallMock syscallMocker;
    size_t fd0Sz = 0;
    size_t fd1Sz = 1;
    int fd0 = 0;
    int fd1 = 1;
};

TEST_F(FileDescriptorTest, ConstructorValidFd) {
    EXPECT_CALL(syscallMocker, close(fd0));

    ASSERT_NO_THROW(FileDescriptor{std::move(fd0)});
}

TEST_F(FileDescriptorTest, ConstructorNegativeFd) {
    ASSERT_THROW(FileDescriptor(-1), invalid_argument);
}

TEST_F(FileDescriptorTest, CopyConstructor) {
    EXPECT_CALL(syscallMocker, close(fd0));

    FileDescriptor rhs = FileDescriptor(std::move(fd0));
    ASSERT_GE(rhs, 0);

    EXPECT_CALL(syscallMocker, dup(fd0)).WillOnce(Return(fd1));
    EXPECT_CALL(syscallMocker, close(fd1));

    FileDescriptor lhs(rhs);
    EXPECT_NE(lhs, rhs);
    EXPECT_GE(lhs, 0);
    EXPECT_GE(rhs, 0);
}

TEST_F(FileDescriptorTest, MoveConstructor) {
    EXPECT_CALL(syscallMocker, close(fd0));

    FileDescriptor rhs = FileDescriptor(std::move(fd0));
    ASSERT_GE(rhs, 0);

    FileDescriptor lhs(std::move(rhs));
    EXPECT_LT(rhs, 0);
    EXPECT_GE(lhs, 0);
}

TEST_F(FileDescriptorTest, CopyAssignmentOperator) {
    EXPECT_CALL(syscallMocker, close(fd0));

    FileDescriptor rhs = FileDescriptor(std::move(fd0));
    ASSERT_GE(rhs, 0);

    EXPECT_CALL(syscallMocker, dup(fd0)).WillOnce(Return(fd1));
    EXPECT_CALL(syscallMocker, close(fd1));

    FileDescriptor lhs = rhs;
    EXPECT_NE(lhs, rhs);
    EXPECT_GE(lhs, 0);
    EXPECT_GE(rhs, 0);
}

TEST_F(FileDescriptorTest, MoveAssignmentOperator) {
    EXPECT_CALL(syscallMocker, close(fd0));

    FileDescriptor rhs = FileDescriptor(std::move(fd0));
    ASSERT_GE(rhs, 0);

    FileDescriptor lhs = std::move(rhs);
    EXPECT_NE(lhs, rhs);
    EXPECT_GE(lhs, 0);
    EXPECT_LT(rhs, 0);
}

TEST_F(FileDescriptorTest, CloseSetsNegativeFd) {
    EXPECT_CALL(syscallMocker, close(fd0));

    FileDescriptor fd = FileDescriptor(std::move(fd0));
    fd.close();
    ASSERT_LT(fd, 0);
}

TEST_F(FileDescriptorTest, Read0) {
    EXPECT_CALL(syscallMocker, close(fd0));

    FileDescriptor fd = FileDescriptor(std::move(fd0));
    vector<uint8_t> buf(fd0Sz);
    size_t readBytes = fd.read(buf, buf.size());
    ASSERT_EQ(readBytes, buf.size());
}

TEST_F(FileDescriptorTest, Read1) {
    vector<uint8_t> buf(fd1Sz);

    EXPECT_CALL(syscallMocker, close(fd1));
    EXPECT_CALL(syscallMocker, read(fd1, buf.data(), fd1Sz))
        .WillOnce(Return(fd1Sz));

    FileDescriptor fd = FileDescriptor(std::move(fd1));
    size_t readBytes = fd.read(buf, buf.size());
    ASSERT_EQ(readBytes, buf.size());
}

TEST_F(FileDescriptorTest, ReadTooSmallBuffer) {
    EXPECT_CALL(syscallMocker, close(fd0));

    FileDescriptor fd = FileDescriptor(std::move(fd0));
    vector<uint8_t> buf(fd0Sz);
    ASSERT_THROW(fd.read(buf, buf.size() + 1), invalid_argument);
}

TEST_F(FileDescriptorTest, Write0) {
    EXPECT_CALL(syscallMocker, close(fd0));

    FileDescriptor fd = FileDescriptor(std::move(fd0));
    vector<uint8_t> buf(fd0Sz);
    size_t writtenBytes = fd.write(buf, buf.size());
    ASSERT_EQ(writtenBytes, buf.size());
}

TEST_F(FileDescriptorTest, Write1) {
    vector<uint8_t> buf(fd1Sz);

    EXPECT_CALL(syscallMocker, close(fd1));
    EXPECT_CALL(syscallMocker, write(fd1, buf.data(), fd1Sz))
        .WillOnce(Return(fd1Sz));

    FileDescriptor fd = FileDescriptor(std::move(fd1));
    size_t writtenBytes = fd.write(buf, buf.size());
    ASSERT_EQ(writtenBytes, buf.size());
}

TEST_F(FileDescriptorTest, WriteTooSmallBuffer) {
    EXPECT_CALL(syscallMocker, close(fd0));

    FileDescriptor fd = FileDescriptor(std::move(fd0));
    vector<uint8_t> buf(fd0Sz);
    ASSERT_THROW(fd.write(buf, buf.size() + 1), invalid_argument);
}

TEST_F(FileDescriptorTest, Mmap) {
    EXPECT_CALL(syscallMocker, close(fd1));
    EXPECT_CALL(syscallMocker,
                mmap(NULL, fd1Sz, PROT_READ | PROT_WRITE, MAP_SHARED, fd1, 0));

    FileDescriptor fileDesc(std::move(fd1));
    shared_ptr<uint8_t[]> mapPtr =
        fileDesc.map(0, fd1Sz, PROT_READ | PROT_WRITE);

    EXPECT_CALL(syscallMocker, munmap(mapPtr.get(), fd1Sz));
}

TEST_F(FileDescriptorTest, MMapWithOffset) {
    int fd2 = 2;
    size_t fd2Sz = 2;
    off_t offset = 1;

    EXPECT_CALL(syscallMocker, close(fd2));
    EXPECT_CALL(syscallMocker, mmap(NULL, fd2Sz, PROT_READ | PROT_WRITE,
                                    MAP_SHARED, fd2, offset));

    FileDescriptor fd(std::move(fd2));
    shared_ptr<uint8_t[]> mapPtr =
        fd.map(offset, fd2Sz, PROT_READ | PROT_WRITE);

    EXPECT_CALL(syscallMocker, munmap(mapPtr.get(), fd2Sz));
}
