/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <algorithm>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <deque>
#include <fcntl.h>
#include <fstream>
#include <functional>
#include <gtest/gtest.h>
#include <mutex>
#include <random>
#include <stdexcept>
#include <thread>

#include "argparse.h"
#include "callbackhandler.hh"
#include "jobfunctest.hh"
#include "larod.h"
#include "testmacros.h"
#include "testutils.hh"
#include "utils.h"

using namespace std;
using namespace std::chrono;
using namespace larod::test;

// Decides how much bytes of the output from a job may differ from a precomputed
// "correct" output. Needed since different HW produces slightly different
// output (but are very similar).
#define MAX_ALLOWED_DIFF 15

/**
 * @brief Empty Larod job callback.
 *
 * Empty callback for Larod async jobs to be used with the 'warm-up' jobs.
 */
void jobCallbackNoOp(void*, larodError*) {
}

/**
 * @brief Larod job callback with book-keeping.
 *
 * Larod async job callback that collects info about the job that was
 * finished by calling into the associated CallBackHandler.
 */
void jobCallbackCount(void* userData, larodError* error) {
    CallBackHandler::CallbackItem* cbi =
        static_cast<CallBackHandler::CallbackItem*>(userData);

    if (error) {
        cbi->cbh->callBackReceived(cbi->prio, cbi->sequenceNumber, error->code);
    } else {
        cbi->cbh->callBackReceived(cbi->prio, cbi->sequenceNumber,
                                   LAROD_ERROR_NONE);
    }

    delete cbi;
}

/**
 * @brief Get byte size of @c larodTensorDataType.
 *
 * @param dataType Data type of a tensor.
 * @param size Reference to the variable where the result will be stored.
 * @return False if any errors occur; otherwise true.
 */
bool getDataTypeSize(const larodTensorDataType& dataType, size_t& size) {
    switch (dataType) {
    case LAROD_TENSOR_DATA_TYPE_BOOL:
        size = sizeof(bool);
        return true;
    case LAROD_TENSOR_DATA_TYPE_UINT8:
        size = sizeof(uint8_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_INT8:
        size = sizeof(int8_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_UINT16:
        size = sizeof(uint16_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_INT16:
        size = sizeof(int16_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_UINT32:
        size = sizeof(uint32_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_INT32:
        size = sizeof(int32_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_UINT64:
        size = sizeof(uint64_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_INT64:
        size = sizeof(int64_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_FLOAT16:
        // TfLiteFloat16 uses a struct containing a uint16_t. That could be
        // padded though...
        size = sizeof(uint16_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_FLOAT32:
        size = sizeof(float);
        return true;
    case LAROD_TENSOR_DATA_TYPE_FLOAT64:
        size = sizeof(double);
        return true;
    default:
        return false;
    }
}

/**
 * @brief: Retrieves unpadded data.
 *
 * Fills @p unpaddedData with the data in @p data, without the elements that
 * correspond to padding.
 *
 * This function recursively calls itself until it reaches the last dimension;
 * where it inserts a row of elements. The result is a nest of loops (one for
 * each dimension). The @p dim denotes the dimension level and @p pos denotes
 * the "element position"; these should be set to 0 in the first call of
 * getUnpaddedData() to retrieve all of the data.
 */
void getUnpaddedData(vector<uint8_t>& unpaddedData, const vector<uint8_t>& data,
                     const vector<size_t>& dims, const vector<size_t>& pitches,
                     size_t dataTypeSize, size_t dim, size_t pos) {
    assert(dim <= dims.size() - 1);
    for (size_t i = 0; i < dims[dim]; ++i) {
        if (dim == dims.size() - 1) {
            assert(pos + (i + 1) * dataTypeSize <= data.size());
            unpaddedData.insert(
                unpaddedData.end(),
                data.begin() + static_cast<ptrdiff_t>(pos + i * dataTypeSize),
                data.begin() +
                    static_cast<ptrdiff_t>(pos + (i + 1) * dataTypeSize));
        } else {
            getUnpaddedData(unpaddedData, data, dims, pitches, dataTypeSize,
                            dim + 1, pos + i * pitches[dim + 1]);
        }
    }
}

/**
 * @brief: Removes any padding from a tensor data vector.
 *
 * Removes padding from @p tensorData and also checks that the attributes of the
 * tensor are consistent with one another. The @p tensor must have specified
 * data type, dims and pitches.
 */
void removeAnyPadding(vector<uint8_t>& tensorData, const larodTensor* tensor,
                      larodError* error) {
    const larodTensorDims* dimsPtr = larodGetTensorDims(tensor, &error);
    ASSERT_TRUE(checkLarodResultWithPrint((dimsPtr != nullptr), &error));
    const larodTensorPitches* pitchesPtr =
        larodGetTensorPitches(tensor, &error);
    ASSERT_TRUE(checkLarodResultWithPrint((pitchesPtr != nullptr), &error));
    ASSERT_NE(dimsPtr, nullptr);
    ASSERT_NE(pitchesPtr, nullptr);
    ASSERT_GT(dimsPtr->len, 0);
    ASSERT_EQ(dimsPtr->len, pitchesPtr->len);

    // Creating std::vector pitches/dims.
    size_t dimensions = dimsPtr->len;
    vector<size_t> pitches(pitchesPtr->pitches,
                           pitchesPtr->pitches + dimensions);
    ASSERT_EQ(pitches[0], tensorData.size());
    vector<size_t> dims(dimsPtr->dims, dimsPtr->dims + dimensions);

    // Retrieve data type byte size of tensor.
    larodTensorDataType dataType = larodGetTensorDataType(tensor, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(
        (dataType != LAROD_TENSOR_DATA_TYPE_INVALID), &error));
    size_t typeSize = 0;
    bool ret = getDataTypeSize(dataType, typeSize);
    ASSERT_TRUE(ret);

    // Calculating byte size of unpadded tensor.
    size_t num_elements = accumulate(
        dims.begin(), dims.end(), static_cast<size_t>(1), multiplies<size_t>());
    ASSERT_NE(num_elements, 0);
    size_t unpaddedSize = num_elements * typeSize;

    // Fill vector with unpadded data.
    vector<uint8_t> data;
    getUnpaddedData(data, tensorData, dims, pitches, typeSize, 0, 0);

    ASSERT_EQ(data.size(), unpaddedSize);

    // Substitute tensorData with the unpadded data.
    tensorData = std::move(data);
}

void verifyTensorData(vector<uint8_t>& tensorData,
                      vector<uint8_t>& tensorRefData, const larodTensor* tensor,
                      larodError* error) {
    larodTensorLayout layout = larodGetTensorLayout(tensor, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(
        (layout != LAROD_TENSOR_LAYOUT_INVALID), &error));

    if (layout == LAROD_TENSOR_LAYOUT_420SP) {
        // Retrieve data type byte size of tensor.
        larodTensorDataType dataType = larodGetTensorDataType(tensor, &error);
        size_t typeSize = 0;
        bool ret = getDataTypeSize(dataType, typeSize);
        ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

        // Get dims and pitches
        const larodTensorDims* dimsPtr = larodGetTensorDims(tensor, &error);
        ASSERT_TRUE(checkLarodResultWithPrint((dimsPtr != nullptr), &error));
        ASSERT_GT(dimsPtr->len, 0);
        const larodTensorPitches* pitchesPtr =
            larodGetTensorPitches(tensor, &error);
        ASSERT_TRUE(checkLarodResultWithPrint((pitchesPtr != nullptr), &error));
        ASSERT_EQ(pitchesPtr->len, dimsPtr->len);

        // Calculate width, height, pitch.
        size_t bufferWidth = dimsPtr->dims[2] * typeSize;
        size_t bufferHeight = 3 * dimsPtr->dims[1] / 2;
        size_t pitch = pitchesPtr->pitches[2];

        for (size_t i = 0; i < bufferHeight; i++) {
            for (size_t j = 0; j < bufferWidth; j++) {
                ASSERT_LE(abs(tensorData[i * pitch + j] -
                              tensorRefData[i * pitch + j]),
                          MAX_ALLOWED_DIFF);
            }
        }
    } else {
        removeAnyPadding(tensorData, tensor, error);
        removeAnyPadding(tensorRefData, tensor, error);

        ASSERT_EQ(tensorData.size(), tensorRefData.size());

        for (size_t j = 0; j < tensorRefData.size(); j++) {
            ASSERT_LE(abs(tensorData[j] - tensorRefData[j]), MAX_ALLOWED_DIFF);
        }
    }
}

// Verify basic functionality of job related API calls
TEST_F(JobFuncTest, JobRequest) {
    larodError* error = nullptr;

    shared_ptr<Connection> c = addConnection(0);
    larodConnection* conn = c->larodConn;

    larodJobRequest* jobReq = c->createJobReq();

    bool ret = larodRunJob(conn, jobReq, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    size_t numOutputs = 0;
    larodTensor** outputTensors =
        larodCreateModelOutputs(c->modelPtr, &numOutputs, &error);
    ASSERT_TRUE(checkLarodResultWithPrint((outputTensors != nullptr), &error));
    ASSERT_GE(numOutputs, 1);

    if (!verificationFiles.empty()) {
        ASSERT_EQ(numOutputs, verificationFiles.size());
    }

    size_t* outputByteSizes =
        larodGetModelOutputByteSizes(c->modelPtr, nullptr, &error);
    ASSERT_TRUE(
        checkLarodResultWithPrint((outputByteSizes != nullptr), &error));

    // Verify that expected number of bytes was written to each output file.
    for (size_t i = 0; i < numOutputs; i++) {
        if ((outputBufType == BUF_TYPE_EXT_DISKFD) ||
            (outputBufType == BUF_TYPE_LAROD_DISKFD)) {
            struct stat stat_buf;
            ASSERT_EQ(fstat(c->outputFileFds[i], &stat_buf), 0);
            ASSERT_EQ(stat_buf.st_size, outputByteSizes[i]);
        }

        if (!verificationFileFds.empty()) {
            vector<uint8_t> verificationVector = readFdToVector(
                verificationFileFds[i], 0, AUTO_SIZE, BUF_TYPE_EXT_DISKFD);
            ASSERT_GT(verificationVector.size(), 0);

            size_t bufferSize = AUTO_SIZE;
            // At least Ambarella dmabufs does not report actual size as result
            // of lseek(SEEk_END) so we just use the verification data size.
            if (outputBufType == BUF_TYPE_LAROD_DMABUF) {
                bufferSize = verificationVector.size();
            }

            vector<uint8_t> outputVector = readFdToVector(
                c->outputFileFds[i], 0, bufferSize, outputBufType);
            ASSERT_GT(outputVector.size(), 0);

            ASSERT_EQ(outputVector.size(), verificationVector.size());

            verifyTensorData(outputVector, verificationVector, outputTensors[i],
                             error);
        }
    }

    larodDestroyTensors(&outputTensors, numOutputs);
    free(outputByteSizes);
}

/**
 * @brief Test to verify Larod async priorities.
 *
 * This test case verifies that queued job requests are processed and reported
 * back in an order that satisfies larod's priority guarantees, namely that a
 * high priority request may only be ordered past lower priority requests. In
 * other words, high priority requests may never be reordered _after_ lower
 * priority requests.
 *
 * 1. A set of Larod connections are created.
 * 2. X number of low priority job requests are made,
 *    followed by 1 high priority request (per larodConnection),
 *    followed by another X low priority requests.
 * 3. A number of 'dummy' warm-up jobs are queued to add some items in
 *    Larod service queue.
 * 4. Items created in step 2 are queued in random order on the Larod
 *    connections. The only requests that are not randomized are the high
 *    priority ones, since we want to verify that they are not moved down the
 *    list.
 * 5. The test case blocks until all job callbacks are received.
 * 6. The priority and sequence ordering for all the Larod connections are
 *    verified.
 */
TEST_F(JobFuncTest, AsyncPrioMulti) {
    // Too many concurrent async jobs will max out larod's fd pool.
    const unsigned int NUM_CONNECTIONS = 3;
    const unsigned int NUM_JOBS_PER_CONNECTION = 10;

    const uint8_t LOW_PRIO = 1;
    const uint8_t HIGH_PRIO = 99;

    bool ret;
    larodError* error = nullptr;

    vector<pair<unsigned int, uint8_t>> jobs;

    for (unsigned int i = 0; i < NUM_CONNECTIONS; ++i) {
        addConnection(i);
    }

    // Add low prio jobs.
    for (unsigned int i = 0; i < NUM_CONNECTIONS; ++i) {
        for (unsigned int j = 0; j < NUM_JOBS_PER_CONNECTION; ++j) {
            jobs.emplace_back(i, LOW_PRIO);
        }
    }

    // Add high prio jobs.
    for (unsigned int i = 0; i < NUM_CONNECTIONS; ++i) {
        jobs.emplace_back(i, HIGH_PRIO);
    }

    // Add more low prio jobs.
    for (unsigned int i = 0; i < NUM_CONNECTIONS; ++i) {
        for (unsigned int j = 0; j < NUM_JOBS_PER_CONNECTION; ++j) {
            jobs.emplace_back(i, LOW_PRIO);
        }
    }

    gtestDebugPrint("Issuing 'warmup jobs'...");
    for (auto conn : larodConns) {
        const unsigned int WARMUP_JOBS = 3;
        conn->createJobReq();

        for (unsigned int i = 0; i < WARMUP_JOBS; i++) {
            ret = larodRunJobAsync(conn->larodConn, conn->jobReq,
                                   jobCallbackNoOp, nullptr, &error);
            ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
        }
    }

    // Always print the rand seed to log to be able to re-create failed Jenkins
    // tests.
    unsigned int seed = (unsigned int) (time(nullptr));
    gtestPrint(string("Using random seed: ") + to_string(seed));

    gtestDebugPrint("Randomizing jobs...");
    random_device rd;
    mt19937 g(rd());
    g.seed(seed);
    auto begin = jobs.begin();
    auto high_jobs_begin = begin + NUM_CONNECTIONS * NUM_JOBS_PER_CONNECTION;
    auto high_jobs_end =
        begin + NUM_CONNECTIONS * (NUM_JOBS_PER_CONNECTION + 1);
    auto end = jobs.end();

    // Shuffle first batch of LOW_PRIO jobs.
    shuffle(begin, high_jobs_begin, g);

    // Shuffle batch of HIGH_PRIO jobs.
    shuffle(high_jobs_begin, high_jobs_end, g);

    // Shuffle second batch of LOW_PRIO jobs.
    shuffle(high_jobs_end, end, g);

    for (auto job : jobs) {
        auto conn = larodConns[job.first];
        unsigned int seqNum = conn->cbh->getSequenceNumber();

        gtestDebugPrint(string("  Adding req seqnum=") + to_string(seqNum) +
                        " prio=" + to_string((unsigned int) job.second) +
                        " connection=" + to_string(conn->cbh->ID));

        void* cbParam =
            new CallBackHandler::CallbackItem(job.second, seqNum, conn->cbh);

        ret = larodSetJobRequestPriority(conn->jobReq, job.second, &error);
        ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

        ret = larodRunJobAsync(conn->larodConn, conn->jobReq, jobCallbackCount,
                               cbParam, &error);
        ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
    }

    // Wait until all requests on all Larod connections has finished.
    gtestDebugPrint("Waiting for jobs to finish.");
    for (auto conn : larodConns) {
        shared_ptr<CallBackHandler> cbh = conn->cbh;
        unique_lock<mutex> lk(cbh->callbackMutex);
        cbh->callbackCond.wait(
            lk, [cbh] { return (cbh->numPendingCallbacks <= 0); });
    }

    gtestDebugPrint("Verifying callbacks..");
    for (auto conn : larodConns) {
        gtestDebugPrint(string("  conn id: ") + to_string(conn->cbh->ID));
        shared_ptr<CallBackHandler> cbh = conn->cbh;

        unsigned int outSeqNum = 0;
        for (auto cbItem : cbh->receivedCallbacks) {
            gtestDebugPrint(string("    prio: ") +
                            to_string((unsigned int) cbItem.priority) +
                            " seq num: " + to_string(cbItem.sequenceNumber));

            ASSERT_EQ(cbItem.returnCode, LAROD_ERROR_NONE);

            // Whenever we get a HIGH_PRIO request, verify that it was handled
            // before the second batch of LOW_PRIO requests.
            if (cbItem.priority == HIGH_PRIO) {
                ASSERT_LE(outSeqNum, cbItem.sequenceNumber);
            }

            outSeqNum += 1;
        }
    }
}

TEST_F(JobFuncTest, AsyncModelAccessDenied) {
    bool ret;
    larodError* error = nullptr;

    gtestDebugPrint("Creating Larod connections...");
    shared_ptr<Connection> connA = addConnection(0);
    connA->createJobReq();
    shared_ptr<Connection> connB = addConnection(1);
    connB->createJobReq();

    // Execute a couple of jobs on each connection to 'warm up' before issuing
    // the jobs where we check order etc. This gets the backend unit in Larod
    // service busy, making sure that the interesting jobs we add will be placed
    // in the order we expect rather than being picked up directly by busy
    // execution units in the backend.
    const unsigned int INITIAL_JOBS = 5;
    uint8_t prio = 50;
    void* jobParam = nullptr;

    ret = larodSetJobRequestPriority(connA->jobReq, prio, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    for (unsigned int i = 0; i < INITIAL_JOBS; i++) {
        jobParam = new CallBackHandler::CallbackItem(
            prio, connA->cbh->getSequenceNumber(), connA->cbh);

        ret = larodRunJobAsync(connA->larodConn, connA->jobReq,
                               jobCallbackCount, jobParam, &error);
        ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
    }

    // Now enqueue a high-prio request on connection A but use model pointer
    // from connection B.
    prio = 90;
    ret = larodSetJobRequestPriority(connA->jobReq, prio, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    ret = larodSetJobRequestModel(connA->jobReq, connB->modelPtr, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    jobParam = new CallBackHandler::CallbackItem(
        prio, connA->cbh->getSequenceNumber(), connA->cbh);
    ret = larodRunJobAsync(connA->larodConn, connA->jobReq, jobCallbackCount,
                           jobParam, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    gtestDebugPrint("Waiting for jobs to finish.");
    {
        unique_lock<mutex> lk(connA->cbh->callbackMutex);
        connA->cbh->callbackCond.wait(
            lk, [connA] { return (connA->cbh->numPendingCallbacks <= 0); });
    }

    bool failedCallbackFound = false;
    for (auto cbitem : connA->cbh->receivedCallbacks) {
        if (cbitem.priority == prio) {
            ASSERT_EQ(cbitem.returnCode, LAROD_ERROR_PERMISSION);
            failedCallbackFound = true;
            break;
        }
    }
    ASSERT_EQ(failedCallbackFound, true);
}

// Exercise jobReq setters.
TEST_F(JobFuncTest, jobReqSetters) {
    bool ret;
    larodError* error = nullptr;

    shared_ptr<Connection> c = addConnection(0);
    larodConnection* conn = c->larodConn;
    larodJobRequest* jobReq = c->createJobReq();

    ret = larodSetJobRequestModel(jobReq, c->modelPtr, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    ret =
        larodSetJobRequestInputs(jobReq, c->inputTensors, c->numInputs, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    ret = larodSetJobRequestOutputs(jobReq, c->outputTensors, c->numOutputs,
                                    &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    ret = larodRunJob(conn, jobReq, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
}

/**
 * @brief Offset on input tensor.
 */
TEST_F(JobFuncTest, InputWithOffset) {
    bool ret = false;
    larodError* error = nullptr;
    off_t offset = sysconf(_SC_PAGE_SIZE);

    if ((inputBufType == BUF_TYPE_LAROD_DMABUF) ||
        (inputBufType == BUF_TYPE_LAROD_DISKFD)) {
        gtestPrint("Skipping test since larod allocs does not provide any "
                   "slack in allocated buffer");
        return;
    }
    if (chip == LAROD_CHIP_TPU && (inputBufType == BUF_TYPE_EXT_DMABUF)) {
        gtestPrint("Skipping test since TFLiteTPU does not allow for any "
                   "offset in external dma buffers");
        return;
    }

    // Tracking the tensors does not make sense for this particular testcase.
    shared_ptr<Connection> c = addConnection(0, false);

    auto [inputFd, bytesWritten] =
        buildTempFdWithOffset(c->inputFileFds[0], offset, inputBufType);
    ASSERT_GE(inputFd, 0);

    size_t fdSize = static_cast<size_t>(offset) + bytesWritten;
    ret = larodSetTensorFdSize(c->inputTensors[0], fdSize, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
    ret = larodSetTensorFd(c->inputTensors[0], inputFd, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
    ret = larodSetTensorFdOffset(c->inputTensors[0], offset, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
    c->createJobReq();

    ret = larodRunJob(c->larodConn, c->jobReq, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    if (verificationFileFds.size() > 0) {
        vector<uint8_t> verificationVector = readFdToVector(
            verificationFileFds[0], 0, AUTO_SIZE, BUF_TYPE_EXT_DISKFD);
        ASSERT_GT(verificationVector.size(), 0);

        size_t bufferSize = AUTO_SIZE;
        // At least Ambarella dmabufs does not report actual size as result
        // of lseek(SEEk_END) so we just use the verification data size.
        if (outputBufType == BUF_TYPE_LAROD_DMABUF) {
            bufferSize = verificationVector.size();
        }

        vector<uint8_t> outputVector =
            readFdToVector(c->outputFileFds[0], 0, bufferSize, outputBufType);
        ASSERT_GT(outputVector.size(), 0);

        ASSERT_EQ(outputVector.size(), verificationVector.size());

        verifyTensorData(outputVector, verificationVector, c->outputTensors[0],
                         error);
    } else {
        gtestPrint("Skipping file verification -- missing verification file");
    }
}

/**
 * @brief Offset on output tensor.
 */
TEST_F(JobFuncTest, OutputWithOffset) {
    bool ret = false;
    larodError* error = nullptr;
    off_t offset = sysconf(_SC_PAGE_SIZE);

    if ((outputBufType == BUF_TYPE_LAROD_DMABUF) ||
        (outputBufType == BUF_TYPE_LAROD_DISKFD)) {
        gtestPrint("Skipping test since larod allocs does not provide any "
                   "slack in allocated buffer");
        return;
    }

    // Tracking the tensors does not make sense for this particular testcase.
    shared_ptr<Connection> c = addConnection(0, false);

    size_t outputFdSize = outputByteSizes[0] + static_cast<size_t>(offset);
    int outputFd = createTempFd(outputFdSize, outputBufType);
    ASSERT_GE(outputFd, 0);

    ret = larodSetTensorFdSize(c->outputTensors[0], outputFdSize, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
    ret = larodSetTensorFd(c->outputTensors[0], outputFd, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
    ret = larodSetTensorFdOffset(c->outputTensors[0], offset, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    c->createJobReq();

    ret = larodRunJob(c->larodConn, c->jobReq, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    if (verificationFileFds.size() > 0) {
        vector<uint8_t> verificationVector = readFdToVector(
            verificationFileFds[0], 0, AUTO_SIZE, BUF_TYPE_EXT_DISKFD);
        ASSERT_GT(verificationVector.size(), 0);

        // Read exactly verificationVector.size() bytes from the result buffer -
        // the buffer is larger due to the offset so SIZE_AUTO should not be
        // used.
        vector<uint8_t> outputVector = readFdToVector(
            outputFd, offset, verificationVector.size(), outputBufType);

        verifyTensorData(outputVector, verificationVector, c->outputTensors[0],
                         error);
    } else {
        gtestPrint("Skipping file verification -- missing verification file");
    }
}

TEST_F(JobFuncTest, InputWithSize) {
    if ((inputBufType == BUF_TYPE_LAROD_DMABUF) ||
        (inputBufType == BUF_TYPE_LAROD_DISKFD)) {
        gtestPrint("Skipping test since larod allocs will always have fdSize "
                   "set thus tested in all other job runs");
        return;
    }
    bool ret = false;
    larodError* error = nullptr;

    // Tracking the tensors does not make sense for this particular testcase.
    shared_ptr<Connection> c = addConnection(0, false);

    auto [inputFd, inputSize] =
        buildTempFdWithOffset(c->inputFileFds[0], 0, inputBufType);
    ASSERT_GE(inputFd, 0);
    ASSERT_GT(inputSize, 0);

    ret = larodSetTensorFd(c->inputTensors[0], inputFd, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
    ret = larodSetTensorFdSize(c->inputTensors[0], inputSize, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    c->createJobReq();

    ret = larodRunJob(c->larodConn, c->jobReq, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    if (verificationFileFds.size() > 0) {
        vector<uint8_t> verificationVector = readFdToVector(
            verificationFileFds[0], 0, AUTO_SIZE, BUF_TYPE_EXT_DISKFD);
        ASSERT_GT(verificationVector.size(), 0);

        size_t bufferSize = AUTO_SIZE;
        // At least Ambarella dmabufs does not report actual size as result
        // of lseek(SEEk_END) so we just use the verification data size.
        if (outputBufType == BUF_TYPE_LAROD_DMABUF) {
            bufferSize = verificationVector.size();
        }

        vector<uint8_t> outputVector =
            readFdToVector(c->outputFileFds[0], 0, bufferSize, outputBufType);
        ASSERT_GT(outputVector.size(), 0);

        ASSERT_EQ(outputVector.size(), verificationVector.size());

        verifyTensorData(outputVector, verificationVector, c->outputTensors[0],
                         error);
    } else {
        gtestPrint("Skipping file verification -- missing verification file");
    }
}

TEST_F(JobFuncTest, OutputWithSize) {
    bool ret = false;
    larodError* error = nullptr;

    if ((outputBufType == BUF_TYPE_LAROD_DMABUF) ||
        (outputBufType == BUF_TYPE_LAROD_DISKFD)) {
        gtestPrint("Skipping test since larod allocs will always have fdSize "
                   "set thus tested in all other job runs");
        return;
    }

    // Tracking the tensors does not make sense for this particular testcase.
    shared_ptr<Connection> c = addConnection(0, false);

    auto [outputFd, outputSize] =
        buildTempFdWithOffset(c->outputFileFds[0], 0, outputBufType);
    ASSERT_GE(outputFd, 0);
    ASSERT_GT(outputSize, 0);

    ret = larodSetTensorFd(c->outputTensors[0], outputFd, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
    ret = larodSetTensorFdSize(c->outputTensors[0], outputSize, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    c->createJobReq();

    ret = larodRunJob(c->larodConn, c->jobReq, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    if (verificationFileFds.size() > 0) {
        vector<uint8_t> verificationVector = readFdToVector(
            verificationFileFds[0], 0, AUTO_SIZE, BUF_TYPE_EXT_DISKFD);
        ASSERT_EQ(verificationVector.size(), outputSize);

        size_t bufferSize = AUTO_SIZE;
        // At least Ambarella dmabufs does not report actual size as result
        // of lseek(SEEk_END) so we just use the verification data size.
        if (outputBufType == BUF_TYPE_LAROD_DMABUF) {
            bufferSize = verificationVector.size();
        }

        vector<uint8_t> outputVector =
            readFdToVector(outputFd, 0, bufferSize, outputBufType);
        ASSERT_GT(outputVector.size(), 0);

        ASSERT_EQ(outputVector.size(), verificationVector.size());

        verifyTensorData(outputVector, verificationVector, c->outputTensors[0],
                         error);
    } else {
        gtestPrint("Skipping file verification -- missing verification file");
    }
}

// Check input parameter validation of larodRunJob
TEST_F(JobFuncTest, larodRunJobInvalidParameter) {
    larodError* error = nullptr;
    shared_ptr<Connection> c = addConnection(0);

    // Connection=NULL, 'valid' request ptr
    bool ret = larodRunJob(nullptr, c->jobReq, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    // 'Valid' connection but request ptr is NULL
    ret = larodRunJob(c->larodConn, nullptr, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    // Both params NULL
    ret = larodRunJob(nullptr, nullptr, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

// Check input parameter validation of larodRunJobAsync
TEST_F(JobFuncTest, larodRunJobAsyncInvalidParameter) {
    larodError* error = nullptr;
    shared_ptr<Connection> c = addConnection(0);

    bool ret =
        larodRunJobAsync(nullptr, c->jobReq, jobCallbackNoOp, nullptr, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    ret = larodRunJobAsync(c->larodConn, nullptr, jobCallbackNoOp, nullptr,
                           &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    ret = larodRunJobAsync(c->larodConn, c->jobReq, nullptr, nullptr, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(JobFuncTest, MmapInService) {
    bool ret = false;
    larodError* error = nullptr;

    vector<string> inputFdPaths;
    vector<string> outputFdPaths;

    uint32_t inputFdProps = 0;
    uint32_t outputFdProps = 0;

    // Skip if using dmabuf on Ambarella backend.
    if ((chip == LAROD_CHIP_CVFLOW_PROC || chip == LAROD_CHIP_CVFLOW_NN) &&
        (inputBufType == BUF_TYPE_EXT_DMABUF ||
         inputBufType == BUF_TYPE_LAROD_DMABUF)) {
        gtestPrint("Skipping test since dmabufs on Ambarella platform only "
                   "have filenames \"/dmabuf:\" making them impossible to "
                   "distinguish.");
        return;
    }
    shared_ptr<Connection> c = addConnection(0);
    inputFdPaths = getFdPaths(c->inputFileFds);
    outputFdPaths = getFdPaths(c->outputFileFds);

    ret = larodGetTensorFdProps(c->inputTensors[0], &inputFdProps, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
    ret = larodGetTensorFdProps(c->outputTensors[0], &outputFdProps, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    c->createJobReq();

    // The service might wanna map up allocated dma buffers for cache
    // synchronization.
    if (JobFuncTest::inputBufType != BUF_TYPE_LAROD_DMABUF) {
        ASSERT_FALSE(larodHasMaped(inputFdPaths));
    }
    if (JobFuncTest::outputBufType != BUF_TYPE_LAROD_DMABUF) {
        ASSERT_FALSE(larodHasMaped(outputFdPaths));
    }

    ret = larodRunJob(c->larodConn, c->jobReq, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    if ((inputFdProps & LAROD_FD_PROP_MAP) && trackInputs) {
        ASSERT_TRUE(larodHasMaped(inputFdPaths));
    } else if (JobFuncTest::inputBufType != BUF_TYPE_LAROD_DMABUF &&
               JobFuncTest::inputBufType != BUF_TYPE_EXT_DMABUF) {
        ASSERT_FALSE(larodHasMaped(inputFdPaths));
    }

    if ((outputFdProps & LAROD_FD_PROP_MAP) && trackOutputs) {
        ASSERT_TRUE(larodHasMaped(outputFdPaths));
    } else if (JobFuncTest::outputBufType != BUF_TYPE_LAROD_DMABUF &&
               JobFuncTest::outputBufType != BUF_TYPE_EXT_DMABUF) {
        ASSERT_FALSE(larodHasMaped(outputFdPaths));
    }

    if (verificationFileFds.size() > 0) {
        vector<uint8_t> verificationVector = readFdToVector(
            verificationFileFds[0], 0, AUTO_SIZE, BUF_TYPE_EXT_DISKFD);
        ASSERT_GT(verificationVector.size(), 0);

        size_t bufferSize = AUTO_SIZE;
        // At least Ambarella dmabufs does not report actual size as result
        // of lseek(SEEk_END) so we just use the verification data size.
        if (outputBufType == BUF_TYPE_LAROD_DMABUF) {
            bufferSize = verificationVector.size();
        }

        vector<uint8_t> outputVector =
            readFdToVector(c->outputFileFds[0], 0, bufferSize, outputBufType);
        ASSERT_GT(outputVector.size(), 0);

        ASSERT_EQ(outputVector.size(), verificationVector.size());

        verifyTensorData(outputVector, verificationVector, c->outputTensors[0],
                         error);
    } else {
        gtestPrint("Skipping file verification -- missing verification file");
    }

    JobFuncTest::eraseAllConnections();
    // Since the actual session destruction in the service happens in parallel
    // to the client process execution we need to yield here for a while to
    // allow the service clean-up to take place.
    this_thread::sleep_for(milliseconds(500));

    ASSERT_FALSE(larodHasMaped(inputFdPaths));
    ASSERT_FALSE(larodHasMaped(outputFdPaths));
}

TEST_F(JobFuncTest, AllocTensorsNoFdLeak) {
    bool ret = false;
    larodError* error = nullptr;

    size_t initialNumFd = getNumOpenLarodFds();
    shared_ptr<Connection> c = addConnection(0);

    larodMap* bufferAllocParams = nullptr;
    uint32_t fdPropFlags = 0;

    larodTensor** inputTensors = c->inputTensors;
    larodTensor** outputTensors = c->outputTensors;
    size_t numInputs = c->numInputs;
    size_t numOutputs = c->numOutputs;

    // NOTE: This test allocates one more set of input/output tensors in
    // addition to the buffers allocated in
    // JobFuncTest::initLarodAllocInputTensors.

    if ((inputBufType == BUF_TYPE_EXT_DISKFD) ||
        (inputBufType == BUF_TYPE_EXT_DMABUF)) {
        gtestPrint("Skipping input allocs since external inputs was requested");
    } else {
        // Ordinary disk-type fd.
        if (forceMapInput) {
            fdPropFlags = LAROD_FD_PROP_MAP;
        } else {
            fdPropFlags = LAROD_FD_PROP_READWRITE;
        }

        // dmabuf case.
        if (inputBufType == BUF_TYPE_LAROD_DMABUF) {
            fdPropFlags = LAROD_FD_PROP_DMABUF;
            if (forceMapInput) {
                fdPropFlags |= LAROD_FD_PROP_MAP;
            }
        }

        inputTensors =
            larodAllocModelInputs(c->larodConn, c->modelPtr, fdPropFlags,
                                  &numInputs, bufferAllocParams, &error);
        ASSERT_TRUE(
            checkLarodResultWithPrint((inputTensors != nullptr), &error));
    }

    if ((outputBufType == BUF_TYPE_EXT_DISKFD) ||
        (outputBufType == BUF_TYPE_EXT_DMABUF)) {
        gtestPrint(
            "Skipping output allocs since external inputs was requested");
    } else {
        // Ordinary disk-type fd.
        if (forceMapOutput) {
            fdPropFlags = LAROD_FD_PROP_MAP;
        } else {
            fdPropFlags = LAROD_FD_PROP_READWRITE;
        }

        // dmabuf case.
        if (outputBufType == BUF_TYPE_LAROD_DMABUF) {
            fdPropFlags = LAROD_FD_PROP_DMABUF;
            if (forceMapOutput) {
                fdPropFlags |= LAROD_FD_PROP_MAP;
            }
        }

        size_t numOutputs = 0;
        outputTensors =
            larodAllocModelOutputs(c->larodConn, c->modelPtr, fdPropFlags,
                                   &numOutputs, bufferAllocParams, &error);
        ASSERT_TRUE(
            checkLarodResultWithPrint((outputTensors != nullptr), &error));
    }

    larodJobRequest* jobRequest =
        larodCreateJobRequest(c->modelPtr, inputTensors, numInputs,
                              outputTensors, numOutputs, jobParams, &error);
    ASSERT_TRUE(checkLarodResultWithPrint((jobRequest != nullptr), &error));

    ret = larodRunJob(c->larodConn, jobRequest, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    if (inputTensors != c->inputTensors) {
        larodDestroyTensors(&inputTensors, numInputs);
    }
    if (outputTensors != c->outputTensors) {
        larodDestroyTensors(&outputTensors, numOutputs);
    }

    larodDestroyJobRequest(&jobRequest);

    JobFuncTest::eraseAllConnections();
    // Since the actual session destruction in the service happens in paralell
    // to the client process execution we need to yield here for a while to
    // allow the service clean-up to take place.
    this_thread::sleep_for(milliseconds(500));

    size_t currentNumFd = getNumOpenLarodFds();
    ASSERT_EQ(currentNumFd, initialNumFd);
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

    args_t args;

    if (parseArgs(argc, argv, &args) != 0) {
        destroyArgs(&args);
        exit(EXIT_FAILURE);
    }
    JobFuncTest::chip = args.chip;
    JobFuncTest::debugPrints = args.debugPrints;
    JobFuncTest::modelParams = args.modelParams;
    JobFuncTest::jobParams = args.jobParams;
    JobFuncTest::trackInputs = args.trackInputs;
    JobFuncTest::trackOutputs = args.trackOutputs;
    JobFuncTest::forceMapInput = args.forceMapInput;
    JobFuncTest::forceMapOutput = args.forceMapOutput;
    JobFuncTest::inputBufType = args.inputBufType;
    JobFuncTest::outputBufType = args.outputBufType;
    if (args.model) {
        JobFuncTest::modelFile = args.model;
    }
    if (args.numInputFiles > 0) {
        JobFuncTest::inputFiles = vector<string>(
            args.inputFiles, args.inputFiles + args.numInputFiles);
    }
    if (args.numVerificationFiles > 0) {
        JobFuncTest::verificationFiles =
            vector<string>(args.verificationFiles,
                           args.verificationFiles + args.numVerificationFiles);
    }

    int ret = RUN_ALL_TESTS();

    destroyArgs(&args);

    return ret;
}
