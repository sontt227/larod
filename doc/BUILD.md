Building
========

larod uses [Meson][1] as the build system. To build, run the following in the
root directory:

```
meson build [OPTIONS]
cd build
ninja
```

Where `OPTIONS` can for example specify that you want to build larod with the
TFLite CPU backend in which case `OPTIONS = -Duse_tflite_cpu=true`. See
`meson_options.txt` in the project root for more flags.

larod requires a C++17 compiler and some other dependencies such as
`libsystemd`,`pthreads` and [sdbusplus][2] (see meson.build for more
details). There is also a Dockerfile that can be used for development.

## Experimental

Some versions of larod (both lib and service) may contain experimental
features. These features are marked clearly as experimental and **may be
completely removed from future versions of larod without backwards
compatibility**. The idea here is to be able to give users early access to
features that may not be completely finished or needs to be evaluated before
official inclusion into larod.

## Docker

The easiest way to build larod would probably be using the Dockerfile in the
root directory:

```
docker build -t larod .
```

If your Docker version is really old you might need the following proxy
configuration in `/etc/systemd/system/docker.service.d/proxy.conf`:

```
docker run --volume /run/dbus/system_bus_socket:/run/dbus/system_bus_socket \
    -it larod
```

The host system needs to have a D-Bus policy in place allowing user `root` to
create a new name on the system bus. This can be achieved by placing the
`com.axis.Larod1.conf` file in the D-Bus configuration folder on the host
system:

```
sudo cp src/service/com.axis.Larod1.conf /etc/dbus-1/system.d/
```

You can now run larod in a container as well as interface with this container
over D-Bus (on the system bus). larod's source itself can be mounted as a volume
when running the container (see the example in the Dockerfile). This way you can
develop and run larod in a container. For example, one can run the service
`larod` in one container and run `larod-client` in another.

If your host system has AppArmor (such as Ubuntu) the default AppArmor profile
used for Docker containers might prevent the container to talk to the host
D-Bus instance. To work around that you can launch your container with the
"unconfined" profile to disable AppArmor, like this:
`--security-opt apparmor=unconfined`.

Note that in order to run `gdb` in a container, one must start the container
with `ptrace` capabilities `--cap-add=SYS_PTRACE` and `--security-opt
seccomp=unconfined`. To be able to attach to a running process from within the
container you need to change to classic ptrace permissions on the host:
`echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope`.

### NVIDIA GPU

The Dockerfile supports running inference on GPU with TFLite using EGL and a
NVIDIA GPU (``LAROD_CHIP_TFLITE_GLGPU``). In order to run a Docker container
with GPU support the NVIDIA Container Toolkit is needed. Install that by
following these [instructions](https://github.com/NVIDIA/nvidia-docker). Then to
build and run the container with GPU enabled:

```
docker build -t larod --build-arg USE_TFLITE_GLGPU=true .
docker run --volume /run/dbus/system_bus_socket:/run/dbus/system_bus_socket \
    --gpus all -it larod
```

[1]: https://mesonbuild.com/
[2]: https://github.com/openbmc/sdbusplus
