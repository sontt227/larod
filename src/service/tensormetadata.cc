/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "tensormetadata.hh"

#include <iterator>
#include <numeric>
#include <sstream>
#include <string.h>
#include <unistd.h>

#include "log.hh"

using namespace std;

namespace larod {

TensorMetadata::TensorMetadata(const larodTensorDataType dataType,
                               const larodTensorLayout layout,
                               const vector<size_t> dims,
                               const vector<size_t> pitches,
                               const size_t byteSize, const string name)
    : DATA_TYPE(dataType), LAYOUT(layout), DIMS(dims), PITCHES(pitches),
      BYTE_SIZE(byteSize), NAME(name) {
    // Validate arguments.
    if (DATA_TYPE <= LAROD_TENSOR_DATA_TYPE_INVALID ||
        DATA_TYPE > LAROD_TENSOR_DATA_TYPE_MAX) {
        throw invalid_argument("Tensor has invalid data type");
    }

    if (LAYOUT <= LAROD_TENSOR_LAYOUT_INVALID ||
        LAYOUT > LAROD_TENSOR_LAYOUT_MAX) {
        throw invalid_argument("Tensor has invalid layout");
    }

    // A dimension vector that has zero size is valid, but is unspecified
    // (see larodTensorDims in the library).
    if (!DIMS.empty()) {
        if (DIMS.size() > LAROD_TENSOR_MAX_LEN) {
            throw invalid_argument("Tensor has invalid dimension length (" +
                                   to_string(DIMS.size()) + ")");
        }

        if (any_of(DIMS.cbegin(), DIMS.cend(),
                   [](size_t dim) { return dim == 0; })) {
            throw invalid_argument("Tensor has invalid dimension values");
        }

        if ((LAYOUT == LAROD_TENSOR_LAYOUT_NCHW ||
             LAYOUT == LAROD_TENSOR_LAYOUT_NHWC) &&
            DIMS.size() != 4) {
            throw invalid_argument("Tensor has invalid dimension length (" +
                                   to_string(DIMS.size()) +
                                   ") with regards to specified layout");
        }
    }

    // A pitches vector that has zero size is valid, but is unspecified
    // (see larodTensorPitches in the library).
    if (!PITCHES.empty()) {
        if (PITCHES.size() > LAROD_TENSOR_MAX_LEN) {
            throw invalid_argument("Tensor has pitch length (" +
                                   to_string(PITCHES.size()) +
                                   ") that exceeds the maximum allowed (" +
                                   to_string(LAROD_TENSOR_MAX_LEN) + ")");
        }

        if (!DIMS.empty() && PITCHES.size() != DIMS.size()) {
            throw invalid_argument("Mismatch between tensor's dimensions "
                                   "length (" +
                                   to_string(DIMS.size()) +
                                   ") and pitches length (" +
                                   to_string(PITCHES.size()) + ")");
        }

        if (any_of(PITCHES.cbegin(), PITCHES.cend(),
                   [](size_t pitch) { return pitch == 0; })) {
            throw invalid_argument("Tensor has invalid zero pitch values");
        }

        // Note that byte size can be zero (which means it is undefined).
        if (BYTE_SIZE && PITCHES[0] != BYTE_SIZE) {
            throw invalid_argument(
                "Tensor's pitches values (pitches[0]=" + to_string(PITCHES[0]) +
                ") do not match tensor's byte size (" + to_string(BYTE_SIZE) +
                ")");
        }
    }
}

void TensorMetadata::assertEqual(const TensorMetadata& lhs,
                                 const TensorMetadata& rhs) {
    if (lhs.DATA_TYPE != LAROD_TENSOR_DATA_TYPE_UNSPECIFIED &&
        rhs.DATA_TYPE != LAROD_TENSOR_DATA_TYPE_UNSPECIFIED &&
        lhs.DATA_TYPE != rhs.DATA_TYPE) {
        throw invalid_argument("Tensors data type mismatch (got " +
                               to_string(lhs.DATA_TYPE) + ", expected " +
                               to_string(rhs.DATA_TYPE) + ")");
    }

    if (lhs.LAYOUT != LAROD_TENSOR_LAYOUT_UNSPECIFIED &&
        rhs.LAYOUT != LAROD_TENSOR_LAYOUT_UNSPECIFIED &&
        lhs.LAYOUT != rhs.LAYOUT) {
        throw invalid_argument("Tensors layout mismatch (got " +
                               to_string(lhs.LAYOUT) + ", expected " +
                               to_string(rhs.LAYOUT) + ")");
    }

    if (!lhs.DIMS.empty() && !rhs.DIMS.empty() && lhs.DIMS != rhs.DIMS) {
        throw invalid_argument("Tensors dimensions mismatch (got " +
                               vectorToString(lhs.DIMS) + ", expected " +
                               vectorToString(rhs.DIMS) + ")");
    }

    if (!lhs.PITCHES.empty() && !rhs.PITCHES.empty() &&
        lhs.PITCHES != rhs.PITCHES) {
        throw invalid_argument("Tensors pitches mismatch (got " +
                               vectorToString(lhs.PITCHES) + ", expected " +
                               vectorToString(rhs.PITCHES) + ")");
    }

    if (lhs.BYTE_SIZE && rhs.BYTE_SIZE && (lhs.BYTE_SIZE != rhs.BYTE_SIZE)) {
        throw invalid_argument("Tensors byte size mismatch (got " +
                               to_string(lhs.BYTE_SIZE) + ", expected " +
                               to_string(rhs.BYTE_SIZE) + ")");
    }
}

string TensorMetadata::vectorToString(const vector<size_t>& vec) {
    ostringstream oss;
    oss << "[";
    if (!vec.empty()) {
        copy(vec.begin(), vec.end() - 1, ostream_iterator<size_t>(oss, ","));
        oss << vec.back();
    }
    oss << "]";

    return oss.str();
}

} // namespace larod
