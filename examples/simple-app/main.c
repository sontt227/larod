/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * This application connects to larod and loads a model, run inference
 * on it and then finally deletes the loaded model.
 *
 * The application expects its parameters on the command line in the following
 * order: MODEL_FILE INPUT_FILE.
 *
 * The first two parameters are expected to be file names and the output tensor
 * size is expected to be a positive integer (given in either decimal,
 * hexadecimal or octal notation). The output will be written to current
 * directory with suffix `.out`.
 *
 * Example:
 *     ./simple-app a.model b.in
 */

#include <errno.h>
#include <inttypes.h>
#include <libgen.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "larod.h"

bool deleteModel(larodConnection* conn, larodModel* model);
bool loadModel(larodConnection* conn, char* modelFile, const larodAccess access,
               const char* modelName, larodModel** model);
bool runJob(larodConnection* conn, larodModel* model, char* inputFile,
            char* outputFile);
larodError* error = NULL;

int main(int argc, char** argv) {
    bool ret = false;
    larodConnection* conn = NULL;
    larodModel* model = NULL;

    // Check for valid number of arguments
    if (argc != 3) {
        fprintf(stderr, "ERROR: Invalid number of arguments\n"
                        "Usage: simple-app MODEL_FILE INPUT_FILE\n");
        goto end;
    }

    puts("Connecting to larod...");
    if (!larodConnect(&conn, &error)) {
        fprintf(stderr, "ERROR: Could not connect to larod (%d): %s\n",
                error->code, error->msg);

        larodClearError(&error);
        goto end;
    }
    puts("Connected");

    if (!loadModel(conn, argv[1], LAROD_ACCESS_PUBLIC, NULL, &model)) {
        goto end;
    }

    if (!runJob(conn, model, argv[2], NULL)) {
        goto end;
    }

    if (!deleteModel(conn, model)) {
        goto end;
    }

    ret = true;

end:
    larodDestroyModel(&model);

    // Disconnect
    if (conn && !larodDisconnect(&conn, &error)) {
        fprintf(stderr, "ERROR: Could not disconnect (%d): %s\n", error->code,
                error->msg);

        larodClearError(&error);
    }

    return ret ? EXIT_SUCCESS : EXIT_FAILURE;
}

bool loadModel(larodConnection* conn, char* modelFile, const larodAccess access,
               const char* modelName, larodModel** model) {
    puts("Loading model...");
    larodModel* localModel = NULL;
    FILE* fpModel = fopen(modelFile, "rb");
    if (!fpModel) {
        fprintf(stderr, "ERROR: Could not open model file %s: %s\n", modelFile,
                strerror(errno));
        return false;
    }

    const int fd = fileno(fpModel);
    if (fd < 0) {
        fprintf(stderr,
                "ERROR: Could not get file descriptor for model file: %s\n",
                strerror(errno));
        fclose(fpModel);
        return false;
    }

    if (!modelName) {
        modelName = basename(modelFile);
    }

    // Using LAROD_CHIP_TFLITE_CPU as default.
    localModel = larodLoadModel(conn, fd, LAROD_CHIP_TFLITE_CPU, access,
                                modelName, NULL, &error);
    if (!localModel) {
        fprintf(stderr, "ERROR: Could not load model (%d): %s\n", error->code,
                error->msg);

        larodClearError(&error);
        fclose(fpModel);
        return false;
    }

    if (fclose(fpModel)) {
        fprintf(stderr, "ERROR: Could not close file %s\n", modelFile);
    }

    *model = localModel;
    printf("Model %s loaded \n", modelFile);

    return true;
}

bool deleteModel(larodConnection* conn, larodModel* model) {
    const uint64_t modelId = larodGetModelId(model, &error);
    if (modelId == LAROD_INVALID_MODEL_ID) {
        fprintf(stderr, "ERROR: Could not retrieve model ID (%d): %s\n",
                error->code, error->msg);

        larodClearError(&error);
        return false;
    }
    printf("Deleting model with ID %" PRIu64 "...\n", modelId);

    if (!larodDeleteModel(conn, model, &error)) {
        fprintf(stderr,
                "ERROR: Could not delete model with ID %" PRIu64 " (%d): %s\n",
                modelId, error->code, error->msg);

        larodClearError(&error);
        return false;
    }

    printf("Deleted model with ID %" PRIu64 "\n", modelId);

    return true;
}

bool runJob(larodConnection* conn, larodModel* model, char* inputFile,
            char* outputFile) {
    puts("Running inference...");

    bool ret = false;
    larodJobRequest* jobReq = NULL;
    FILE* fpInput = NULL;
    FILE* fpOutput = NULL;
    larodTensor** inputTensors = NULL;
    size_t numInputs = 0;
    larodTensor** outputTensors = NULL;
    size_t numOutputs = 0;
    size_t* outByteSizes = NULL;

    fpInput = fopen(inputFile, "rb");
    if (!fpInput) {
        fprintf(stderr, "ERROR: Could not open input file %s: %s\n", inputFile,
                strerror(errno));
        goto end;
    }

    const int inFd = fileno(fpInput);
    if (inFd < 0) {
        fprintf(stderr,
                "ERROR: Could not get file descriptor for input file: %s\n",
                strerror(errno));
        goto end;
    }

    if (!outputFile) {
        outputFile = strcat(basename(inputFile), ".out");
    }
    fpOutput = fopen(outputFile, "wb+");
    if (!fpOutput) {
        fprintf(stderr, "ERROR: Could not open output file %s: %s\n",
                outputFile, strerror(errno));
        goto end;
    }

    const int outFd = fileno(fpOutput);
    if (outFd < 0) {
        fprintf(stderr,
                "ERROR: Could not get file descriptor for output file: %s\n",
                strerror(errno));
        goto end;
    }

    inputTensors = larodCreateModelInputs(model, &numInputs, &error);
    if (!inputTensors) {
        fprintf(stderr, "ERROR: Failed retrieving input tensors (%d): %s\n",
                error->code, error->msg);
        goto end;
    }
    // This example only supports one input/output tensor at the moment.
    if (numInputs != 1) {
        fprintf(stderr,
                "ERROR: This model has %zu input tensors, but example only "
                "supports 1 input tensor.\n",
                numInputs);
        goto end;
    }
    if (!larodSetTensorFd(inputTensors[0], inFd, &error)) {
        fprintf(stderr, "ERROR: Could not set input tensor fd (%d): %s\n",
                error->code, error->msg);
        goto end;
    }
    // Tell larod that it is possible to use read() and write(), and mmap() on
    // the fd.
    if (!larodSetTensorFdProps(inputTensors[0],
                               LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP,
                               &error)) {
        fprintf(stderr,
                "ERROR: Could not set input tensor fd properties (%d): %s\n",
                error->code, error->msg);
        goto end;
    }

    outputTensors = larodCreateModelOutputs(model, &numOutputs, &error);
    if (!outputTensors) {
        fprintf(stderr, "ERROR: Failed retrieving output tensors (%d): %s\n",
                error->code, error->msg);
        goto end;
    }
    // This example only supports one input/output tensor at the moment.
    if (numOutputs != 1) {
        fprintf(stderr,
                "ERROR: This model has %zu output tensors, but example only "
                "supports 1 output tensor.\n",
                numOutputs);
        goto end;
    }
    if (!larodSetTensorFd(outputTensors[0], outFd, &error)) {
        fprintf(stderr, "ERROR: Could not set output tensor fd (%d): %s\n",
                error->code, error->msg);
        goto end;
    }
    // Tell larod that it is possible to use read() and write(), and mmap() on
    // the fd.
    if (!larodSetTensorFdProps(outputTensors[0],
                               LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP,
                               &error)) {
        fprintf(stderr,
                "ERROR: Could not set output tensor fd properties (%d): %s\n",
                error->code, error->msg);
        goto end;
    }

    outByteSizes = larodGetModelOutputByteSizes(model, NULL, &error);
    if (!outByteSizes) {
        fprintf(stderr, "ERROR: Could not get output byte sizes (%d): %s\n",
                error->code, error->msg);
        goto end;
    }

    if (ftruncate(outFd, (off_t) outByteSizes[0]) == -1) {
        fprintf(stderr, "ERROR: Could not ftruncate output file: %s\n",
                strerror(errno));
    }

    // Only one input/output tensor supported in this example.
    jobReq = larodCreateJobRequest(model, inputTensors, 1, outputTensors, 1,
                                   NULL, &error);
    if (!jobReq) {
        fprintf(stderr, "ERROR: Could not create inference request (%d): %s\n",
                error->code, error->msg);
        goto end;
    }

    if (!larodRunJob(conn, jobReq, &error)) {
        fprintf(stderr, "Could not run inference (%d): %s\n", error->code,
                error->msg);
        goto end;
    }

    ret = true;

    printf("Output written to %s\n", outputFile);

end:
    if (fpInput && fclose(fpInput)) {
        fprintf(stderr, "ERROR: Could not close file %s\n", inputFile);
    }
    if (fpOutput && fclose(fpOutput)) {
        fprintf(stderr, "ERROR: Could not close file %s\n", outputFile);
    }

    larodDestroyJobRequest(&jobReq);
    larodDestroyTensors(&inputTensors, numInputs);
    larodDestroyTensors(&outputTensors, numOutputs);

    if (outByteSizes) {
        free(outByteSizes);
    }

    larodClearError(&error);

    return ret;
}
