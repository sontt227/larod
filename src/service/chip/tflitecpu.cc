/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "tflitecpu.hh"

#include <tensorflow/lite/kernels/register.h>

using namespace std;

namespace larod {
namespace backendunit {

unique_ptr<tflite::Interpreter>
    TFLiteCPU::buildInterpreter(const tflite::FlatBufferModel& model) {
    tflite::ops::builtin::BuiltinOpResolver resolver;
    tflite::InterpreterBuilder builder(model, resolver);
    unique_ptr<tflite::Interpreter> interpreter;
    builder(&interpreter);

    return interpreter;
}

} // namespace backendunit
} // namespace larod
