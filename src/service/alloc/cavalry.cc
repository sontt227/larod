/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "cavalry.hh"

#include <cassert>
#include <cavalry/cavalry_ioctl.h>
#include <cavalry_mem.h>
#include <fcntl.h>
#include <thread>

#include "larod.h"
#include "log.hh"

using namespace std;

namespace larod {
namespace allocator {

Cavalry::Cavalry() : Allocator(LAROD_FD_PROP_DMABUF | LAROD_FD_PROP_MAP) {
    // Initialize cavalary.
    int fdCav = open(CAVALRY_DEV_NODE, O_RDWR, 0);
    if (fdCav < 0) {
        throw runtime_error("Cavalry dev open failed: " + to_string(errno));
    }
    fdCavalry = std::move(fdCav);

    // cavalry_mem_init() can only be called once per process.
    if (cavalry_mem_init(fdCavalry, 0) < 0) {
        throw runtime_error("Cavalry mem init failed");
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

Cavalry::~Cavalry() {
    cavalry_mem_exit();

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed");
}

unique_ptr<larod::Buffer> Cavalry::allocateVirtual(const size_t size,
                                                   const uint32_t fdProps,
                                                   const ParamsMap& params) {
    // We don't support any additional parameters.
    if (params.size()) {
        throw invalid_argument("Additional parameters are not supported");
    }

    if (size > numeric_limits<uint32_t>::max()) {
        throw invalid_argument("size is too big (" + to_string(size) + " > " +
                               to_string(numeric_limits<uint32_t>::max()) +
                               ")");
    }

    int fd = -1;
    void* virtAddr = nullptr;

    uint8_t nrTries = 8;
    int sleepMS = 1;
    int ret = -1;
    while (ret && nrTries--) {
        ret = cavalry_mem_alloc_mfd(size, &fd, &virtAddr, true);
        if (ret < 0 && errno == EBUSY) {
            // The kernel allocation may fail and in that case the cavalry
            // driver will return EBUSY (by setting errno). Let's try again...
            this_thread::sleep_for(chrono::milliseconds(sleepMS));
            sleepMS *= 2;

            LOG.warning("Could not allocate cavalry memory. Trying again...");
        } else if (ret < 0) {
            break;
        }
    }

    if (ret < 0) {
        throw runtime_error("Could not allocate " + to_string(size) +
                            " bytes cavalry memory (" + to_string(ret) +
                            "): " + strerror(errno));
    }

    return make_unique<Cavalry::Buffer>(fd, size, fdProps, virtAddr);
}

void Cavalry::flushCache(const int fd, const size_t size,
                         const int64_t offset) {
    if (static_cast<uint32_t>(offset) > size) {
        throw invalid_argument("offset is larger than total buffer size");
    }

    int ret = cavalry_mem_sync_cache_mfd(size - static_cast<size_t>(offset),
                                         static_cast<uint32_t>(offset), fd,
                                         true, false);
    if (ret < 0) {
        throw runtime_error("Could not flush cavalry memory cache (" +
                            to_string(ret) + ")");
    }
}

void Cavalry::invalidateCache(const int fd, const size_t size,
                              const int64_t offset) {
    if (static_cast<uint32_t>(offset) > size) {
        throw invalid_argument("offset is larger than total buffer size");
    }

    int ret = cavalry_mem_sync_cache_mfd(size - static_cast<size_t>(offset),
                                         static_cast<uint32_t>(offset), fd,
                                         false, true);
    if (ret < 0) {
        throw runtime_error("Could not invalidate cavalry memory cache (" +
                            to_string(ret) + ")");
    }
}

Cavalry::Buffer* Cavalry::castBuffer(const larod::Buffer& buf) {
    try {
        return &dynamic_cast<Cavalry::Buffer&>(const_cast<larod::Buffer&>(buf));
    } catch (const bad_cast&) {
        return nullptr;
    }
}

unique_ptr<Cavalry::Buffer>
    Cavalry::convertBufferPtr(unique_ptr<larod::Buffer>&& ptr) {
    if (!ptr) {
        return unique_ptr<Cavalry::Buffer>();
    }

    Cavalry::Buffer* destPtr = &dynamic_cast<Cavalry::Buffer&>(*ptr.get());

    // Release the input ptr without destroying the pointee.
    ptr.release();
    return unique_ptr<Cavalry::Buffer>(destPtr);
}

Cavalry::Buffer::Buffer(const int fd, const size_t maxSize,
                        const uint32_t fdProps, void* virtAddr)
    : larod::Buffer(FileDescriptor(std::move(fd)), maxSize, 0, fdProps),
      virtAddr(virtAddr) {
    cavFd = dup(fd);
    if (cavFd < 0) {
        throw FileDescriptorError("Could no dup cavalry fd (" +
                                  to_string(cavFd) + "): " + strerror(errno));
    }
}

Cavalry::Buffer::~Buffer() {
    // Close parent fd explicitly here (before parent's destructor) because
    // cavalry_mem_free_mfd() needs to be called after a regular close.
    fd.close();

    if (cavalry_mem_free_mfd(maxSize, cavFd, virtAddr)) {
        LOG.error("Could not free cavalry memory");
    }
}

void Cavalry::Buffer::flushCache() const {
    Cavalry::flushCache(fd, maxSize, startOffset);
}

void Cavalry::Buffer::invalidateCache() const {
    Cavalry::invalidateCache(fd, maxSize, startOffset);
}

} // namespace allocator
} // namespace larod
