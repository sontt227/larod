/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "argparse.h"

#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "larod.h"
#include "utils.h"

#define KEY_USAGE (127)

static int parseOpt(int key, char* arg, struct argp_state* state);

/**
 * @brief Parses a string as an unsigned long long.
 *
 * @param arg String to parse.
 * @param i Pointer to the number being the result of parsing.
 * @return Positive errno style return code (zero means success).
 */
static int parsePosInt(char* arg, unsigned long long* i);

const struct argp_option opts[] = {
    {"chip", 'c', "CHIP", 0,
     "Chooses chip CHIP to run tests on, where CHIP is the enum type "
     "larodChip from the library. If not specified, the default chip is the "
     "debug chip.",
     0},
    {"debug", 'd', NULL, 0, "Enable debug prints.", 0},
    {"model", 'g', "MODEL", 0,
     "Specifies from which path to load a model. Can be left blank if at least "
     "one model parameter is specified or if the selected chip is the debug "
     "chip",
     0},
    {"model-params", 'b', "KEY:VAL", 0,
     "Add additonal parameters when loading model (c.f. larodLoadModel()). "
     "KEY should be a string and VAL should either be a string, integer or an "
     "array of integers separated by \",\". For example "
     "\"image.input.format:rgb-interleaved\", \"image.input.row-pitch:224\" or "
     "\"image.input.size:224,224\".",
     0},
    {"job-params", 'e', "KEY:VAL", 0,
     "Add additional parameters when running a job (c.f. "
     "larodCreateJobRequest()). See option \"--model-params\" for a description"
     " of KEY and VAL.",
     0},
    {"help", 'h', NULL, 0, "Print this help text and exit.", 0},
    {"usage", KEY_USAGE, NULL, 0, "Print short usage message and exit.", 0},
    {0}};
const struct argp argp = {opts,
                          parseOpt,
                          NULL,
                          "Executes the Larod job request functional test "
                          "through Google test framework.",
                          NULL,
                          NULL,
                          NULL};

int parseArgs(int argc, char** argv, args_t* args) {
    int ret = argp_parse(&argp, argc, argv, ARGP_NO_HELP, NULL, args);
    if (ret) {
        return ret;
    }

    // Make sure a model or parameters are provided if not using debug chip.
    if (!args->model && !args->hasModelParams &&
        args->chip != LAROD_CHIP_DEBUG) {
        fprintf(stderr,
                "%s: Path to model file or model parameters must be specified "
                "when using other backend than DebugChip (use option "
                "\"--model\")\n",
                argv[0]);

        return EINVAL;
    }

    return 0;
}

int parseOpt(int key, char* arg, struct argp_state* state) {
    args_t* args = state->input;
    switch (key) {
    case 'c': {
        unsigned long long chip;
        int ret = parsePosInt(arg, &chip);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid chip type");
        }
        // Make sure we don't overflow when casting to enum below.
        if (chip > INT_MAX) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ERANGE,
                         "chip type value too large");
        }
        args->chip = (larodChip) chip;
        break;
    }
    case 'b': {
        int ret = addParam(args->modelParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to model parameters", arg);
        }
        args->hasModelParams = true;
        break;
    }
    case 'e': {
        int ret = addParam(args->jobParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to job parameters", arg);
        }
        break;
    }
    case 'd':
        args->debugPrints = true;
        break;
    case 'g':
        args->model = arg;
        break;
    case 'h':
        argp_state_help(state, stdout, ARGP_HELP_STD_HELP);
        break;
    case KEY_USAGE:
        argp_state_help(state, stdout, ARGP_HELP_USAGE | ARGP_HELP_EXIT_OK);
        break;
    case ARGP_KEY_INIT:
        args->model = NULL;
        args->chip = LAROD_CHIP_DEBUG;
        args->debugPrints = false;
        args->modelParams = NULL;
        args->jobParams = NULL;
        args->hasModelParams = false;

        args->modelParams = larodCreateMap(NULL);
        if (!args->modelParams) {
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not allocate model parameters");
        }

        args->jobParams = larodCreateMap(NULL);
        if (!args->jobParams) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not allocate job parameters");
        }
        break;
    case ARGP_KEY_END:
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static int parsePosInt(char* arg, unsigned long long* i) {
    char* endPtr;
    *i = strtoull(arg, &endPtr, 0);
    if (*endPtr != '\0') {
        return EINVAL;
    } else if (arg[0] == '-' || *i == 0) {
        return EINVAL;
    } else if (*i == ULLONG_MAX) {
        return ERANGE;
    }

    return 0;
}

void destroyArgs(args_t* args) {
    larodDestroyMap(&args->modelParams);
    larodDestroyMap(&args->jobParams);
}
