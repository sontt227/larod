/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

#include "larod.h"

typedef struct args_t {
    unsigned long long jobTimeLimit; // Milliseconds.
    unsigned long long rounds;
    char* modelFile;
    char** inputFiles;
    char** outputFiles;
    larodMap* modelParams;
    larodMap* jobParams;
    size_t numInputs;
    size_t numOutputs;
    size_t inputsArrLen;
    size_t outputsArrLen;
    char* modelName;
    char* csvPath;
    larodChip chip;
    larodAccess modelAccess;
    unsigned int sleepTime; // Milliseconds.
    bool async;
    bool debug;
    bool useDmaInput;
    bool useDmaOutput;
    bool forceMapInput;
    bool forceMapOutput;
    bool trackInputs;
    bool trackOutputs;
    bool timeJob;
    bool hasModelParams;
} args_t;

/**
 * @brief Parses command line arguments.
 *
 * This allocates necessary members in @c args_t.
 *
 * @param argc argc from main(int argc, char** argv).
 * @param argv argv from main(int argc, char** argv).
 * @param args Pointer to @c args_t. This will be filled in and allocated after
 * a successful call. Deallocate it with @c destroyArgs() when no longer needed.
 * @return Returns true if no errors occurred, otherwise false.
 */
bool parseArgs(int argc, char** argv, args_t* args);

/**
 * @brief Destructs arguments.
 *
 * This deallocates necessary members in @p args.
 *
 * @param args Arguments to destruct.
 */
void destroyArgs(args_t* args);
