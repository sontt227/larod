/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include "dmaoperations.h"
#include "dmautils.h"
#include "larod.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef LAROD_USE_CVFLOW

extern const FdOps cavalryOps;

#endif

#ifdef __cplusplus
}
#endif
