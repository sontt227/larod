/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <unordered_map>
#include <variant>

namespace larod {

using ParamsMap = std::unordered_map<
    std::string, std::variant<int64_t, std::string, std::vector<int64_t>>>;

} // namespace larod
