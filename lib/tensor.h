/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <systemd/sd-bus.h>

#include "error.h"
#include "larod.h"

/**
 * @brief D-Bus signature for tensor meta data.
 *
 * i: larodTensorDataType enum.
 * i: larodTensorLayout enum.
 * t: byte size.
 * at: dims array.
 * at: pitches array.
 * s: Tensor name.
 */
#define TENSOR_METADATA_FROM_SERVICE "iitatats"

/**
 * @brief D-Bus signature for tensor meta data.
 *
 * i: larodTensorDataType enum.
 * i: larodTensorLayout enum.
 * t: byte size.
 * at: dims array.
 * at: pitches array.
 */
#define TENSOR_METADATA_TO_SERVICE "iitatat"

/**
 * @brief D-Bus signature for tensor fd, its size and offset.
 *
 * h: File descriptor.
 * t: Size.
 * x: Offset.
 * t: ID.
 * u: File descriptor properties.
 */
#define TENSOR_BUFFERDATA "htxtu"

/**
 * Dummy value used to check for invalid tensor fdProps. It should track
 * the @c LAROD_FD_PROP* flags in larod.h to always be shifted one bit higher
 * than the highest bit of the valid flags.
 */
#define LAROD_FD_PROP_MAX (1UL << 3)

/**
 * @brief Type representing a tensor.
 */
struct larodTensor {
    /// File descriptor representing the data.
    int fd;
    /// Maximum accessible area in the fd.
    size_t fdSize;
    /// Offset from the beginning of the fd to the tensor data.
    int64_t fdOffset;
    /// Identifier to keep track of the buffer.
    uint64_t id;
    /// Properties of the fd.
    uint32_t fdProps;

    /// Struct containing tensor dimensions data.
    larodTensorDims dims;
    /// Struct containing tensor pitch data.
    larodTensorPitches pitches;
    /// Data type of the tensor data.
    larodTensorDataType dataType;
    /// Layout of the tensor.
    larodTensorLayout layout;
    /// Byte size of the tensor. A value of zero means that it is undefined.
    size_t byteSize;
    /// Name of tensor.
    char* name;
};

/**
 * @brief Read array of tensor meta data structs from D-Bus message.
 *
 * @param msg Message to read array of tensor meta data from.
 * @param numTensors Output pointer, will be filled with number of retrieved
 * tensors.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return NULL if any error occurred, otherwise a pointer to newly allocated
 * array of @c larodTensor pointers.
 */
larodTensor** tensorMetaDataArrayFromMsg(sd_bus_message* msg,
                                         size_t* numTensors,
                                         larodError** error);

/**
 * @brief Write tensors to sd-bus message.
 *
 * Write an array of larodTensor objects into a suitable D-Bus array.
 *
 * @param msg Message to write to.
 * @param tensors Tensors to write into message.
 * @param numTensors Number of entries in @p tensors array.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool appendTensorsToMsg(sd_bus_message* msg, larodTensor** tensors,
                        size_t numTensors, larodError** error);

/**
 * @brief Create identical tensor from an existing one.
 *
 * Allocate memory for a new tensor and copy all elements of @p tensor to
 * the new one, including the file descriptor value. It must be released
 * with @c larodDestroyTensor() when no longer needed.
 *
 * @param tensor Tensor to be duplicated.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return NULL if error occurred, otherwise a valid @c larodTensor pointer.
 */
larodTensor* copyTensor(const larodTensor* tensor, larodError** error);

/**
 * @brief Duplicate an array of tensors.
 *
 * Create a new array of tensor pointers and fill it with copies of each tensor
 * in the original array.
 *
 * @param sourceTensors Array of tensor objects to duplicate.
 * @param numSrcTensors Number of tensors in the array.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return NULL if error occurred, otherwise a valid array of @c larodTensor
 * objects.
 */
larodTensor** copyTensorArray(larodTensor** sourceTensors, size_t numSrcTensors,
                              larodError** error);

/**
 * @brief Validate tensors.
 *
 * @param tensors List of tensors to validate.
 * @param numTensors Number of tensors in @p tensors.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return True if @p tensors does not contain any errors, otherwise false.
 */
bool validateTensors(larodTensor** tensors, const size_t numTensors,
                     larodError** error);

/**
 * @brief Set fd props of a list of tensors.
 *
 * @param tensors List of tensors to set fd props on.
 * @param numTensors Number of tensors in @p tensors.
 * @param fdProps An fdProps bitmask to set on all the @p tensors.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return True if successfully set @p fdProps on all @p tensors , otherwise
 * false.
 */
bool setTensorsFdProps(larodTensor** tensors, const size_t numTensors,
                       const uint32_t fdProps, larodError** error);
