/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <libgen.h>
#include <limits.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#ifdef READLINE
#include <readline/history.h>
#include <readline/readline.h>
#include <wordexp.h>
#endif // READLINE

#include "argparse.h"
#include "larod.h"
#include "log.h"
#include "version.h"

#ifdef READLINE
void interactive(larodConnection* conn);
#endif

// Used to declare that loadModelAsync has failed.
#define INVALID_MODEL_ADDRESS (-1)

bool processArgs(args_t args, larodConnection* conn);

bool listChips(larodConnection* conn);
bool getNbrOfSessions(larodConnection* conn);
bool listModels(larodConnection* conn);
bool deleteModel(larodConnection* conn, uint64_t modelId);
bool loadModel(larodConnection* conn, char* modelFile, larodMap* params,
               const larodChip chip, const larodAccess access,
               const char* modelName, bool async, larodModel** model);
bool runJob(larodConnection* conn, larodModel* model, larodMap* params,
            char** inputFiles, size_t numUserInputs, char** outputFiles,
            size_t numUserOutputs, bool async);

bool debug = false;
larodError* error = NULL;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

const char* const DATA_TYPES[] = {
    "LAROD_TENSOR_DATA_TYPE_INVALID", "LAROD_TENSOR_DATA_TYPE_UNSPECIFIED",
    "LAROD_TENSOR_DATA_TYPE_BOOL",    "LAROD_TENSOR_DATA_TYPE_UINT8",
    "LAROD_TENSOR_DATA_TYPE_INT8",    "LAROD_TENSOR_DATA_TYPE_UINT16",
    "LAROD_TENSOR_DATA_TYPE_INT16",   "LAROD_TENSOR_DATA_TYPE_UINT32",
    "LAROD_TENSOR_DATA_TYPE_INT32",   "LAROD_TENSOR_DATA_TYPE_UINT64",
    "LAROD_TENSOR_DATA_TYPE_INT64",   "LAROD_TENSOR_DATA_TYPE_FLOAT16",
    "LAROD_TENSOR_DATA_TYPE_FLOAT32", "LAROD_TENSOR_DATA_TYPE_FLOAT64",
};
const char* const LAYOUTS[] = {
    "LAROD_TENSOR_LAYOUT_INVALID", "LAROD_TENSOR_LAYOUT_UNSPECIFIED",
    "LAROD_TENSOR_LAYOUT_NHWC", "LAROD_TENSOR_LAYOUT_NCHW",
    "LAROD_TENSOR_LAYOUT_420SP"};

int main(int argc, char** argv) {
    larodConnection* conn = NULL;
    bool ret = false;

    args_t args;
    ret = parseArgs(argc, argv, &args, false);
    if (!ret) {
        goto end;
    }

    debug = args.debug;

    // Connect
    logInfo("Connecting to larod...");

    if (!larodConnect(&conn, &error)) {
        ret = false;
        logError("Could not connect to larod (%d): %s", error->code,
                 error->msg);
        larodClearError(&error);

        goto end;
    }

    logInfo("Connected");

#ifdef READLINE
    if (args.interactive) {
        interactive(conn);
        goto end;
    }
#endif

    ret = processArgs(args, conn);

end:
    // Disconnect
    if (conn && !larodDisconnect(&conn, &error)) {
        ret = false;
        logError("Could not disconnect from larod (%d): %s", error->code,
                 error->msg);
        larodClearError(&error);
    }

    destroyArgs(&args);

    return ret == false ? EXIT_FAILURE : EXIT_SUCCESS;
}

bool processArgs(args_t args, larodConnection* conn) {
    larodModel* loadedModel = NULL;

    // List chips
    if (args.listChips) {
        listChips(conn);
    }

    // Get number of sessions
    if (args.getSessions) {
        getNbrOfSessions(conn);
    }

    // Delete model
    if (args.deleteModelId != ULLONG_MAX) {
        if (!deleteModel(conn, args.deleteModelId)) {
            return false;
        }
    }

    // Load model
    if (args.modelFile || args.hasModelParams) {
        if (!loadModel(conn, args.modelFile, args.modelParams, args.chip,
                       args.modelAccess, args.modelName, args.asyncLoadModel,
                       &loadedModel)) {
            return false;
        }
    }

    // List models
    if (args.listModels) {
        listModels(conn);
    }

    // Run job
    if (args.numInputs) {
        if (args.inferModelId != ULLONG_MAX) {
            loadedModel = larodGetModel(conn, args.inferModelId, &error);
            if (!loadedModel) {
                logError("When getting model (%d): %s", error->code,
                         error->msg);
                larodClearError(&error);

                return false;
            }
        }

        if (!runJob(conn, loadedModel, args.jobParams, args.inputFiles,
                    args.numInputs, args.outputFiles, args.numOutputs,
                    args.asyncInfer)) {
            larodDestroyModel(&loadedModel);

            return false;
        }
    }

    larodDestroyModel(&loadedModel);

    return true;
}

bool listChips(larodConnection* conn) {
    logDebug(debug, "Requesting list of chips...");
    larodChip* chips = NULL;
    size_t numChips = 0;
    if (larodListChips(conn, &chips, &numChips, &error)) {
        logInfo("Chips:");
        for (size_t i = 0; i < numChips; ++i) {
            printf("%zu: %s [larodChip %d]\n", i, larodGetChipName(chips[i]),
                   chips[i]);
        }
        free(chips);
    } else {
        logError("Could not list chips (%d): %s", error->code, error->msg);
        larodClearError(&error);

        return false;
    }

    return true;
}

bool getNbrOfSessions(larodConnection* conn) {
    logDebug(debug, "Requesting number of sessions...");
    uint64_t nbrOfSessions;
    if (larodGetNumSessions(conn, &nbrOfSessions, &error)) {
        logInfo("Number of sessions: %" PRIu64, nbrOfSessions);
    } else {
        logError("When getting number of sessions (%d): %s", error->code,
                 error->msg);
        larodClearError(&error);

        return false;
    }

    return true;
}

bool listModels(larodConnection* conn) {
    logDebug(debug, "Requesting list of models...");
    bool ret = false;
    size_t numModels;

    larodModel** models = larodGetModels(conn, &numModels, &error);
    if (!models) {
        logError("Could not list models (%d): %s", error->code, error->msg);

        goto end;
    }

    logInfo("Models:");
    printf("_______________________________________________________________"
           "_________________\n");
    printf("%17s %18s %9s %17s %10s\n", "Name", "ID", "Chip", "Size (bytes)",
           "Access");
    printf("‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾"
           "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾\n");
    for (size_t i = 0; i < numModels; ++i) {
        const char* name = larodGetModelName(models[i], &error);
        if (!name) {
            logError("Could not get model name (%d): %s", error->code,
                     error->msg);

            goto end;
        }
        const uint64_t id = larodGetModelId(models[i], &error);
        if (id == LAROD_INVALID_MODEL_ID) {
            logError("Could not get model id (%d): %s", error->code,
                     error->msg);

            goto end;
        }

        const size_t size = larodGetModelSize(models[i], &error);
        if (error) {
            logError("Could not get model size (%d): %s", error->code,
                     error->msg);

            goto end;
        }

        const larodAccess access = larodGetModelAccess(models[i], &error);
        if (access == LAROD_ACCESS_INVALID) {
            logError("Could not get model access (%d): %s", error->code,
                     error->msg);

            goto end;
        }

        const larodChip chip = larodGetModelChip(models[i], &error);
        if (chip == LAROD_CHIP_INVALID) {
            logError("Could not get model chip (%d): %s", error->code,
                     error->msg);

            goto end;
        }

#define MAX_NAME_LEN 31
        // Truncate name if too long.
        if (strnlen(name, MAX_NAME_LEN + 1) <= MAX_NAME_LEN) {
            printf(" %-*.*s", MAX_NAME_LEN, MAX_NAME_LEN, name);
        } else {
            printf(" %-*.*s...", MAX_NAME_LEN - 3, MAX_NAME_LEN - 3, name);
        }

        printf(" %3" PRIu64 " %8d %16zu       %-12s\n", id, chip, size,
               access == LAROD_ACCESS_PRIVATE ? "private" : "public");
    }
    printf("_______________________________________________________________"
           "_________________\n\n");

    ret = true;

end:
    larodDestroyModels(&models, numModels);
    larodClearError(&error);

    return ret;
}

void printTensorInfo(larodTensor** tensors, size_t numTensors) {
    larodError* error = NULL;

    for (size_t i = 0; i < numTensors; i++) {
        larodTensor* tensor = tensors[i];
        const larodTensorDims* dims = larodGetTensorDims(tensor, &error);
        if (!dims) {
            logError("Could not get dimensions of tensor (%d): %s", error->code,
                     error->msg);

            goto end;
        }

        const larodTensorPitches* pitches =
            larodGetTensorPitches(tensor, &error);
        if (!pitches) {
            logError("Could not get pitches of tensor (%d): %s", error->code,
                     error->msg);

            goto end;
        }

        larodTensorDataType dataType = larodGetTensorDataType(tensor, &error);
        if (dataType == LAROD_TENSOR_DATA_TYPE_INVALID) {
            logError("Could not get data type of tensor (%d): %s", error->code,
                     error->msg);

            goto end;
        }

        larodTensorLayout layout = larodGetTensorLayout(tensor, &error);
        if (layout == LAROD_TENSOR_LAYOUT_INVALID) {
            logError("Could not get layout of tensor (%d): %s", error->code,
                     error->msg);

            goto end;
        }

        const char* tensorName = larodGetTensorName(tensor, &error);
        if (!tensorName) {
            logError("Could not get name of tensor (%d): %s", error->code,
                     error->msg);

            goto end;
        }

        logDebug(debug,
                 "  name = \"%s\", dataType = %s, layout = %s, dims.len = %zu",
                 tensorName, DATA_TYPES[dataType], LAYOUTS[layout], dims->len);
        for (size_t j = 0; j < dims->len; j++) {
            logDebug(debug,
                     "    Dim %zu: size = %zu element(s), pitch = %zu byte(s)",
                     j, dims->dims[j], pitches->pitches[j]);
        }
    }

end:
    larodClearError(&error);
}

void printModelInfo(const larodModel* model) {
    larodError* error = NULL;
    size_t numInputTensors = 0;
    size_t numOutputTensors = 0;
    larodTensor** inputTensors = NULL;
    larodTensor** outputTensors = NULL;

    // Dump inputs.
    inputTensors = larodCreateModelInputs(model, &numInputTensors, &error);
    if (!inputTensors) {
        logError("Could not create model inputs (%d): %s", error->code,
                 error->msg);

        goto end;
    }

    logDebug(debug, "Model has %zu input tensor(s):", numInputTensors);
    printTensorInfo(inputTensors, numInputTensors);

    // Dump outputs.
    outputTensors = larodCreateModelOutputs(model, &numOutputTensors, &error);
    if (!outputTensors) {
        logError("Could not create model outputs (%d): %s", error->code,
                 error->msg);

        goto end;
    }

    logDebug(debug, "Model has %zu output tensor(s):", numOutputTensors);
    printTensorInfo(outputTensors, numOutputTensors);

end:
    larodDestroyTensors(&inputTensors, numInputTensors);
    larodDestroyTensors(&outputTensors, numOutputTensors);
    larodClearError(&error);
}

static void loadModelDone(larodModel* model, void* userData,
                          larodError* error) {
    pthread_mutex_lock(&mutex);

    assert(userData);

    if (error) {
        logError("Asynchronous model load failure: %s", error->msg);
        *(larodModel**) userData = (void*) INVALID_MODEL_ADDRESS;
    } else {
        logDebug(debug, "Asynchronous model load success");
        *(larodModel**) userData = model;
    }
    pthread_cond_broadcast(&cond);

    pthread_mutex_unlock(&mutex);
}

bool loadModel(larodConnection* conn, char* modelFile, larodMap* params,
               const larodChip chip, const larodAccess access,
               const char* modelName, bool async, larodModel** model) {
    bool ret = true;
    int fd = -1;
    FILE* fpModel = NULL;
    logDebug(debug, "Loading model...");

    if (modelFile) {
        fpModel = fopen(modelFile, "rb");
        if (!fpModel) {
            logError("Could not open model file %s: %s", modelFile,
                     strerror(errno));

            ret = false;
            goto end;
        }

        fd = fileno(fpModel);
        if (fd < 0) {
            logError("Could not get file descriptor for model file: %s",
                     strerror(errno));
            fclose(fpModel);

            ret = false;
            goto end;
        }

        if (!modelName) {
            modelName = basename(modelFile);
        }
    } else if (!modelName) {
        modelName = "params-model";
    }

    if (!async) {
        *model =
            larodLoadModel(conn, fd, chip, access, modelName, params, &error);
        if (!*model) {
            logError("When loading model synchronously (%d): %s", error->code,
                     error->msg);

            ret = false;
            goto end;
        }
    } else {
        if (!larodLoadModelAsync(conn, fd, chip, access, modelName, params,
                                 loadModelDone, (void*) model, &error)) {
            logError("When loading model asynchronously (%d): %s", error->code,
                     error->msg);

            ret = false;
            goto end;
        }

        pthread_mutex_lock(&mutex);

        // Wait for load model callback.
        logDebug(debug, "Load model queued, waiting for results...");
        while (!*model) {
            pthread_cond_wait(&cond, &mutex);
        }

        pthread_mutex_unlock(&mutex);

        if (*model == (void*) INVALID_MODEL_ADDRESS) {
            *model = NULL;

            ret = false;
            goto end;
        }
    }

    logInfo("Model %s loaded", modelName);

    if (debug) {
        printModelInfo(*model);
    }

end:
    if (fpModel) {
        if (fclose(fpModel)) {
            logWarning("Could not close file %s", modelFile);
        }
    }
    larodClearError(&error);

    return ret;
}

bool deleteModel(larodConnection* conn, uint64_t modelId) {
    logDebug(debug, "Deleting model with ID %" PRIu64 "...", modelId);

    bool ret = false;

    larodModel* loadedModel = larodGetModel(conn, modelId, &error);
    if (!loadedModel) {
        logError("When getting model (%d): %s", error->code, error->msg);

        goto end;
    }

    if (larodDeleteModel(conn, loadedModel, &error)) {
        logInfo("Deleted model with ID %" PRIu64, modelId);
    } else {
        logError("When deleting model with ID %" PRIu64 " (%d): %s", modelId,
                 error->code, error->msg);

        goto end;
    }

    ret = true;

end:
    larodDestroyModel(&loadedModel);
    larodClearError(&error);

    return ret;
}

static void runJobDone(void* jobDone, larodError* error) {
    pthread_mutex_lock(&mutex);

    assert(jobDone);

    *(bool*) jobDone = true;
    if (error) {
        logError("Asynchronous job failure: %s", error->msg);
    } else {
        logDebug(debug, "Asynchronous job success");
    }
    pthread_cond_broadcast(&cond);

    pthread_mutex_unlock(&mutex);
}

bool runJob(larodConnection* conn, larodModel* model, larodMap* params,
            char** userInputFiles, size_t numUserInputs, char** userOutputFiles,
            size_t numUserOutputs, bool async) {
    logDebug(debug, "Running job...");

    bool ret = false;
    larodJobRequest* jobReq = NULL;
    larodTensor** inputTensors = NULL;
    larodTensor** outputTensors = NULL;
    size_t numInputs = 0;
    size_t numOutputs = 0;
    FILE** fpInputs = NULL;
    FILE** fpOutputs = NULL;
    char** outputFiles = NULL;
    size_t* outputByteSizes = NULL;

    inputTensors = larodCreateModelInputs(model, &numInputs, &error);
    if (!inputTensors) {
        logError("Could not create input tensor (%d): %s", error->code,
                 error->msg);

        goto end;
    }
    if (numUserInputs != numInputs) {
        logError("Provided %zu inputs but model requires %zu", numUserInputs,
                 numInputs);

        goto end;
    }

    outputTensors = larodCreateModelOutputs(model, &numOutputs, &error);
    if (!outputTensors) {
        logError("Could not create output tensor (%d): %s", error->code,
                 error->msg);

        goto end;
    }
    if (numUserOutputs > numOutputs) {
        logError("Provided %zu outputs but model only has %zu", numUserOutputs,
                 numOutputs);

        goto end;
    }

    outputFiles = calloc(numOutputs, sizeof(char*));
    if (!outputFiles) {
        logError("Could not allocate memory for output files array");

        goto end;
    }

    for (size_t i = 0; i < numUserOutputs; ++i) {
        outputFiles[i] = malloc(1 + strlen(userOutputFiles[i]));
        if (!outputFiles[i]) {
            logError("Could not allocate memory for output file name");

            goto end;
        }

        strcpy(outputFiles[i], userOutputFiles[i]);
    }

    for (size_t i = numUserOutputs; i < numOutputs; ++i) {
        // Files were not specified for all output tensors. Use the first input
        // file's name with suffix ".outX" for integers X = numUserOutputs,
        // numUserOutputs + 1,...
        const size_t MAX_FILE_SUFFIX_LEN = 32;
        char fileSuffix[MAX_FILE_SUFFIX_LEN];
        int len = snprintf(fileSuffix, MAX_FILE_SUFFIX_LEN, ".out%zu", i);
        if (len < 0) {
            logError("Could not encode file suffix string");

            goto end;
        } else if ((size_t) len >= MAX_FILE_SUFFIX_LEN) {
            logError("File suffix buffer is too small (need %d)", len + 1);

            goto end;
        }

        const char* inputBaseName = basename(userInputFiles[0]);

        outputFiles[i] = malloc(1 + strlen(inputBaseName) + strlen(fileSuffix));
        if (!outputFiles[i]) {
            logError("Could not allocate memory for output file name");

            goto end;
        }

        strcpy(outputFiles[i], inputBaseName);
        strcat(outputFiles[i], fileSuffix);
    }

    fpInputs = calloc(numInputs, sizeof(FILE*));
    if (!fpInputs) {
        logError("Could not allocate memory for input files array");

        goto end;
    }
    fpOutputs = calloc(numOutputs, sizeof(FILE*));
    if (!fpOutputs) {
        logError("Could not allocate memory for output files array");

        goto end;
    }

    for (size_t i = 0; i < numInputs; ++i) {
        assert(userInputFiles[i]);

        fpInputs[i] = fopen(userInputFiles[i], "rb");
        if (!fpInputs[i]) {
            logError("Could not open input file %s: %s", userInputFiles[i],
                     strerror(errno));

            goto end;
        }

        const int inFd = fileno(fpInputs[i]);
        if (inFd < 0) {
            logError("Could not get file descriptor for input file: %s",
                     strerror(errno));

            goto end;
        }

        if (!larodSetTensorFd(inputTensors[i], inFd, &error)) {
            logError("Could not set input tensor fd (%d): %s", error->code,
                     error->msg);

            goto end;
        }

        if (!larodSetTensorFdProps(inputTensors[i],
                                   LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP,
                                   &error)) {
            logError("Could not set input tensor fd props (%d): %s",
                     error->code, error->msg);

            goto end;
        }
    }

    outputByteSizes = larodGetModelOutputByteSizes(model, NULL, &error);
    if (!outputByteSizes) {
        logError("Could not get byte sizes of output tensors (%d): %s",
                 error->code, error->msg);

        goto end;
    }

    for (size_t i = 0; i < numOutputs; ++i) {
        assert(outputFiles[i]);

        fpOutputs[i] = fopen(outputFiles[i], "w+b");
        if (!fpOutputs[i]) {
            logError("Could not open output file %s: %s", outputFiles[i],
                     strerror(errno));

            goto end;
        }

        const int outFd = fileno(fpOutputs[i]);
        if (outFd < 0) {
            logError("Could not get file descriptor for output file: %s",
                     strerror(errno));

            goto end;
        }

        // Truncate outputs to make mmap work on them from the service.
        if (ftruncate(outFd, (off_t) outputByteSizes[i])) {
            logError("Truncating output file to %zu length failed: %s",
                     outputByteSizes[i], strerror(errno));

            goto end;
        }

        if (!larodSetTensorFd(outputTensors[i], outFd, &error)) {
            logError("Could not set output tensor fd (%d): %s", error->code,
                     error->msg);

            goto end;
        }

        if (!larodSetTensorFdProps(outputTensors[i],
                                   LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP,
                                   &error)) {
            logError("Could not set output tensor fd props (%d): %s",
                     error->code, error->msg);

            goto end;
        }
    }

    jobReq = larodCreateJobRequest(model, inputTensors, numInputs,
                                   outputTensors, numOutputs, params, &error);
    if (!jobReq) {
        logError("Could not create job request (%d): %s", error->code,
                 error->msg);

        goto end;
    }

    if (!async) {
        if (!larodRunJob(conn, jobReq, &error)) {
            logError("When running job synchronously (%d): %s", error->code,
                     error->msg);

            goto end;
        }
    } else {
        bool jobDone = false;
        if (!larodRunJobAsync(conn, jobReq, runJobDone, (void*) &jobDone,
                              &error)) {
            logError("When running job asynchronously (%d): %s", error->code,
                     error->msg);
            larodClearError(&error);
            goto end;
        }

        pthread_mutex_lock(&mutex);

        // Wait for job callback.
        logDebug(debug, "Job queued, waiting for results...");
        while (!jobDone) {
            pthread_cond_wait(&cond, &mutex);
        }

        pthread_mutex_unlock(&mutex);
    }

    logInfo("Output written to:");
    for (size_t i = 0; i < numOutputs; ++i) {
        logInfo("\t%s", outputFiles[i]);
    }

    ret = true;

end:
    if (fpInputs) {
        for (size_t i = 0; i < numInputs; ++i) {
            if (fpInputs[i] && fclose(fpInputs[i])) {
                logWarning("Could not close file %s", userInputFiles[i]);
            }
        }
        free(fpInputs);
    }
    if (fpOutputs) {
        for (size_t i = 0; i < numOutputs; ++i) {
            if (fpOutputs[i] && fclose(fpOutputs[i])) {
                logWarning("Could not close file %s", outputFiles[i]);
            }
        }
        free(fpOutputs);
    }

    if (outputFiles) {
        for (size_t i = 0; i < numOutputs; ++i) {
            free(outputFiles[i]);
        }
        free(outputFiles);
    }

    larodDestroyTensors(&inputTensors, numInputs);
    larodDestroyTensors(&outputTensors, numOutputs);
    larodDestroyJobRequest(&jobReq);
    larodClearError(&error);
    free(outputByteSizes);

    return ret;
}

#ifdef READLINE
void interactive(larodConnection* conn) {
    logInfo("larod-client %s. Use Ctrl-D to exit.", LAROD_VERSION_STR);

    char* prog = "larod-client";
    char* buf;
    wordexp_t wordExp;
    int ret;
    args_t args;
    while ((buf = readline("> "))) {
        if (!strlen(buf)) {
            free(buf);
            continue;
        }

        add_history(buf);

        wordExp.we_offs = 1; // Reserve space for program name (argv[0])
        if ((ret = wordexp(buf, &wordExp,
                           WRDE_DOOFFS | WRDE_NOCMD | WRDE_SHOWERR))) {
            switch (ret) {
            case WRDE_BADCHAR:
                logError("One of the unquoted characters <newline>, '|', '&', "
                         "';', '<', '>', '(', ')', '{' or '}' appears in an "
                         "inappropriate context");
                break;
            case WRDE_CMDSUB:
                logError("Command substitution is not supported");
                break;
            case WRDE_NOSPACE:
                logError("Memory allocation failed");
                break;
            default:
                logError("Unknown syntax error");
            }

            goto end;
        }

        wordExp.we_wordv[0] = prog;          // Set program name in argv[0]
        wordExp.we_wordc += wordExp.we_offs; // Update argc accordingly

        if (!parseArgs((int) wordExp.we_wordc, wordExp.we_wordv, &args, true)) {
            goto end;
        }

        processArgs(args, conn);

    end:
        destroyArgs(&args);
        wordfree(&wordExp);
        free(buf);
    }
}
#endif // READLINE
