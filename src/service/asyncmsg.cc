/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "asyncmsg.hh"

using namespace std;

namespace larod {

AsyncMsg::AsyncMsg(weak_ptr<Session> session) : Msg(session) {
}

} // namespace larod
