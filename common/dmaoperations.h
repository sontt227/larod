/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include "dmautils.h"

#ifdef __cplusplus
extern "C" {
#endif

// Functions to be implemented for each buffer type.
typedef struct FdOps {
    bool (*initFramework)();
    void (*exitFramework)();
    bool (*allocFd)(size_t size, DmaBuffer* buffer);
    bool (*freeFd)(DmaBuffer* buffer);
    bool (*writeData)(void* data, size_t size, DmaBuffer* buffer);
} FdOps;

#ifdef __cplusplus
}
#endif
