/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <libgen.h>
#include <limits.h>
#include <pthread.h>
#include <signal.h>
#include <stdatomic.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "argparse.h"
#include "dmautils.h"
#include "larod.h"
#include "log.h"

#define NSEC_PER_SEC (1e9L)
#define NSEC_PER_MSEC (1e6L)

static void signalHandler(int sig);

bool loadModel(larodConnection* conn, char* modelFile, const larodChip chip,
               const larodAccess access, const char* modelName,
               larodModel** model, larodMap* params);
bool runJob(larodConnection* conn, const larodJobRequest* const jobReq,
            unsigned long long rounds, FILE* csvFile);
bool runJobAsync(larodConnection* conn, const larodJobRequest* const jobReq,
                 const unsigned long long rounds, const unsigned int sleepTime,
                 FILE* csvFile);
void jobCallback(void* data, larodError* error);
bool logTimeToCSV(FILE* csvFile, unsigned long long jobTimeUs);

bool debug = false;
volatile sig_atomic_t isRunning = false;
static unsigned long long totalJobTimeNs = 0;
static unsigned long long nbrCallbacksDone = 0;

// This struct contains an approximate time stamp for when the currently
// executing job started. This variable is updated, for each job request, in one
// of the two following ways:
//   1. From inside runJobAsync(). This occurs if the previous job has finished
//      before the client has woken up to send the next job request.
//   2. From the previous job's callback function, jobCallback(). This occurs if
//      the client has already sent the request for the next job. This is based
//      on the assumption that the next job will start immediately after the one
//      before finished.
static struct timespec nextJobStartTime;

pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condVar = PTHREAD_COND_INITIALIZER;

static void signalHandler(int sig) {
    static volatile sig_atomic_t signalHandled = 0;
    if (signalHandled) {
        signal(sig, SIG_DFL);
        raise(sig);

        return;
    }

    signalHandled = 1;

    isRunning = false;
}

static bool supplementOutputArgs(args_t* args, size_t numOutputs,
                                 size_t* allocedOutputFiles) {
    // Files were not specified for all output tensors. Use the first input
    // file's name with suffix ".outX" for integers X = numUserOutputs,
    // numUserOutputs + 1,...
    char** newOutputFiles =
        realloc(args->outputFiles, numOutputs * sizeof(char*));
    if (!newOutputFiles) {
        logError("Could not allocate memory for output file names array");

        return false;
    }
    args->outputFiles = newOutputFiles;

    const size_t MAX_FILE_SUFFIX_LEN = 32;
    char fileSuffix[MAX_FILE_SUFFIX_LEN];
    for (size_t i = args->numOutputs; i < numOutputs; ++i) {
        int len = snprintf(fileSuffix, MAX_FILE_SUFFIX_LEN, ".out%zu", i);
        if (len < 0) {
            logError("Could not encode file suffix string");

            return false;
        } else if ((size_t) len >= MAX_FILE_SUFFIX_LEN) {
            logError("File suffix buffer is too small (need %d)", len + 1);

            return false;
        }

        const char* inputBaseName = basename(args->inputFiles[0]);

        args->outputFiles[i] =
            malloc(1 + strlen(inputBaseName) + strlen(fileSuffix));
        if (!args->outputFiles[i]) {
            logError("Could not allocate memory for output file name");

            return false;
        }
        *allocedOutputFiles += 1;

        strcpy(args->outputFiles[i], inputBaseName);
        strcat(args->outputFiles[i], fileSuffix);
    }

    return true;
}

typedef struct Tensors {
    larodTensor** tensors;
    size_t num;
    size_t* byteSizes;
    FILE** files;
    DmaBuffer** dma;
} Tensors;

static void freeTensors(Tensors* tensors, args_t* args) {
    // Close files
    if (tensors->files) {
        for (size_t i = 0; i < tensors->num; ++i) {
            if (tensors->files[i] && fclose(tensors->files[i])) {
                logWarning("Could not close file %s", args->inputFiles[i]);
            }
        }
        free(tensors->files);
        tensors->files = NULL;
    }

    if (tensors->dma) {
        for (size_t i = 0; i < tensors->num; ++i) {
            freeDmaBuffer(tensors->dma[i]);
        }
        free(tensors->dma);
        tensors->dma = NULL;
    }

    if (tensors->byteSizes) {
        free(tensors->byteSizes);
        tensors->byteSizes = NULL;
    }

    larodDestroyTensors(&tensors->tensors, tensors->num);
}

static bool prepareInputs(Tensors* inputs, args_t* args, larodModel* model,
                          larodError** error) {
    inputs->tensors = larodCreateModelInputs(model, &inputs->num, error);
    if (!inputs->tensors) {
        logError("Could not create input tensor (%d): %s", (*error)->code,
                 (*error)->msg);

        return false;
    }

    if (args->numInputs != inputs->num) {
        logError("Provided %zu inputs but model requires %zu", args->numInputs,
                 inputs->num);

        return false;
    }

    if (args->useDmaInput) {
        inputs->dma = calloc(inputs->num, sizeof(DmaBuffer*));
        if (!inputs->dma) {
            logError("Could not allocate memory for input-buffer array");

            return false;
        }
    } else {
        inputs->files = calloc(inputs->num, sizeof(FILE*));
        if (!inputs->files) {
            logError("Could not allocate memory for input-files array");

            return false;
        }
    }

    inputs->byteSizes = larodGetModelInputByteSizes(model, NULL, error);
    if (!inputs->byteSizes) {
        logError("Could not allocate get input byte sizes (%d): %s",
                 (*error)->code, (*error)->msg);

        return false;
    }

    for (size_t i = 0; i < inputs->num; ++i) {
        assert(args->inputFiles[i]);

        int inFd = -1;
        uint32_t inFdProps = 0;
        size_t inFdSize = 0;

        if (args->useDmaInput) {
            logDebug(debug, "Creating dma buffers for input tensors");
            inputs->dma[i] =
                allocDmaBufferFromFile(args->inputFiles[i], args->chip);
            if (!inputs->dma[i]) {
                logError("Could not copy input to dma buffer: %s",
                         strerror(errno));

                return false;
            }

            inFd = inputs->dma[i]->fd;
            inFdSize = inputs->dma[i]->size;
            inFdProps = LAROD_FD_PROP_MAP | LAROD_FD_PROP_DMABUF;
        } else {
            // Regular fd.
            inputs->files[i] = fopen(args->inputFiles[i], "rb");
            if (!inputs->files[i]) {
                logError("Could not open input file %s: %s",
                         args->inputFiles[i], strerror(errno));

                return false;
            }

            inFd = fileno(inputs->files[i]);
            if (inFd < 0) {
                logError("Could not get file descriptor for input file: %s",
                         strerror(errno));

                return false;
            }

            inFdProps = LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP;

            // Set size of fd in input tensor. If left unspecified, the size of
            // a input tensor's fd will be retrieved in larodRunJobAsync(). This
            // is problematic if one quickly queues up job requests containing
            // input tensors with the same fd; since then the lib and service
            // can perform a lseek/read at the same time.  For debug chip, we
            // set it to what the model expects so that one can give it any file
            // as input.
            if (args->chip == LAROD_CHIP_DEBUG) {
                inFdSize = inputs->byteSizes[i];
            } else {
                off_t filePos = lseek(inFd, 0, SEEK_END);
                if (filePos < 0) {
                    logError("Could not get file size of fd: %s",
                             strerror(errno));

                    return false;
                }

                inFdSize = (size_t) filePos;
            }
        }

        if (args->forceMapInput) {
            inFdProps = LAROD_FD_PROP_MAP;
        }

        if (!larodSetTensorFd(inputs->tensors[i], inFd, error)) {
            logError("Could not set input tensor fd (%d): %s", (*error)->code,
                     (*error)->msg);

            return false;
        }
        if (!larodSetTensorFdProps(inputs->tensors[i], inFdProps, error)) {
            logError("Could not set input tensor props (%d): %s",
                     (*error)->code, (*error)->msg);

            return false;
        }

        if (!larodSetTensorFdSize(inputs->tensors[i], inFdSize, error)) {
            logError("Could not set input tensor fd size (%d): %s",
                     (*error)->code, (*error)->msg);

            return false;
        }
    }

    return true;
}

static bool prepareOutputs(Tensors* outputs, args_t* args, larodModel* model,
                           larodError** error, size_t* allocedOutputFiles) {
    outputs->tensors = larodCreateModelOutputs(model, &outputs->num, error);
    if (!outputs->tensors) {
        logError("Could not create output tensor (%d): %s", (*error)->code,
                 (*error)->msg);

        return false;
    }

    if (args->numOutputs < outputs->num) {
        if (!supplementOutputArgs(args, outputs->num, allocedOutputFiles)) {
            return false;
        }
    } else if (args->numOutputs != outputs->num) {
        logError("Provided %zu outputs but model requires %zu",
                 args->numOutputs, outputs->num);

        return false;
    }

    outputs->files = calloc(outputs->num, sizeof(FILE*));
    if (!outputs->files) {
        logError("Could not allocate memory for output-files array");

        return false;
    }

    if (args->useDmaOutput) {
        outputs->dma = calloc(outputs->num, sizeof(DmaBuffer*));
        if (!outputs->dma) {
            logError("Could not allocate memory for output-buffer array");

            return false;
        }
    }

    outputs->byteSizes = larodGetModelOutputByteSizes(model, NULL, error);
    if (!outputs->byteSizes) {
        logError("Could not allocate get output byte sizes (%d): %s",
                 (*error)->code, (*error)->msg);

        return false;
    }

    for (size_t i = 0; i < outputs->num; ++i) {
        assert(args->outputFiles[i]);

        outputs->files[i] = fopen(args->outputFiles[i], "w+b");
        if (!outputs->files[i]) {
            logError("Could not open output file %s: %s", args->outputFiles[i],
                     strerror(errno));

            return false;
        }

        int outFd = -1;
        uint32_t outFdProps = 0;

        if (args->useDmaOutput) {
            logDebug(debug, "Creating dma buffers for output tensors");
            outputs->dma[i] = allocDmaBuffer(outputs->byteSizes[i], args->chip);
            if (!outputs->dma[i]) {
                logError("Could not allocate dma buffer: %s", strerror(errno));

                return false;
            }

            outFd = outputs->dma[i]->fd;
            outFdProps = LAROD_FD_PROP_MAP | LAROD_FD_PROP_DMABUF;
        } else {
            outFd = fileno(outputs->files[i]);
            if (outFd < 0) {
                logError("Could not get file descriptor for output file: %s",
                         strerror(errno));

                return false;
            }

            if (ftruncate(outFd, (off_t) outputs->byteSizes[i])) {
                logError("Could not truncate output file: %s", strerror(errno));

                return false;
            }

            outFdProps = LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP;
        }

        if (args->forceMapOutput) {
            outFdProps = LAROD_FD_PROP_MAP;
        }

        if (!larodSetTensorFd(outputs->tensors[i], outFd, error)) {
            logError("Could not set output tensor fd (%d): %s", (*error)->code,
                     (*error)->msg);

            return false;
        }

        if (!larodSetTensorFdProps(outputs->tensors[i], outFdProps, error)) {
            logError("Could not set output tensor props (%d): %s",
                     (*error)->code, (*error)->msg);

            return false;
        }

        if (!larodSetTensorFdSize(outputs->tensors[i], outputs->byteSizes[i],
                                  error)) {
            logError("Could not set output tensor fd size (%d): %s",
                     (*error)->code, (*error)->msg);

            return false;
        }
    }

    logDebug(debug, "Will write output to:");
    for (size_t i = 0; i < outputs->num; ++i) {
        logDebug(debug, "\t%s", args->outputFiles[i]);
    }

    return true;
}

static bool trackTensors(Tensors* tensors, larodConnection* conn) {
    assert(conn);
    assert(tensors);

    larodError* error = NULL;

    for (size_t i = 0; i < tensors->num; ++i) {
        if (!larodTrackTensor(conn, tensors->tensors[i], &error)) {
            logError("Could not track tensor (%d): %s", error->code,
                     error->msg);
            larodClearError(&error);

            return false;
        }
    }

    return true;
}

// Call with rounds = 0 to run function until SIGINT
bool doLoad(args_t* args) {
    bool ret = false;
    larodError* error = NULL;
    larodConnection* conn = NULL;
    larodModel* model = NULL;
    larodJobRequest* jobReq = NULL;
    size_t allocedOutputFiles = 0;
    FILE* csvFile = NULL;
    Tensors inputs = {.tensors = NULL,
                      .num = 0,
                      .byteSizes = NULL,
                      .files = NULL,
                      .dma = NULL};
    Tensors outputs = {.tensors = NULL,
                       .num = 0,
                       .byteSizes = NULL,
                       .files = NULL,
                       .dma = NULL};

    debug = args->debug;

    // Connect
    logInfo("Connecting to larod...");
    if (!larodConnect(&conn, &error)) {
        logError("Could not connect to larod (%d): %s", error->code,
                 error->msg);
        larodClearError(&error);
        goto end;
    }
    logInfo("Connected");

    // Load model
    if (!loadModel(conn, args->modelFile, args->chip, args->modelAccess,
                   args->modelName, &model, args->modelParams)) {
        goto end;
    }

    // Prepare input/output tensors.
    if (!prepareInputs(&inputs, args, model, &error)) {
        goto end;
    }
    if (args->trackInputs && !trackTensors(&inputs, conn)) {
        goto end;
    }
    if (!prepareOutputs(&outputs, args, model, &error, &allocedOutputFiles)) {
        goto end;
    }
    if (args->trackInputs && !trackTensors(&outputs, conn)) {
        goto end;
    }

    jobReq = larodCreateJobRequest(model, inputs.tensors, inputs.num,
                                   outputs.tensors, outputs.num,
                                   args->jobParams, &error);
    if (!jobReq) {
        logError("Could not create job request (%d): %s", error->code,
                 error->msg);

        goto end;
    }

    signal(SIGINT, signalHandler);
    isRunning = true;

    // There's typically extra time needed to run the first job so we do a
    // warmup so that time measurement isn't affected. Furthermore this protects
    // against fd overflow in larod for backends that are VERY slow on the first
    // job.
    logInfo("Running first job as warm up...");
    if (!larodRunJob(conn, jobReq, &error)) {
        logError("Could not run first job (%d): %s", error->code, error->msg);
        goto end;
    }

    if (args->csvPath) {
        csvFile = fopen(args->csvPath, "wb");
        if (!csvFile) {
            logError("Could not open CSV output file %s: %s", args->csvPath,
                     strerror(errno));

            goto end;
        }

        logInfo("Will write job times to file \"%s\"", args->csvPath);
    }

    struct timespec startTime;
    if (args->timeJob) {
        clock_gettime(CLOCK_MONOTONIC, &startTime);
    }

    // Run job
    if (args->async) {
        logInfo("Running continuous job asynchronously...");
        ret = runJobAsync(conn, jobReq, args->rounds, args->sleepTime, csvFile);
    } else {
        logInfo("Running continuous job...");
        ret = runJob(conn, jobReq, args->rounds, csvFile);
    }
    if (!ret) {
        goto end;
    }

    bool measureTime = args->timeJob || args->jobTimeLimit < ULONG_MAX;
    if (measureTime) {
        double meanJobTimeMs = (double) totalJobTimeNs /
                               (double) NSEC_PER_MSEC / (double) args->rounds;

        if (args->timeJob) {
            logInfo("Mean execution time for job: %.2lf ms", meanJobTimeMs);
        }

        double timeLimit = (double) args->jobTimeLimit;
        if (meanJobTimeMs > timeLimit) {
            logError("Job time limit exceeded: Took %.2lf ms but needs to be "
                     "less than or equal to %.lf ms",
                     meanJobTimeMs, timeLimit);
            ret = false;
            goto end;
        }
    }

    // Copy output data from dma buffers to output files.
    if (args->useDmaOutput) {
        for (size_t i = 0; i < outputs.num; ++i) {
            size_t bytesWritten =
                fwrite(outputs.dma[i]->data, sizeof(char), outputs.dma[i]->size,
                       outputs.files[i]);
            if (bytesWritten != outputs.dma[i]->size) {
                logWarning("Could only write %zu of %zu bytes to output file.",
                           bytesWritten, outputs.dma[i]->size);
            }
        }
    }

    logInfo("Test terminated");

end:
    freeTensors(&inputs, args);
    freeTensors(&outputs, args);

    for (size_t i = 0; i < allocedOutputFiles; ++i) {
        free(args->outputFiles[args->numOutputs + i]);
    }

    if (csvFile) {
        fclose(csvFile);
    }

    // Delete model.
    if (model && !larodDeleteModel(conn, model, &error)) {
        logError("Could not delete model (%d): %s", error->code, error->msg);
    }

    // Disconnect
    if (conn && !larodDisconnect(&conn, &error)) {
        logError("Could not disconnect (%d): %s", error->code, error->msg);
    }

    larodDestroyJobRequest(&jobReq);
    larodDestroyModel(&model);
    larodClearError(&error);

    return ret;
}

bool loadModel(larodConnection* conn, char* modelFile, const larodChip chip,
               const larodAccess access, const char* modelName,
               larodModel** model, larodMap* params) {
    logDebug(debug, "Loading model...");
    larodError* error = NULL;

    // File descriptor is negative if no model file is specified.
    int fd = -1;
    FILE* fpModel = NULL;

    if (modelFile) {
        fpModel = fopen(modelFile, "rb");
        if (!fpModel) {
            logError("Could not open model file %s: %s", modelFile,
                     strerror(errno));

            return false;
        }

        fd = fileno(fpModel);
        if (fd < 0) {
            logError("Could not get file descriptor for model file: %s",
                     strerror(errno));
            fclose(fpModel);

            return false;
        }

        if (!modelName) {
            modelName = basename(modelFile);
        }
    } else if (!modelName) {
        modelName = "params-model";
    }

    *model = larodLoadModel(conn, fd, chip, access, modelName, params, &error);
    if (!*model) {
        logError("When loading model (%d): %s", error->code, error->msg);
        larodClearError(&error);

        return false;
    }

    if (modelFile && fclose(fpModel)) {
        logWarning("Could not close file %s", modelFile);
    }

    logInfo("Model %s loaded", modelName);

    return true;
}

bool runJob(larodConnection* conn, const larodJobRequest* jobReq,
            unsigned long long rounds, FILE* csvFile) {
    logDebug(debug, "Running job...");

    bool ret = true;

    while (isRunning) {
        struct timespec jobStartTime;
        clock_gettime(CLOCK_MONOTONIC, &jobStartTime);

        larodError* error = NULL;
        if (!larodRunJob(conn, jobReq, &error)) {
            logError("When running job (%d): %s", error->code, error->msg);
            larodClearError(&error);

            ret = false;
            break;
        }

        struct timespec jobEndTime;
        clock_gettime(CLOCK_MONOTONIC, &jobEndTime);

        // Compute the time taken for this job.
        unsigned long long jobTimeNs =
            (unsigned long long) (jobEndTime.tv_sec - jobStartTime.tv_sec) *
            (unsigned long long) NSEC_PER_SEC;
        jobTimeNs +=
            (unsigned long long) (jobEndTime.tv_nsec - jobStartTime.tv_nsec);

        totalJobTimeNs += jobTimeNs;

        if (csvFile) {
            ret = logTimeToCSV(csvFile, jobTimeNs / 1000);
            if (!ret) {
                break;
            }
        }

        logDebug(debug, "Output written (%llu us)", jobTimeNs / 1000);

        // Decrement rounds and update isRunning if rounds != 0.
        isRunning = rounds ? isRunning && (--rounds > 0) : isRunning;
    }

    return ret;
}

typedef struct {
    unsigned long long reqNbr;
    unsigned long long rounds;
    bool* hasRecvAll;
    bool* hasFailed;
    FILE* csvFile;
} CallbackData;

bool runJobAsync(larodConnection* conn, const larodJobRequest* const jobReq,
                 const unsigned long long rounds, const unsigned int sleepTime,
                 FILE* csvFile) {
    larodError* error = NULL;
    bool hasRecvAll = false;
    bool hasFailed = false;
    CallbackData* callbackData;
    bool ret = true;

    for (unsigned long long i = 1; isRunning; ++i) {
        callbackData = malloc(sizeof(CallbackData));
        if (!callbackData) {
            isRunning = false;
            ret = false;
            break;
        }

        callbackData->reqNbr = i;
        callbackData->rounds = rounds;
        callbackData->hasRecvAll = &hasRecvAll;
        callbackData->hasFailed = &hasFailed;
        callbackData->csvFile = csvFile;

        // If all outgoing requests are completed, set nextInfStartTime here
        // because time has passed since the last callback updated it.
        pthread_mutex_lock(&mtx);
        if (nbrCallbacksDone == i - 1) {
            clock_gettime(CLOCK_MONOTONIC, &nextJobStartTime);
        }
        pthread_mutex_unlock(&mtx);

        ret = larodRunJobAsync(conn, jobReq, jobCallback, callbackData, &error);
        if (!ret) {
            logError("Could not run job: (%d): %s", error->code, error->msg);
            larodClearError(&error);

            isRunning = false;
            break;
        }

        logDebug(debug, "Sent job request %llu", i);

        if (rounds && i == rounds) {
            break;
        }

        if (sleepTime) {
            const unsigned int SLEEP_SEC = sleepTime / 1000;
            const unsigned int SLEEP_MSEC = sleepTime % 1000;
            if (SLEEP_SEC) {
                // Use sleep() for the seconds.
                sleep(SLEEP_SEC);
            }

            if (SLEEP_MSEC) {
                // Use usleep() for the remaining milliseconds (usleep()'s
                // domain is [0, 1000000]).
                usleep(SLEEP_MSEC * 1000);
            }
        }
    }

    if (!isRunning) {
        goto end;
    }

    logInfo("Waiting for callbacks...");
    pthread_mutex_lock(&mtx);
    while (isRunning && !hasRecvAll) {
        pthread_cond_wait(&condVar, &mtx);
    }
    pthread_mutex_unlock(&mtx);

end:
    pthread_cond_destroy(&condVar);
    pthread_mutex_destroy(&mtx);

    return !hasFailed && ret;
}

void jobCallback(void* data, larodError* error) {
    assert(data);
    CallbackData* callbackData = (CallbackData*) data;

    struct timespec jobEndTime;
    clock_gettime(CLOCK_MONOTONIC, &jobEndTime);

    // Compute the time taken for this job.
    unsigned long long jobTimeNs =
        (unsigned long long) (jobEndTime.tv_sec - nextJobStartTime.tv_sec) *
        (unsigned long long) NSEC_PER_SEC;
    jobTimeNs +=
        (unsigned long long) (jobEndTime.tv_nsec - nextJobStartTime.tv_nsec);

    pthread_mutex_lock(&mtx);
    totalJobTimeNs += jobTimeNs;
    // Update nextJobStartTime for the next executing callback function.
    memcpy(&nextJobStartTime, &jobEndTime, sizeof(struct timespec));

    if (callbackData->csvFile) {
        logTimeToCSV(callbackData->csvFile, jobTimeNs / 1000);
    }

    ++nbrCallbacksDone;
    pthread_mutex_unlock(&mtx);

    if (error) {
        pthread_mutex_lock(&mtx);
        *callbackData->hasFailed = true;
        pthread_mutex_unlock(&mtx);

        logError("Callback %llu received: errCode = %d, errMsg = \"%s\" "
                 "(total %llu recieved)",
                 callbackData->reqNbr, error->code, error->msg,
                 nbrCallbacksDone);
    } else {
        logDebug(debug, "Callback %llu received: (total %llu received)",
                 callbackData->reqNbr, nbrCallbacksDone);
    }

    if (isRunning && nbrCallbacksDone >= callbackData->rounds) {
        pthread_mutex_lock(&mtx);
        *callbackData->hasRecvAll = true;
        pthread_mutex_unlock(&mtx);

        pthread_cond_broadcast(&condVar);
    }

    free(callbackData);
}

bool logTimeToCSV(FILE* csvFile, unsigned long long infTimeUs) {
    assert(csvFile);

    char csvLine[64];
    int lineLen = snprintf(csvLine, sizeof(csvLine) - 1, "%llu\n", infTimeUs);
    if (lineLen >= 0 && (size_t) lineLen < sizeof(csvLine)) {
        size_t writeLen = fwrite(csvLine, (size_t) lineLen, 1, csvFile);
        if (writeLen < 1) {
            logError("Could not write CSV line: %s", strerror(errno));
            return false;
        }
    } else {
        logError("Could not format CSV line");
        return false;
    }

    return true;
}
