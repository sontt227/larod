Neural Network Inference
========================

- [Neural network models](#neural-network-models)
- [Supported backends](#supported-backends)
  - [TFLite@CPU backend](#tflitecpu-backend)
  - [TFLite@EdgeTPU backend](#tfliteedgetpu-backend)
  - [Ambarella CVFlowNN](#ambarella-cvflownn)
  - [TFLite@GLGPU](#tfliteglgpu)
- [Supported buffer properties](#supported-buffer-properties-1)
  - [When running jobs](#when-running-jobs)
  - [When allocating tensors](#when-allocating-tensors)

## Neural network models

Neural network models are provided as binary blobs to the backends. These binary
blobs are generally produced by the backend specific toolchains. These binary
blobs are provided through an open file descriptor in the
`larodLoadModel()`/`larodLoadModelAsync()` functions. Each backend is then
responsible for unpacking, parsing and loading the binary model using its
specific runtime calls.

## Supported backends

Currently the following neural network inference backends are supported by
larod.

### TFLite@CPU backend

This backend executes TFLite models on the SoC CPU(s). Note that the CPU
subsystem generally have limited performance compared to dedicated hardware
accelerators.

This backend is represented by the `larodChip` `LAROD_CHIP_TFLITE_CPU` in
`larod.h`.

#### Supported format of model data

- A TFLite model, formally identified by a `.tflite` file extension.
- A model as listed above converted to the DEPRECATED `.larod` format is also
  supported for backwards compatibility.

#### Supported buffer properties for running jobs

This backend only supports the fd access types `LAROD_FD_PROP_MAP` and
`LAROD_FD_PROP_READWRITE`.

As the name indicates the former fd prop will allow the backend to map (using
`mmap`) the tensor's file descriptor instead of reading or writing from them.
Combined with tensor tracking (eg. using `larodTrackTensor`) the backend may be
able to cache a tensor's mapping and thus allow for a very efficient zero-copy
map-once memory access pattern.

The access type `LAROD_FD_PROP_READWRITE` will introduce a memory copy and
`read()`/`write()` calls for each input and output tensor buffer - these extra
operations will degrade performance.

#### Allocation support

Tensors allocated using the calls `larodAllocModelInputs()` and
`larodAllocModelOutputs()` with a model loaded to this backend will have file
descriptors that are readable, writable and mappable. Accordingly the tensors
will have the fd props `LAROD_FD_PROP_READWRITE` and `LAROD_FD_PROP_MAP` set.

### TFLite@EdgeTPU backend

This backend executes TFLite models on the Google EdgeTPU accelerator.
This backend is represented by the `larodChip` `LAROD_CHIP_TPU` in
`larod.h`.

#### Supported format of model data

- A TFLite model, formally identified by a `.tflite` file extension. The model
  needs to be compiled for EdgeTPU in an additional step after being converted
  from TensorFlow to TFLite format.
- A model as listed above converted to the DEPRECATED `.larod` format is also
  supported for backwards compatibility.

#### Supported buffer properties for running jobs

This backend supports the fd access types `LAROD_FD_PROP_DMABUF`,
`LAROD_FD_PROP_MAP` and `LAROD_FD_PROP_READWRITE` for *input* tensors, but only
the latter two for *output* tensors.

The access type `LAROD_FD_PROP_DMABUF` provides less overhead since the buffer
fd will be passed directly the to the EdgeTPU lib through TFLite without extra
copies in larod. The fd offsets for input tensors must be 0 when using the
`LAROD_FD_PROP_DMABUF` access method. The application is responsible for
ensuring that the external RAM is up to date with CPU cache before the inference
is started (the service will _not_ initiate any cache flush operations).

The access type `LAROD_FD_PROP_MAP` will allow the backend to map (using `mmap`)
the tensor's file descriptor instead of reading or writing from them. Combined
with tensor tracking (eg. using `larodTrackTensor`) the backend may be able to
cache a tensor's mapping and thus allow for a very efficient zero-copy map-once
memory access pattern.

The access type `LAROD_FD_PROP_READWRITE` will introduce a memory copy and
`read()`/`write()` calls for each tensor buffer - these extra operations will
degrade performance. Only the access type `LAROD_FD_PROP_READWRITE` is supported
for *output* tensors.

#### Allocation support

Using the call `larodAllocModelInputs()` with a model loaded to this backend
will have tensors with readable, writable and mappable file descriptors. As such
these tensors will have the fd props `LAROD_FD_PROP_READWRITE` and
`LAROD_FD_PROP_MAP` set.

Tensors allocated using `larodAllocModelOutputs()` and with a model loaded to
this backend will have file descriptors that are readable, writable and
mappable. Accordingly the tensors will have the fd props
`LAROD_FD_PROP_READWRITE` and `LAROD_FD_PROP_MAP` set.

### Ambarella CVFlowNN

This backend executes neural network models on the VP (Vector Processor) in
Ambarella chips. The models need to be converted to a suitable format using
Ambarella toolchain before deployment on this backend.

This backend is represented by the `larodChip` `LAROD_CHIP_CVFLOW_NN` in
`larod.h`.

#### Supported format of model data

- A cavalary bin, i.e. a model converted using Ambarella's toolchain.
- A model as listed above converted to the DEPRECATED `.larod` format is also
  supported for backwards compatibility.

#### Supported buffer properties

This backend supports the fd access types `LAROD_FD_PROP_DMABUF` and
`LAROD_FD_PROP_READWRITE`.

The access type `LAROD_FD_PROP_DMABUF` provides less overhead since the buffer
will be passed directly the underlying inference framework without extra copies
in larod. When using `LAROD_FD_PROP_DMABUF` the input tensor buffers will have
CPU cache flushed before the processing starts, and output buffers buffers will
have cache invalidated before the results are delivered to the application. Note
that the supplied dmabufs must be allocated by the Ambarella platform.

The access type `LAROD_FD_PROP_READWRITE` will introduce a memory copy and
`read()`/`write()` calls for each input and output tensor buffer - these extra
operations will degrade performance.

#### Allocation support

Using the calls `larodAllocModelInputs()` and `larodAllocModelOutputs()` with a
model loaded to this backend two kinds of tensor buffers can be allocated. If
the `LAROD_FD_PROP_READWRITE` is *not* set as required in the call, then
tensors with mappable file descriptors based on Cavalry Mem dma-bufs will be
returned. As such these tensors will have the fd props `LAROD_FD_PROP_MAP` and
`LAROD_FD_PROP_DMABUF` set. If however `LAROD_FD_PROP_READWRITE` is required,
then tensors with readable, writable and mappable file descriptors will be
returned. As such these tensors will have the fd props `LAROD_FD_PROP_READWRITE`
and `LAROD_FD_PROP_MAP` set.

### TFLite@GLGPU

This backend executes TFLite models on an OpenGL capable HW accelerator.
This backend is represented by the `larodChip` `LAROD_CHIP_TFLITE_GLGPU` in
`larod.h`.

**NOTE**
This is an experimental feature and could be removed or changed at any time.

#### Supported format of model data

- A TFLite model, formally identified by a `.tflite` file extension.
- A model as listed above converted to the DEPRECATED `.larod` format is also
  supported for backwards compatibility.

#### Supported buffer properties for running jobs

This backend only supports the fd access types `LAROD_FD_PROP_MAP` and
`LAROD_FD_PROP_READWRITE`.

As the name indicates the former fd prop will allow the backend to map (using
`mmap`) the tensor's file descriptor instead of reading or writing from them.
Combined with tensor tracking (eg. using `larodTrackTensor`) the backend may be
able to cache a tensor's mapping and thus allow for a very efficient zero-copy
map-once memory access pattern.

The access type `LAROD_FD_PROP_READWRITE` will introduce a memory copy and
`read()`/`write()` calls for each input and output tensor buffer - these extra
operations will degrade performance.

#### Allocation support

Tensors allocated using the calls `larodAllocModelInputs()` and
`larodAllocModelOutputs()` with a model loaded to this backend will have file
descriptors that are readable, writable and mappable. Accordingly the tensors
will have the fd props `LAROD_FD_PROP_READWRITE` and `LAROD_FD_PROP_MAP` set.

## Supported buffer properties

This is an overview of what file descriptor properties are supported by the
various neural network backends. Note that the `LAROD_FD_PROP_` prefix have
been omitted from the table headers in the interest of brevity. Please see
[`larod.h`](../lib/larod.h) for more info about the `LAROD_FD_PROD_*` flags.

### When running jobs

Please note that though several properties may be supported by a backend, a
tensor buffer supplied for running a job need only have at least one of the
backend's supported properties to be usable for the job. Having said that, each
property comes with different implications on memory access performance.

#### Input tensors

| Backend             | READWRITE | MAP | DMABUF |
| ------------------- | --------- | --- | ------ |
| TFLite@CPU          | Yes       | Yes |        |
| TFLite@EdgeTPU      | Yes       | Yes | Yes    |
| CVFlowNN            | Yes       |     | Yes    |
| TFLite@GLGPU        | Yes       | Yes |        |

#### Output tensors

| Backend             | READWRITE | MAP | DMABUF |
| ------------------- | --------- | --- | ------ |
| TFLite@CPU          | Yes       | Yes |        |
| TFLite@EdgeTPU      | Yes       | Yes |        |
| CVFlowNN            | Yes       |     | Yes    |
| TFLite@GLGPU        | Yes       | Yes |        |

### When allocating tensors

Please note that though several properties may be supported by a backend, it may
not be possible to allocate buffers having all the properties at the same time.

#### Input tensors

| Backend             | READWRITE | MAP | DMABUF |
| ------------------- | --------- | --- | ------ |
| TFLite@CPU          | Yes       | Yes |        |
| TFLite@EdgeTPU      | Yes       | Yes |        |
| CVFlowNN            | Yes       | Yes | Yes    |
| TFLite@GLGPU        | Yes       | Yes |        |

#### Output tensors

| Backend             | READWRITE | MAP | DMABUF |
| ------------------- | --------- | --- | ------ |
| TFLite@CPU          | Yes       | Yes |        |
| TFLite@EdgeTPU      | Yes       | Yes |        |
| CVFlowNN            | Yes       | Yes | Yes    |
| TFLite@GLGPU        | Yes       | Yes |        |
