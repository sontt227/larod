/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "memfd.hh"

#include <sys/mman.h>

#include "larod.h"
#include "log.hh"

using namespace std;

namespace larod {
namespace allocator {

MemFd::MemFd() : Allocator(LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

MemFd::~MemFd() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed");
}

std::unique_ptr<larod::Buffer> MemFd::allocateVirtual(const size_t size,
                                                      const uint32_t,
                                                      const ParamsMap& params) {
    // We don't support any additional parameters.
    if (params.size()) {
        throw invalid_argument("Additional parameters are not supported");
    }

    int ret = memfd_create(__func__, MFD_CLOEXEC);
    if (ret < 0) {
        throw runtime_error("Could not create fd: " + string(strerror(errno)));
    }

    int tmpFd = ret;
    FileDescriptor fd = std::move(tmpFd);
    // FIXME: Unsafe cast to off_t from size_t.
    ret = ftruncate(fd, static_cast<off_t>(size));
    if (ret < 0) {
        throw runtime_error("Could not truncate fd: " +
                            string(strerror(errno)));
    }

    return make_unique<MemFd::Buffer>(
        std::move(fd), size, 0, LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP);
}

MemFd::Buffer::Buffer(FileDescriptor&& fd, const size_t maxSize,
                      const int64_t startOffset, const uint32_t fdProps)
    : larod::Buffer(std::move(fd), maxSize, startOffset, fdProps) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed " +
              to_string(getCookie()));
}

MemFd::Buffer::~Buffer() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed " +
              to_string(getCookie()));
}

} // namespace allocator
} // namespace larod
