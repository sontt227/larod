/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <memory>

#include "allocator.hh"
#include "buffer.hh"

namespace larod {
namespace allocator {

/**
 * @brief Allocator using @c memfd_create() syscall.
 */
class MemFd : public Allocator {
public:
    ~MemFd();

    MemFd(const MemFd&) = delete;
    MemFd& operator=(const MemFd&) = delete;

    /**
     * @brief Get an instance of this singleton.
     *
     * @return An instance of this class.
     */
    static std::shared_ptr<MemFd> getInstance() {
        // Since MemFd() is private, we cannot call make_shared<MemFd>() here.
        // Instead we have to use `new`. Note that this is also guaranteed to be
        // thread-safe (since C++11).
        static std::shared_ptr<MemFd> instance(new MemFd);

        return instance;
    }

protected:
    std::unique_ptr<larod::Buffer>
        allocateVirtual(const size_t size, const uint32_t fdProps,
                        const ParamsMap& params) override;

private:
    static inline std::string THIS_NAMESPACE = "MemFd::";

    MemFd();

private:
    class Buffer : public larod::Buffer {
    public:
        Buffer(FileDescriptor&& fd, const size_t maxSize,
               const int64_t startOffset, const uint32_t fdProps);
        ~Buffer();

    private:
        static inline std::string THIS_NAMESPACE =
            MemFd::THIS_NAMESPACE + "Buffer::";
    };
};

} // namespace allocator
} // namespace larod
