/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <functional>
#include <gtest/gtest.h>
#include <sys/mman.h>

#include "argparse.h"
#include "imageprocfunctest.hh"
#include "imageutil.hh"
#include "larod.h"
#include "testutils.hh"

using namespace std;
using namespace larod::test;

TEST_F(ImageProcFuncTest, ConvertImage) {
    // If there is no input image, print the random seed.
    if (!inputFilePath) {
        gtestPrint(string("Generating test pattern using seed: ") +
                   to_string(randomSeed));
    }

    // Connect to larod, load model and create input & output tensors.
    setupLarod();

    // Retrieve width, height, pitch & format from the larodMap given as args.
    setupInputOutputMetaData();

    // Fill input tensor with either generated data or load in file. The output
    // is mapped to a temporary file.
    setupTensors();

    createJobRequestAndRun();

    if (verify) {
        verifyOutput();
    }
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

    args_t args;
    if (parseArgs(argc, argv, &args) != 0) {
        exit(EXIT_FAILURE);
    }
    auto argsHandler = unique_ptr<args_t, function<void(args_t*)>>{
        &args, [](args_t* a) { destroyArgs(a); }};

    // Set attributes from args.
    ImageProcFuncTest::chip = args.chip;
    ImageProcFuncTest::debugPrints = args.debugPrints;
    ImageProcFuncTest::modelParams = args.modelParams;
    ImageProcFuncTest::jobParams = args.jobParams;
    ImageProcFuncTest::inputFilePath = args.inputFilePath;
    ImageProcFuncTest::verificationFilePath = args.verificationFilePath;
    ImageProcFuncTest::verify = args.verifyOutput;
    ImageProcFuncTest::randomSeed = args.randomSeed;
    ImageProcFuncTest::meanAbsDiffThreshold = args.meanAbsDiffThreshold;
    ImageProcFuncTest::maxAbsDiffThreshold = args.maxAbsDiffThreshold;

    return RUN_ALL_TESTS();
}
