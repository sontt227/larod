# Dockerfile for larod
#
# This Dockerfile creates an environment that can build and run larod.
#
# Example build command:
#     docker build -t larod .
#
# One can then mount the cloned larod directory when running. For example:
#     docker run \
#         --volume /home/user/larod:/larod \
#         --volume /run/dbus/system_bus_socket:/run/dbus/system_bus_socket \
#         --cap-add=SYS_PTRACE \
#         --security-opt seccomp=unconfined \
#         -it larod
#
# Further useful options to docker run command:
#   --privileged  Extended privileges, needed for e.g. EdgeTPU on USB.
#   -v /dev:/dev  Mount /dev from host, needed for e.g. EdgeTPU on USB.
#   --gpus all    Used with Nvidia docker to bridge host GPU(s), used with
#                 the GLGPU backend.
# For an explanation of the other options in the example command above, please
# see doc/BUILD.md.

FROM debian:buster-slim
LABEL version="0.8" \
      maintainer="https://gitlab.com/unimatrix/larod"
ENV container docker

# Set these versions to desired commits (e.g. if matching versions with some
# other build environment). The following have been verified to work together.
ARG SDBUSPLUS_SRC_SHA="847a0c3711fc316e1a9532fc333a0b45a034cb96"
ARG LIBYUV_SRC_SHA="b45db3c4af8046f99ababc8ed4181edd2976d2b5"
ARG FLATBUFFER_SRC_BR="v1.12.0"
ARG FLATBUFFER_PIP_VERSION="1.12"
ARG GOOGLE_TEST_SRC_SHA="703bd9caab50b139428cea1aaff9974ebee5742e"
# Tensorflow lite version 2.3.1
ARG TFLITE_SRC_REV="fcc4b966f1265f466e82617020af93670141b009"
ARG USE_TFLITE_GLGPU="false"
# Note that libedgetpu downloads its own snapshot of the Tensorflow git. The
# Tensorflow sha-id used by Bazel when building libedgetpu can be found in
# libedgetpu/workspace.bzl.
# So the TFLITE_SRC_REV above needs to be sufficiently close to ensure
# compatibility in the TFLite interfaces for libedgetpu!
ARG LIBGEDGETPU_SRC_REV="14eee1a076aa1af7ec1ae3c752be79ae2604a708"
ARG USE_TFLITE_EDGETPU="false"

## Install (larod) dependencies.
RUN apt-get update && apt-get install -y \
    build-essential \
    clang-format \
    doxygen \
    gdb \
    git \
    libreadline-gplv2-dev \
    make \
    ninja-build \
    python3 \
    python3-pip \
    sudo \
    systemd

RUN python3 -m pip install meson

# sdbusplus
RUN apt-get install -y \
    g++ \
    libsystemd-dev \
    libtool \
    pkg-config \
    python3-inflection \
    python3-mako \
    python3-yaml

RUN git clone https://github.com/openbmc/sdbusplus.git && cd sdbusplus && \
    git checkout $SDBUSPLUS_SRC_SHA && \
    meson build -Dtests=disabled -Dexamples=disabled && cd build && ninja \
    && ninja install && cd ../tools && ./setup.py install

# Flatbuffers
RUN apt-get install -y \
    cmake

RUN git clone -b $FLATBUFFER_SRC_BR https://github.com/google/flatbuffers.git \
    && cd flatbuffers && cmake CMakeLists.txt && make && make install && \
    ldconfig

# Google test
RUN git clone https://github.com/google/googletest.git && cd googletest && \
    git checkout $GOOGLE_TEST_SRC_SHA && cmake CMakeLists.txt && make install \
    && ldconfig

# Build and install Tensorflow Lite.
# The TARGET definition for 'make' can be tailored for specific platforms, e.g.
# TARGET=rpi can be used to optimize the build for Raspberry Pi platform.
RUN echo "deb http://ftp.de.debian.org/debian buster-backports main" >> \
    # Extra repo for libabsl-dev.
    /etc/apt/sources.list && apt-get update && \
    apt-get install -y \
    curl \
    wget \
    unzip \
    zip \
    zlib1g-dev \
    python3-distutils \
    python3-numpy \
    python3-six \
    libabsl-dev

# The OpenGL ES backend specific dependencies.
RUN if [ "$USE_TFLITE_GLGPU" = "true" ]; then \
        # For nvidia packages.
        sed -i "s/buster main/buster main contrib non-free/" \
        /etc/apt/sources.list && \
        apt-get update && \
        apt-get install -y \
        libegl1-mesa-dev \
        libgles2-mesa-dev \
        libegl-nvidia0 \
        libgles-nvidia2 \
        nvidia-egl-common \
        nvidia-egl-icd; \
    fi


# Clone tensorflow, install bazel, build and install tensorflow-lite.a and
# libtensorflowlite_gpu_gl.so. Bazel builds libtensorflowlite.so but currently
# we are expecting libtensorflow-lite.so. We solve that for now with a symlink.
RUN git clone --depth 1 \
    https://github.com/tensorflow/tensorflow.git && cd tensorflow && \
    git fetch origin $TFLITE_SRC_REV && git reset --hard FETCH_HEAD && \
    bazel_version=$(grep -o "_TF_MIN_BAZEL_VERSION = '.*'" -r . | \
    grep -o "[0-9]\+\.[0-9]\+\.[0-9]\+") && \
    wget "https://github.com/bazelbuild/bazel/releases/download/$bazel_version\
/bazel-$bazel_version-installer-linux-x86_64.sh" && \
    chmod +x bazel-$bazel_version-installer-linux-x86_64.sh && \
    ./bazel-$bazel_version-installer-linux-x86_64.sh && \
    rm bazel-$bazel_version-installer-linux-x86_64.sh && \
    which python3 | ./configure && \
    bazel build --config=opt //tensorflow/lite:libtensorflowlite.so && \
    find ./tensorflow/lite -name "*.h" |  \
    xargs cp --parents -vt /usr/include && \
    cp bazel-bin/tensorflow/lite/libtensorflowlite.so /usr/lib && \
    ln -s /usr/lib/libtensorflowlite.so /usr/lib/libtensorflow-lite.so && \
    if [ "$USE_TFLITE_GLGPU" = "true" ]; then \
        echo "Building TFLite GLGPU support" && \
        bazel build --config=opt --copt -DMESA_EGL_NO_X11_HEADERS \
        tensorflow/lite/delegates/gpu:libtensorflowlite_gpu_gl.so && \
        cp bazel-bin/tensorflow/lite/delegates/gpu/libtensorflowlite_gpu_gl.so \
        /usr/lib; \
    fi

# Install packages required for larod/tools/convert-model (c.f. file
# "requirements.txt" in that directory).
RUN pip3 install flatbuffers==$FLATBUFFER_PIP_VERSION

# libyuv
RUN git clone https://chromium.googlesource.com/libyuv/libyuv && \
    cd libyuv && git checkout $LIBYUV_SRC_SHA && make -f linux.mk && \
    cp -v libyuv.a /usr/lib && cp -vr include /usr

# Deps for libEdgeTPU.
RUN apt-get install -y \
    xxd \
    libusb-1.0-0-dev

# Build/install libedgetpu from source.
RUN if [ "$USE_TFLITE_EDGETPU" = "true" ]; then \
        echo "Building libedgetpu..." &&  \
        git clone --depth 1  https://github.com/google-coral/libedgetpu.git && \
        cd libedgetpu && git fetch origin $LIBGEDGETPU_SRC_REV && \
        git reset --hard FETCH_HEAD && make && \
        ln -s $PWD/out/direct/k8/libedgetpu.so.1 /usr/lib/libedgetpu.so.1 && \
        ln -s $PWD/out/direct/k8/libedgetpu.so.1.0 \
        /usr/lib/libedgetpu.so.1.0 && \
        ln -s /usr/lib/libedgetpu.so.1 /usr/lib/libedgetpu.so && \
        ln -s $PWD/tflite/public/edgetpu.h /usr/include/ && \
        ldconfig; \
    fi
