/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */
#include "utils.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "log.h"

// Max length of key and value strings.
#define STRING_MAX_LENGTH 128

int addParam(larodMap* map, char* keyValue) {
    larodError* error = NULL;
    int ret = EINVAL;

    // Copy input string, because the string functions will manipulate it.
    if (strnlen(keyValue, STRING_MAX_LENGTH) == STRING_MAX_LENGTH) {
        return EINVAL;
    }
    char* keyValueCpy = strndup(keyValue, STRING_MAX_LENGTH);
    if (!keyValueCpy) {
        return ENOMEM;
    }

    // Get key / value
    char* key = strtok(keyValueCpy, ":");
    if (!key) {
        goto end;
    }
    char* val = strtok(NULL, ":");
    if (!val) {
        goto end;
    }

    // Get every element in "value"-array.
    char* el = strtok(val, ",");
    if (!el) {
        goto end;
    }
    size_t arraySize = 0;
    char* stringValue = NULL;
    int64_t numbers[4] = {-1, -1, -1, -1};
    while (el != NULL) {
        if (arraySize >= 4) {
            goto end;
        }

        char* elEnd;
        long long int num = strtoll(el, &elEnd, 0);

        // Check for input like "42abc", which will be interpreted as a string.
        char str[32];
        sprintf(str, "%lld", num);

        if (elEnd == el || strlen(str) != strlen(el)) {
            stringValue = el;
        } else {
            if (num >= INT64_MAX || num <= -INT64_MAX) {
                goto end;
            }

            numbers[arraySize] = (int64_t) num;
        }
        el = strtok(NULL, ",");
        ++arraySize;
    }

    // Cannot have strings in arrays.
    if (stringValue && arraySize > 1) {
        goto end;
    }

    // Set either string, number or array in map.
    if (stringValue) {
        larodMapSetStr(map, key, stringValue, &error);
    } else {
        // Set number parameter depending on the size of the array.
        switch (arraySize) {
        case 1: {
            larodMapSetInt(map, key, numbers[0], &error);
            break;
        }
        case 2: {
            larodMapSetIntArr2(map, key, numbers[0], numbers[1], &error);
            break;
        }
        case 4: {
            larodMapSetIntArr4(map, key, numbers[0], numbers[1], numbers[2],
                               numbers[3], &error);
            break;
        }
        default: { goto end; }
        }
    }

    ret = 0;
end:
    if (error) {
        ret = error->code;
        larodClearError(&error);
    }
    free(keyValueCpy);

    return ret;
}
