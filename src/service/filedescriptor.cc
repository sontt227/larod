/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "filedescriptor.hh"

#include <cassert>
#include <cstring>
#include <fcntl.h>
#include <stdexcept>
#include <sys/mman.h>
#include <unistd.h>

#include "log.hh"

using namespace std;

namespace larod {

FileDescriptor::FileDescriptor(const int&& fd) {
    if (fd < 0) {
        throw invalid_argument("fd is negative");
    }

    this->fd = fd;
}

FileDescriptor::~FileDescriptor() {
    try {
        reset();
    } catch (const exception& e) {
        LOG.error(e.what());
    }
}

FileDescriptor::FileDescriptor(const FileDescriptor& other) {
    if (other.fd >= 0) {
        fd = duplicate(other.fd);
    }
}

FileDescriptor::FileDescriptor(FileDescriptor&& other) noexcept
    : fd(std::move(other.fd)) {
    other.fd = -1;
}

FileDescriptor& FileDescriptor::operator=(const FileDescriptor& other) {
    if (this != &other) {
        reset();
        if (other.fd >= 0) {
            fd = duplicate(other.fd);
        }
    }

    return *this;
}

FileDescriptor& FileDescriptor::operator=(FileDescriptor&& other) {
    if (this != &other) {
        reset();
        fd = std::move(other.fd);
        other.fd = -1;
    }

    return *this;
}

void FileDescriptor::reset() {
    if (fd < 0) {
        return;
    }

    int ret = ::close(fd);
    fd = -1;
    if (ret) {
        assert(errno != EBADF);
        throw runtime_error("Could not close fd: " + string(strerror(errno)));
    }
}

int FileDescriptor::duplicate(const int fd) {
    int ret = dup(fd);
    if (ret == -1) {
        throw runtime_error("Could not duplicate fd: " +
                            string(strerror(errno)));
    }

    return ret;
}

void FileDescriptor::close() {
    reset();
}

shared_ptr<uint8_t[]> FileDescriptor::map(const off_t offset, const size_t sz,
                                          const int mmapProt) const {
    void* tmpRaw = mmap(NULL, sz, mmapProt, MAP_SHARED, fd, offset);
    if (tmpRaw == MAP_FAILED) {
        throw runtime_error("Could not map " + to_string(sz) +
                            " bytes at offset " + to_string(offset) +
                            " in file descriptor: " + string(strerror(errno)));
    }

    return shared_ptr<uint8_t[]>(
        static_cast<uint8_t*>(tmpRaw), [sz](uint8_t* p) {
            if (munmap(p, sz)) {
                // Should never happen...
                LOG.error("Could not unmap " + to_string(sz) +
                          " bytes at address: " +
                          to_string(reinterpret_cast<size_t>(p)) + ": " +
                          string(strerror(errno)));
                assert(false);
            }
        });
}

size_t FileDescriptor::read(const span<uint8_t>& buf, const size_t sz) const {
    if (buf.size() < sz) {
        throw invalid_argument(
            "Buffer size is smaller than requested read size (" +
            to_string(buf.size()) + " < " + to_string(sz) + ")");
    }

    size_t totalBytes = 0;
    ssize_t readBytes = 0;
    while (totalBytes != sz) {
        readBytes = ::read(fd, buf.data() + totalBytes, sz - totalBytes);
        if (readBytes == 0) {
            // EOF.
            break;
        } else if (readBytes < 0) {
            // TODO: Handle EINTR, EAGAIN and EWOULDBLOCK?
            throw runtime_error("Could not read from file descriptor: " +
                                string(strerror(errno)));
        }

        totalBytes += static_cast<size_t>(readBytes);

        LOG.debug(THIS_NAMESPACE + __func__ + "(): Read " +
                  to_string(readBytes) + " bytes");
    }

    return totalBytes;
}

size_t FileDescriptor::write(const span<uint8_t>& buf, const size_t sz) const {
    if (buf.size() < sz) {
        throw invalid_argument(
            "Buffer size is smaller than requested write size (" +
            to_string(buf.size()) + " < " + to_string(sz) + ")");
    }

    size_t totalBytes = 0;
    ssize_t writtenBytes = 0;
    while (totalBytes != sz) {
        writtenBytes = ::write(fd, buf.data() + totalBytes, sz - totalBytes);
        if (writtenBytes == 0) {
            // EOF
            break;
        } else if (writtenBytes < 0) {
            // TODO: Handle EINTR, EAGAIN and EWOULDBLOCK?
            throw runtime_error("Could not write to file descriptor: " +
                                string(strerror(errno)));
        }

        totalBytes += static_cast<size_t>(writtenBytes);

        LOG.debug(THIS_NAMESPACE + __func__ + "(): Wrote " +
                  to_string(writtenBytes) + " bytes");
    }

    return totalBytes;
}

} // namespace larod
