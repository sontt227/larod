/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <functional>
#include <memory>
#include <string>

#include "buffer.hh"
#include "larod.h"
#include "tensormetadata.hh"

namespace larod {

/**
 * @brief Class to represent a tensor. The metadata is stored in class
 * TensorMetadata. This class just additionally adds the actual byte data
 * (Buffer).
 */
class Tensor : public TensorMetadata {
public:
    /**
     * @brief Create a tensor.
     */
    Tensor(const larodTensorDataType dataType, const larodTensorLayout layout,
           const std::vector<size_t> dims, const std::vector<size_t> pitches,
           const size_t byteSize, const std::string name,
           const std::shared_ptr<Buffer> buffer);

    /**
     * @brief Constructor for serialized data types.
     */
    Tensor(const int32_t dataType, const int32_t layout,
           const std::vector<uint64_t> dims,
           const std::vector<uint64_t> pitches, const size_t byteSize,
           const std::string name, const std::shared_ptr<Buffer> buffer);

    Buffer& getBuffer() const { return *buffer; };

private:
    std::shared_ptr<Buffer> buffer;

    /**
     * @brief Cast vector<S> into vector<D>.
     */
    template<typename S, typename D>
    static std::vector<D> castVector(const std::vector<S>& s);
};

} // namespace larod
