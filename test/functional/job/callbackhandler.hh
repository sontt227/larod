/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */
#pragma once

#include <condition_variable>
#include <cstdint>
#include <deque>
#include <memory>
#include <mutex>

#include "larod.h"

/**
 * @brief Callback handling for one Larod connection.
 *
 * Keeps track of outstanding job requests for one Larod connection. Collects
 * all incoming results from jobs in a queue for later verification that all
 * jobs was delivered according to priority.
 */
struct CallBackHandler {
    /**
     * @brief Container for one callback.
     *
     * Represents one 'job' for a Larod connection. Typically used as the void*
     * userData parameter to larodRunJobAsync().
     */
    struct CallbackItem {
        uint8_t prio;                ///< Priority of the job.
        unsigned int sequenceNumber; ///< Sequence number among queued jobs.
        std::shared_ptr<CallBackHandler> cbh; ///< Callback handler.

        CallbackItem(uint8_t prio, unsigned int sequenceNumber,
                     std::shared_ptr<CallBackHandler> handler)
            : prio(prio), sequenceNumber(sequenceNumber), cbh(handler) {}
    };

    /**
     * @brief Grouping of results received in a callback call.
     *
     */
    struct CallbackResult {
        uint8_t priority;
        unsigned int sequenceNumber;
        larodErrorCode returnCode;

        CallbackResult(uint8_t prio, unsigned int seq, larodErrorCode retCode)
            : priority(prio), sequenceNumber(seq), returnCode(retCode) {}
    };

    /**
     * @brief CallBackHandler Constructor.
     *
     * @param id Integer identifying this handler for e.g. debug prints.
     */
    CallBackHandler(unsigned int id)
        : ID(id), nextSequenceNumber(0), numPendingCallbacks(0) {}

    /**
     * @brief Return sequence number.
     *
     * Returns a sequence number and updates the internal callback counter. We
     * expect to receive the same amount of callbacks as getSequenceNumber() is
     * called.
     *
     * @return A sequence number.
     */
    unsigned int getSequenceNumber() {
        std::unique_lock<std::mutex> lk(callbackMutex);
        numPendingCallbacks++;
        return nextSequenceNumber++;
    }
    /**
     * @brief Callback to be called when the result from a job is received.
     *
     * @param prio Priority of the job request generating the callback.
     * @param sequenceNumber Sequence number for the callback.
     * @return The first job request in line for @p chipId.
     */
    void callBackReceived(uint8_t prio, unsigned int sequenceNumber,
                          larodErrorCode errCode) {
        std::unique_lock<std::mutex> lk(callbackMutex);

        receivedCallbacks.emplace_back(prio, sequenceNumber, errCode);
        if (--numPendingCallbacks <= 0) {
            callbackCond.notify_one();
        }
    }

    /// Debug identifier.
    const unsigned int ID;
    /// Next sequence number to be returned by getSequenceNumber.
    unsigned int nextSequenceNumber;
    /// Callback counter.
    unsigned int numPendingCallbacks;
    /// Mutex protecting numPendingCallbacks and cond var.
    std::mutex callbackMutex;
    /// Cond var to notify that all pending callbacks have been received.
    std::condition_variable callbackCond;
    /// Callback result queue.
    std::deque<CallbackResult> receivedCallbacks;
};
