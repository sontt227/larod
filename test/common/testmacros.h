#pragma once

#include <cstring>

#define ASSERT_LAROD_ERROR(error)                                              \
    ASSERT_NE(error, nullptr);                                                 \
    EXPECT_NE(error->code, LAROD_ERROR_NONE);                                  \
    EXPECT_GT(std::strlen(error->msg), 0);

#define ASSERT_LAROD_ERROR_CODE(error, errCode)                                \
    ASSERT_NE(error, nullptr);                                                 \
    EXPECT_EQ(error->code, errCode);                                           \
    EXPECT_GT(std::strlen(error->msg), 0);
