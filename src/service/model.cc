/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "model.hh"

#include <stdexcept>

using namespace std;

namespace larod {

Model::Model(const uint64_t id, const string name, const size_t size,
             const Access access,
             const vector<TensorMetadata> inputTensorMetadata,
             const vector<TensorMetadata> outputTensorMetadata)
    : ID(id), NAME(name), SIZE(size), ACCESS(access),
      INPUT_TENSOR_METADATA(inputTensorMetadata),
      OUTPUT_TENSOR_METADATA(outputTensorMetadata) {
    // Check input tensor metadata.
    if (!INPUT_TENSOR_METADATA.size()) {
        throw invalid_argument("Input tensor metadata are empty");
    }

    // We require a non-zero (defined) byte size.
    for (size_t i = 0; i < INPUT_TENSOR_METADATA.size(); ++i) {
        if (!INPUT_TENSOR_METADATA[i].getByteSize()) {
            throw invalid_argument("Input tensor metadata " + to_string(i) +
                                   " has invalid zero byte size");
        }
    }

    // Check output tensor metadata.
    if (!OUTPUT_TENSOR_METADATA.size()) {
        throw invalid_argument("Output tensor metadata are empty");
    }

    // We require a non-zero (defined) byte size.
    for (size_t i = 0; i < OUTPUT_TENSOR_METADATA.size(); ++i) {
        if (!OUTPUT_TENSOR_METADATA[i].getByteSize()) {
            throw invalid_argument("Output tensor metadata " + to_string(i) +
                                   " has invalid zero byte size");
        }
    }
}

uint64_t Model::getId() const {
    return ID;
}

string Model::getName() const {
    return NAME;
}

size_t Model::getSize() const {
    return SIZE;
}

Model::Access Model::getAccessType() const {
    return ACCESS;
}

bool Model::operator==(const Model& other) const {
    return this->ID == other.ID;
}

Model::operator string() const {
    string access = ACCESS == Access::PUBLIC ? "public" : "private";
    return to_string(ID) + " " + NAME + " " + to_string(SIZE) + " " + access;
}

} // namespace larod
