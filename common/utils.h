/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include "larod.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Add parameter to map.
 *
 * Adds a parameter to @c larodMap. The parameter can be of the form
 * "key:value". Where key can either be a string or an array of numbers
 * separated by ','. For example "image.input.dims:224,224" or
 * "image.input.format:rgb-interleaved".
 *
 * @param map Pointer to map which parameter should be inserted to.
 * @param arg Pointer to string of the form "key:value".
 * @return 0 if no error occurs; otherwise an error code in errno style.
 */
int addParam(larodMap* map, char* arg);

#ifdef __cplusplus
}
#endif
