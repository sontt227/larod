/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "testutils.hh"

#include <algorithm>
#include <cstring>
#include <errno.h>
#include <fstream>
#include <functional>
#include <iostream>
#include <limits>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>

#include "larod.h"

using namespace std;

namespace larod {
namespace test {

static constexpr size_t MAX_PATH_LENGTH = 256;

// This is the content of an "empty" larod model file generated by our
// larod-convert script. It has been generated so that the version field
// contains a 7.
vector<uint8_t> VALID_MODEL_BUF = {
    0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0a, 0x00, 0x12, 0x00, 0x0c,
    0x00, 0x0b, 0x00, 0x04, 0x00, 0x0a, 0x00, 0x00, 0x00, 0x14, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x07, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x06, 0x00, 0x08, 0x00, 0x04, 0x00, 0x06, 0x00, 0x00, 0x00,
    0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

int getTestModelFd() {
    int modelFd = memfd_create("modelFile", 0);
    if (modelFd == -1) {
        throw runtime_error("memfd_syscall failed, errno = " +
                            to_string(-errno));
    }

    size_t totalBytes = 0;
    ssize_t writtenBytes = 0;
    while (totalBytes != VALID_MODEL_BUF.size()) {
        writtenBytes = write(modelFd, VALID_MODEL_BUF.data() + totalBytes,
                             VALID_MODEL_BUF.size() - totalBytes);
        if (writtenBytes == 0) {
            // Whole buffer has been written to file.
            break;
        } else if (writtenBytes < 0) {
            close(modelFd);
            throw runtime_error("Failed writing to file, errno = " +
                                to_string(-errno));
        }
        totalBytes += static_cast<size_t>(writtenBytes);
    }

    // Return file offset to start of file.
    off_t offset = lseek(modelFd, 0, SEEK_SET);
    if (offset == -1) {
        close(modelFd);
        throw runtime_error("Failed lseek to beginning of file, errno = " +
                            to_string(-errno));
    }

    return modelFd;
}

string getErrMsgAndClear(larodError** error) {
    if (!error) {
        return "error ptr is NULL (internal error)";
    }

    if (!*error) {
        return "larodError ptr is NULL";
    }

    string errMsg = "msg member of larodError is NULL";
    if ((*error)->msg) {
        errMsg = (*error)->msg;
    }
    larodClearError(error);

    return errMsg;
}

bool checkLarodResultWithPrint(bool ret, larodError** error) {
    if (!error) {
        throw runtime_error("error ptr is NULL (internal error)");
    }

    if (*error) {
        // error param is valid pointer. This should clearly be treated as
        // failing case regardless of ret parameter.
        string errMsg = "msg member of larodError is NULL";
        if ((*error)->msg) {
            errMsg = string("larodError->msg = ") + (*error)->msg;
        }
        gtestPrint(errMsg);
        larodClearError(error);

        return false;
    }
    return ret;
}

void gtestPrint(const string& msg) {
    cout << "[          ] " << msg << endl;
}

int createTmpFile(size_t sz) {
    string fileName = "/tmp/larod-test-tmp-file-XXXXXX";
    int fd = mkstemp(fileName.data());
    if (fd < 0) {
        throw runtime_error("Could not create temporary file: " +
                            string(strerror(errno)));
    }

    // FIXME: Unsafe cast to off_t from size_t.
    if (ftruncate(fd, static_cast<off_t>(sz)) < 0) {
        close(fd);
        remove(fileName.c_str());
        throw runtime_error("Could not truncate temporary file to size " +
                            to_string(sz) + ": " + strerror(errno));
    }

    remove(fileName.c_str());

    return fd;
}

vector<string> getFdPaths(const vector<int>& fds) {
    vector<string> filePaths;
    char fdPath[MAX_PATH_LENGTH];
    for (auto fd : fds) {
        string fdLink = "/proc/self/fd/" + to_string(fd);
        ssize_t len = ::readlink(fdLink.c_str(), fdPath, sizeof(fdPath) - 1);
        if (len == -1) {
            throw runtime_error("Could not read link");
        }
        fdPath[len] = '\0';
        filePaths.emplace_back(fdPath);
    }

    return filePaths;
}

bool larodHasMaped(vector<string> files) {
    if (files.empty()) {
        throw invalid_argument("No files given");
    }

    // Get the maps file path for larod, i.e. /proc/"pidof larod"/maps.
    char mapsPath[MAX_PATH_LENGTH] = {0};
    strcpy(mapsPath, "/proc/");
    unique_ptr<FILE, function<void(FILE*)>> cmd{popen("pidof larod", "r"),
                                                [](FILE* f) { pclose(f); }};
    if (!cmd ||
        !fgets(mapsPath + strlen(mapsPath), MAX_PATH_LENGTH, cmd.get())) {
        throw runtime_error("Could not get pid of larod");
    }
    strcpy(mapsPath + strlen(mapsPath) - 1, "/maps");

    fstream mapsFile(mapsPath, ios::in);
    if (!mapsFile.good()) {
        throw runtime_error("Could not open " + string(mapsPath));
    }

    // Go through each line and if it encounters a given file, it removes it
    // from the vector. Returns true if all files are found.
    string line;
    while (getline(mapsFile, line)) {
        files.erase(remove_if(files.begin(), files.end(),
                              [&line](const string& file) {
                                  return (line.find(file) != string::npos);
                              }),
                    files.end());
        if (files.empty()) {
            return true;
        }
    }

    return false;
}

size_t getNumOpenLarodFds() {
    char numFdString[4096] = {0};
    unique_ptr<FILE, function<void(FILE*)>> cmd{
        popen("ls /proc/$(pidof larod)/fd | wc -l", "r"),
        [](FILE* f) { pclose(f); }};
    if (!cmd || !fgets(numFdString, MAX_PATH_LENGTH, cmd.get())) {
        throw runtime_error("Could not exec count getting num larod fd:s");
    }
    // Strip away trailing new-line.
    numFdString[strcspn(numFdString, "\r\n")] = '\0';

    istringstream iss(numFdString);
    size_t numFd = 0;
    iss >> numFd;

    return numFd;
}

} // namespace test
} // namespace larod
