/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

#include "larod.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct args_t {
    char* model;
    larodMap* modelParams;
    larodMap* jobParams;
    larodChip chip;
    bool debugPrints;
    bool hasModelParams;
} args_t;

int parseArgs(int argc, char** argv, args_t* args);

void destroyArgs(args_t* args);

#ifdef __cplusplus
}
#endif
