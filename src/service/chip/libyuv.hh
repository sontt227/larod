/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <string>

#include "backendunit.hh"
#include "imageparams.hh"
#include "log.hh"

namespace larod {
namespace backendunit {

class LibYuv : public BackEndUnit {
public:
    LibYuv();
    ~LibYuv();

    LibYuv(const LibYuv&) = delete;
    LibYuv& operator=(const LibYuv&) = delete;

    larodChip getChip() override { return LAROD_CHIP_LIBYUV; }

protected:
    std::pair<std::vector<TensorMetadata>, std::vector<TensorMetadata>>
        loadModelVirtual(const span<uint8_t>& data, const uint64_t modelId,
                         const ParamsMap& params) override;
    void deleteModelVirtual(const uint64_t modelId) override;
    void runJobVirtual(const uint64_t modelId,
                       std::vector<Tensor>& inputTensors,
                       std::vector<Tensor>& outputTensors,
                       const ParamsMap& params) override;

    int getMinModelFormatVersion() const override { return -1; }

private:
    struct Context {
        Context(const ParamsMap& map);

        imageparams::MetaData inputMetaData;
        imageparams::MetaData outputMetaData;
    };

    static inline const std::string THIS_NAMESPACE = "LibYuv::";

    std::unordered_map<uint64_t, std::shared_ptr<Context>> contexts;
    std::mutex mtxContexts;

    std::vector<TensorMetadata> createTensorMetadataFromImgMetadata(
        const imageparams::MetaData& metadata);

    /**
     * @brief Convert format of image buffer.
     *
     * Based on the input image data format, convertFormat() calls
     * convertFormatFromRgbInterleaved() or convertFormatFromNV12().
     *
     * @param inputData Pointer to input image buffer.
     * @param outputData Pointer to output image buffer.
     * @param inputMetaData Meta data describing input image.
     * @param outputMetaDaa Meta data describing output image.
     */
    void convertFormat(uint8_t* inputData, uint8_t* outputData,
                       const imageparams::MetaData& inputMetaData,
                       const imageparams::MetaData& outputMetaData) const;
    void convertFormatFromRgbInterleaved(
        uint8_t* inputData, uint8_t* outputData,
        const imageparams::MetaData& inputMetaData,
        const imageparams::MetaData& outputMetaData) const;
    void convertFormatFromNv12(
        uint8_t* inputData, uint8_t* outputData,
        const imageparams::MetaData& inputMetaData,
        const imageparams::MetaData& outputMetaData) const;

    /**
     * @brief Crop and scale image buffer.
     *
     * @param inputData Pointer to input image buffer.
     * @param outputData Pointer to output image buffer.
     * @param inputMetaData Meta data describing input image.
     * @param outputMetaData Meta data describing output image.
     * @param dynamic Pointer to the transformation struct that specifies ROI.
     */
    void cropScale(uint8_t* inputData, uint8_t* outputData,
                   const imageparams::MetaData& inputMetaData,
                   imageparams::MetaData& outputMetaData,
                   const imageparams::CropInfo& crop) const;
};

} // namespace backendunit
} // namespace larod
