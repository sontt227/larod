/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <edgetpu.h>
#include <string.h>

#include "log.hh"
#include "tflite.hh"
#include "memfd.hh"

namespace larod {
namespace backendunit {

class TFLiteTPU : public TFLite {
public:
    TFLiteTPU();
    ~TFLiteTPU();

    TFLiteTPU(const TFLiteTPU&) = delete;
    TFLiteTPU& operator=(const TFLiteTPU&) = delete;

    larodChip getChip() override { return LAROD_CHIP_TPU; };

    std::unique_ptr<Buffer>
        allocateInput(const size_t size, const uint32_t fdProps,
                      const ParamsMap& params) const override;
    std::unique_ptr<Buffer>
        allocateOutput(const size_t size, const uint32_t fdProps,
                       const ParamsMap& params) const override;

private:
    static inline const std::string THIS_NAMESPACE = "TFLiteTPU::";
    static TfLiteStatus delegatePrepare(TfLiteContext* ctx,
                                        TfLiteDelegate* delegate);
    static TfLiteStatus delegateCopyFromBufferHandle(TfLiteContext* ctx,
                                                     TfLiteDelegate* delegate,
                                                     TfLiteBufferHandle buf,
                                                     TfLiteTensor* tensor);
    static TfLiteStatus delegateCopyToBufferHandle(TfLiteContext* ctx,
                                                   TfLiteDelegate* delegate,
                                                   TfLiteBufferHandle buf,
                                                   TfLiteTensor* tensor);
    static void delegateFreeBufferHandle(TfLiteContext* ctx,
                                         TfLiteDelegate* delegate,
                                         TfLiteBufferHandle* buf);

    std::unique_ptr<edgetpu::EdgeTpuContext> tpuContext;
    TfLiteDelegate dmaDelegate;

    std::unique_ptr<tflite::Interpreter>
        buildInterpreter(const tflite::FlatBufferModel& model) override;
    void setupInput(const std::vector<TfLiteTensor*>& tfInputTensors,
                    const std::vector<Tensor>& inputTensors,
                    tflite::Interpreter* interpreter) override;

    std::shared_ptr<allocator::MemFd> memFdAllocator;
};

} // namespace backendunit
} // namespace larod
