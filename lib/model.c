/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "model.h"

#include <assert.h>
#include <errno.h>

#include "larod.h"
#include "tensor.h"

// As fd properties are added to tensors we make sure that tensors created with
// API versions' < 2.2.0 get such props set on creation and so does not need to
// set any props explicitly.
__asm__(".symver larodCreateModelInputs_v1, "
        "larodCreateModelInputs@@LIBLAROD_1.0.0");

// TODO: Enable this for larod 3.0 (breaks API since by default fd props will
// not be set).
// __asm__(".symver larodCreateModelInputs_v2, "
//         "larodCreateModelInputs@@LIBLAROD_2.2.0");

__asm__(".symver larodCreateModelOutputs_v1, "
        "larodCreateModelOutputs@@LIBLAROD_1.0.0");

// TODO: Enable this for larod 3.0 (breaks API since by default fd props will
// not be set).
// __asm__(".symver larodCreateModelOutputs_v2, "
//         "larodCreateModelOutputs@@LIBLAROD_2.2.0");

bool readModelFromMessage(sd_bus_message* msg, larodModel** model,
                          larodError** error) {
    assert(msg);
    assert(model);

    // Enter model struct.
    int ret = sd_bus_message_enter_container(
        msg, 'r',
        MODEL_SIGNATURE "a(" TENSOR_METADATA_FROM_SERVICE
                        ")a(" TENSOR_METADATA_FROM_SERVICE ")");
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not enter base container: %s",
                         strerror(ret));

        return false;
    } else if (ret == 0) {
        // sd-bus returns 0 upon end-of-array.
        *model = NULL;
        return true;
    }

    larodModel* localModel = calloc(1, sizeof(*localModel));
    if (!localModel) {
        ret = errno;
        larodCreateError(error, ret, "Failed to allocate localModel");
        goto error;
    }

    char* name = NULL;
    ret = sd_bus_message_read(msg, MODEL_SIGNATURE, &localModel->size,
                              &localModel->id, &localModel->chipId,
                              &localModel->chip, &localModel->access, &name);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not read base message: %s",
                         strerror(ret));

        goto error;
    }

    // We have to copy this string since sd-bus owns strings returned from
    // sd_bus_message_read.
    localModel->name = strdup(name);
    if (!localModel->name) {
        ret = errno;
        larodCreateError(error, ret, "Failed to allocate model name buffer");
        goto error;
    }

    // Read input tensor meta data array.
    localModel->inputs =
        tensorMetaDataArrayFromMsg(msg, &localModel->numInputs, error);
    if (!localModel->inputs) {
        goto error;
    }

    // Read output tensor meta data array.
    localModel->outputs =
        tensorMetaDataArrayFromMsg(msg, &localModel->numOutputs, error);
    if (!localModel->outputs) {
        goto error;
    }

    // Exit model struct.
    ret = sd_bus_message_exit_container(msg);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not exit base struct: %s",
                         strerror(ret));

        goto error;
    }

    *model = localModel;
    return true;

error:
    larodDestroyModel(&localModel);
    return false;
}

uint64_t larodGetModelId(const larodModel* model, larodError** error) {
    if (!model) {
        larodCreateError(error, EINVAL, "larodModel is NULL");
        return LAROD_INVALID_MODEL_ID;
    }

    return model->id;
}

larodChip larodGetModelChip(const larodModel* model, larodError** error) {
    if (!model) {
        larodCreateError(error, EINVAL, "larodModel is NULL");
        return LAROD_CHIP_INVALID;
    }

    return model->chip;
}

size_t larodGetModelSize(const larodModel* model, larodError** error) {
    if (!model) {
        larodCreateError(error, EINVAL, "larodModel is NULL");
        return 0;
    }

    return model->size;
}

const char* larodGetModelName(const larodModel* model, larodError** error) {
    if (!model) {
        larodCreateError(error, EINVAL, "larodModel is NULL");
        return NULL;
    }

    return model->name;
}

larodAccess larodGetModelAccess(const larodModel* model, larodError** error) {
    if (!model) {
        larodCreateError(error, EINVAL, "larodModel is NULL");
        return LAROD_ACCESS_INVALID;
    }

    return model->access;
}

size_t larodGetModelNumInputs(const larodModel* model, larodError** error) {
    if (!model) {
        larodCreateError(error, EINVAL, "larodModel is NULL");
        return 0;
    }

    return model->numInputs;
}

size_t larodGetModelNumOutputs(const larodModel* model, larodError** error) {
    if (!model) {
        larodCreateError(error, EINVAL, "larodModel is NULL");
        return 0;
    }

    return model->numOutputs;
}

size_t* larodGetModelInputByteSizes(const larodModel* model, size_t* numInputs,
                                    larodError** error) {
    if (!model) {
        larodCreateError(error, EINVAL, "larodModel is NULL");

        return NULL;
    }

    if (numInputs) {
        *numInputs = model->numInputs;
    }

    size_t* inputByteSizes =
        (size_t*) malloc(model->numInputs * sizeof(size_t));
    if (!inputByteSizes) {
        larodCreateError(error, errno,
                         "Failed allocating input byte size array: %s",
                         strerror(errno));

        return NULL;
    }

    larodTensor** inputTensors = model->inputs;
    for (size_t i = 0; i < model->numInputs; ++i) {
        assert(inputTensors[i]->byteSize);
        assert(inputTensors[i]->byteSize <= SIZE_MAX);
        inputByteSizes[i] = inputTensors[i]->byteSize;
    }

    return inputByteSizes;
}

size_t* larodGetModelOutputByteSizes(const larodModel* model,
                                     size_t* numOutputs, larodError** error) {
    if (!model) {
        larodCreateError(error, EINVAL, "larodModel is NULL");

        return NULL;
    }

    if (numOutputs) {
        *numOutputs = model->numOutputs;
    }

    size_t* outputByteSizes =
        (size_t*) malloc(model->numOutputs * sizeof(size_t));
    if (!outputByteSizes) {
        larodCreateError(error, errno,
                         "Failed allocating output byte size array: %s",
                         strerror(errno));

        return NULL;
    }

    larodTensor** outputTensors = model->outputs;
    for (size_t i = 0; i < model->numOutputs; ++i) {
        assert(outputTensors[i]->byteSize);
        assert(outputTensors[i]->byteSize <= SIZE_MAX);
        outputByteSizes[i] = outputTensors[i]->byteSize;
    }

    return outputByteSizes;
}

larodTensor** larodCreateModelInputs_v2(const larodModel* model,
                                        size_t* numTensors,
                                        larodError** error) {
    if (!model) {
        larodCreateError(error, EINVAL, "larodModel is NULL");

        return NULL;
    }

    larodTensor** tensors =
        copyTensorArray(model->inputs, model->numInputs, error);
    if (!tensors) {
        return NULL;
    }

    if (numTensors) {
        *numTensors = model->numInputs;
    }

    return tensors;
}

larodTensor** larodCreateModelInputs_v1(const larodModel* model,
                                        size_t* numTensors,
                                        larodError** error) {
    larodTensor** tensors = larodCreateModelInputs_v2(model, numTensors, error);
    if (!tensors) {
        return NULL;
    }

    if (!setTensorsFdProps(tensors, model->numInputs, LAROD_FD_PROP_READWRITE,
                           NULL)) {
        // Should never happen...
        larodCreateError(error, 1,
                         "Unable to initialize tensors properly (fd props)");

        return NULL;
    }

    return tensors;
}

larodTensor** larodCreateModelOutputs_v2(const larodModel* model,
                                         size_t* numTensors,
                                         larodError** error) {
    if (!model) {
        larodCreateError(error, EINVAL, "larodModel is NULL");

        return NULL;
    }

    larodTensor** tensors =
        copyTensorArray(model->outputs, model->numOutputs, error);
    if (!tensors) {
        return NULL;
    }

    if (numTensors) {
        *numTensors = model->numOutputs;
    }

    return tensors;
}

larodTensor** larodCreateModelOutputs_v1(const larodModel* model,
                                         size_t* numTensors,
                                         larodError** error) {
    larodTensor** tensors =
        larodCreateModelOutputs_v2(model, numTensors, error);
    if (!tensors) {
        return NULL;
    }

    if (!setTensorsFdProps(tensors, model->numOutputs, LAROD_FD_PROP_READWRITE,
                           NULL)) {
        // Should never happen...
        larodCreateError(error, 1,
                         "Unable to initialize tensors properly (fd props)");

        return NULL;
    }

    return tensors;
}

void larodDestroyModel(larodModel** model) {
    if (!model || !(*model)) {
        return;
    }

    if ((*model)->name) {
        free((char*) (*model)->name);
    }

    if ((*model)->inputs) {
        larodDestroyTensors(&(*model)->inputs, (*model)->numInputs);
    }

    if ((*model)->outputs) {
        larodDestroyTensors(&(*model)->outputs, (*model)->numOutputs);
    }

    free(*model);
    *model = NULL;
}

void larodDestroyModels(larodModel*** models, size_t numModels) {
    if (!models || !(*models)) {
        return;
    }

    for (size_t i = 0; i < numModels; i++) {
        larodModel* model = (*models)[i];
        larodDestroyModel(&model);
    }

    free(*models);
    *models = NULL;
}
