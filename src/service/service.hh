/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <com/axis/Larod1/Service/server.hpp>
#include <sdbusplus/bus/match.hpp>
#include <sdbusplus/server.hpp>
#include <string>
#include <unordered_map>

#include "backendunit.hh"
#include "larod.h"
#include "session.hh"

namespace larod {

using ServiceServerObj =
    sdbusplus::server::object_t<sdbusplus::com::axis::Larod1::server::Service>;

class Service : private ServiceServerObj {
public:
    /**
     * @brief  Constructor for Service.
     *
     * Creates a Service that handles D-Bus requests.
     *
     * @param name Name of the service.
     * @param chips List of chips containing enum larodChip values defined in
     * "larod.h". The service will interface these chips.
     * @param bus sdbusplus bus.
     */
    Service(std::string name, std::string objPath, sdbusplus::bus::bus& bus,
            std::vector<larodChip> chips = {LAROD_CHIP_DEBUG});

    // Services can not be copied
    Service(const Service&) = delete;
    Service& operator=(const Service&) = delete;

    /**
     * @brief Start running the service.
     *
     * This will start the event loop for the service. @p stopEventFd can be
     * used to signal the service to stop (stop() will be called). Note that if
     * @p stopEventFd is closed it will trigger a signal (EPOLLRDHUP) and stop
     * the service.
     *
     * @param stopEventFd Optional stop eventfd to signal the service to stop.
     */
    void start(int stopEventFd = -1);

    /**
     * @brief Stop the service.
     *
     * Note that this function should clearly not be used in any signal handler,
     * use @c stopEventFd in start() instead.
     */
    void stop();

    // D-Bus interface members.

    /**
     * @brief Create a new session to be handled by the service
     */
    std::tuple<uint64_t, sdbusplus::message::unix_fd,
               sdbusplus::message::unix_fd, int32_t, std::string>
        createSession(std::string clientName, uint32_t versionMajor,
                      uint32_t versionMinor, uint32_t versionPatch) override;

    uint64_t nbrOfSessions() const override;
    std::vector<int32_t> availableChips() const override;

private:
    // For debug output.
    static inline const std::string THIS_NAMESPACE = "Service::";

    const std::string SERVICE_NAME;
    const std::string SERVICE_OBJ_PATH;

    sdbusplus::bus::bus& bus; ///< sd-bus bus.
    /// Initialized back end units.
    std::vector<std::unique_ptr<BackEndUnit>> units;
    /// Maps clients' D-Bus names to clients' corresponding sessions.
    std::unordered_map<std::string, std::shared_ptr<Session>> sessions;
    /// Maps clients' D-Bus names to clients' corresponding disconnect matches.
    std::unordered_map<std::string, sdbusplus::bus::match::match> matches;
    /// Maps session IDs to clients' D-Bus names.
    std::unordered_map<uint64_t, std::string> clients;
    /// Event for handling sd-bus messages.
    std::unique_ptr<sd_event, std::function<void(sd_event*)>> event{
        nullptr, [](sd_event* e) { sd_event_unref(e); }};

    /**
     * @brief I/O handler for sd-bus messages.
     *
     * This is the sd_event_io_handler_t function.
     *
     * @param es Event source
     * @param fd Event fd.
     * @param revents Returned revents.
     * @param userdata User data.
     * @return Always returns zero.
     */
    static int sdbusIOHandler(sd_event_source* es, int fd, uint32_t revents,
                              void* userdata);

    /**
     * @brief I/O handler for stopping the service.
     *
     * This is the sd_event_io_handler_t function.
     *
     * @param es Event source
     * @param fd Event fd.
     * @param revents Returned revents.
     * @param userdata User data.
     * @return Always returns zero.
     */
    static int stopServiceIOHandler(sd_event_source* es, int fd,
                                    uint32_t revents, void* userdata);

    /**
     * @brief Get a match rule filtered for a particular client disconnecting
     * from the bus.
     *
     * We narrow down the name change match with the following args:
     *
     * arg0: the client's D-Bus unique name.
     * arg1: the client's D-Bus unique name before the name change.
     * arg2: the client's D-Bus unique name after the name change.
     *
     * Assigning arg0 and arg1 with @p clientName, and leaving arg2 empty,
     * implies that the client used to have a D-Bus name (meaning it was
     * connected to a bus) but no longer does (so cannot be connected to a bus
     * anymore).
     *
     * @param clientName D-Bus unique name of client to filter match rule for
     * name change.
     */
    static std::string filterNameChangeMatch(const std::string& clientName) {
        // Matching the event that any peer on the bus has their D-Bus unique
        // name changed for some reason. In particular this happens if a peer
        // connects to or disconnects from the bus.
        const std::string CLIENT_NAME_CHANGE_MATCH =
            "type=signal,"
            "sender=org.freedesktop.DBus,"
            "path=/org/freedesktop/DBus,"
            "interface=org.freedesktop.DBus,"
            "member=NameOwnerChanged";
        return CLIENT_NAME_CHANGE_MATCH + ",arg0=" + clientName +
               ",arg1=" + clientName + ",arg2=";
    }

    /**
     * @brief Removes a session.
     *
     * @param sessionId ID of session to remove.
     */
    void deleteSession(const uint64_t sessionId);

    /**
     * @brief Cleans up session upon client leaving unannounced.
     *
     * A D-Bus signal handler called whenever a client disconnects unexpectedly
     * from their @c Session. Goes on to erase the session corresponding to the
     * lost client.
     *
     * @param msg The D-Bus message sent along with the signal.
     */
    void clientLostHandler(sdbusplus::message::message& msg);
};

} // namespace larod
