/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "argparse.hh"

#include <argp.h>
#include <iostream>

#include "version.hh"

#define KEY_USAGE (127)

using namespace std;

namespace larod {

static int parseOpt(int key, char* arg, struct argp_state* state);

const struct argp_option opts[] = {
    {"no-syslog", 's', nullptr, 0, "Do not print to syslog.", 0},
    {"help", 'h', nullptr, 0, "Print this help text and exit.", 0},
    {"usage", KEY_USAGE, nullptr, 0, "Print short usage message and exit.", 0},
    {"version", 'v', nullptr, 0, "Print version information and exit.", 0},
    {nullptr, 0, nullptr, 0, nullptr, 0}};
const char* argp_program_bug_address = BUG_REPORT_ADDRESS.c_str();
const struct argp argp = {
    opts,    parseOpt, nullptr, "\nMachine learning service",
    nullptr, nullptr,  nullptr};

int parseArgs(int argc, char** argv, args_t* args) {
    int ret = argp_parse(&argp, argc, argv, ARGP_NO_HELP, nullptr, args);
    if (ret) {
        return ret;
    }

    return 0;
}

int parseOpt(int key, char*, struct argp_state* state) {
    args_t* args = static_cast<args_t*>(state->input);
    switch (key) {
    case 's':
        args->printToSyslog = false;
        break;
    case 'h':
        argp_state_help(state, stdout, ARGP_HELP_STD_HELP);
        break;
    case 'v':
        cout << "larod version " << LAROD_VERSION_STR
#ifdef DEBUG
             << " (debug build)"
#endif
             << "\n\n\x1B[1m" << LOGO << "\x1B[0m" << endl;
        exit(EXIT_SUCCESS);
        break;
    case KEY_USAGE:
        argp_state_help(state, stdout, ARGP_HELP_USAGE | ARGP_HELP_EXIT_OK);
        break;
    case ARGP_KEY_ARG:
        argp_error(state, "too many arguments");
        break;
    case ARGP_KEY_INIT:
        args->printToSyslog = true;
        break;
    case ARGP_KEY_END:
        // check eventual args
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

} // namespace larod
