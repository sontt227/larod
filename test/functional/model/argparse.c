/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "argparse.h"

#include <argp.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "larod.h"
#include "utils.h"

#define KEY_USAGE (127)

static int parseOpt(int key, char* arg, struct argp_state* state);

/**
 * @brief Parses a string as an unsigned long long
 *
 * @param arg String to parse.
 * @param i Pointer to the number being the result of parsing.
 * @return Positive errno style return code (zero means success).
 */
static int parsePosInt(char* arg, unsigned long long* i);

const struct argp_option opts[] = {
    {"chip", 'c', "CHIP", 0,
     "Chooses chip CHIP to run tests on, where CHIP is the enum type "
     "larodChip from the library. If not specified, the default chip is the "
     "debug chip.",
     0},
    {"model", 'g', "MODEL", 0,
     "Specifies from which path to load a model. This option is optional "
     "since some backends do not use a dedicated model file to run jobs with.",
     0},
    {"model-params", 'b', "KEY:VAL", 0,
     "Add additonal parameters when loading model (c.f. larodLoadModel()). "
     "KEY should be a string and VAL should either be a string, integer or an "
     "array of integers separated by \",\". For example "
     "\"image.input.format:rgb-interleaved\", \"image.input.row-pitch:224\" or "
     "\"image.input.size:224,224\".",
     0},
    {"inputs", 'i', "NUM_INPUTS", 0,
     "Specifies the correct number of input tensors. If not specified, the "
     "default number of inputs is 1.",
     0},
    {"outputs", 'o', "NUM_OUTPUTS", 0,
     "Specifies the correct number of output tensors. If not specified, the "
     "default number of outputs is 1.",
     0},
    {"input-byte-sizes", 'j', "INPUT_BYTE_SIZES", 0,
     "Specifies the correct byte sizes of the input tensors (separated by "
     "\":\"). This option must be set if not using debug chip.",
     0},
    {"output-byte-sizes", 'k', "OUTPUT_BYTE_SIZES", 0,
     "Specifies the correct byte sizes of the output tensors (separated by "
     "\":\"). This option must be set if not using debug chip.",
     0},
    {"help", 'h', NULL, 0, "Print this help text and exit.", 0},
    {"usage", KEY_USAGE, NULL, 0, "Print short usage message and exit.", 0},
    {0}};
const struct argp argp = {
    opts,
    parseOpt,
    NULL,
    "Executes the Larod model functional test through Google test framework.",
    NULL,
    NULL,
    NULL};

int parseArgs(int argc, char** argv, args_t* args) {
    int ret = argp_parse(&argp, argc, argv, ARGP_NO_HELP, NULL, args);
    if (ret) {
        return ret;
    }

    // Make sure a model is provided if not using debug chip.
    if (!args->model && args->chip != LAROD_CHIP_DEBUG &&
        !args->hasModelParams) {
        fprintf(stderr,
                "%s: Path to model file or model parameters must be specified "
                "when using other backend than DebugChip.\n",
                argv[0]);
        return EINVAL;
    }

    // Correct values for debug chip.
    if (args->chip == LAROD_CHIP_DEBUG) {
        args->inputByteSizesString = "1";
        args->outputByteSizesString = "4096";
    }

    if (!args->inputByteSizesString && args->chip != LAROD_CHIP_DEBUG) {
        fprintf(stderr,
                "%s: Input byte sizes must be provided if not using debug chip "
                "(use option \"--input-byte-sizes\")\n",
                argv[0]);
        return EINVAL;
    }

    if (!args->outputByteSizesString && args->chip != LAROD_CHIP_DEBUG) {
        fprintf(stderr,
                "%s: Output byte sizes must be provided if not using debug "
                "chip (use option \"--output-byte-sizes\")\n",
                argv[0]);
        return EINVAL;
    }

    return 0;
}

int parseOpt(int key, __attribute__((unused)) char* arg,
             struct argp_state* state) {
    args_t* args = state->input;
    switch (key) {
    case 'c': {
        unsigned long long chip;
        int ret = parsePosInt(arg, &chip);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid chip type");
        }
        // Make sure we don't overflow when casting to enum below.
        if (chip > INT_MAX) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ERANGE,
                         "chip type value too large");
        }
        args->chip = (larodChip) chip;
        break;
    }
    case 'g':
        args->model = arg;
        break;
    case 'b': {
        int ret = addParam(args->modelParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to model parameters", arg);
            return ret;
        }
        args->hasModelParams = true;
        break;
    }
    case 'i': {
        unsigned long long inputs;
        int ret = parsePosInt(arg, &inputs);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid num inputs");
        }
        // Make sure we don't overflow when casting to size_t below.
        if (inputs > SIZE_MAX) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ERANGE, "num inputs too large");
        }
        args->inputs = (size_t) inputs;
        break;
    }
    case 'o': {
        unsigned long long outputs;
        int ret = parsePosInt(arg, &outputs);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid num outputs");
        }
        // Make sure we don't overflow when casting to size_t below.
        if (outputs > SIZE_MAX) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ERANGE, "num outputs too large");
        }
        args->outputs = (size_t) outputs;
        break;
    }
    case 'j': {
        args->inputByteSizesString = arg;
        break;
    }
    case 'k': {
        args->outputByteSizesString = arg;
        break;
    }
    case 'h':
        argp_state_help(state, stdout, ARGP_HELP_STD_HELP);
        break;
    case KEY_USAGE:
        argp_state_help(state, stdout, ARGP_HELP_USAGE | ARGP_HELP_EXIT_OK);
        break;
    case ARGP_KEY_INIT:
        args->chip = LAROD_CHIP_DEBUG;
        args->model = NULL;
        args->inputs = 1;
        args->outputs = 1;
        args->inputByteSizesString = NULL;
        args->outputByteSizesString = NULL;

        args->modelParams = larodCreateMap(NULL);
        if (!args->modelParams) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not allocate model parameters");
        }

        break;
    case ARGP_KEY_END:
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static int parsePosInt(char* arg, unsigned long long* i) {
    char* endPtr;
    *i = strtoull(arg, &endPtr, 0);
    if (*endPtr != '\0') {
        return EINVAL;
    } else if (arg[0] == '-' || *i == 0) {
        return EINVAL;
    } else if (*i == ULLONG_MAX) {
        return ERANGE;
    }

    return 0;
}

void destroyArgs(args_t* args) {
    assert(args);

    larodDestroyMap(&args->modelParams);
}
