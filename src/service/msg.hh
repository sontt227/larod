/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <condition_variable>
#include <exception>

namespace larod {

class Session; // Forward declaration.

class Msg {
public:
    Msg(std::weak_ptr<Session> session);
    virtual ~Msg() = default;

    // Msg can not be copied
    Msg(const Msg&) = delete;
    Msg& operator=(const Msg&) = delete;

    template<class T> void setException(const T& e) {
        setException(make_exception_ptr(e));
    }
    void setException(std::exception_ptr eptr) { this->eptr = eptr; }
    std::exception_ptr getException() const { return eptr; }
    uint64_t getSessionId();

protected:
    std::weak_ptr<Session> session;
    const std::chrono::time_point<std::chrono::steady_clock> CREATION_TIME;

private:
    std::exception_ptr eptr;

public:
    struct SessionExpired : std::runtime_error {
        SessionExpired(const std::string& msg = "Session has expired")
            : runtime_error(msg) {}
    };
};

} // namespace larod
