/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <stddef.h>
#include <systemd/sd-bus.h>

#include "error.h"

/**
 * @brief Enum to represent type of the value of a @c larodPair.
 */
typedef enum {
    LAROD_MAP_TYPE_INVALID,       ///< Uninitialized type.
    LAROD_MAP_TYPE_INT64,         ///< Integer type.
    LAROD_MAP_TYPE_STRING,        ///< String type.
    LAROD_MAP_TYPE_INT64_ARRAY_2, ///< Integer array type size 2.
    LAROD_MAP_TYPE_INT64_ARRAY_4, ///< Integer array type size 4.
} larodMapType;

/**
 * @brief Struct to represent one element in a @c larodMap object.
 *
 * Represents a key-value pair which are the elements in @c larodMap objects.
 * The type of the @c value member is expressed by the @c type member.
 */
typedef struct {
    char* key;         ///< String identifier for key-value pair.
    void* value;       ///< Value tied to @c key.
    larodMapType type; ///< Type of the @c value member.
} larodPair;

/**
 * @brief A struct representing mappings between keys and values.
 */
struct larodMap {
    larodPair** elems; ///< Array of key-value pairs.
    size_t numElems;   ///< Number of elements currently in @c elems.
    size_t capElems;   ///< Current maximum capacity of @c elems.
};

/**
 * @brief Copy a map.
 *
 * @param map Map to copy.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return NULL if error occurred, otherwise a pointer to the copied @c
 * larodMap.
 */
larodMap* copyMap(const larodMap* map, larodError** error);

/**
 * @brief Serialize map to sd-bus message.
 *
 * Writes the @p map collection as an array of dictionary container string as
 * key and variant as value (a{sv}) to the sd-bus message @p msg.
 *
 * @param msg Destination sd-bus message.
 * @param map Key/value collection to serialize.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return true if everything succeeded, otherwise false.
 */
bool appendMapToMsg(sd_bus_message* msg, const larodMap* map,
                    larodError** error);
