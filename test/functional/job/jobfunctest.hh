/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once
#include <gtest/gtest.h>
#include <map>
#include <memory>
#include <string>
#include <tuple>
#include <vector>

#include "argparse.h"
#include "callbackhandler.hh"
#include "dmautils.h"
#include "larod.h"

using DmaBufPtr = std::unique_ptr<DmaBuffer, void (*)(DmaBuffer*)>;

/**
 * @brief Test fixture for job tests.
 *
 * GoogleTest fixture for job tests. Loads/creates necessary file
 * descriptors and releases them when class is destroyed. Test cases creates one
 * or several larod connections by calling addConnection() - these connections
 * are then cleaned up when test case is done.
 */
class JobFuncTest : public ::testing::Test {
public:
    /**
     * @brief Container for a larod connection.
     *
     * Container to group an open Larod connection with a job request etc.
     */
    struct Connection {
        larodConnection* larodConn;
        larodJobRequest* jobReq;
        larodModel* modelPtr;
        size_t numInputs;
        larodTensor** inputTensors;
        size_t numOutputs;
        larodTensor** outputTensors;
        // Note that fds should not be shared among connections since reads on
        // the service side of the fds' contents may happen concurrently and
        // cause file position race conditions.
        std::vector<int> inputFileFds;
        std::vector<int> outputFileFds;
        std::shared_ptr<CallBackHandler> cbh;

        Connection(larodConnection* conn, larodModel* model,
                   larodTensor** inputTensors, larodTensor** outputTensors,
                   std::vector<int>&& inputFileFds,
                   std::vector<int>&& outputFileFds,
                   std::shared_ptr<CallBackHandler> handler)
            : larodConn(conn), jobReq(nullptr), modelPtr(model),
              numInputs(inputFileFds.size()), inputTensors(inputTensors),
              numOutputs(outputFileFds.size()), outputTensors(outputTensors),
              inputFileFds(inputFileFds), outputFileFds(outputFileFds),
              cbh(handler) {}

        larodJobRequest* createJobReq();
        void disconnect();
    };

    JobFuncTest();
    void eraseAllConnections();
    void TearDown() override;

    // Command-line arguments - these are populated by the main function based
    // on the argparse info.
    static bool debugPrints;
    static JobTestBufType inputBufType;
    static JobTestBufType outputBufType;
    static bool trackInputs;
    static bool trackOutputs;
    static bool forceMapInput;
    static bool forceMapOutput;
    static larodChip chip;
    static std::string modelFile;
    static std::vector<std::string> inputFiles;
    static std::vector<std::string> verificationFiles;
    static larodMap* modelParams;
    static larodMap* jobParams;

protected:
    /**
     * @brief Open a larod connection, get graph and tensors.
     *
     * Open a larod connection and loads the graph specified by the client in @c
     * modelFile. Tensor input and verification files will be loaded. The
     * created @c Connection instance will have tensors created based on the
     * model info and populated with corresponding file descriptors.
     *
     * @param chHandlerId Id number for the associated callback handler object.
     * @param enableTracking Set this to true to use @c trackInputs and @c
     * trackOutputs to decide if larodTrackTensor() should be used. Set this to
     * false to never do any calls to larodTrackTensor while setting up
     * input/output tensors.
     * @return Connection object reference if success, otherwise exception is
     * thrown.
     */
    std::shared_ptr<Connection> addConnection(unsigned int chHandlerId,
                                              bool enableTracking = true);

    void gtestDebugPrint(std::string msg);

    /**
     * @brief Init input tensors for external input case.
     *
     * Configures the input tensors when client requested "external allocation".
     * Temp files or dmabufs are created and filled with contents if client
     * have provided input files.
     *
     * @param conn larod connection.
     * @param model The model to configure inputs for.
     * @param enableTracking Set this to true to use @c trackInputs to decide if
     * larodTrackTensor() should be used. Set this to false to never do any
     * calls to larodTrackTensor while setting up the tensors.
     * @return A pair containing an array with the initialized input tensors
     * and a vector with the corresponding buffer file descriptors.
     */
    std::pair<larodTensor**, std::vector<int>>
        initExternalInputTensors(larodConnection* conn, larodModel* model,
                                 bool enableTracking);

    /**
     * @brief Init output tensors for external output case.
     *
     * Configures the output tensors when client requested "external
     * allocation". Temp files or dmabufs are created for the output buffers.
     *
     * @param conn larod connection.
     * @param model The model to configure outputs for.
     * @param enableTracking Set this to true to use @c trackOutputs to decide
     * if larodTrackTensor() should be used. Set this to false to never do any
     * calls to larodTrackTensor while setting up the tensors.
     * @return A pair containing an array with the initialized output tensors
     * and a vector with the corresponding buffer file descriptors.
     */
    std::pair<larodTensor**, std::vector<int>>
        initExternalOutputTensors(larodConnection* conn, larodModel* model,
                                  bool enableTracking);

    /**
     * @brief Initialize input tensor file descriptors for external alloc.
     *
     * Create buffers for input tensors when client have requested "external
     * allocation". If input data files are provided these are read and copied
     * into the allocated input buffers.
     *
     * @param numInputs Number of input tensors.
     * @return A vector containing the created and initialized file descriptors.
     */
    std::vector<int> initExtInputFileDescriptors(size_t numInputs);

    /**
     * @brief Init input tensors for larod-allocated buffers.
     *
     * Configures the input tensors when client requested "larod allocation".
     * Buffers are allocated from larod and filled with input data files (if
     * any).
     *
     * @param conn larod connection.
     * @param model The model to configure inputs for.
     * @return A pair containing an array with the initialized input tensors
     * and a vector with the corresponding buffer file descriptors.
     */
    std::pair<larodTensor**, std::vector<int>>
        initLarodAllocInputTensors(larodConnection* conn, larodModel* model);

    /**
     * @brief Init output tensors for larod-allocated buffers.
     *
     * Buffers are allocated from larod for the "larod allocation" use-case.
     *
     * @param conn larod connection.
     * @param model The model to configure inputs for.
     * @return A pair containing an array with the initialized input tensors
     * and a vector with the corresponding buffer file descriptors.
     */
    std::pair<larodTensor**, std::vector<int>>
        initLarodAllocOutputTensors(larodConnection* conn, larodModel* model);

    /**
     * @brief Initialize input tensor file descriptors for larod buffers.
     *
     * Read input data from files into buffers allocated by larod.
     *
     * @param numInputs Number of input tensors.
     * @param tensors Array of larod input tensors.
     * @return A vector containing the initialized file descriptors
     * corresponding to the @p tensors array.
     */
    std::vector<int> initLarodAllocInputFileDescriptors(size_t numInputs,
                                                        larodTensor** tensors);
    /**
     * @brief Update info fields on a larodTensor.
     *
     * Updates fd, fdSize and fdProps on a larodTensor.
     *
     * @param conn larod connection.
     * @param tensor Pointer to the tensor being updated.
     * @param fd File descriptor for the buffer to associated with the
     * tensor.
     * @param modelReportedSize The size reported by the model for this tensor.
     * @param forceMap True if fd props should only be set to
     * LAROD_FD_PROP_MAP.
     * @param trackTensor Set this to true to call larodTrackTensor() on the
     * @p tensor.
     */
    void updateTensorInfo(larodConnection* conn, larodTensor* const tensor,
                          const int fd, const size_t modelReportedSize,
                          bool forceMap, bool trackTensor);
    /**
     * @brief Create a temp file with given size.
     *
     * Creates a temp file or dmabuf and sets its size to @p size bytes. The
     * created fd is pushed to the end of the @c fdTracker vector to be
     * cleaned up in the fixture tear-down method.
     *
     * @param size Created fd will be truncated to this size.
     * @param bufType Indicates if DmaBuf or temp file should be created.
     * @return File descriptor for the newly created temp file. Exception is
     * thrown upon error.
     */
    int createTempFd(size_t size, JobTestBufType bufType);

    /**
     * @brief Read (parts) of an open fd to buffer.
     *
     * The @p fd can refer both to ordinary fd or to a dmabuf.
     *
     * @param fd File to read data from.
     * @param offset Offset where reading should start.
     * @param size Number of bytes to read. Set this to 0 zero for system to
     * determine the current size of the file.
     * @param bufType Select buffer type of @p fd.
     * @return Vector holding the read bytes. Exception is thrown on error.
     */
    std::vector<uint8_t> readFdToVector(int fd, off_t offset, size_t size,
                                        JobTestBufType bufType);

    /**
     * @brief Write a buffer to an open fd.
     *
     * Write the contents of @p buffer to @p fd starting at @p fdOffset. Note
     * that offset != 0 is not supported for larod-allocated buffers.
     *
     * @param bf Buffer contents to write.
     * @param fd File to read data from.
     * @param offset Offset where writing should start.
     * @param bufType Select buffer type of @p fd.
     */
    void writeVectorToFd(std::vector<uint8_t>& buffer, int fd, off_t fdOffset,
                         JobTestBufType bufType);

    /**
     * @brief Create a temp file and insert data.
     *
     * Creates an empty temp file or dmabuf with size @p offset. The full
     * contents of @p inputFd will be copied into the newly created temp
     * file starting at @p offset.
     *
     * @param fd File to read data from.
     * @param offset Offset where writing should start.
     * @param bufType Indicates buffer type of @p inputFd, also the new fd will
     * get this type.
     * @return Tuple containing the newly created file descriptor and the
     * number of bytes copied from @p inputFd at the end of new temp file.
     */
    std::tuple<int, size_t> buildTempFdWithOffset(int inputFd, off_t offset,
                                                  JobTestBufType bufType);

    /**
     * @brief Custom deleter for unique_ptr DmaBufPtr.
     *
     * @param buffer Pointer to the DmaBuffer object to be destroyed.
     */
    static void destroyDmaBuffer(DmaBuffer* buffer);

protected:
    int modelFd;                          ///< Fd containing the model data.
    size_t* inputByteSizes;               ///< Byte sizes for input tensors.
    size_t* outputByteSizes;              ///< Byte sizes for output tensors.
    std::vector<int> verificationFileFds; ///< Fds with verification data.
    std::vector<int> fdTracker;           ///< Fds in this vector will closed in
                                          ///< the fixture tear-down method.
    std::map<int, DmaBufPtr> dmaBufMap;   ///< Map for tracking DmaBuf objects.
    std::vector<std::shared_ptr<Connection>>
        larodConns; ///< Vector of connection objects.

    const size_t AUTO_SIZE = 0;
};
