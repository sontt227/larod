/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "larod.h"

typedef struct args_t {
    uint64_t deleteModelId;
    uint64_t inferModelId;
    char** inputFiles;
    char** outputFiles;
    size_t numInputs;
    size_t numOutputs;
    size_t inputsArrLen;
    size_t outputsArrLen;
    char* modelFile;
    char* modelName;
    larodMap* modelParams;
    larodMap* jobParams;
    larodAccess modelAccess;
    larodChip chip;
    bool hasModelParams;
    bool listChips;
    bool getSessions;
    bool listModels;
    bool asyncLoadModel;
    bool asyncInfer;
#ifdef READLINE
    bool interactive;
#endif
    bool debug;
} args_t;

/**
 * @brief Parses command line arguments.
 *
 * This allocates necessary members in @c args_t.
 *
 * @param argc argc from main(int argc, char** argv).
 * @param argv argv from main(int argc, char** argv).
 * @param args Pointer to @c args_t. This will be filled in and allocated after
 * a successful call. Deallocate it with @c destroyArgs() when no longer needed.
 * @param interactive Flag to indicate if parsing is done interactively or
 * not. If false, then exit() will be called upon parsing errors (otherwise
 * not).
 * @return Returns true if no errors occurred, otherwise false.
 */
bool parseArgs(int argc, char** argv, args_t* args, const bool interactive);

/**
 * @brief Destructs arguments.
 *
 * This deallocates necessary members in @p args.
 *
 * @param args Arguments to destruct.
 */
void destroyArgs(args_t* args);
