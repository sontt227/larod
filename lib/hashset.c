/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "hashset.h"

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>

#include "linkedlist.h"

#define INIT_NBR_OF_BUCKETS 16 // Should be a power of 2.
#define MAX_LOAD_FACTOR 0.75

struct HashSet {
    LinkedList** buckets;
    size_t size;         ///< Number of elements.
    size_t nbrOfBuckets; ///< Number of buckets.
};

/**
 * @brief Hash function.
 *
 * Based on Knuth's multiplicative method for 64-bit values.
 *
 * @param val Pointer value to hash.
 * @return Hash value.
 */
static inline uint64_t hash(void* val) {
#define PHI_PRIME_64 UINT64_C(11400714819323198483)
    // We multiply the pointer value of val with 11400714819323198483 (a prime
    // number close to the fractional part of golden ratio times 2^64).
    return PHI_PRIME_64 * (uint64_t)(uintptr_t) val;
}

/**
 * @brief Checks if a hash set needs rehashing.
 *
 * @param hashSet Hash set to check need to rehash.
 * @return True if @p hashSet needs rehashing, otherwise false.
 */
static inline bool needsRehashing(HashSet* hashSet) {
    return (float) hashSet->size / (float) hashSet->nbrOfBuckets >=
           MAX_LOAD_FACTOR;
}

/**
 * @brief Create a new hash set.
 *
 * @param hashSet Handle to a hash set to be allocated.
 * @param nbrOfBuckets Number of buckets to allocate for @p hashSet.
 * @return Positive errno style return code (zero means success).
 */
static int newHashSet(HashSet** hashSet, const size_t nbrOfBuckets);

/**
 * @brief Rehash a hash set.
 *
 * @param Handle to a hash set to rehash.
 * @return Positive errno style return code (zero means success).
 */
static int rehash(HashSet** hashSet);

static int newHashSet(HashSet** hashSet, const size_t nbrOfBuckets) {
    assert(hashSet);
    assert(nbrOfBuckets);

    *hashSet = malloc(sizeof(HashSet));
    if (!(*hashSet)) {
        return ENOMEM;
    }

    (*hashSet)->nbrOfBuckets = nbrOfBuckets;
    (*hashSet)->buckets =
        malloc((*hashSet)->nbrOfBuckets * sizeof(LinkedList*));
    if (!(*hashSet)->buckets) {
        free(*hashSet);
        return ENOMEM;
    }

    for (size_t i = 1; i < (*hashSet)->nbrOfBuckets; ++i) {
        (*hashSet)->buckets[i] = NULL;
    }

    int ret;
    for (size_t i = 0; i < (*hashSet)->nbrOfBuckets; ++i) {
        if ((ret = listConstruct(&(*hashSet)->buckets[i]))) {
            hashSetDestruct(hashSet);
            return ret;
        }
    }

    (*hashSet)->size = 0;

    return 0;
}

int hashSetConstruct(HashSet** hashSet) {
    if (!hashSet) {
        return EINVAL;
    }

    return newHashSet(hashSet, INIT_NBR_OF_BUCKETS);
}

void hashSetDestruct(HashSet** hashSet) {
    if (!hashSet || !(*hashSet)) {
        return;
    }

    for (size_t i = 0; i < (*hashSet)->nbrOfBuckets; ++i) {
        listDestruct(&(*hashSet)->buckets[i]);
    }
    free((*hashSet)->buckets);

    free(*hashSet);
    *hashSet = NULL;
}

int hashSetInsert(HashSet** hashSet, void* data) {
    if (!hashSet || !(*hashSet)) {
        return EINVAL;
    }

    int ret;
    if (needsRehashing(*hashSet) && (ret = rehash(hashSet))) {
        return ret;
    }

    uint64_t key = hash(data) & ((*hashSet)->nbrOfBuckets - 1);

    ret = listPushBack((*hashSet)->buckets[key], data);
    if (!ret) {
        (*hashSet)->size += 1;
    }

    return ret;
}

int hashSetErase(HashSet* hashSet, void* data) {
    if (!hashSet) {
        return EINVAL;
    }

    uint64_t key = hash(data) & (hashSet->nbrOfBuckets - 1);

    int ret = listErase(hashSet->buckets[key], data);
    if (!ret) {
        hashSet->size -= 1;
    }

    return ret;
}

int hashSetGetSize(HashSet* hashSet, size_t* size) {
    if (!hashSet || !size) {
        return EINVAL;
    }

    *size = hashSet->size;

    return 0;
}

int hashSetExtractAll(HashSet* hashSet, void*** data, size_t* dataSize) {
    if (!hashSet || !data || !dataSize) {
        return EINVAL;
    }

    *dataSize = hashSet->size;
    *data = malloc(hashSet->size * sizeof(void*));
    if (!(*data)) {
        return ENOMEM;
    }

    void** dataPtr = *data;
    for (size_t i = 0; i < hashSet->nbrOfBuckets; ++i) {
        const Node* firstNode;
        int ret = listGetFirstNode(hashSet->buckets[i], &firstNode);
        if (ret) {
            // Should never happen.
            assert(!ret);
            free(*data);
            *data = NULL;
            *dataSize = 0;

            return ret;
        }

        for (const Node* node = firstNode; node != NULL; node = node->next) {
            *dataPtr++ = node->data;
        }
    }

    return 0;
}

static int rehash(HashSet** hashSet) {
    HashSet* rehashedSet;
    int ret = newHashSet(&rehashedSet, (*hashSet)->nbrOfBuckets << 1);
    if (ret) {
        return ret;
    }

    for (size_t i = 0; i < (*hashSet)->nbrOfBuckets; ++i) {
        const Node* firstNode;
        ret = listGetFirstNode((*hashSet)->buckets[i], &firstNode);
        if (ret) {
            // Should never happen.
            assert(!ret);
            hashSetDestruct(&rehashedSet);

            return ret;
        }

        for (const Node* node = firstNode; node != NULL; node = node->next) {
            ret = hashSetInsert(&rehashedSet, node->data);
            if (ret) {
                hashSetDestruct(&rehashedSet);
                return ret;
            }
        }
    }

    hashSetDestruct(hashSet);
    *hashSet = rehashedSet;

    return 0;
}
