/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "argparse.h"

#include <argp.h>
#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "larod.h"
#include "utils.h"

#define KEY_USAGE (127)

/**
 * @brief Parses a single input key and saves to state.
 *
 * In the case where this is not called by the interactive client this function
 * will call @c argp_failure() with EXIT_FAILURE if an input is deemed invalid
 * or something goes wrong. If the interactive client is used however it will
 * simply return an error code since the interactive client doesn't exit upon
 * argp_failures.
 *
 * @param key Describing which input key has been used.
 * @param arg Pointer to the value assigned to input referenced by @p key.
 * @param state Pointer to argparse state (including inputs state).
 * @return Positive errno style return code (zero means success).
 */
static int parseOpt(int key, char* arg, struct argp_state* state);

/**
 * @brief Generate seed for RNG functions.
 *
 * @param seed Pointer to seed to be generated.
 * @return Positive errno style return code (zero means success).
 */
static int seedRng(unsigned int* seed);

/**
 * @brief Parses a string as an unsigned long long.
 *
 * @param arg String to parse.
 * @param i Pointer to the number being the result of parsing.
 * @return Positive errno style return code (zero means success).
 */
static int parsePosInt(char* arg, unsigned long long* i);

/**
 * @brief Parses a string as a non-negative unsigned long long.
 *
 * @param arg String to parse.
 * @param i Pointer to the number being the result of parsing.
 * @param limit Max limit for data type integer will be saved to.
 * @return Positive errno style return code (zero means success).
 */
static int parseNonNegInt(char* arg, unsigned long long* i,
                          unsigned long long limit);

/**
 * @brief Parses a string as a non-negative float.
 *
 * @param arg String to parse.
 * @param f Pointer to the number being the result of parsing.
 * @param limit Max limit for data type float will be saved to.
 * @return Positive errno style return code (zero means success).
 */
static int parseNonNegFloat(char* arg, float* f, float limit);

const struct argp_option opts[] = {
    {"chip", 'c', "CHIP", 0,
     "Chooses chip CHIP to run tests on, where CHIP is the enum type "
     "larodChip from the library. Supported chip types are "
     "LAROD_CHIP_CVFLOW_PROC and LAROD_CHIP_LIBYUV",
     0},
    {"debug", 'd', NULL, 0, "Enable debug prints.", 4},
    {"model-params", 'b', "KEY:VAL", 0,
     "Add additonal parameters when loading model (c.f. larodLoadModel()). "
     "KEY should be a string and VAL should either be a string, integer or an "
     "array of integers separated by \",\". For example "
     "\"image.input.format:rgb-interleaved\", \"image.input.row-pitch:224\" or "
     "\"image.input.size:224,224\".",
     0},
    {"job-params", 'e', "KEY:VAL", 0,
     "Add additional parameters when running a job (c.f. "
     "larodCreateJobRequest()). See option \"--model-params\" for a description"
     " of KEY and VAL.",
     0},
    {"input", 'i', "INPUT_FILE", 0, "Input image file.", 0},
    {"verify", 'v', "VERIFY_FILE", 0, "Verification image file.", 0},
    {"no-verify", 'n', NULL, 0, "Disable verification of output.", 0},
    {"seed", 's', "SEED", 0,
     "Random seed for generating YCbCr patterns. If no seed is given a random "
     "one will be generated.",
     3},
    {"mean", 'm', "THRESHOLD", 0,
     "The highest acceptable value for the mean difference between the image "
     "to be tested and the reference image. The mean is computed over each "
     "pixel.",
     2},
    {"max", 'x', "THRESHOLD", 0,
     "The highest acceptable value for the maximum difference between the "
     "image to be tested and the reference image. The maximum value is "
     "computed over each pixel.",
     2},
    {"help", 'h', NULL, 0, "Print this help text and exit.", 0},
    {"usage", KEY_USAGE, NULL, 0, "Print short usage message and exit.", 0},
    {0}};

const struct argp argp = {
    opts,
    parseOpt,
    NULL,
    "Executes the larod image process functional test. The test is "
    "performing pre-processing operations on an image that is either "
    "from a file or it will use a generated pseudo-random image pattern, if a "
    "file is not specified. The generated pattern only works with pure format "
    "conversion, i.e. no crop/scale.",
    NULL,
    NULL,
    NULL};

int parseArgs(int argc, char** argv, args_t* args) {
    int ret = argp_parse(&argp, argc, argv, ARGP_NO_HELP, NULL, args);
    if (ret) {
        return ret;
    }

    // Check option conditions

    // Chip type must support image converion
    if (args->chip != LAROD_CHIP_CVFLOW_PROC &&
        args->chip != LAROD_CHIP_LIBYUV && args->chip != LAROD_CHIP_OPENCL) {
        fprintf(stderr,
                "%s: Specify a chip type that is supported by this test\n",
                argv[0]);
        return EINVAL;
    }

    // Input file without verification file.
    if (args->inputFilePath && !args->verificationFilePath &&
        args->verifyOutput) {
        fprintf(stderr, "%s: Input file specified without verification file\n",
                argv[0]);
        return EINVAL;
    }

    // Verification file without input file.
    if (args->verificationFilePath && !args->inputFilePath) {
        fprintf(stderr, "%s: Verification file specified without input file\n",
                argv[0]);
        return EINVAL;
    }

    // Disable verification when verification file is specified.
    if (args->verificationFilePath && !args->verifyOutput) {
        fprintf(stderr,
                "%s: Verification file is specified when verification is "
                "disabled\n",
                argv[0]);
        return EINVAL;
    }

    // Input file with random seed.
    if (args->inputFilePath && args->randomSeed > 0) {
        fprintf(stderr, "%s: Input file specified together with random seed\n",
                argv[0]);
        return EINVAL;
    }

    // If no input file and no input seed, generate a seed.
    if (!args->inputFilePath && args->randomSeed == 0) {
        int ret = seedRng(&(args->randomSeed));
        if (ret) {
            fprintf(stderr, "%s: Could not generate random seed: %s", argv[0],
                    strerror(ret));
            return ret;
        }
    }

    return 0;
}

int parseOpt(int key, char* arg, struct argp_state* state) {
    args_t* args = state->input;
    switch (key) {
    case 'c': {
        unsigned long long chip;
        int ret = parsePosInt(arg, &chip);
        if (ret) {
            argp_failure(state, EXIT_FAILURE, ret, "invalid chip type");
        }
        // Make sure we don't overflow when casting to enum below.
        if (chip > INT_MAX) {
            argp_failure(state, EXIT_FAILURE, ERANGE,
                         "chip type value too large");
        }

        args->chip = (larodChip) chip;
        break;
    }
    case 'd':
        args->debugPrints = true;
        break;
    case 'b': {
        int ret = addParam(args->modelParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to static larodMap", arg);
            exit(EXIT_FAILURE);
        }
        break;
    }
    case 'e': {
        int ret = addParam(args->jobParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to dynamic larodMap", arg);
            exit(EXIT_FAILURE);
        }
        break;
    }
    case 'i':
        args->inputFilePath = arg;
        break;
    case 'n':
        args->verifyOutput = false;
        break;
    case 'v':
        args->verificationFilePath = arg;
        break;
    case 's': {
        unsigned long long seed;
        int ret = parseNonNegInt(arg, &seed, INT_MAX);
        if (ret) {
            argp_failure(state, EXIT_FAILURE, ret, "invalid random seed");
        }

        args->randomSeed = (unsigned int) seed;
        break;
    }
    case 'm': {
        float threshold;
        int ret = parseNonNegFloat(arg, &threshold, 255.0f);
        if (ret) {
            argp_failure(state, EXIT_FAILURE, ret,
                         "invalid mean-abs-diff threshold");
        }

        args->meanAbsDiffThreshold = threshold;
        break;
    }
    case 'x': {
        unsigned long long threshold;
        int ret = parseNonNegInt(arg, &threshold, 255);
        if (ret) {
            argp_failure(state, EXIT_FAILURE, ret,
                         "invalid max-abs-diff threshold");
        }

        args->maxAbsDiffThreshold = (unsigned int) threshold;
        break;
    }
    case 'h':
        argp_state_help(state, stdout, ARGP_HELP_STD_HELP);
        break;
    case KEY_USAGE:
        argp_state_help(state, stdout, ARGP_HELP_USAGE | ARGP_HELP_EXIT_OK);
        break;
    case ARGP_KEY_INIT:
        args->chip = LAROD_CHIP_DEBUG;
        args->debugPrints = false;
        larodError* error = NULL;

        args->modelParams = larodCreateMap(&error);
        if (!args->modelParams || error) {
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not create larodMap: %s", error->msg);
            destroyArgs(args);
            larodClearError(&error);
            exit(EXIT_FAILURE);
        }

        args->jobParams = larodCreateMap(&error);
        if (!args->jobParams || error) {
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not create larodMap: %s", error->msg);
            destroyArgs(args);
            larodClearError(&error);
            exit(EXIT_FAILURE);
        }

        args->inputFilePath = NULL;
        args->verificationFilePath = NULL;
        args->maxAbsDiffThreshold = 0;
        args->randomSeed = 0;
        args->meanAbsDiffThreshold = 0.0f;
        args->verifyOutput = true;
        break;
    case ARGP_KEY_END:
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static int seedRng(unsigned int* seed) {
    unsigned int localSeed;

    int fd = open("/dev/urandom", O_RDONLY);
    if (fd == -1) {
        return errno;
    }

    int ret = 0;
    size_t totalBytesRead = 0;
    ssize_t numBytesRead = -1;
    size_t bytesToRead = sizeof(localSeed);
    char* buf = (char*) &localSeed;

    while (totalBytesRead < bytesToRead) {
        numBytesRead = read(fd, buf, bytesToRead - totalBytesRead);
        if (numBytesRead < 1) {
            ret = errno;

            goto end;
        }

        totalBytesRead += (size_t) numBytesRead;
        buf += numBytesRead;
    }

    *seed = localSeed;

end:
    close(fd);

    return ret;
}

static int parsePosInt(char* arg, unsigned long long* i) {
    char* endPtr;
    *i = strtoull(arg, &endPtr, 0);
    if (*endPtr != '\0') {
        return EINVAL;
    } else if (arg[0] == '-' || *i == 0) {
        return EINVAL;
    } else if (*i == ULLONG_MAX) {
        return ERANGE;
    }

    return 0;
}

static int parseNonNegInt(char* arg, unsigned long long* i,
                          unsigned long long limit) {
    char* endPtr;
    *i = strtoull(arg, &endPtr, 0);
    if (*endPtr != '\0') {
        return EINVAL;
    } else if (arg[0] == '-') {
        return EINVAL;
    } else if (*i == ULLONG_MAX || *i > limit) {
        return ERANGE;
    }

    return 0;
}

static int parseNonNegFloat(char* arg, float* f, float limit) {
    char* endPtr = NULL;
    *f = strtof(arg, &endPtr);
    if (*endPtr != '\0') {
        return EINVAL;
    } else if (*f < 0 || *f > limit) {
        return EINVAL;
    }

    return 0;
}

void destroyArgs(args_t* args) {
    assert(args);

    larodDestroyMap(&args->modelParams);
    larodDestroyMap(&args->jobParams);
}
