/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <nnctrl.h>
#include <tuple>

#include "backendunit.hh"
#include "cavalry.hh"
#include "log.hh"

namespace larod {
namespace backendunit {

class CvFlowNNCtrl : public BackEndUnit {
public:
    CvFlowNNCtrl();
    ~CvFlowNNCtrl();

    larodChip getChip() override { return LAROD_CHIP_CVFLOW_NN; };

protected:
    std::pair<std::vector<TensorMetadata>, std::vector<TensorMetadata>>
        loadModelVirtual(const span<uint8_t>& data, const uint64_t modelId,
                         const ParamsMap& params) override;
    void deleteModelVirtual(const uint64_t modelId) override;
    void runJobVirtual(const uint64_t modelId,
                       std::vector<Tensor>& inputTensors,
                       std::vector<Tensor>& outputTensors,
                       const ParamsMap& params) override;

    std::unique_ptr<Buffer>
        allocateInput(const size_t size, const uint32_t fdProps,
                      const ParamsMap& params) const override;

    std::unique_ptr<Buffer>
        allocateOutput(const size_t size, const uint32_t fdProps,
                       const ParamsMap& params) const override;

    int getMinModelFormatVersion() const override { return 0; }

protected:
    /**
     * @brief Class describing NNCtrl's net (ID).
     *
     * This handles allocations (upon constructing) and deallocations (upon
     * destructing) of all resources regarding a specific net ID.
     */
    class Net {
    public:
        explicit Net(const uint8_t* net);

        Net(const Net& other) = delete;
        Net(Net&& other) = delete;
        Net& operator=(const Net& other) = delete;
        Net& operator=(Net&& other) = delete;

        /**
         * @brief Get the net ID.
         *
         * Be careful with the return value here. Specifically, do not use this
         * with functions such as nnctr_exit_net(). This class takes care of
         * this automatically on destruction.
         *
         * @return The net ID.
         */
        int getId() { return *idPtr; }

        /**
         * @brief Get a copy of the I/O config structs.
         *
         * @return A tuple of config structs with info about input/output.
         */
        std::tuple<net_input_mfd_cfg, net_output_mfd_cfg> getIOConfigs() const;

    private:
        /**
         * @brief Set netIn and netOut.
         *
         * Using manual buffer allocation requires nnctrl_init_net_by_mfd() to
         * be called with I/O configs with no_mem=1. To create such
         * configurations, this function initializes a net from the data,
         * retrieves input/output configurations, which are then modified to set
         * no_mem=1. After this, the I/O configs can be used to initialize a net
         * with manual allocation of buffers.
         */
        void setIOConfigs(const uint8_t* data);

        /**
         * @brief Allocate I/O buffers.
         *
         * Fill cavMemBuffers and set I/O configurations. There is one @c Buffer
         * for each input/output tensor.
         */
        void allocateBuffers();

        /// This is used for automatically release resources allocated by
        /// nnctrl_init_net(). The pointer to @c id is set immediately after a
        /// successful allocation and the custom deleter takes care of calling
        /// nnctrl_exit_net().
        std::unique_ptr<int, std::function<void(int*)>> idPtr{
            nullptr, [](int* id) {
                int ret = nnctrl_exit_net(*id);
                if (ret < 0) {
                    LOG.warning("Could not free net resources (" +
                                std::to_string(ret) + ")");
                }
            }};
        int id;
        net_input_mfd_cfg netIn = {};
        net_output_mfd_cfg netOut = {};
        std::vector<std::string> bufferNames;
        std::vector<std::unique_ptr<allocator::Cavalry::Buffer>> cavMemBuffers;
    };

private:
    static inline const std::string THIS_NAMESPACE = "CvFlowNNCtrl::";
    static constexpr bool CAVALRY_MEM_CLEAN_CACHE = 1;
    static constexpr bool CAVALRY_MEM_INVALIDATE_CACHE = 1;
    static constexpr bool CAVALRY_MEM_ENABLE_CACHE = 1;
    static inline const std::vector<std::pair<io_data_fmt, larodTensorDataType>>
        DATA_TYPES = {
            {{1, 2, 0, 7}, LAROD_TENSOR_DATA_TYPE_FLOAT32},
            {{1, 1, 0, 4}, LAROD_TENSOR_DATA_TYPE_FLOAT16},
            {{1, 3, 0, 0}, LAROD_TENSOR_DATA_TYPE_INT64},
            {{0, 3, 0, 0}, LAROD_TENSOR_DATA_TYPE_UINT64},
            {{1, 2, 0, 0}, LAROD_TENSOR_DATA_TYPE_INT32},
            {{0, 2, 0, 0}, LAROD_TENSOR_DATA_TYPE_UINT32},
            {{1, 1, 0, 0}, LAROD_TENSOR_DATA_TYPE_INT16},
            {{0, 1, 0, 0}, LAROD_TENSOR_DATA_TYPE_UINT16},
            {{1, 0, 0, 0}, LAROD_TENSOR_DATA_TYPE_INT8},
            // Default if no matching type is found
            {{0, 0, 0, 0}, LAROD_TENSOR_DATA_TYPE_UINT8},
        };

    /// Maps model ID to NNCtrl's net.
    std::unordered_map<uint64_t, std::shared_ptr<Net>> models;
    std::mutex mtxModels; ///< Mutex for model set.

    std::pair<std::vector<TensorMetadata>, std::vector<TensorMetadata>>
        getTensorMetadata(int id);
    larodTensorDataType getDataType(uint8_t sign, uint8_t sz, uint8_t expbits);
    template<typename T> TensorMetadata createTensorMetadata(T desc);
    void prepareInputs(net_input_mfd_cfg& netIn,
                       const std::vector<Tensor>& inputTensors) const;
    void prepareOutputs(net_output_mfd_cfg& netOut,
                        const std::vector<Tensor>& outputTensors) const;
    void copyOutputs(net_output_mfd_cfg& netOut,
                     const std::vector<Tensor>& outputTensors) const;

    static std::unique_ptr<Buffer> allocate(const size_t size,
                                            const uint32_t fdProps,
                                            const ParamsMap& params);
};

} // namespace backendunit
} // namespace larod
