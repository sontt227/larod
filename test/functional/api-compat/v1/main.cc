/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <chrono>
#include <condition_variable>
#include <gtest/gtest.h>
#include <mutex>

#include "apicompatversion1.hh"
#include "compatv1utils.hh"

// It is important that the LAROD_API_VERSION_1 macro is defined before
// including larod.h in every file related to this compatibility test.
#include "larod.h"

using namespace std;
using namespace larod::compatv1;

static mutex mtx;
static condition_variable cond;

constexpr int MAX_ERROR_MESSAGE = 30;

typedef struct {
    bool keepWaiting;
    larodErrorCode err;
    char errMsg[MAX_ERROR_MESSAGE];
} CallbackData;

void callback(void* data, larodError* error) {
    CallbackData* callbackData = (CallbackData*) data;

    {
        unique_lock<mutex> lock(mtx);
        // Stop waiting for asynchronous inference.
        callbackData->keepWaiting = false;

        if (error) {
            callbackData->err = error->code;
            strncpy(callbackData->errMsg, error->msg, MAX_ERROR_MESSAGE - 1);

            // Make sure string is NULL-terminated.
            callbackData->errMsg[MAX_ERROR_MESSAGE - 1] = '\0';
        }
    }

    cond.notify_all();
}

TEST_F(ApiCompatVersion1, ApiCompatVersion1) {
    bool ret;
    error = nullptr;
    conn = nullptr;

    ret = larodConnect(&conn, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(conn != nullptr, &error));
    ASSERT_TRUE(ret);

    modelFd = getTestModelFd();

    // Verify version 1 chip setters and getters.
    gtestPrint("Validating version 1 setters and getters");
    ret = larodSetChip(conn, LAROD_CHIP_DEBUG, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    larodChip chip = LAROD_CHIP_INVALID;
    ret = larodGetChipType(conn, &chip, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(chip == LAROD_CHIP_DEBUG, &error));
    ASSERT_TRUE(ret);

    uint64_t chipId = 0;
    ret = larodGetChipId(conn, &chipId, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    ret = larodSetChipId(conn, chipId, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    // Verify version 1 model loading.
    gtestPrint("Validating version 1 model loading");
    model = larodLoadModel(conn, modelFd, LAROD_ACCESS_PRIVATE, "test-model",
                           &error);
    ASSERT_TRUE(checkLarodResultWithPrint(model != nullptr, &error));

    // Verify version 1 error code.
    gtestPrint("Validating version 1 error code");
    larodErrorCode errorCode = LAROD_ERROR_INFERENCE;
    (void) errorCode; // Suppress unused variable warning.

    numInputs = 0;
    inputTensors = larodCreateModelInputs(model, &numInputs, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(inputTensors != nullptr, &error));
    ASSERT_TRUE(numInputs != 0);

    numOutputs = 0;
    outputTensors = larodCreateModelOutputs(model, &numOutputs, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(outputTensors != nullptr, &error));
    ASSERT_TRUE(numOutputs != 0);

    for (size_t i = 0; i < numInputs; ++i) {
        // Set tensor fds to pass tensor validation step.
        ret = larodSetTensorFd(inputTensors[i], modelFd, &error);
        ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
    }
    for (size_t i = 0; i < numOutputs; ++i) {
        // Set tensor fds to pass tensor validation step.
        ret = larodSetTensorFd(outputTensors[i], modelFd, &error);
        ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));
    }

    // Verify version 1 inference request.
    gtestPrint("Validating version 1 inference request creation");
    infReq = larodCreateInferenceRequest(model, inputTensors, numInputs,
                                         outputTensors, numOutputs, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(infReq, &error));

    ret = larodSetInferenceRequestModel(infReq, model, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    ret =
        larodSetInferenceRequestInputs(infReq, inputTensors, numInputs, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    ret = larodSetInferenceRequestOutputs(infReq, outputTensors, numOutputs,
                                          &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    ret = larodSetInferenceRequestPriority(infReq, 100, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    gtestPrint("Validating version 1 synchronous inference");
    ret = larodRunInference(conn, infReq, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    gtestPrint("Validating version 1 asynchronous inference");

    CallbackData callbackData = {true, LAROD_ERROR_NONE, {0}};
    ret = larodRunInferenceAsync(conn, infReq, callback, &callbackData, &error);
    ASSERT_TRUE(checkLarodResultWithPrint(ret, &error));

    {
        unique_lock<mutex> lock(mtx);
        while (callbackData.keepWaiting) {
            // Wait for a maximum of 2 seconds.
            if (cond.wait_for(lock, 2s) == cv_status::timeout) {
                throw runtime_error("Async inference timed out");
            }
        }
    }

    if (callbackData.err) {
        throw runtime_error(string("Async inference error: ") +
                            callbackData.errMsg + " (" +
                            to_string(callbackData.err) + ")");
    }
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

    int ret = RUN_ALL_TESTS();

    return ret;
}
