/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <string.h>

#include "log.hh"
#include "tflite.hh"

namespace larod {
namespace backendunit {

class TFLiteCPU : public TFLite {
public:
    TFLiteCPU() = default;

    TFLiteCPU(const TFLiteCPU&) = delete;
    TFLiteCPU& operator=(const TFLiteCPU&) = delete;

    larodChip getChip() override { return LAROD_CHIP_TFLITE_CPU; };

private:
    static inline const std::string THIS_NAMESPACE = "TFLiteCPU::";

    std::unique_ptr<tflite::Interpreter>
        buildInterpreter(const tflite::FlatBufferModel& model) override;
};

} // namespace backendunit
} // namespace larod
