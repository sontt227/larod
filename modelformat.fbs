// Copyright 2019 Axis Communications
// SPDX-License-Identifier: Apache-2.0

// !!!DEPRECATED!!!
// The larod model format is deprecated and will be removed in the future.

// Describes the larod model format.
//
// Rules in order to keep backwards compatibility:
//
// 1. Fields must never be removed, but can be made deprecated (see FB docs).
// 2. New fields/entries in data structures has to be appended at the end.
//
// The intention of the versioning scheme is to both allow the different
// backends in the Larod service to implement a backend-specific handling of old
// flatbuffer binaries as well as to allow a "best-effort" handling of newer
// ones.
//
// To support this, we add a (single) version number to this file that is
// intended to be incremented when the file is changed.
//
// This enables each individual backend in the service to detect which version
// of the input that it is being given at a certain time, and to take one of the
// following actions:
//
// 1. If the binary is older than the service, but newer than a certain
// (backend-specific) version, it will be completely handled.
// 2. If the binary is older than what the backend can handle, an exception will
// be thrown.
// 3. If the binary is newer, the service will access the fields it has
// knowledge of but others will be ignored.
//
// (This can be implemented based on properties (1,2) above)

namespace larod.modelformat;

// Denotes the version of the format. Due to some technicalities of the
// Flatbuffers representation, we need to handle the version with care.
//
// Values in Flatbuffers have defaults, and defaults are not actually
// represented in the binary data but taken from the generated C++ code. Hence,
// if we happen to use the default version number in some place and then
// increment it in the schema then Flatbuffers will return the incremented
// version number (which makes it impossible for us to retrieve the actual
// version).
//
// To avoid this, we always need to force Flatbuffers to include the version in
// the binary data, and the way to achieve this is to avoid using default
// values. This is why we need to add a "do not use" placeholder (i.e. default
// value) in the enumeration and start the actual version number at 1.

enum Version:int { placeholder_do_not_use = 0, schema = 2 }

// Note that the order in this union is enough to permit a check that a Model is
// being loaded in the correct BackEndUnit subclass.
union BackEnd { Deprecated, TFLite, CvFlow }

table Deprecated {}

table TFLite {
  nativeData:[ubyte];
}

table CvFlow {
  nativeData:[ubyte];
}

table Model {
  version:Version;
  backEndData:BackEnd;
}

root_type Model;
