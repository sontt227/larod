/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "argparse.h"

#include <argp.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "log.h"
#include "utils.h"
#include "version.h"

#define KEY_USAGE (127)

static bool isInteractive; // Interactive mode flag

/**
 * @brief Parses a single input key and saves to state.
 *
 * In the case where this is not called by the interactive client this function
 * will call @c argp_failure() with EXIT_FAILURE if an input is deemed invalid
 * or something goes wrong. If the interactive client is used however it will
 * simply return an error code since the interactive client doesn't exit upon
 * argp_failures.
 *
 * @param key Describing which input key has been used.
 * @param arg Pointer to the value assigned to input referenced by @p key.
 * @param state Pointer to argparse state (including inputs state).
 * @return Positive errno style return code (zero means success).
 */
static int parseOpt(int key, char* arg, struct argp_state* state);

/**
 * @brief Parses a string as a non-negative unsigned long long.
 *
 * @param arg String to parse.
 * @param i Pointer to the number being the result of parsing.
 * @param limit Max limit for data type integer will be saved to.
 * @return Positive errno style return code (zero means success).
 */
static int parseNonNegInt(char* arg, unsigned long long* i,
                          unsigned long long limit);

/**
 * @brief Increases the capacity of a char** if need be.
 *
 * @param strArr Array to expand.
 * @param neededLen The array's currently required length.
 * @param currentLen Pointer to the current length of the array.
 * @return False if any errors occur, otherwise true.
 */
static bool expandStrArr(char*** strArr, size_t neededLen, size_t* currentLen);

const struct argp_option opts[] = {
    {"list-chips", 'a', NULL, 0, "List supported chips.", 0},
    {"debug", 'd', NULL, 0, "Enable debug output.", 0},
    {"model-id", 'f', "ID", 0, "Loaded model ID to run jobs on.", 0},
    {"load-model", 'g', "FILE", 0, "Load model file.", 0},
    {"model-params", 'b', "KEY:VAL", 0,
     "Add additonal parameters when loading model (c.f. larodLoadModel()). "
     "KEY should be a string and VAL should either be a string, integer or an "
     "array of integers separated by \",\". For example "
     "\"image.input.format:rgb-interleaved\", \"image.input.row-pitch:224\" or "
     "\"image.input.size:224,224\".",
     0},
    {"job-params", 'e', "KEY:VAL", 0,
     "Add additional parameters when running a job (c.f. "
     "larodCreateJobRequest()). See option \"--model-params\" for a description"
     " of KEY and VAL.",
     0},
    {"infer", 'i', "FILE", 0,
     "Run jobs/inference on files. Add one of these switches for each input "
     "tensor. The order you add them will map to the order of the input "
     "tensors of the model. You also need to specify which model to run on, "
     "either with option \"--model-id\", or a new model with option "
     "\"--load-model\". The results will be written to \"FILE.outX\" (for "
     "X=1, 2, 3, ...) if not otherwise specified with option \"--out\".",
     0},
    {"chip", 'j', "CHIP", 0,
     "Set chip to load model on (option \"--load-model\"). CHIP is the value "
     "of enum type larodChip from the library.",
     0},
    {"async-load-model", 'k', NULL, 0,
     "Asynchronously load a model. Add this to a \"--load-model\" command. If "
     "not specified, the model will be loaded synchronously. Please note that "
     "the client program will still wait until the model is reported as loaded "
     "by the service. The process is asynchronous only in the service.",
     0},
    {"list-models", 'l', NULL, 0, "List loaded models.", 0},
    {"name", 'n', "NAME", 0,
     "Optional model name. If not specified, the file name will be used.", 0},
    {"out", 'o', "FILE", 0,
     "Write job/inference result to specific files. The order you add them "
     "will map to the order of the output tensors of the model. If fewer files "
     "than the number of output tensors of the model are specified, then the "
     "remaining output tensors will be written to \"INPUT_FILE.outX\" where "
     "INPUT_FILE refers to the first input file given, and X corresponds to "
     "the model's ordering of the output tensors.",
     0},
    {"private", 'p', NULL, 0, "Make model private. The default is public.", 0},
    {"async-infer", 'q', NULL, 0,
     "Asynchronously run a job/inference. Add this to a \"--infer\" command. "
     "If not specified, the job will be performed synchronously. Please "
     "note that the client program will still wait until the job is reported "
     "as finished by the service. The process is asynchronous only in "
     "the service.",
     0},
#ifdef READLINE
    {"interactive", 'r', NULL, 0,
     "Use client in interactive mode. The arguments to this interactive "
     "command line interface are the same ones as the arguments to the client "
     "(e.g. \"-g path/to/model\", \"-l\" and so on).",
     0},
#endif
    {"get-sessions", 's', NULL, 0, "Print current number of sessions.", 0},
    {"delete-model", 't', "ID", 0, "Delete loaded model.", 0},
    {"help", 'h', NULL, 0, "Print this help text and exit.", 0},
    {"usage", KEY_USAGE, NULL, 0, "Print short usage message and exit.", 0},
    {"version", 'v', NULL, 0, "Print version information and exit.", 0},
    {0}};

/**
 * @brief Used by Argp interface to indicate who maintains this code.
 *
 * This is a global variable declared by Argp, used to indicate to end users
 * of this application who to report bugs to.
 */
const char* argp_program_bug_address = BUG_REPORT_ADDRESS;

const struct argp argp = {
    opts, parseOpt, "OPTION", "\nClient for communicating with larod",
    NULL, NULL,     NULL};

bool parseArgs(int argc, char** argv, args_t* args, const bool interactive) {
    isInteractive = interactive;

    int ret;
    if (!isInteractive) {
        ret = argp_parse(&argp, argc, argv, ARGP_NO_HELP, NULL, args);
    } else {
        ret = argp_parse(&argp, argc, argv, ARGP_NO_EXIT | ARGP_NO_HELP, NULL,
                         args);
    }
    if (ret) {
        return false;
    }

    // Check option conditions

    // Infer without model ID or model parameters.
    if (args->numInputs > 0 && args->inferModelId == ULLONG_MAX &&
        !args->modelFile && !args->hasModelParams) {
        fprintf(stderr,
                "%s: no model has been specified to run jobs on (option "
                "\"--infer\" requires either option \"--model-id\", "
                "\"--load-model\" or \"--model-params\")\n",
                argv[0]);
        return false;
    }

    // Model ID without infer
    if (args->inferModelId != ULLONG_MAX && args->numInputs == 0) {
        fprintf(stderr,
                "%s: option \"--model-id\" is only required with option "
                "\"--infer\"\n",
                argv[0]);
        return false;
    }

    // Model ID with loading model or model parameters.
    if (args->inferModelId != ULLONG_MAX &&
        (args->modelFile || args->hasModelParams)) {
        fprintf(stderr,
                "%s: option \"--model-id\" is only required with option "
                "\"--infer\" when no model is being loaded (option "
                "\"--load-model\" or \"--model-params\")\n",
                argv[0]);
        return false;
    }

    // Model name or model access without model file or model parameters.
    if ((args->modelName || args->modelAccess == LAROD_ACCESS_PRIVATE) &&
        !args->modelFile && !args->hasModelParams) {
        fprintf(stderr,
                "%s: model not specified (option \"--load-model\" or "
                "\"--model-params\")\n",
                argv[0]);
        return false;
    }

    // Async load model option without model file or model parameters.
    if (args->asyncLoadModel && !args->modelFile && !args->hasModelParams) {
        fprintf(stderr,
                "%s: option \"--async-load-model\" can only be specified for "
                "option \"--load-model\" or \"--model-params\"\n",
                argv[0]);
        return false;
    }

    // Chip type without model file or model parameters.
    if (args->chip && !args->modelFile && !args->hasModelParams) {
        fprintf(stderr,
                "%s: option \"--chip\" can only be specified for "
                "option \"--load-model\" or \"--model-params\"\n",
                argv[0]);
        return false;
    }

    // Async run job option without running any job.
    if (args->asyncInfer && args->numInputs == 0) {
        fprintf(
            stderr,
            "%s: option \"--async-infer\" can only be specified when "
            "running a job/inference (using at least one \"--infer\" option)\n",
            argv[0]);
        return false;
    }

    // Load model without specifying chip.
    if ((args->modelFile || args->hasModelParams) && args->chip == 0) {
        fprintf(stderr, "%s: chip not specified (option \"--chip\")\n",
                argv[0]);
        return false;
    }

    return true;
}

int parseOpt(int key, char* arg, struct argp_state* state) {
    args_t* args = state->input;
    switch (key) {
    case 'a':
        args->listChips = true;
        break;
    case 'b': {
        int ret = addParam(args->modelParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to model parameters", arg);

            return ret;
        }
        args->hasModelParams = true;
        break;
    }
    case 'e': {
        int ret = addParam(args->jobParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to job parameters", arg);

            return ret;
        }
        break;
    }
    case 'd':
        args->debug = true;
        break;
    case 'f': {
        unsigned long long inferModelId;
        int ret = parseNonNegInt(arg, &inferModelId, UINT64_MAX);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid model ID");

            return ret;
        }
        args->inferModelId = (uint64_t) inferModelId;
        break;
    }
    case 'g':
        args->modelFile = arg;
        break;
    case 'i':
        args->numInputs++;
        if (!expandStrArr(&args->inputFiles, args->numInputs,
                          &args->inputsArrLen)) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "unable to reallocate input file array");

            return ENOMEM;
        }

        if (!strlen(arg)) {
            destroyArgs(args);
            argp_error(state, "input file name cannot be empty");

            return EINVAL;
        }

        args->inputFiles[args->numInputs - 1] = arg;
        break;
    case 'j': {
        unsigned long long chip;
        int ret = parseNonNegInt(arg, &chip, INT_MAX);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid chip value");

            return ret;
        } else if (chip == 0) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, EINVAL,
                         "chip value must be greater than 0");

            return EINVAL;
        }
        args->chip = (larodChip) chip;
        break;
    }
    case 'k':
        args->asyncLoadModel = true;
        break;
    case 'l':
        args->listModels = true;
        break;
    case 'n':
        args->modelName = arg;
        break;
    case 'o':
        args->numOutputs++;
        if (!expandStrArr(&args->outputFiles, args->numOutputs,
                          &args->outputsArrLen)) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "unable to reallocate output file array");

            return ENOMEM;
        }
        args->outputFiles[args->numOutputs - 1] = arg;
        break;
    case 'p':
        args->modelAccess = LAROD_ACCESS_PRIVATE;
        break;
    case 'q':
        args->asyncInfer = true;
        break;
#ifdef READLINE
    case 'r':
        if (!isInteractive) {
            args->interactive = true;
        } else {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, 0, "already in interactive mode");

            return EINVAL;
        }
        break;
#endif
    case 's':
        args->getSessions = true;
        break;
    case 't': {
        unsigned long long deleteModelId;
        int ret = parseNonNegInt(arg, &deleteModelId, UINT64_MAX);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid model ID");

            return ret;
        }
        args->deleteModelId = (uint64_t) deleteModelId;
        break;
    }
    case 'h':
        destroyArgs(args);
        argp_state_help(state, stdout, ARGP_HELP_STD_HELP);
        break;
    case 'v':
        printf("larod-client version %s\n", LAROD_VERSION_STR);
        if (!isInteractive) {
            destroyArgs(args);
            exit(EXIT_SUCCESS);
        }
        break;
    case KEY_USAGE:
        destroyArgs(args);
        argp_state_help(state, stdout, ARGP_HELP_USAGE | ARGP_HELP_EXIT_OK);
        break;
    case ARGP_KEY_ARG:
        destroyArgs(args);
        argp_error(state, "too many arguments");

        return EINVAL;
    case ARGP_KEY_INIT:
        args->deleteModelId = ULLONG_MAX;
        args->inferModelId = ULLONG_MAX;
        args->inputFiles = NULL;
        args->outputFiles = NULL;
        args->numInputs = 0;
        args->numOutputs = 0;
        args->modelFile = NULL;
        args->modelName = NULL;
        args->modelAccess = LAROD_ACCESS_PUBLIC;
        args->chip = LAROD_CHIP_INVALID;
        args->listChips = false;
        args->getSessions = false;
        args->listModels = false;
        args->asyncLoadModel = false;
        args->asyncInfer = false;
        args->modelParams = NULL;
        args->jobParams = NULL;
        args->inputFiles = NULL;
        args->outputFiles = NULL;

        args->modelParams = larodCreateMap(NULL);
        if (!args->modelParams) {
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not allocate model parameters");
        }
        args->jobParams = larodCreateMap(NULL);
        if (!args->jobParams) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not create larodMap");
        }
        args->hasModelParams = false;

        args->inputsArrLen = 1;
        args->inputFiles =
            calloc(args->inputsArrLen, sizeof(*(args->inputFiles)));
        if (!args->inputFiles) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "unable to allocate input file array");

            return ENOMEM;
        }

        args->outputsArrLen = 1;
        args->outputFiles =
            calloc(args->outputsArrLen, sizeof(*(args->outputFiles)));
        if (!args->outputFiles) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "unable to allocate output file array");

            return ENOMEM;
        }
#ifdef READLINE
        args->interactive = false;
#endif
#ifdef DEBUG
        args->debug = true;
#else
        args->debug = false;
#endif
        break;
    case ARGP_KEY_END:
        if (state->argc == 1) {
            destroyArgs(args);
            argp_error(state, "no options given");
        }
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static int parseNonNegInt(char* arg, unsigned long long* i,
                          unsigned long long limit) {
    char* endPtr;
    *i = strtoull(arg, &endPtr, 0);
    if (*endPtr != '\0') {
        return EINVAL;
    } else if (arg[0] == '-') {
        return EINVAL;
    } else if (*i == ULLONG_MAX || *i > limit) {
        return ERANGE;
    }

    return 0;
}

void destroyArgs(args_t* args) {
    assert(args);

    free(args->inputFiles);
    args->inputFiles = NULL;

    free(args->outputFiles);
    args->outputFiles = NULL;

    larodDestroyMap(&args->modelParams);
    larodDestroyMap(&args->jobParams);
}

static bool expandStrArr(char*** strArr, size_t neededLen, size_t* currentLen) {
    assert(strArr);
    assert(currentLen);

    if (neededLen <= *currentLen) {
        return true;
    }
    *currentLen *= 2;

    char** newStrArr = realloc(*strArr, *currentLen * sizeof(*newStrArr));
    if (!newStrArr) {
        return false;
    }
    *strArr = newStrArr;

    return true;
}
