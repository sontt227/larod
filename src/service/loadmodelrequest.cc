/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "loadmodelrequest.hh"

#include "backendunitmessenger.hh"
#include "log.hh"
#include "session.hh"

using namespace std;

namespace larod {

LoadModelRequest::LoadModelRequest(weak_ptr<Session> session, BackEndUnit* unit,
                                   const uint64_t creatorId,
                                   const uint64_t chipId,
                                   const vector<uint8_t>&& data,
                                   const Model::Access access,
                                   const string name, const uint64_t metaData,
                                   const ParamsMap&& params)
    : AsyncMsg(session), CREATOR_ID(creatorId), CHIP_ID(chipId), DATA(data),
      ACCESS(access), NAME(name), META_DATA(metaData),
      PARAMS(std::move(params)), modelId(numeric_limits<uint64_t>::max()),
      backEndUnit(unit) {
}

void LoadModelRequest::signal() {
    // The caller to this request has signaled that the model has indeed been
    // loaded. We should therefore match this in BackEndUnit.
    BackEndUnitMessenger::addModel(backEndUnit, this);

    if (auto sessionPtr = session.lock()) {
        sessionPtr->sendLoadModelResult(this);
    } else {
        LOG.warning("Could not send load model result: Session has expired");

        if (ACCESS == Model::Access::PRIVATE) {
            // Whenever signal() is called, the caller to this LoadModelRequest
            // signals that the model has been loaded. However, if the Session
            // owner of this request (creatorId) unexpectedly dies, and the
            // model was loaded with Model::Access::PRIVATE, then no one is
            // actually "owning" this model. We should therefore take
            // responsiblity to delete this on behalf of our creator to
            // eliminate dangling private models.
            backEndUnit->deleteModel(CREATOR_ID, modelId);
        }
    }
}

void LoadModelRequest::setTensorMetadata(
    pair<vector<TensorMetadata>, vector<TensorMetadata>>&& tensorMetadata) {
    if (tensorMetadata.first.empty() || tensorMetadata.second.empty()) {
        throw invalid_argument("Tensor metadata are empty");
    }

    tie(inputTensorMetadata, outputTensorMetadata) = std::move(tensorMetadata);
}

void LoadModelRequest::setTensorMetadata(
    const pair<vector<TensorMetadata>, vector<TensorMetadata>>&
        tensorMetadata) {
    if (tensorMetadata.first.empty() || tensorMetadata.second.empty()) {
        throw invalid_argument("Tensor metadata are empty");
    }

    inputTensorMetadata.clear();
    for (const auto& tensor : tensorMetadata.first) {
        inputTensorMetadata.emplace_back(tensor);
    }

    outputTensorMetadata.clear();
    for (const auto& tensor : tensorMetadata.second) {
        outputTensorMetadata.emplace_back(tensor);
    }
}

} // namespace larod
