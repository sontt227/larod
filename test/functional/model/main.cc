/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <algorithm>
#include <condition_variable>
#include <cstdio>
#include <fstream>
#include <functional>
#include <gtest/gtest.h>
#include <limits>
#include <stdexcept>

#include "argparse.h"
#include "larod.h"
#include "testmacros.h"

using namespace std;

// Default to debug chip unless otherwise specified from cmd line
static larodChip chip;
static char* modelFile; // Not std::string since argument obtained from argparse
static larodMap* modelParams;
static size_t inputs;
static size_t outputs;
static char* inputByteSizesString;
static char* outputByteSizesString;

static condition_variable cv;
static mutex mtx;

void loadModelSuccessCb(larodModel* model, void* userData, larodError* error) {
    bool* success = static_cast<bool*>(userData);
    if (!model || error) {
        *success = false;
    }

    larodDestroyModel(&model);

    unique_lock<mutex> lk(mtx);
    cv.notify_one();
}

static vector<size_t> parseByteSizes(const string& byteSizesString,
                                     size_t numTensors) {
    vector<size_t> byteSizes;
    stringstream sstream(byteSizesString);
    string str;
    for (size_t i = 0; i < numTensors; ++i) {
        getline(sstream, str, ':');
        unsigned long long sizeLongLong = stoull(str);
        if (sizeLongLong > numeric_limits<size_t>::max()) {
            throw invalid_argument(
                "Parsed bytesize (" + to_string(sizeLongLong) +
                ") is larger than size_t max (" +
                to_string(numeric_limits<size_t>::max()) + ")");
        }
        byteSizes.push_back(static_cast<size_t>(sizeLongLong));
    }

    return byteSizes;
}

// Verify functionality related to loading and listing models
TEST(larodFunctionalTest, TwoModels) {
    larodError* error = nullptr;
    larodConnection* conn = nullptr;

    bool ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    // Make sure we can list models
    size_t numModels;
    larodModel** models = larodGetModels(conn, &numModels, &error);
    ASSERT_NE(models, nullptr);
    ASSERT_EQ(error, nullptr);
    larodDestroyModels(&models, numModels);

    // Create a file to use as model input if debug chip
    bool useDummyFile = false;
    if (modelFile == NULL && chip == LAROD_CHIP_DEBUG) {
        modelFile = const_cast<char*>("/tmp/debug.model");
        ofstream dummyModel(modelFile);
        // larod does not allow empty model files
        dummyModel << "This is a dummy model";
        useDummyFile = true;
    }

    FILE* fpModel = NULL;
    int fd = -1;
    if (modelFile) {
        fpModel = fopen(modelFile, "rb");
        ASSERT_NE(fpModel, nullptr);
        fd = fileno(fpModel);
        ASSERT_GE(fd, 0);
    }

    // Make sure that we can load a private model
    larodModel* privModel =
        larodLoadModel(conn, fd, chip, LAROD_ACCESS_PRIVATE,
                       "private-larod-model", modelParams, &error);
    ASSERT_NE(privModel, nullptr);
    ASSERT_EQ(error, nullptr);

    uint64_t privModelId = larodGetModelId(privModel, &error);
    ASSERT_NE(privModelId, LAROD_INVALID_MODEL_ID);
    ASSERT_EQ(error, nullptr);

    // Make sure larod has accounted for us loading the private model
    models = larodGetModels(conn, &numModels, &error);
    ASSERT_NE(models, nullptr);
    ASSERT_GE(numModels, 1);
    ASSERT_EQ(error, nullptr);
    ptrdiff_t instances =
        count_if(models, models + numModels, [privModelId](larodModel* model) {
            return larodGetModelId(model, NULL) == privModelId;
        });
    ASSERT_EQ(instances, 1);
    larodDestroyModels(&models, numModels);

    // Make sure that we can load a public model with different id than the
    // private model we loaded before
    larodModel* pubModel =
        larodLoadModel(conn, fd, chip, LAROD_ACCESS_PUBLIC,
                       "public-larod-model", modelParams, &error);
    ASSERT_NE(pubModel, nullptr);
    ASSERT_EQ(error, nullptr);

    uint64_t pubModelId = larodGetModelId(pubModel, &error);
    ASSERT_NE(pubModelId, LAROD_INVALID_MODEL_ID);
    ASSERT_NE(pubModelId, privModelId);
    ASSERT_EQ(error, nullptr);

    if (fpModel) {
        int intRet = fclose(fpModel);
        ASSERT_EQ(intRet, 0);
    }

    if (useDummyFile) {
        remove(modelFile);
        modelFile = NULL;
    }

    // Make sure larod has accounted for us loading the public model.
    models = larodGetModels(conn, &numModels, &error);
    ASSERT_NE(models, nullptr);
    ASSERT_GE(numModels, 2);
    ASSERT_EQ(error, nullptr);
    instances =
        count_if(models, models + numModels, [pubModelId](larodModel* model) {
            return larodGetModelId(model, NULL) == pubModelId;
        });
    ASSERT_EQ(instances, 1);
    larodDestroyModels(&models, numModels);

    // Make sure we can delete a private model
    ret = larodDeleteModel(conn, privModel, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    // Make sure we can delete a public model
    ret = larodDeleteModel(conn, pubModel, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    // Make sure larod has actually deleted the models
    models = larodGetModels(conn, &numModels, &error);
    ASSERT_NE(models, nullptr);
    ASSERT_EQ(error, nullptr);
    ret =
        none_of(models, models + numModels,
                [privModelId, pubModelId](larodModel* model) {
                    uint64_t modelId = larodGetModelId(model, NULL);
                    return (modelId == privModelId) || (modelId == pubModelId);
                });
    ASSERT_TRUE(ret);
    larodDestroyModels(&models, numModels);

    ret = larodDisconnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    larodDestroyModel(&privModel);
    larodDestroyModel(&pubModel);
}

// Verify that all private models are deleted by the service when client
// disconnect, even if they were loaded on two different chips.
// The test assume that the DebugChip is available in addition to the currently
// used chip. The test report success/skip when active chip is DebugChip.
TEST(larodFunctionalTest, TwoModelsDifferentChip) {
    larodError* error = nullptr;
    larodConnection* conn = nullptr;

    bool ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    // This test-case require DebugChip + one other chip.
    if (chip == LAROD_CHIP_DEBUG) {
        cout << "Skipping test: We need DebugChip and one more chip for this "
                "test case."
             << endl;
        return;
    }

    FILE* fpModel = NULL;
    int fd = -1;
    if (modelFile) {
        fpModel = fopen(modelFile, "rb");
        ASSERT_NE(fpModel, nullptr);
        fd = fileno(fpModel);
        ASSERT_GE(fd, 0);
    }

    // Load private model onto the current chip.
    larodModel* privModel =
        larodLoadModel(conn, fd, chip, LAROD_ACCESS_PRIVATE,
                       "private-larod-model1", modelParams, &error);
    ASSERT_NE(privModel, nullptr);
    ASSERT_EQ(error, nullptr);

    // Now load second private model on debug chip.
    char* dbgChipModelFile = const_cast<char*>("/tmp/debug.model");
    ofstream dummyModel(dbgChipModelFile);
    // larod does not allow empty model files, let's write a couple of bytes.
    dummyModel << "This is a dummy model";
    dummyModel.flush();

    FILE* fpDbgChipModel = fopen(dbgChipModelFile, "rb");
    ASSERT_NE(fpDbgChipModel, nullptr);
    const int fdDbgChipModel = fileno(fpDbgChipModel);
    ASSERT_GE(fdDbgChipModel, 0);
    larodModel* dbgChipModel = larodLoadModel(
        conn, fdDbgChipModel, LAROD_CHIP_DEBUG, LAROD_ACCESS_PRIVATE,
        "private-larod-model2", nullptr, &error);
    ASSERT_NE(dbgChipModel, nullptr);
    ASSERT_EQ(error, nullptr);

    larodDestroyModel(&privModel);
    ASSERT_EQ(privModel, nullptr);
    larodDestroyModel(&dbgChipModel);
    ASSERT_EQ(dbgChipModel, nullptr);

    if (fpModel) {
        int intRet = fclose(fpModel);
        ASSERT_EQ(intRet, 0);
    }

    remove(dbgChipModelFile);
    int intRet = fclose(fpDbgChipModel);
    ASSERT_EQ(intRet, 0);

    // Close current connection and open a new connection. We expect that
    // service have deleted the two private graphs when the first connection
    // was closed.
    ret = larodDisconnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    // Verify that all models where removed when closing the first larod
    // connection.
    size_t numModels = numeric_limits<std::size_t>::max();
    larodModel** models = larodGetModels(conn, &numModels, &error);
    ASSERT_NE(models, nullptr);
    // We expect no other models being left in the service by other test cases.
    ASSERT_EQ(numModels, 0);
    ASSERT_EQ(error, nullptr);
    larodDestroyModels(&models, numModels);

    ret = larodDisconnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_EQ(error, nullptr);
}

TEST(larodFunctionalTest, InvalidParamsGetModelChip) {
    larodError* error = nullptr;
    larodChip retChip = larodGetModelChip(nullptr, &error);
    EXPECT_EQ(retChip, LAROD_CHIP_INVALID);
    ASSERT_LAROD_ERROR_CODE(error, EINVAL);
    larodClearError(&error);
}

// Verify functionality of model getters
TEST(larodFunctionalTest, ModelGetters) {
    const string MODEL_NAME = "ModelGettersTest";
    const larodAccess MODEL_ACCESS = LAROD_ACCESS_PUBLIC;
    larodError* error = nullptr;
    larodConnection* conn = nullptr;

    bool ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    // Create a file to use as model input if debug chip
    bool useDummyFile = false;
    if (modelFile == NULL && chip == LAROD_CHIP_DEBUG) {
        modelFile = const_cast<char*>("/tmp/debug.model");
        ofstream dummyModel(modelFile);
        // larod does not allow empty model files
        dummyModel << "This is a dummy model";
        useDummyFile = true;
    }

    FILE* fpModel = NULL;
    int fd = -1;
    off_t fileSize = 0;
    if (modelFile) {
        fpModel = fopen(modelFile, "rb");
        ASSERT_NE(fpModel, nullptr);
        fd = fileno(fpModel);
        ASSERT_GE(fd, 0);

        fileSize = lseek(fd, 0, SEEK_END);
        ASSERT_GE(fileSize, 0);
    }

    larodModel* model = larodLoadModel(conn, fd, chip, MODEL_ACCESS,
                                       MODEL_NAME.c_str(), modelParams, &error);
    ASSERT_NE(model, nullptr);
    ASSERT_EQ(error, nullptr);

    uint64_t modelId = larodGetModelId(model, &error);
    ASSERT_NE(modelId, LAROD_INVALID_MODEL_ID);
    ASSERT_EQ(error, nullptr);

    larodChip retChip = larodGetModelChip(model, &error);
    EXPECT_EQ(retChip, chip);
    EXPECT_EQ(error, nullptr);

    larodDestroyModel(&model);
    ASSERT_EQ(model, nullptr);

    model = larodGetModel(conn, modelId, &error);
    ASSERT_NE(model, nullptr);
    ASSERT_EQ(error, nullptr);

    size_t modelSize = larodGetModelSize(model, &error);
    ASSERT_EQ(modelSize, fileSize);
    ASSERT_EQ(error, nullptr);

    string modelName = larodGetModelName(model, &error);
    ASSERT_EQ(modelName, MODEL_NAME);
    ASSERT_EQ(error, nullptr);

    larodAccess modelAccess = larodGetModelAccess(model, &error);
    ASSERT_EQ(modelAccess, MODEL_ACCESS);
    ASSERT_EQ(error, nullptr);

    size_t numInputs = larodGetModelNumInputs(model, &error);
    ASSERT_EQ(numInputs, inputs);
    ASSERT_EQ(error, nullptr);

    size_t numOutputs = larodGetModelNumOutputs(model, &error);
    ASSERT_EQ(numOutputs, outputs);
    ASSERT_EQ(error, nullptr);

    size_t numSizeInputs = 0;
    size_t* modelInputSizes =
        larodGetModelInputByteSizes(model, &numSizeInputs, &error);
    ASSERT_NE(modelInputSizes, nullptr);
    ASSERT_EQ(error, nullptr);
    ASSERT_EQ(numSizeInputs, inputs);

    vector<size_t> userInputSizes =
        parseByteSizes(string(inputByteSizesString), numSizeInputs);
    vector<size_t> modelInputSizesVec(modelInputSizes,
                                      modelInputSizes + numSizeInputs);
    ASSERT_EQ(userInputSizes, modelInputSizesVec);

    size_t numSizeOutputs = 0;
    size_t* modelOutputSizes =
        larodGetModelOutputByteSizes(model, &numSizeOutputs, &error);
    ASSERT_NE(modelOutputSizes, nullptr);
    ASSERT_EQ(error, nullptr);
    ASSERT_EQ(numSizeOutputs, outputs);

    vector<size_t> userOutputSizes =
        parseByteSizes(string(outputByteSizesString), numSizeOutputs);
    vector<size_t> modelOutputSizesVec(modelOutputSizes,
                                       modelOutputSizes + numSizeOutputs);
    ASSERT_EQ(userOutputSizes, modelOutputSizesVec);

    ret = larodDeleteModel(conn, model, &error);
    ASSERT_EQ(ret, true);
    ASSERT_EQ(error, nullptr);

    larodDestroyModel(&model);
    ASSERT_EQ(model, nullptr);

    ret = larodDisconnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    if (fpModel) {
        int intRet = fclose(fpModel);
        ASSERT_EQ(intRet, 0);
    }

    if (useDummyFile) {
        remove(modelFile);
        modelFile = NULL;
    }

    free(modelInputSizes);
    free(modelOutputSizes);
}

TEST(larodFunctionalTest, LoadModelAsync) {
    bool ret;
    larodConnection* conn = nullptr;
    larodError* error = nullptr;

    ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    // Create a file to use as model input if debug chip
    bool useDummyFile = false;
    if (modelFile == NULL && chip == LAROD_CHIP_DEBUG) {
        modelFile = const_cast<char*>("/tmp/debug.model");
        ofstream dummyModel(modelFile);
        // larod does not allow empty model files
        dummyModel << "This is a dummy model";
        useDummyFile = true;
    }

    FILE* fpModel = NULL;
    int fd = -1;
    if (modelFile) {
        fpModel = fopen(modelFile, "rb");
        ASSERT_NE(fpModel, nullptr);
        fd = fileno(fpModel);
        ASSERT_GE(fd, 0);
    }

    bool success = true;
    unique_lock<mutex> lk(mtx);
    ret = larodLoadModelAsync(conn, fd, chip, LAROD_ACCESS_PRIVATE, "test",
                              modelParams, loadModelSuccessCb,
                              static_cast<void*>(&success), &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    cv.wait(lk);
    ASSERT_TRUE(success);

    ret = larodDisconnect(&conn, nullptr);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);

    if (fpModel) {
        int intRet = fclose(fpModel);
        ASSERT_EQ(intRet, 0);
    }

    if (useDummyFile) {
        remove(modelFile);
        modelFile = NULL;
    }
}

// This tests tries to load a TFLite CPU model with the TFLite TPU backend
// and we expect that to fail.
TEST(larodFunctionalTest, LoadModelNonEdgeTPUModelOnEdgeTPUBackend) {
    bool ret;
    larodConnection* conn = nullptr;
    larodError* error = nullptr;
    larodChip* chips = nullptr;
    size_t numChips = 0;

    // We need a TFLite CPU model file for this test.
    if (!modelFile || chip != LAROD_CHIP_TFLITE_CPU) {
        return;
    }

    ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    ret = larodListChips(conn, &chips, &numChips, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(chips, nullptr);
    ASSERT_GT(numChips, 0);
    ASSERT_EQ(error, nullptr);
    bool hasTFLiteTPU = false;
    for (size_t i = 0; i < numChips; i++) {
        if (chips[i] == LAROD_CHIP_TPU) {
            hasTFLiteTPU = true;
        }
    }
    free(chips);

    // We need the EdgeTPU backend for this test.
    if (hasTFLiteTPU) {
        FILE* fpModel = fopen(modelFile, "rb");
        ASSERT_NE(fpModel, nullptr);
        const int fd = fileno(fpModel);
        ASSERT_GE(fd, 0);

        ret = larodLoadModel(conn, fd, LAROD_CHIP_TPU, LAROD_ACCESS_PRIVATE,
                             "test", modelParams, &error);
        EXPECT_FALSE(ret);
        ASSERT_LAROD_ERROR_CODE(error, LAROD_ERROR_LOAD_MODEL);
        larodClearError(&error);

        int intRet = fclose(fpModel);
        ASSERT_EQ(intRet, 0);
    }

    ret = larodDisconnect(&conn, nullptr);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
}

TEST(larodFunctionalTest, LoadModelInvalidParameter) {
    larodError* error = nullptr;
    larodConnection* conn = nullptr;

    bool ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    ret = larodLoadModel(nullptr, 0, chip, LAROD_ACCESS_PRIVATE, "", nullptr,
                         &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR_CODE(error, EINVAL);
    larodClearError(&error);

    ret = larodLoadModel(conn, -1, chip, LAROD_ACCESS_PRIVATE, "", nullptr,
                         &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR_CODE(error, EBADF);
    larodClearError(&error);

    ret = larodDisconnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_EQ(error, nullptr);
}

TEST(larodFunctionalTest, LoadModelAsyncInvalidParameter) {
    larodError* error = nullptr;
    larodConnection* conn = nullptr;

    bool ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    // Create a file to use as model input if debug chip
    bool useDummyFile = false;
    if (modelFile == NULL && chip == LAROD_CHIP_DEBUG) {
        modelFile = const_cast<char*>("/tmp/debug.model");
        ofstream dummyModel(modelFile);
        // larod does not allow empty model files
        dummyModel << "This is a dummy model";
        useDummyFile = true;
    }

    FILE* fpModel = NULL;
    int fd = -1;
    if (modelFile) {
        fpModel = fopen(modelFile, "rb");
        ASSERT_NE(fpModel, nullptr);
        fd = fileno(fpModel);
        ASSERT_GE(fd, 0);
    }

    ret = larodLoadModelAsync(nullptr, 0, chip, LAROD_ACCESS_PRIVATE, "",
                              nullptr, nullptr, nullptr, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR_CODE(error, EINVAL);
    larodClearError(&error);

    bool success = true;
    ret = larodLoadModelAsync(conn, -1, chip, LAROD_ACCESS_PRIVATE, "", nullptr,
                              loadModelSuccessCb, static_cast<void*>(&success),
                              &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR_CODE(error, EBADF);
    larodClearError(&error);

    ret = larodLoadModelAsync(conn, fd, chip, LAROD_ACCESS_PRIVATE, "", nullptr,
                              nullptr, nullptr, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR_CODE(error, EINVAL);
    larodClearError(&error);

    ret = larodDisconnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    if (fpModel) {
        int intRet = fclose(fpModel);
        ASSERT_EQ(intRet, 0);
    }

    if (useDummyFile) {
        remove(modelFile);
        modelFile = NULL;
    }
}

TEST(larodFunctionalTest, DeleteModelInvalidParameter) {
    larodError* error = nullptr;

    bool ret = larodDeleteModel(nullptr, 0, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR_CODE(error, EINVAL);
    larodClearError(&error);
}

TEST(larodFunctionalTest, DeleteModelPermissionDenied) {
    larodError* error = nullptr;
    larodConnection* conn1 = nullptr;
    larodConnection* conn2 = nullptr;
    larodAccess MODEL_ACCESS = LAROD_ACCESS_PRIVATE;
    const string MODEL_NAME = "PermissionDeniedTest";

    bool ret = larodConnect(&conn1, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn1, nullptr);
    ASSERT_EQ(error, nullptr);

    ret = larodConnect(&conn2, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn2, nullptr);
    ASSERT_EQ(error, nullptr);

    // Create a file to use as model input if debug chip
    bool useDummyFile = false;
    if (modelFile == NULL && chip == LAROD_CHIP_DEBUG) {
        modelFile = const_cast<char*>("/tmp/debug.model");
        ofstream dummyModel(modelFile);
        // larod does not allow empty model files
        dummyModel << "This is a dummy model";
        useDummyFile = true;
    }

    FILE* fpModel = NULL;
    int fd = -1;
    if (modelFile) {
        fpModel = fopen(modelFile, "rb");
        ASSERT_NE(fpModel, nullptr);
        fd = fileno(fpModel);
        ASSERT_GE(fd, 0);
    }

    larodModel* model = larodLoadModel(conn1, fd, chip, MODEL_ACCESS,
                                       MODEL_NAME.c_str(), modelParams, &error);
    ASSERT_NE(model, nullptr);
    ASSERT_EQ(error, nullptr);

    // Make sure we cannot delete the private model from another connection.
    ret = larodDeleteModel(conn2, model, &error);
    ASSERT_EQ(ret, false);
    ASSERT_NE(error, nullptr);

    larodClearError(&error);
    ASSERT_EQ(error, nullptr);

    ret = larodDeleteModel(conn1, model, &error);
    ASSERT_EQ(ret, true);
    ASSERT_EQ(error, nullptr);

    larodDestroyModel(&model);
    ASSERT_EQ(model, nullptr);

    ret = larodDisconnect(&conn1, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn1, nullptr);
    ASSERT_EQ(error, nullptr);

    ret = larodDisconnect(&conn2, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn2, nullptr);
    ASSERT_EQ(error, nullptr);

    if (fpModel) {
        int intRet = fclose(fpModel);
        ASSERT_EQ(intRet, 0);
    }

    if (useDummyFile) {
        remove(modelFile);
        modelFile = NULL;
    }
}

TEST(larodFunctionalTest, GetModelsInvalidParameter) {
    larodError* error = nullptr;
    larodConnection* conn = nullptr;
    larodModel** models = nullptr;
    size_t numModels = numeric_limits<size_t>::max();

    bool ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    models = larodGetModels(nullptr, &numModels, &error);
    ASSERT_EQ(models, nullptr);
    ASSERT_NE(error, nullptr);
    ASSERT_EQ(error->code, EINVAL);
    larodClearError(&error);
    ASSERT_EQ(error, nullptr);

    models = larodGetModels(conn, nullptr, &error);
    ASSERT_EQ(models, nullptr);
    ASSERT_NE(error, nullptr);
    ASSERT_EQ(error->code, EINVAL);
    larodClearError(&error);
    ASSERT_EQ(error, nullptr);

    ret = larodDisconnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_EQ(error, nullptr);
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

    args_t args;

    if (parseArgs(argc, argv, &args) != 0) {
        exit(EXIT_FAILURE);
    }
    auto argsHandler = unique_ptr<args_t, function<void(args_t*)>>{
        &args, [](args_t* a) { destroyArgs(a); }};

    chip = args.chip;
    modelFile = args.model;
    modelParams = args.modelParams;
    inputs = args.inputs;
    outputs = args.outputs;
    inputByteSizesString = args.inputByteSizesString;
    outputByteSizesString = args.outputByteSizesString;

    return RUN_ALL_TESTS();
}
