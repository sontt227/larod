/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "map.h"

#include <assert.h>
#include <errno.h>
#include <string.h>
#include <systemd/sd-bus.h>

#include "error.h"
#include "larod.h"

// Stringify functions.
#define XSTR(x) STR(x)
#define STR(x) #x

// Max length of key and value strings.
#define STRING_MAX_LENGTH 128

// Initial length of array in larodMap.
#define MAP_INITIAL_LEN 8

// Used as return value by findPair() to indicate outcome.
typedef enum {
    SUCCESS,
    ERROR_TYPE_MISMATCH,
    ERROR_PAIR_NOT_FOUND,
} returnStatus;

/**
 * @brief Create a new map.
 *
 * @param len Maximum length of this new map (@c capElems)
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return NULL if error occurred, otherwise a pointer to an initialized @c
 * larodMap struct. Should be destroyed using @c larodDestroyMap when no longer
 * needed.
 */
static larodMap* newMap(const size_t len, larodError** error);

/**
 * @brief Increase capacity of @c elems of a @c larodMap object.
 *
 * Increases the capacity of @c elems by a factor 2 in a @c larodMap object.
 *
 * @param map Map to increase capacity of.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if reallocation was unsuccessful, otherwise true.
 */
static bool reallocElems(larodMap* map, larodError** error);

/**
 * @brief Create an empty pair.
 *
 * The pair will have key, value and type set to NULL, NULL and
 * LAROD_MAP_INVALID, respectively.
 *
 * @return NULL if error occured, otherwise an empty allocated pair. Use @c
 * destroyPair() when not needed anymore.
 */
static larodPair* createEmptyPair();

/**
 * @brief Copy a pair.
 *
 * @param lhs Pair to copy to.
 * @param rhs Pair to copy from.
 * @return True if no errors occured, otherwise false.
 */
static bool copyPair(larodPair* lhs, larodPair* rhs);

/**
 * @brief Search for element with given key and type.
 *
 * Searches @p map for the given key string and given @c larodMapType.
 *
 * @param map Object to search through.
 * @param key Key string to search for.
 * @param type Value @c larodMapType to search for.
 * @param outPair Output handle which will be pointed towards the requested @c
 * larodPair in @p map, provided that one with the correct key and type is
 * found.
 * @return ERROR_TYPE_MISMATCH if the given key was found, but the @c
 * larodMapType of the element did not match @p type, ERROR_PAIR_NOT_FOUND if
 * the given key was not found, otherwise SUCCESS.
 */
static returnStatus findPair(const larodMap* map, const char* key,
                             larodMapType type, larodPair** outPair,
                             larodError** error);

static bool addPairIntArr(larodMap* map, const char* key, const int64_t* value,
                          larodMapType type, larodError** error);

static const int64_t* getIntArr(larodMap* map, const char* key,
                                larodMapType type, larodError** error);

static bool validateString(const char* key, larodError** error);

static larodPair* createEmptyPair() {
    larodPair* pair = malloc(sizeof(larodPair));
    if (!pair) {
        return NULL;
    }

    pair->key = NULL;
    pair->value = NULL;
    pair->type = LAROD_MAP_TYPE_INVALID;

    return pair;
}

static void destroyPair(larodPair** pair) {
    if (!pair || !*pair) {
        return;
    }

    free((*pair)->key);
    free((*pair)->value);
    free(*pair);
    *pair = NULL;
}

static larodMap* newMap(const size_t len, larodError** error) {
    larodMap* newMap = calloc(1, sizeof(larodMap));
    if (!newMap) {
        larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));

        return NULL;
    }

    assert(len);
    newMap->elems = malloc(len * sizeof(larodPair*));
    if (!newMap->elems) {
        larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));

        goto error;
    }

    newMap->capElems = len;

    for (size_t i = 0; i < newMap->capElems; ++i) {
        newMap->elems[i] = NULL;
    }

    for (size_t i = 0; i < newMap->capElems; ++i) {
        newMap->elems[i] = createEmptyPair();
        if (!newMap->elems[i]) {
            larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));

            goto error;
        }
    }

    return newMap;

error:
    larodDestroyMap(&newMap);

    return NULL;
}

larodMap* larodCreateMap(larodError** error) {
    return newMap(MAP_INITIAL_LEN, error);
}

void larodDestroyMap(larodMap** map) {
    if (!map || !*map) {
        return;
    }

    for (size_t i = 0; i < (*map)->capElems; ++i) {
        destroyPair(&(*map)->elems[i]);
    }

    free((*map)->elems);
    free(*map);
    *map = NULL;
}

bool reallocElems(larodMap* map, larodError** error) {
    assert(map);

    larodPair** elems =
        realloc(map->elems, 2 * map->capElems * sizeof(larodPair*));
    if (!elems) {
        larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));

        return false;
    }

    size_t oldCapacity = map->capElems;
    map->capElems = 2 * map->capElems;
    map->elems = elems;
    for (size_t i = oldCapacity; i < map->capElems; ++i) {
        map->elems[i] = NULL;
    }

    for (size_t i = oldCapacity; i < map->capElems; ++i) {
        map->elems[i] = createEmptyPair();
        if (!map->elems[i]) {
            larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));

            return false;
        }
    }

    return true;
}

returnStatus findPair(const larodMap* map, const char* key,
                      const larodMapType type, larodPair** outPair,
                      larodError** error) {
    assert(map);
    assert(validateString(key, NULL));
    assert(outPair);

    // Linearly search list of elements for the given key.
    for (size_t i = 0; i < map->numElems; ++i) {
        larodPair* pair = (map->elems[i]);

        if (!strncmp(pair->key, key, STRING_MAX_LENGTH)) {
            if (pair->type != type) {
                larodCreateError(
                    error, EINVAL,
                    "Element with the given key existed, but the type of "
                    "corresponding value did not match the expected type");
                return ERROR_TYPE_MISMATCH;
            }

            *outPair = pair;
            return SUCCESS;
        }
    }

    larodCreateError(error, EINVAL, "Element with the given key was not found");
    return ERROR_PAIR_NOT_FOUND;
}

bool validateString(const char* inStr, larodError** error) {
    if (!inStr) {
        larodCreateError(error, EINVAL, "Input string is NULL");
        return false;
    }
    if (!strncmp(inStr, "", STRING_MAX_LENGTH)) {
        larodCreateError(error, EINVAL, "Input string is empty");
        return false;
    }
    if (strnlen(inStr, STRING_MAX_LENGTH) == STRING_MAX_LENGTH) {
        larodCreateError(error, EINVAL,
                         "Input strings must be no more than " XSTR(
                             STRING_MAX_LENGTH) " characters (including "
                                                "terminating null byte)");
        return false;
    }

    return true;
}

bool addPair(larodMap* map, const char* key, void* value, larodMapType type,
             larodError** error) {
    assert(map);
    assert(validateString(key, NULL));
    assert(value);

    larodPair* foundPair = NULL;
    returnStatus ret = findPair(map, key, type, &foundPair, error);
    switch (ret) {
    case SUCCESS:
        break;
    case ERROR_PAIR_NOT_FOUND:
        // If a given key is not found it should not be considered an error when
        // setting a value, so clear this error here.
        larodClearError(error);
        break;
    case ERROR_TYPE_MISMATCH:
        return false;
    default:
        // Should never happen.
        assert(0);
        return false;
    }

    // If key exists, replace the value.
    if (foundPair) {
        free(foundPair->value);
        foundPair->value = value;

        return true;
    }

    // Insert new pair.
    // Check if we neeed to reallocate first.
    if (map->numElems == map->capElems) {
        if (!reallocElems(map, error)) {
            return false;
        }
    }

    larodPair* pair = map->elems[map->numElems];
    pair->key = strndup(key, STRING_MAX_LENGTH);
    if (!pair->key) {
        larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));

        return false;
    }

    pair->value = value;
    pair->type = type;

    ++map->numElems;

    return true;
}

bool larodMapSetStr(larodMap* map, const char* key, const char* value,
                    larodError** error) {
    if (!map) {
        larodCreateError(error, EINVAL, "larodMap is NULL");
        return false;
    }

    if (!validateString(key, error)) {
        return false;
    }

    if (!validateString(value, error)) {
        return false;
    }

    char* cpyValue = strndup(value, STRING_MAX_LENGTH);
    if (!cpyValue) {
        larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));
        return false;
    }

    if (!addPair(map, key, cpyValue, LAROD_MAP_TYPE_STRING, error)) {
        goto error;
    }

    return true;

error:
    free(cpyValue);

    return false;
}

bool larodMapSetInt(larodMap* map, const char* key, const int64_t value,
                    larodError** error) {
    if (!map) {
        larodCreateError(error, EINVAL, "larodMap is NULL");
        return false;
    }

    if (!validateString(key, error)) {
        return false;
    }

    // Copy input value.
    int64_t* cpyValue = calloc(1, sizeof(*cpyValue));
    if (!cpyValue) {
        larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));
        return false;
    }
    *cpyValue = value;

    if (!addPair(map, key, cpyValue, LAROD_MAP_TYPE_INT64, error)) {
        goto error;
    }

    return true;

error:
    free(cpyValue);

    return false;
}

bool addPairIntArr(larodMap* map, const char* key, const int64_t* value,
                   larodMapType type, larodError** error) {
    if (!map) {
        larodCreateError(error, EINVAL, "larodMap is NULL");
        return false;
    }

    if (!validateString(key, error)) {
        return false;
    }

    if (!value) {
        larodCreateError(error, EINVAL, "value is NULL");
        return false;
    }

    size_t numElems = 0;
    if (type == LAROD_MAP_TYPE_INT64_ARRAY_2) {
        numElems = 2;
    } else if (type == LAROD_MAP_TYPE_INT64_ARRAY_4) {
        numElems = 4;
    } else {
        // Should never happen.
        assert(0);
        larodCreateError(error, EINVAL, "Value type is not an array");
        return false;
    }

    int64_t* cpyValue = calloc(numElems, sizeof(*cpyValue));
    if (!cpyValue) {
        larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));
        return false;
    }
    memcpy(cpyValue, value, numElems * sizeof(*cpyValue));

    if (!addPair(map, key, cpyValue, type, error)) {
        goto error;
    }

    return true;

error:
    free(cpyValue);

    return false;
}

bool larodMapSetIntArr2(larodMap* map, const char* key, int64_t value0,
                        int64_t value1, larodError** error) {
    int64_t tmpArr[2] = {value0, value1};
    return addPairIntArr(map, key, tmpArr, LAROD_MAP_TYPE_INT64_ARRAY_2, error);
}

bool larodMapSetIntArr4(larodMap* map, const char* key, int64_t value0,
                        int64_t value1, int64_t value2, int64_t value3,
                        larodError** error) {
    int64_t tmpArr[4] = {value0, value1, value2, value3};
    return addPairIntArr(map, key, tmpArr, LAROD_MAP_TYPE_INT64_ARRAY_4, error);
}

const char* larodMapGetStr(larodMap* map, const char* key, larodError** error) {
    if (!map) {
        larodCreateError(error, EINVAL, "larodMap is NULL");
        return NULL;
    }

    if (!validateString(key, error)) {
        return NULL;
    }

    larodPair* foundPair = NULL;
    returnStatus ret =
        findPair(map, key, LAROD_MAP_TYPE_STRING, &foundPair, error);
    switch (ret) {
    case SUCCESS:
        break;
    case ERROR_PAIR_NOT_FOUND:
    case ERROR_TYPE_MISMATCH:
        return NULL;
    default:
        // Should never happen.
        assert(0);
        return NULL;
    }

    return foundPair->value;
}

bool larodMapGetInt(larodMap* map, const char* key, int64_t* value,
                    larodError** error) {
    if (!map) {
        larodCreateError(error, EINVAL, "larodMap is NULL");
        return false;
    }

    if (!validateString(key, error)) {
        return false;
    }

    if (!value) {
        larodCreateError(error, EINVAL, "value is NULL");
        return false;
    }

    larodPair* foundPair = NULL;
    returnStatus ret =
        findPair(map, key, LAROD_MAP_TYPE_INT64, &foundPair, error);
    switch (ret) {
    case SUCCESS:
        break;
    case ERROR_PAIR_NOT_FOUND:
    case ERROR_TYPE_MISMATCH:
        return false;
    default:
        // Should never happen.
        assert(0);
        return false;
    }

    *value = *((int64_t*) foundPair->value);
    return true;
}

const int64_t* getIntArr(larodMap* map, const char* key, larodMapType type,
                         larodError** error) {
    if (!map) {
        larodCreateError(error, EINVAL, "larodMap is NULL");
        return false;
    }

    if (!validateString(key, error)) {
        return false;
    }

    larodPair* foundPair = NULL;
    returnStatus ret = findPair(map, key, type, &foundPair, error);
    switch (ret) {
    case SUCCESS:
        break;
    case ERROR_PAIR_NOT_FOUND:
    case ERROR_TYPE_MISMATCH:
        return NULL;
    default:
        // Should never happen.
        assert(0);
        return NULL;
    }

    return foundPair->value;
}

const int64_t* larodMapGetIntArr2(larodMap* map, const char* key,
                                  larodError** error) {
    return getIntArr(map, key, LAROD_MAP_TYPE_INT64_ARRAY_2, error);
}

const int64_t* larodMapGetIntArr4(larodMap* map, const char* key,
                                  larodError** error) {
    return getIntArr(map, key, LAROD_MAP_TYPE_INT64_ARRAY_4, error);
}

static bool copyPair(larodPair* lhs, larodPair* rhs) {
    assert(lhs);
    assert(!lhs->key);
    assert(!lhs->value);
    assert(rhs);
    assert(rhs->key);
    assert(rhs->value);

    // Copy key.
    lhs->key = strndup(rhs->key, STRING_MAX_LENGTH);
    if (!lhs->key) {
        goto error;
    }

    // Determine size of value to copy.
    size_t valueSz = 0;
    switch (rhs->type) {
    case LAROD_MAP_TYPE_INT64:
        valueSz = sizeof(int64_t);
        break;
    case LAROD_MAP_TYPE_STRING:
        valueSz = strnlen(rhs->value, STRING_MAX_LENGTH);
        if (valueSz >= STRING_MAX_LENGTH) {
            // Should never happen...
            goto error;
        }
        valueSz = valueSz + 1; // Add one for the null terminator.
        break;
    case LAROD_MAP_TYPE_INT64_ARRAY_2:
        valueSz = 2 * sizeof(int64_t);
        break;
    case LAROD_MAP_TYPE_INT64_ARRAY_4:
        valueSz = 4 * sizeof(int64_t);
        break;
    default:
        // Should never happen...
        assert(0);
        return false;
    }

    // Copy value.
    lhs->value = malloc(valueSz);
    if (!lhs->value) {
        goto error;
    }

    memcpy(lhs->value, rhs->value, valueSz);
    lhs->type = rhs->type;

    return true;

error:
    free(lhs->key);
    free(lhs->value);
    lhs->key = NULL;
    lhs->value = NULL;

    return false;
}

larodMap* copyMap(const larodMap* map, larodError** error) {
    assert(map);

    // Create a new map with the same length as map to copy.
    larodMap* retMap = newMap(map->capElems, error);
    if (!retMap) {
        goto error;
    }

    // Copy each pair.
    for (size_t i = 0; i < map->numElems; ++i) {
        assert(retMap->elems[i]);
        assert(map->elems[i]);
        if (!copyPair(retMap->elems[i], map->elems[i])) {
            larodCreateError(error, ENOMEM, "Could not copy pair");

            goto error;
        }
    }

    retMap->numElems = map->numElems;

    return retMap;

error:
    larodDestroyMap(&retMap);

    return NULL;
}

bool appendMapToMsg(sd_bus_message* msg, const struct larodMap* map,
                    larodError** error) {
    assert(msg);
    assert(map);

    // Open an array of dictionary entries with string as key and variant as a
    // value.
    int ret = 0;
    ret = sd_bus_message_open_container(msg, SD_BUS_TYPE_ARRAY, "{sv}");
    if (ret < 0) {
        larodCreateError(error, -ret, "Could not open array container: %s",
                         strerror(-ret));

        return false;
    }

    bool hasError = false;
    for (size_t i = 0; !hasError && i < map->numElems; i++) {
        // Open a dictionary entry container.
        ret = sd_bus_message_open_container(msg, 'e', "sv");
        if (ret < 0) {
            larodCreateError(error, -ret, "Could not open dict container: %s",
                             strerror(-ret));
            hasError = true;

            break;
        }

        larodPair* pair = map->elems[i];
        assert(pair);
        assert(pair->key);
        assert(pair->value);

        // Append key to message.
        ret = sd_bus_message_append(msg, "s", pair->key);
        if (ret < 0) {
            larodCreateError(error, -ret, "Could not add key to message: %s",
                             strerror(-ret));
            hasError = true;

            goto closedict;
        }

        // Append value to message.
        switch (pair->type) {
        case LAROD_MAP_TYPE_INT64: {
            int64_t int64Value = *((int64_t*) pair->value);
            ret = sd_bus_message_append(msg, "v", "x", int64Value);
            if (ret < 0) {
                larodCreateError(error, -ret,
                                 "Could not add int64 value to message: %s",
                                 strerror(-ret));
                hasError = true;

                goto closedict;
            }
            break;
        }
        case LAROD_MAP_TYPE_STRING: {
            ret = sd_bus_message_append(msg, "v", "s", pair->value);
            if (ret < 0) {
                larodCreateError(error, -ret,
                                 "Could not add string value to message: %s",
                                 strerror(-ret));
                hasError = true;

                goto closedict;
            }
            break;
        }

        case LAROD_MAP_TYPE_INT64_ARRAY_2: {
            int64_t* array = (int64_t*) pair->value;
            ret = sd_bus_message_append(msg, "v", "ax", 2, array[0], array[1]);
            if (ret < 0) {
                larodCreateError(
                    error, -ret,
                    "Could not add array of two int64 values to message: %s",
                    strerror(-ret));
                hasError = true;

                goto closedict;
            }
            break;
        }
        case LAROD_MAP_TYPE_INT64_ARRAY_4: {
            int64_t* array = (int64_t*) pair->value;
            ret = sd_bus_message_append(msg, "v", "ax", 4, array[0], array[1],
                                        array[2], array[3]);
            if (ret < 0) {
                larodCreateError(
                    error, -ret,
                    "Could not add array of four int64 values to message: %s",
                    strerror(-ret));
                hasError = true;

                goto closedict;
            }
            break;
        }
        default:
            // Should never happen...
            assert(false);
            return false;
        }

    closedict:
        ret = sd_bus_message_close_container(msg);
        if (ret < 0) {
            larodPrependErrorMsg(
                error, "Could not close dict container: %s: ", strerror(-ret));
            hasError = true;
        }
    }

    ret = sd_bus_message_close_container(msg);
    if (ret < 0) {
        larodPrependErrorMsg(
            error, "Could not close array container: %s: ", strerror(-ret));
        hasError = true;
    }

    return !hasError;
}
