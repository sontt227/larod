/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <atomic>
#include <systemd/sd-bus.h>
#include <thread>
#include <unordered_set>

#include "backendunit.hh"
#include "filedescriptor.hh"
#include "larod.h"
#include "sessionsdbusserver.hh"

namespace larod {

// Define a comparison function for std::shared_ptr<JobRequest>.
struct sharedPtrJobReqIsLessThan {
    bool operator()(const std::shared_ptr<JobRequest>& lhs,
                    const std::shared_ptr<JobRequest>& rhs) const {
        return *lhs < *rhs;
    }
};

class Session : public std::enable_shared_from_this<Session> {
public:
    explicit Session(sdbusplus::bus::bus& bus, const std::string objPath,
                     const std::vector<std::unique_ptr<BackEndUnit>>& units);

    /**
     * @brief Destructor.
     *
     * Make sure to call stopBusProcessing() before destructing. Since during
     * destruction, new requests might get submitted on the sd-bus interface,
     * and if we already have other outgoing requests at the same time,
     * potential deadlocks may occur. Even if one knows that there are no
     * outgoing requests (or incoming), stopBusProcessing() should still be
     * called before actually destructing the Session.
     */
    ~Session();

    // Sessions can not be copied
    Session(const Session&) = delete;
    Session& operator=(const Session&) = delete;

    static uint64_t getNextId();

    std::pair<int, int> getClientAsyncFds() const;
    void queueJob(const uint64_t modelId, const size_t chipId,
                  std::vector<Tensor>&& inputTensors,
                  std::vector<Tensor>&& outputTensors, const uint8_t priority,
                  const uint64_t metaData, const ParamsMap&& params = {});
    void queueLoadModel(const FileDescriptor& fd, const uint64_t sz,
                        const size_t chipId, const int32_t access,
                        const std::string name, const uint64_t metaData,
                        const ParamsMap&& params = {});
    void sendJobResult(JobRequest* req);
    void sendLoadModelResult(const LoadModelRequest* req);
    void deleteModel(uint64_t modelId, const size_t chipId);

    /**
     * @brief Stop processing messages buses.
     *
     * This will delete the sd-bus interface server and also close the
     * asynchronous bus processing file descriptors. Note that it is important
     * to call this before destructing Session (c.f. ~Session()).
     */
    void stopBusProcessing() noexcept;

    /**
     * @brief Get the first job request in line for a specific chip.
     *
     * The job requests are queued according to their priorities. This
     * method will return the first in a queue for a chip, i.e. the most
     * prioritized.
     *
     * @param chipId Chip id for the job request to get.
     * @return The first job request in line for @p chipId.
     */
    std::shared_ptr<JobRequest> popJobRequest(const size_t chipId);

    // Return type of createTensorMetadataMsg().
    using TensorMetadataMsg =
        std::tuple<int32_t, int32_t, uint64_t, std::vector<uint64_t>,
                   std::vector<uint64_t>, std::string>;

    // Return type for QueueLoadModel message.
    using LoadModelRetType =
        std::tuple<std::tuple<uint64_t, uint64_t, uint64_t, int32_t, int32_t,
                              std::string, std::vector<TensorMetadataMsg>,
                              std::vector<TensorMetadataMsg>>,
                   uint64_t, int32_t, std::string>;

    // Argument type for tensors in QueueJob message. The tuple contains,
    // in order: data type, layout, byte size, dims vector, pitches vector, fd,
    // fd size, fd offset, fd id, fd properties.
    using QueueJobTensorsMsg = std::vector<
        std::tuple<int32_t, int32_t, uint64_t, std::vector<uint64_t>,
                   std::vector<uint64_t>, sdbusplus::message::unix_fd, uint64_t,
                   int64_t, uint64_t, uint32_t>>;

    // D-Bus interface members.

    // Return type for models().
    using ModelsRetType =
        std::vector<std::tuple<uint64_t, uint64_t, uint64_t, int32_t, int32_t,
                               std::string, std::vector<TensorMetadataMsg>,
                               std::vector<TensorMetadataMsg>>>;

    uint64_t getSessionId() const;
    ModelsRetType getModels() const;

private:
    // For debug output.
    inline static const std::string THIS_NAMESPACE = "Session::";
    inline static std::atomic<uint64_t> nextId = 0;
    static const uint64_t MAX_BUFFERS = 32;

    static TensorMetadataMsg
        createTensorMetadataMsg(const TensorMetadata& tensorMetadata);
    static std::vector<TensorMetadataMsg> createTensorMetadataMsg(
        const std::vector<TensorMetadata>& tensorMetadata);
    static void busDeleter(sd_bus* bus) { sd_bus_flush_close_unref(bus); }
    static void msgDeleter(sd_bus_message* msg) { sd_bus_message_unref(msg); }
    static sd_bus* createAsyncBus(int fd);

    /**
     * @brief Check if sd-bus message contains a dictionary type.
     *
     * It will check for a{sv} in the message signature.
     *
     * @param msg Message to check.
     * @return True if @p msg contains a dictionary type, otherwise false.
     */
    // FIXME: Make msg const when sdbusplus::message::message.get_signature()
    // gets declared as const upstream.
    static bool containsDict(sdbusplus::message::message& msg) {
        static constexpr std::string_view DICT_SIGNATURE = "a{sv}";
        return std::string(msg.get_signature()).find(DICT_SIGNATURE) !=
               std::string::npos;
    }

    /// Sd-bus interface server.
    std::unique_ptr<SessionSdbusServer> sdbusServer;
    /// Bus for reading messages from client.
    std::unique_ptr<sd_bus, decltype(busDeleter)*> asyncReadBus{nullptr,
                                                                busDeleter};
    std::thread busProcThread; ///< Processing thread for asyncReadBus.
    /// Flag to signal busProcThread to exit.
    std::atomic<bool> stopBusProc;
    FileDescriptor busProcEventFd;
    /// Peer handshake flag, indicating if a handshake has been made.
    bool hasHandshaked;

    /// Bus for writing messages to client.
    std::unique_ptr<sd_bus, decltype(busDeleter)*> asyncWriteBus{nullptr,
                                                                 busDeleter};
    FileDescriptor clientAsyncReadFd;
    FileDescriptor clientAsyncWriteFd;
    ///< For sd-bus functions regarding sending messages.
    std::recursive_mutex mtxSend;
    std::mutex mtxPrivateModels; ///< Mutex for set of private models.

    const uint64_t ID;
    const std::string ID_STR;
    const std::vector<std::unique_ptr<BackEndUnit>>& UNITS;
    /// Set of private models for each chip (list index corresponds to the same
    /// chip index in UNITS).
    std::vector<std::unordered_set<uint64_t>> privateModels;
    /// Priority queues with job requests for each chip index.
    std::unordered_map<
        size_t, std::priority_queue<std::shared_ptr<JobRequest>,
                                    std::vector<std::shared_ptr<JobRequest>>,
                                    sharedPtrJobReqIsLessThan>>
        jobReqs;
    /// Mutex for the queued job requests @c jobReqs.
    std::mutex mtxJobReqs;

    /// Maps buffer ID (the ID given to us by the client) to the @c Buffer.
    std::unordered_map<uint64_t, std::shared_ptr<Buffer>> buffers;

    // TODO: static?
    /**
     * @brief Parse serialized tensor messages.
     *
     * This parses the input argument messages for @c Session::procQueueJobMsg()
     * (QueueJobTensorsMsg).
     *
     * @param inputsMsg Input tensors message.
     * @param outputsMsg Output tensors message.
     * @return A tuple containing the parsed input tensors and parsed output
     * tensors, respectively.
     */
    std::tuple<std::vector<Tensor>, std::vector<Tensor>>
        readTensorMsg(QueueJobTensorsMsg inputsMsg,
                      QueueJobTensorsMsg outputsMsg);

    void processAsyncReadBus() noexcept;
    void sendJobResultError(const larodErrorCode error,
                            const std::string& errorMsg,
                            const uint64_t metaData);
    void sendLoadModelResultError(const larodErrorCode error,
                                  const std::string& errorMsg,
                                  const uint64_t metaData);

    void procHandshakeMsg(sd_bus_message* msg);
    void procQueueLoadModelMsg(sd_bus_message* msg);
    void procQueueJobMsg(sd_bus_message* msg);
    void procDeleteModelMsg(sd_bus_message* msg);
    void procAllocTensorsMsg(sd_bus_message* msg);

    /**
     * @brief Allocate buffers for tensors.
     *
     * A successful call will save the buffers in the @c buffers map.
     *
     * @param inputOutputSpecifier Input or output specifier.
     * @param metadata Tensor metadata for which to allocate buffers for.
     * @param ids Buffer IDs to set for the allocated buffers.
     * @param fdProps Fd props to use when allocating.
     * @param params Optional extra paramaters to @p allocator.
     * @return Allocated buffers.
     */
    std::vector<std::shared_ptr<Buffer>> allocTensorBuffers(
        const BackEndUnit& unit, const uint8_t inputOutputSpecifier,
        const uint64_t modelId, const std::vector<uint64_t>& ids,
        const uint32_t fdProps, const ParamsMap& params);

    /**
     * @brief Check if a buffer has been saved before, otherwise save it.
     *
     * This will check if @p buffer has been saved in @c buffers map before
     * (with @p id as key). If it has not been saved before, it will be
     * saved.
     *
     * If it has been already saved, it will then be checked against the saved
     * one. If @p buffer differ from the saved one, an exception will be thrown.
     *
     * @param buffer Buffer to check.
     * @param id Buffer ID to lookup in saved @c buffers.
     * @return Either a saved buffer with ID @p id, or @p buffer (as newly
     * inserted in @c buffers map).
     */
    std::shared_ptr<Buffer> resolveBuffer(std::shared_ptr<Buffer> buffer,
                                          const uint64_t id);

    /**
     * @brief Send an asynchronous message response.
     *
     * This allocates a sd_bus_message with appropriate appending types defined
     * by @p args and sends it over the @c asyncWriteBus. It can handle all
     * fundamental types and as well as strings, vectors, tuples etc. as defined
     * by sdbusplus.
     *
     * @param msgType The type of message.
     * @param args Function paramater pack of the arguments to append in the
     * message (corresponding to the types defined by @p msgType).
     */
    template<typename... Args>
    void sendMsg(const std::string& msgType, Args&&... args);

    void handleSendMsgError(const int retCode, const std::string& msg);

public:
    struct ConnectionError : std::runtime_error {
        ConnectionError(const std::string& msg) : runtime_error(msg) {}
    };

    struct TypeSizeOverflow : std::runtime_error {
        TypeSizeOverflow(const std::string& msg) : runtime_error(msg) {}
    };
};

} // namespace larod
