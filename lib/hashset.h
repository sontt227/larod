/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct HashSet HashSet;

/**
 * @brief Construct an empty hash set.
 *
 * This allocates a handle that needs to be freed with @ref hashSetDestruct()
 * afterwards.
 *
 * @param hashSet An uninitialized handle to a hash set.
 * @return Positive errno style return code (zero means success).
 */
int hashSetConstruct(HashSet** hashSet);

/**
 * @brief Destructs the hash set.
 *
 * @param hashSet Handle to a hash set to deallocate.
 * @return Positive errno style return code (zero means success).
 */
void hashSetDestruct(HashSet** hashSet);

/**
 * @brief Inserts an element.
 *
 * @param hashSet Hash set to insert an element to.
 * @param data Data to insert.
 * @return Positive errno style return code (zero means success).
 */
int hashSetInsert(HashSet** hashSet, void* data);

/**
 * @brief Erases an element.
 *
 * @param hashSet Hash set to erase an element from.
 * @param data Data to erase.
 * @return Positive errno style return code (zero means success). If @p data
 * could not be found, @c ENODATA is returned.
 */
int hashSetErase(HashSet* hashSet, void* data);

/**
 * @brief Get the number of elements.
 *
 * @param hashSet Hash set to get the total number of elements from.
 * @param size Pointer to the returned size.
 * @return Positive errno style return code (zero means success).
 */
int hashSetGetSize(HashSet* hashSet, size_t* size);

/**
 * @brief Extract (by copying) all data pointers from the hash set.
 *
 * This only copies the addresses of the data (the void*) and not the the data
 * it points to itself.
 *
 * @param hashSet Hash set to extract from.
 * @param data Pointer to an array of void pointers where the extracted data
 * will be allocated. After a successful call, you should thus free @p *data!
 * @param dataSize Size of @p *data.
 * @return Positive errno style return code (zero means success).
 */
int hashSetExtractAll(HashSet* hashSet, void*** data, size_t* dataSize);

#ifdef __cplusplus
}
#endif
