/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "sessionsdbusserver.hh"

#include "log.hh"

using namespace std;

namespace larod {

SessionSdbusServer::SessionSdbusServer(sdbusplus::bus::bus& bus,
                                       const string path,
                                       function<uint64_t()> sessionIdFn,
                                       function<ModelsRetType()> modelsFn)
    : SessionServerObj(bus, path.c_str()), OBJ_PATH(path),
      sessionIdFunc(sessionIdFn), modelsFunc(modelsFn) {
    // Affix an object manager for the subtree path for path. This needs to be
    // kept alive as long as this SessionSdbusServer object's lifetime. We just
    // save it in a private member managerPtr.
    managerPtr = make_unique<sdbusplus::server::manager_t>(bus, path.c_str());

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed " + OBJ_PATH);
}

SessionSdbusServer::~SessionSdbusServer() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed " + OBJ_PATH);
}

} // namespace larod
