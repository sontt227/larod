# Copyright 2018 Axis Communications
# SPDX-License-Identifier: Apache-2.0

description: >-
  Interface for larod service.

methods:
  - name: CreateSession
    flags:
      - unprivileged
    description: >-
      Create a new session and return the new session ID and file descriptors
      for asynchronous messaging.
    parameters:
      - name: clientName
        type: string
        description: >-
          D-Bus connection identifier.
      - name: versionMajor
        type: uint32
        description: >-
          Library major version number.
      - name: versionMinor
        type: uint32
        description: >-
          Library minor version number.
      - name: versionPatch
        type: uint32
        description: >-
          Library patch version number.
    returns:
      - name: result
        type: struct[uint64, unixfd, unixfd, int32, string]
        description: >-
          The newly created session ID (uint64), a pair of socket file
          descriptors, an error code and an error message. The first file
          descriptor in the struct is used for reading, the second for writing
          (struct[session ID, read socket, write socket, error code, error
          message]).

properties:
  - name: NbrOfSessions
    type: uint64
    flags:
      - const
    description: >-
      Current number of sessions.

  - name: AvailableChips
    type: array[int32]
    flags:
      - const
    description: >-
      List of available chips. The values in the array are from the enumeration
      larodChip (defined in larod.h), while the index in the list is the unique
      ID for this chip (use this chip ID for calls regarding that chip, for
      example the property ChipId in Sessions). For example, array[2, 4, 1]
      represents:

      0: LAROD_CHIP_TFLITE_CPU       (enumeration value 2)
      1: LAROD_CHIP_TPU              (enumeration value 4)
      2: LAROD_CHIP_DEBUG            (enumeration value 1)

      That is, chip ID 0, 1 and 2 is a LAROD_CHIP_TFLITE_CPU,
      LAROD_CHIP_TPU, and LAROD_CHIP_DEBUG, respectively.
