/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "larod.h"
#include "map.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Type containing job request information.
 */
struct larodJobRequest {
    uint64_t modelId;
    uint64_t chipId;
    larodTensor** inputs;
    size_t numInputs;
    larodTensor** outputs;
    size_t numOutputs;
    larodMap* params;
    uint8_t priority;
};

/**
 * @brief Validate a job request.
 *
 * Validates that all mandatatory fields have been set in a job request.
 *
 * @param An initialized job request handle.
 * @return True if @jobReq is valid, otherwise false.
 */
bool validateJobReqOpt(const larodJobRequest* jobReq);

#ifdef __cplusplus
}
#endif
