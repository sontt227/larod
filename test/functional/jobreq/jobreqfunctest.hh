/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <gtest/gtest.h>
#include <string>

#include "argparse.h"
#include "larod.h"

/**
 * @brief Test fixture for job request tests.
 *
 * GoogleTest fixture for job request tests. The test creates a larod
 * connection and loads a model for each test. Everything is then cleaned up
 * when test case is done.
 */
class JobReqFuncTest : public ::testing::Test {
public:
    void SetUp() override;
    void TearDown() override;

    // Command-line arguments - these are populated by the main function based
    // on the argparse info.
    static bool debugPrints;
    static larodChip chip;
    static std::string modelFile;
    static larodMap* modelParams;
    static larodMap* jobParams;

protected:
    larodConnection* conn = nullptr;
    larodModel* model = nullptr;
    size_t numInputs = 0;
    larodTensor** inputTensors = nullptr;
    size_t numOutputs = 0;
    larodTensor** outputTensors = nullptr;
};
