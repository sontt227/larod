/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once
// It is important that the LAROD_API_VERSION_1 macro is defined before
// including larod.h in every file related to this compatibility test.
#include <string>

#include "larod.h"

namespace larod {
namespace compatv1 {

bool checkLarodResultWithPrint(bool ret, larodError** error);

int getTestModelFd();

void gtestPrint(const std::string& msg);

} // namespace compatv1
} // namespace larod
