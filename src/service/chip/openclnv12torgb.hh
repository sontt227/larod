/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <string>

#define CL_KERNEL(...) #__VA_ARGS__

/**
 * The following NV12 -> RGB conversion is more specifically a YCbCr conversion
 * as defined by Rec.ITU-R BT.601 (used for JPEG File Interchange Format).
 * Essentially, it boils down to the formula:
 *
 * R = Min{Max[0, Round(Y                       + 1.402*(Cr - 128))   ], 255}
 * G = Min{Max[0, Round(Y - 0.344136*(Cb - 128) - 0.714136*(Cr - 128))], 255}
 * B = Min{Max[0, Round(Y + 1.772*(Cb - 128))                         ], 255}
 */
std::string openCLnv12torgbSource(CL_KERNEL(
    __constant float R_CR_CONST = 1.402f;
    __constant float G_CB_CONST = -0.344136f;
    __constant float G_CR_CONST = -0.714136f;
    __constant float B_CB_CONST = 1.772f;

    /* Takes 3 uchar16 of r, g, b, interleaves them, and writes them to out.
     * This is made specifically for use in nv12ToRgb which has values:
     *     r = [r0 r2 r4 r6 r8 r10 r12 r14 r1 r3 r5 r7 r9 r11 r13 r15]
     *     (same for g and b)
     * So this function takes that into account to get the correct rgb-result
     * [r0 g0 b0 r1 g1 b1 ...]
     */
    void interleaveAndWrite(const uchar16 r, const uchar16 g, const uchar16 b,
                            uchar* out) {
        const uchar16 res0 = {r.s0, g.s0, b.s0, r.s8, g.s8, b.s8, r.s1, g.s1,
                              b.s1, r.s9, g.s9, b.s9, r.s2, g.s2, b.s2, r.sa};
        vstore16(res0, 0, out);
        const uchar16 res1 = {g.sa, b.sa, r.s3, g.s3, b.s3, r.sb, g.sb, b.sb,
                              r.s4, g.s4, b.s4, r.sc, g.sc, b.sc, r.s5, g.s5};
        vstore16(res1, 1, out);
        const uchar16 res2 = {b.s5, r.sd, g.sd, b.sd, r.s6, g.s6, b.s6, r.se,
                              g.se, b.se, r.s7, g.s7, b.s7, r.sf, g.sf, b.sf};
        vstore16(res2, 2, out);
    }

    __kernel void nv12ToRgb(__global uchar* nv12, int width, int nv12Pitch,
                            int height, __global uchar* rgb, int rgbPitch) {
        // This kernel is designed to run nv12->rgb conversion
        // on 2 whole rows.
        // The reason we choose to run 2 whole rows is because
        // each CbCr pair corresponds to 2x2 pixels, i.e. 4 output pixels,
        // which means we can reuse calculations across 2 rows.

        const int id = get_global_id(0);
        const int groupSize = get_global_size(0);

        const uchar* cbcr = nv12 + nv12Pitch * height;

        int i = id;
        while (i < height / 2) {
            // For each loop iteration, run over 2 y-rows and one corresponding
            // CbCr-row

            const int y = 2 * i; // Each i corresponds to 2 y-rows
            const uchar* yRow0 = nv12 + y * nv12Pitch; // Y-row index 2*i
            const uchar* yRow1 = yRow0 + nv12Pitch;    // Y-row index 2*i+1

            const uchar* cbcrRow = cbcr + i * nv12Pitch; // CbCr-row index i

            uchar* rgbRow0 = rgb + y * rgbPitch; // RGB-row index 2*i
            uchar* rgbRow1 = rgbRow0 + rgbPitch; // RGB-row index 2*i+1

            // A single row is handled by three separate loops.
            // All loops perform the same calculations.
            // The first loop is a vectorized loop handling 16 elements at a
            // time (8 CbCr pairs). The second loop does 2 elements at a time
            // (one CbCr pair). The final loop is actually an if-statement, and
            // it handles the final pixel for when the image width is odd (still
            // one CbCr pair, but only 1 pixel).

            int x = 0;
            while (x < width - 15) {
                // Load 16 interleaved cbcr
                const uchar16 cbcri = vload16(0, cbcrRow + x);
                const float16 cbcr = convert_float16(cbcri) - 128.0f;

                // cbcr = [cb cr cb cr cb cr cb cr cb cr ...]

                // Un-interleave
                const float8 cb = {cbcr.even};
                const float8 cr = {cbcr.odd};

                // Compute rc, gc, bc constants.
                // These are reused for calculating 2x2=4 rgb pixels,
                // using the formula:
                //     r0=y0+rc, g0=y0+gc, b0=y0+bc,
                //     r1=y1+rc, g1=y1+gc, b1=y1+bc
                //     etc.
                const float8 rc8 = R_CR_CONST * cr;

                // Each rc, gc, bc is duplicated into a 16-vector,
                // since we compute for 16 y-values
                const float16 rc = {rc8, rc8};

                const float8 gc8 = G_CB_CONST * cb + G_CR_CONST * cr;
                const float16 gc = {gc8, gc8};

                const float8 bc8 = B_CB_CONST * cb;
                const float16 bc = {bc8, bc8};

                // Load 16 y-values
                uchar16 yi = vload16(0, yRow0 + x);
                float16 yInterleaved = convert_float16(yi);

                // yInterleaved = [y0 y1 y2 y3 y4 y5 y6 y7 ...]
                // where y0 and y1 use CbCr-pair 0, y2 and y3 use CbCr-pair 1
                // etc. so we need to de-interleave the y-values
                float16 y = {yInterleaved.even, yInterleaved.odd};

                // Compute rgb values
                uchar16 r = convert_uchar16_sat(y + rc);
                uchar16 g = convert_uchar16_sat(y + gc);
                uchar16 b = convert_uchar16_sat(y + bc);

                interleaveAndWrite(r, g, b, rgbRow0 + 3 * x);

                // Here we load and compute for 16 Y-values in row 2*i+1
                yi = vload16(0, yRow1 + x);
                yInterleaved = convert_float16(yi);

                y = (float16){yInterleaved.even, yInterleaved.odd};

                r = convert_uchar16_sat(y + rc);
                g = convert_uchar16_sat(y + gc);
                b = convert_uchar16_sat(y + bc);

                interleaveAndWrite(r, g, b, rgbRow1 + 3 * x);

                x += 16;
            }

            // Handle remainder
            while (x < width - 1) {
                float cb = cbcrRow[x] - 128.0f;
                float cr = cbcrRow[x + 1] - 128.0f;

                float rc = R_CR_CONST * cr;
                float gc = G_CB_CONST * cb + G_CR_CONST * cr;
                float bc = B_CB_CONST * cb;

                float y = yRow0[x];
                rgbRow0[3 * x + 0] = convert_uchar_sat(y + rc);
                rgbRow0[3 * x + 1] = convert_uchar_sat(y + gc);
                rgbRow0[3 * x + 2] = convert_uchar_sat(y + bc);

                y = yRow1[x];
                rgbRow1[3 * x + 0] = convert_uchar_sat(y + rc);
                rgbRow1[3 * x + 1] = convert_uchar_sat(y + gc);
                rgbRow1[3 * x + 2] = convert_uchar_sat(y + bc);

                ++x;

                y = yRow0[x];
                rgbRow0[3 * x + 0] = convert_uchar_sat(y + rc);
                rgbRow0[3 * x + 1] = convert_uchar_sat(y + gc);
                rgbRow0[3 * x + 2] = convert_uchar_sat(y + bc);

                y = yRow1[x];
                rgbRow1[3 * x + 0] = convert_uchar_sat(y + rc);
                rgbRow1[3 * x + 1] = convert_uchar_sat(y + gc);
                rgbRow1[3 * x + 2] = convert_uchar_sat(y + bc);

                ++x;
            }

            // Handle final pixel in odd resolutions
            if (x < width) {
                float cb = cbcrRow[x] - 128.0f;
                float cr = cbcrRow[x + 1] - 128.0f;

                float rc = R_CR_CONST * cr;
                float gc = G_CB_CONST * cb + G_CR_CONST * cr;
                float bc = B_CB_CONST * cb;

                float y = yRow0[x];
                rgbRow0[3 * x + 0] = convert_uchar_sat(y + rc);
                rgbRow0[3 * x + 1] = convert_uchar_sat(y + gc);
                rgbRow0[3 * x + 2] = convert_uchar_sat(y + bc);

                y = yRow1[x];
                rgbRow1[3 * x + 0] = convert_uchar_sat(y + rc);
                rgbRow1[3 * x + 1] = convert_uchar_sat(y + gc);
                rgbRow1[3 * x + 2] = convert_uchar_sat(y + bc);
            }

            i += groupSize;
        }

        // This is identical to the above loop, except it only handles one row,
        // in case we have an image with odd height.
        if (i < (height + 1) / 2) {
            const int y = 2 * i;
            const uchar* yRow0 = nv12 + y * width;

            const uchar* cbcrRow = cbcr + i * width;

            uchar* rgbRow0 = rgb + y * rgbPitch;

            int x = 0;
            while (x < width - 15) {
                const uchar16 cbcri = vload16(0, cbcrRow + x);
                const float16 cbcr = convert_float16(cbcri) - 128.0f;

                const float8 cb = {cbcr.even};
                const float8 cr = {cbcr.odd};

                const float8 rc8 = R_CR_CONST * cr;
                const float16 rc = {rc8, rc8};

                const float8 gc8 = G_CB_CONST * cb + G_CR_CONST * cr;
                const float16 gc = {gc8, gc8};

                const float8 bc8 = B_CB_CONST * cb;
                const float16 bc = {bc8, bc8};

                uchar16 yi = vload16(0, yRow0 + x);
                float16 yInterleaved = convert_float16(yi);

                float16 y = {yInterleaved.even, yInterleaved.odd};

                uchar16 r = convert_uchar16_sat(y + rc);
                uchar16 g = convert_uchar16_sat(y + gc);
                uchar16 b = convert_uchar16_sat(y + bc);

                interleaveAndWrite(r, g, b, rgbRow0 + 3 * x);

                x += 16;
            }

            while (x < width - 1) {
                float cb = cbcrRow[x] - 128.0f;
                float cr = cbcrRow[x + 1] - 128.0f;

                float rc = R_CR_CONST * cr;
                float gc = G_CB_CONST * cb + G_CR_CONST * cr;
                float bc = B_CB_CONST * cb;

                float y = yRow0[x];
                rgbRow0[3 * x + 0] = convert_uchar_sat(y + rc);
                rgbRow0[3 * x + 1] = convert_uchar_sat(y + gc);
                rgbRow0[3 * x + 2] = convert_uchar_sat(y + bc);

                ++x;

                y = yRow0[x];
                rgbRow0[3 * x + 0] = convert_uchar_sat(y + rc);
                rgbRow0[3 * x + 1] = convert_uchar_sat(y + gc);
                rgbRow0[3 * x + 2] = convert_uchar_sat(y + bc);

                ++x;
            }

            if (x < width) {
                float cb = cbcrRow[x] - 128.0f;
                float cr = cbcrRow[x + 1] - 128.0f;

                float rc = R_CR_CONST * cr;
                float gc = G_CB_CONST * cb + G_CR_CONST * cr;
                float bc = B_CB_CONST * cb;

                float y = yRow0[x];
                rgbRow0[3 * x + 0] = convert_uchar_sat(y + rc);
                rgbRow0[3 * x + 1] = convert_uchar_sat(y + gc);
                rgbRow0[3 * x + 2] = convert_uchar_sat(y + bc);
            }
        }
    }));
