/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "tensorformattest.hh"

#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <fcntl.h>
#include <gtest/gtest.h>
#include <iostream>
#include <stdexcept>

#include "testutils.hh"

using namespace std;
using namespace larod::test;

// Parameters from argparse.
string TensorFormatTest::modelFile;
larodMap* TensorFormatTest::modelParams;
larodMap* TensorFormatTest::jobParams;
vector<string> TensorFormatTest::inputFormats;
vector<string> TensorFormatTest::outputFormats;
larodChip TensorFormatTest::chip = LAROD_CHIP_INVALID;

const unordered_map<string, larodTensorDataType> TensorFormatTest::dataTypeMap =
    {
        {"unspecified", LAROD_TENSOR_DATA_TYPE_UNSPECIFIED},
        {"bool", LAROD_TENSOR_DATA_TYPE_BOOL},
        {"uint8", LAROD_TENSOR_DATA_TYPE_UINT8},
        {"int8", LAROD_TENSOR_DATA_TYPE_INT8},
        {"uint16", LAROD_TENSOR_DATA_TYPE_UINT16},
        {"int16", LAROD_TENSOR_DATA_TYPE_INT16},
        {"uint32", LAROD_TENSOR_DATA_TYPE_UINT32},
        {"int32", LAROD_TENSOR_DATA_TYPE_INT32},
        {"uint64", LAROD_TENSOR_DATA_TYPE_UINT64},
        {"int64", LAROD_TENSOR_DATA_TYPE_INT64},
        {"float16", LAROD_TENSOR_DATA_TYPE_FLOAT16},
        {"float32", LAROD_TENSOR_DATA_TYPE_FLOAT32},
        {"float64", LAROD_TENSOR_DATA_TYPE_FLOAT64},
};

const unordered_map<string, larodTensorLayout> TensorFormatTest::layoutMap = {
    {"unspecified", LAROD_TENSOR_LAYOUT_UNSPECIFIED},
    {"nhwc", LAROD_TENSOR_LAYOUT_NHWC},
    {"nchw", LAROD_TENSOR_LAYOUT_NCHW},
    {"nv12", LAROD_TENSOR_LAYOUT_420SP},
};

TensorFormatTest::TensorFormatTest()
    : modelFd(-1), larodConn(nullptr), modelPtr(nullptr) {
}

void TensorFormatTest::SetUp() {
    bool ret = larodConnect(&larodConn, NULL);
    if (!ret || !larodConn) {
        throw runtime_error("Failed to open larodConnection!");
    }

    if (modelFile.size() > 0) {
        modelFd = open(modelFile.c_str(), O_RDONLY);
        if (modelFd < 0) {
            throw runtime_error("Failed to open model file");
        }
    }

    modelPtr = larodLoadModel(larodConn, modelFd, chip, LAROD_ACCESS_PRIVATE,
                              "larod-model", modelParams, NULL);
    if (!modelPtr) {
        throw runtime_error("Failed loading model!");
    }
}

void TensorFormatTest::TearDown() {
    if (modelFd >= 0) {
        close(modelFd);
    }

    if (modelPtr && !larodDeleteModel(larodConn, modelPtr, NULL)) {
        throw runtime_error("Failed deleting model!");
    }

    larodDestroyModel(&modelPtr);

    if (larodConn && !larodDisconnect(&larodConn, NULL)) {
        throw runtime_error("Failed closing larod connection!");
    }

    // Call base GoogleTest Test::TearDown() for framework cleanup.
    Test::TearDown();
}

vector<TensorTuple>
    TensorFormatTest::parseTensors(vector<string> tensorFormats) {
    vector<TensorTuple> tensorTuples;
    for (string tensorFormat : tensorFormats) {
        stringstream sstream(tensorFormat);

        // Retrieve data type.
        string str;
        getline(sstream, str, ':');
        transform(str.begin(), str.end(), str.begin(), ::tolower);
        larodTensorDataType dataType;
        {
            auto it = dataTypeMap.find(str);
            if (it == dataTypeMap.end()) {
                throw runtime_error("Unknown data type: " + str);
            }
            dataType = it->second;
        }

        // Retrieve layout.
        getline(sstream, str, ':');
        transform(str.begin(), str.end(), str.begin(), ::tolower);
        larodTensorLayout layout;
        {
            auto it = layoutMap.find(str);
            if (it == layoutMap.end()) {
                throw runtime_error("Unknown layout: " + str);
            }
            layout = it->second;
        }

        // Retrieve dims.
        larodTensorDims dims = {};
        string dimsStr, dimStr;
        getline(sstream, dimsStr, ':');
        stringstream dimsStream(dimsStr);
        while (getline(dimsStream, dimStr, ',')) {
            stringstream dimStream(dimStr);
            dimStream >> dims.dims[dims.len];
            if (dimStream.fail()) {
                throw runtime_error("Failed to parse dim: " + str);
            }

            ++dims.len;
            if (dims.len > LAROD_TENSOR_MAX_LEN) {
                throw runtime_error("Failed to parse dim: " + str);
            }
        }

        // Retrieve pitches.
        larodTensorPitches pitches = {};
        string pitchesStr, pitchStr;
        getline(sstream, pitchesStr, ':');
        stringstream pitchesStream(pitchesStr);
        while (getline(pitchesStream, pitchStr, ',')) {
            stringstream pitchStream(pitchStr);
            pitchStream >> pitches.pitches[pitches.len];
            if (pitchStream.fail()) {
                throw runtime_error("Failed to parse pitches: " + str);
            }

            ++pitches.len;
            if (pitches.len > LAROD_TENSOR_MAX_LEN) {
                throw runtime_error("Failed to parse pitches: " + str);
            }
        }

        if (pitches.len != dims.len) {
            throw runtime_error("Failed to parse tensor: pitches len (" +
                                to_string(pitches.len) +
                                ") does not match dims len(" +
                                to_string(dims.len) + ")");
        }

        tensorTuples.emplace_back(dataType, layout, dims, pitches);
    }

    return tensorTuples;
}

bool TensorFormatTest::compareTensor(TensorTuple tensorTuple,
                                     larodTensor* tensor) {
    larodError* error = nullptr;

    // Compare data type.
    larodTensorDataType dataType = larodGetTensorDataType(tensor, &error);
    if (dataType == LAROD_TENSOR_DATA_TYPE_INVALID) {
        string msg =
            string("Failed retrieving tensor data type: ") + error->msg;
        larodClearError(&error);
        throw runtime_error(msg);
    }

    larodTensorDataType refType = get<0>(tensorTuple);
    if (dataType != refType) {
        gtestPrint("Model data type was " + to_string(dataType) +
                   ", expected " + to_string(refType));
        return false;
    }

    // Compare tensor layout.
    larodTensorLayout layout = larodGetTensorLayout(tensor, &error);
    if (layout == LAROD_TENSOR_LAYOUT_INVALID) {
        string msg = string("Failed retrieving tensor layout: ") + error->msg;
        larodClearError(&error);
        throw runtime_error(msg);
    }

    larodTensorLayout refLayout = get<1>(tensorTuple);
    if (layout != refLayout) {
        gtestPrint("Model layout was " + to_string(layout) + ", expected " +
                   to_string(refLayout));
        return false;
    }

    // Compare tensor dims.
    const larodTensorDims* modelDims = larodGetTensorDims(tensor, &error);
    if (!modelDims) {
        string msg = string("Failed retrieving tensor dims: ") + error->msg;
        larodClearError(&error);
        throw runtime_error(msg);
    }

    larodTensorDims refDims = get<2>(tensorTuple);

    if (refDims.len != modelDims->len) {
        gtestPrint("Model shows dim len = " + to_string(modelDims->len) +
                   ", expected dim len = " + to_string(refDims.len));
        return false;
    }

    for (size_t i = 0; i < refDims.len; ++i) {
        if (refDims.dims[i] != modelDims->dims[i]) {
            gtestPrint("Model dim was " + to_string(modelDims->dims[i]) +
                       ", expected " + to_string(refDims.dims[i]));
            return false;
        }
    }

    // Compare tensor pitches.
    const larodTensorPitches* modelPitches =
        larodGetTensorPitches(tensor, &error);
    if (!modelPitches) {
        string msg = string("Failed retrieving tensor pitches: ") + error->msg;
        larodClearError(&error);
        throw runtime_error(msg);
    }

    larodTensorPitches refPitches = get<3>(tensorTuple);

    if (refPitches.len != modelPitches->len) {
        gtestPrint("Model shows pitches len = " + to_string(modelPitches->len) +
                   ", expected pitches len = " + to_string(refPitches.len));
        return false;
    }

    for (size_t i = 0; i < refPitches.len; ++i) {
        if (refPitches.pitches[i] != modelPitches->pitches[i]) {
            gtestPrint("Model pitch was " +
                       to_string(modelPitches->pitches[i]) + ", expected " +
                       to_string(refPitches.pitches[i]));
            return false;
        }
    }

    return true;
}

bool TensorFormatTest::tensorFormatHelper(
    function<void(vector<TensorTuplesVector>&, bool&)> tweakFormats,
    const bool expectedOutcome) {
    vector<TensorTuple> inFormatTuples = parseTensors(inputFormats);
    vector<size_t> inTensorSizes(numInputs);
    for (size_t i = 0; i < numInputs; ++i) {
        auto [inOk, inSizeBytes] = tensorSizeBytes(inFormatTuples[i]);
        if (!inOk) {
            throw runtime_error("unspecified values in command line formats");
        }

        // Retrieve size of tensor. Needed to create temporary fds with a size
        // that correctly corresponds to the meta data in the larodTensor.
        inTensorSizes[i] = inSizeBytes;
    }

    vector<TensorTuple> outFormatTuples = parseTensors(outputFormats);
    vector<size_t> outTensorSizes(numOutputs);
    for (size_t i = 0; i < numOutputs; ++i) {
        auto [outOk, outSizeBytes] = tensorSizeBytes(outFormatTuples[i]);
        if (!outOk) {
            throw runtime_error("unspecified values in command line formats");
        }

        // Retrieve size of tensor. Needed to create temporary fds with a size
        // that correctly corresponds to the meta data in the larodTensor.
        outTensorSizes[i] = outSizeBytes;
    }

    bool skip = false;
    vector<TensorTuplesVector> tuples = {inFormatTuples, outFormatTuples};

    // Change something in the tensor meta data using the function
    // tweakFormats(). If the thing to be changed is not allowed to be changed
    // due to rules dictated by larod (e.g. changing the dims.len to something
    // other than 4 for a tensor with layout NHWC) tweakFormats() can signal
    // that the test should be skipped by setting the value of skip to true.
    tweakFormats(tuples, skip);
    if (skip) {
        return true;
    }

    // Create tensors to be used for job.
    larodError* error = nullptr;
    larodTensor** inTensors = larodCreateTensors(numInputs, &error);
    if (!inTensors) {
        string msg = string("failed to create input tensors: ") + error->msg;
        larodClearError(&error);
        throw runtime_error(msg);
    }

    vector<int> inFds(numInputs);
    for (size_t i = 0; i < numInputs; ++i) {
        TensorTuple& inFormatTuple = inFormatTuples[i];
        tupleToTensor(inFormatTuple, inTensors[i]);

        // Retrieve bytes of tensor number i (again), if the size has changed
        // from previous count then use the new count. If the new size could not
        // be computed, then use the old size. (It is not possible to retrieve
        // the new size if pitches are not specified.)
        auto [inUpdated, inSizeNew] = tensorSizeBytes(inFormatTuple);

        size_t inSizeBytes = inUpdated ? inSizeNew : inTensorSizes[i];
        int inFd = createTmpFile(inSizeBytes);
        inFds[i] = inFd;
        if (!larodSetTensorFd(inTensors[i], inFd, &error)) {
            string msg =
                string("failed to create temporary input file: ") + error->msg;
            larodClearError(&error);
            throw runtime_error(msg);
        }

        if (!larodSetTensorFdProps(inTensors[i],
                                   LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP,
                                   &error)) {
            string msg =
                string("failed to set input tensor props: ") + error->msg;
            larodClearError(&error);
            throw runtime_error(msg);
        }
    }

    // Create tensors to be used for job.
    larodTensor** outTensors = larodCreateTensors(numOutputs, &error);
    if (!outTensors) {
        string msg = string("failed to create output tensors: ") + error->msg;
        larodClearError(&error);
        throw runtime_error(msg);
    }

    vector<int> outFds(numOutputs);
    for (size_t i = 0; i < numOutputs; ++i) {
        TensorTuple& outFormatTuple = outFormatTuples[i];
        tupleToTensor(outFormatTuple, outTensors[i]);

        // Retrieve bytes of tensor number i (again), if the size has changed
        // from previous count then use the new count. If the new size could not
        // be computed, then use the old size. (It is not possible to retrieve
        // the new size if pitches are not specified.)
        auto [outUpdated, outSizeNew] = tensorSizeBytes(outFormatTuple);

        size_t outSizeBytes = outUpdated ? outSizeNew : outTensorSizes[i];
        int outFd = createTmpFile(outSizeBytes);
        outFds[i] = outFd;
        if (!larodSetTensorFd(outTensors[i], outFd, &error)) {
            string msg =
                string("failed to create temporary output file: ") + error->msg;
            larodClearError(&error);
            throw runtime_error(msg);
        }

        if (!larodSetTensorFdProps(outTensors[i],
                                   LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP,
                                   &error)) {
            string msg =
                string("failed to set output tensor props: ") + error->msg;
            larodClearError(&error);
            throw runtime_error(msg);
        }
    }

    larodJobRequest* jobReq =
        larodCreateJobRequest(modelPtr, inTensors, numInputs, outTensors,
                              numOutputs, jobParams, &error);
    if (!jobReq) {
        string msg = string("failed to create job request: ") + error->msg;
        larodClearError(&error);
        throw runtime_error(msg);
    }

    bool ret = larodRunJob(larodConn, jobReq, &error);

    larodDestroyTensors(&inTensors, numInputs);
    larodDestroyTensors(&outTensors, numOutputs);
    larodDestroyJobRequest(&jobReq);
    for (int inFd : inFds) {
        close(inFd);
    }
    for (int outFd : outFds) {
        close(outFd);
    }
    larodClearError(&error);
    return ret == expectedOutcome;
}

void TensorFormatTest::tupleToTensor(TensorTuple formatTuple,
                                     larodTensor* tensor) {
    larodError* error = nullptr;
    larodTensorDataType dataType = get<0>(formatTuple);
    larodTensorLayout layout = get<1>(formatTuple);
    larodTensorDims dims = get<2>(formatTuple);

    if (!larodSetTensorDataType(tensor, dataType, &error)) {
        string msg = string("failed to set tensor data type: ") + error->msg;
        larodClearError(&error);
        throw runtime_error(msg);
    }

    if (!larodSetTensorDims(tensor, &dims, &error)) {
        string msg = string("failed to set tensor dims: ") + error->msg;
        larodClearError(&error);
        throw runtime_error(msg);
    }

    if (!larodSetTensorLayout(tensor, layout, &error)) {
        string msg = string("failed to set tensor layout: ") + error->msg;
        larodClearError(&error);
        throw runtime_error(msg);
    }
}

pair<bool, size_t>
    TensorFormatTest::tensorSizeBytes(const TensorTuple& tensorTuple) {
    larodTensorPitches pitches = get<3>(tensorTuple);
    if (pitches.len == 0) {
        return {false, 0};
    }

    return {true, pitches.pitches[0]};
}
