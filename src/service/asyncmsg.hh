/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <condition_variable>
#include <exception>

#include "msg.hh"

namespace larod {

class AsyncMsg : public Msg {
public:
    AsyncMsg(std::weak_ptr<Session> session);
    virtual ~AsyncMsg() = default;

    virtual void signal() = 0;
};

} // namespace larod
