/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <com/axis/Larod1/Session/server.hpp>
#include <sdbusplus/server.hpp>

namespace larod {

using SessionServerObj =
    sdbusplus::server::object_t<sdbusplus::com::axis::Larod1::server::Session>;

using TensorMetaData =
    std::tuple<int32_t, int32_t, uint64_t, std::vector<uint64_t>,
               std::vector<uint64_t>, std::string>;

// Return type for SessionSdbusServer::models().
using ModelsRetType = std::vector<
    std::tuple<uint64_t, uint64_t, uint64_t, int32_t, int32_t, std::string,
               std::vector<TensorMetaData>, std::vector<TensorMetaData>>>;

/**
 * @brief Sd-bus interface server for a Session.
 *
 * This class implements a D-Bus interface generated from sdbusplus.
 */
class SessionSdbusServer : private SessionServerObj {
public:
    SessionSdbusServer(sdbusplus::bus::bus& bus, const std::string path,
                       std::function<uint64_t()> sessionIdFunc,
                       std::function<ModelsRetType()> modelsFunc);
    ~SessionSdbusServer();

    // D-Bus interface members.

    uint64_t sessionId() const override { return sessionIdFunc(); }
    ModelsRetType models() const override { return modelsFunc(); }

private:
    // For debug output.
    static inline const std::string THIS_NAMESPACE =  "SessionSdbusServer::";

    const std::string OBJ_PATH;
    std::unique_ptr<sdbusplus::server::manager_t> managerPtr;
    std::function<uint64_t()> sessionIdFunc;
    std::function<ModelsRetType()> modelsFunc;
};

} // namespace larod
