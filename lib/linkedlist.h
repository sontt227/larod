/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct Node Node;

struct Node {
    void* data;
    Node* next;
};

typedef struct LinkedList LinkedList;

/**
 * @brief Constructs an empty list.
 *
 * This allocates a handle that needs to be freed with @ref listDestruct()
 * afterwards.
 *
 * @param list An uninitialized handle to a list.
 * @return Positive errno style return code (zero means success).
 */
int listConstruct(LinkedList** list);

/**
 * @brief Destructs the list.
 *
 * @param list Handle to a list to deallocate.
 * @return Positive errno style return code (zero means success).
 */
void listDestruct(LinkedList** list);

/**
 * @brief Adds an element to the end.
 *
 * @param list List to add an element to.
 * @param data Data to add.
 * @return Positive errno style return code (zero means success).
 */
int listPushBack(LinkedList* list, void* data);

/**
 * @brief Erases an element.
 *
 * @param list List to erase an element from.
 * @param data Data to erase.
 * @return Positive errno style return code (zero means success). If @p data
 * could not be found, @c ENODATA is returned.
 */
int listErase(LinkedList* list, void* data);

/**
 * @brief Get number of elements.
 *
 * @param list List to get the number of elements of.
 * @param size Pointer to the returned size.
 * @return Positive errno style return code (zero means success).
 */
int listGetSize(LinkedList* list, uint64_t* size);

/**
 * @brief Get first node.
 *
 * @param list List to get first node from.
 * @param firstNode Pointer to pointer for the returning first node. If the list
 * is empty, *firstNode will be returned as NULL.
 * @return Positive errno style return code (zero means success).
 */
int listGetFirstNode(LinkedList* list, const Node** firstNode);

#ifdef __cplusplus
}
#endif
