/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "buffer.hh"

#include <assert.h>
#include <cstring>
#include <fcntl.h>
#include <limits>
#include <stdexcept>
#include <sys/mman.h>
#include <unistd.h>
#include <utility>

#include "larod.h"
#include "log.hh"

using namespace std;

namespace larod {

Buffer::Buffer(FileDescriptor&& fileDesc, const size_t size,
               const int64_t offset, const uint32_t props)
    : fd(std::move(fileDesc)), maxSize(size), startOffset(offset),
      fdProps(props), COOKIE(nextCookie++) {
    if (startOffset < 0) {
        throw invalid_argument("Offset is negative");
    }

    if (static_cast<uint64_t>(startOffset) > numeric_limits<size_t>::max()) {
        throw invalid_argument("Offset is too big (" + to_string(startOffset) +
                               " > " +
                               to_string(numeric_limits<size_t>::max()) + ")");
    }

    if (maxSize && (static_cast<size_t>(startOffset) > maxSize)) {
        throw invalid_argument("Offset (" + to_string(startOffset) +
                               ") is greater than max size (" +
                               to_string(maxSize) + ")");
    }

    if (fdProps &
        ~(LAROD_FD_PROP_READWRITE | LAROD_FD_PROP_MAP | LAROD_FD_PROP_DMABUF)) {
        throw invalid_argument("Fd properties flag (" + to_string(fdProps) +
                               ") is invalid");
    }
}

Buffer::~Buffer() {
    // Clear all context-based data to this buffer in the backends. No need to
    // lock bufferObservers mutex since this destructor is alone accessing it.
    for (BufferContextCache* cache : bufferObservers) {
        LOG.debug(THIS_NAMESPACE + __func__ + "(): Deleting context...");
        cache->deleteBufferContext(COOKIE);
    }
}

void Buffer::read(const span<uint8_t>& buf, const size_t sz) const {
    // Verify that the size of the buffer we want to read from the fd is not
    // larger than max fd size.
    if (maxSize && (sz > maxSize)) {
        throw FileDescriptorError("Fd max size is smaller than buffer size (" +
                                  to_string(maxSize) + " < " + to_string(sz) +
                                  ")");
    }

    lock_guard<mutex> lockGuard(mtxFileOffset);

    if (lseek(fd, startOffset, SEEK_SET) == -1) {
        throw FileDescriptorError("Could not go to file descriptor offset " +
                                  to_string(startOffset) + ": " +
                                  strerror(errno));
    }

    size_t readBytes = fd.read(buf, sz);
    if (readBytes != sz) {
        throw FileDescriptorError("Could only read " + to_string(readBytes) +
                                  " out of " + to_string(sz) +
                                  " bytes from file descriptor");
    }
}

void Buffer::read(const span<uint8_t>& buf) const {
    read(buf, buf.size());
}

void Buffer::write(const span<uint8_t>& buf, const size_t sz) const {
    // Verify that the size of the buffer we want to write to the fd is not
    // larger than max fd size.
    if (maxSize && (sz > maxSize)) {
        throw FileDescriptorError("Fd max size is smaller than buffer size (" +
                                  to_string(maxSize) + " < " + to_string(sz) +
                                  ")");
    }

    lock_guard<mutex> lockGuard(mtxFileOffset);

    if (lseek(fd, startOffset, SEEK_SET) == -1) {
        throw FileDescriptorError("Could not go to file descriptor offset " +
                                  to_string(startOffset) + ": " +
                                  strerror(errno));
    }

    size_t writtenBytes = fd.write(buf, sz);
    if (writtenBytes != sz) {
        throw FileDescriptorError("Could only write " +
                                  to_string(writtenBytes) + " out of " +
                                  to_string(sz) + " bytes to file descriptor");
    }
}

void Buffer::write(const span<uint8_t>& buf) const {
    write(buf, buf.size());
}

int Buffer::getMaxProt(int fd, const int reqMmapProt) {
    int fileStatus = fcntl(fd, F_GETFL);
    if (fileStatus == -1) {
        throw FileDescriptorError(string("Could not get file's status: ") +
                                  strerror(errno));
    }

    // We set up our mmap prot flag with as many access rights as possible in
    // case buffer will need to be used for several purposes.
    switch (fileStatus & O_ACCMODE) {
    case O_RDONLY:
        if (reqMmapProt & PROT_WRITE) {
            throw FileDescriptorError(
                "This fd cannot be mapped for writing "
                "(using PROT_WRITE) due to missing file writing access rights");
        }
        return PROT_READ;
    case O_WRONLY:
        if (reqMmapProt & PROT_READ) {
            throw FileDescriptorError(
                "This fd cannot be mapped for reading "
                "(using PROT_READ) due to missing file reading access rights");
        }
        return PROT_WRITE;
    case O_RDWR:
        return (PROT_READ | PROT_WRITE);
    default:
        throw FileDescriptorError(
            "This fd cannot be mapped for neither reading nor writing due to "
            "missing file access rights");
    }
}

void Buffer::map(const size_t sz, const int reqMmapProt) {
    assert(!mappedAddr);
    assert(!mappedAccessAddr);
    assert(!mappedAccessSz);
    assert(mappedMmapProt == -1);
    assert(!maxSize || (static_cast<size_t>(startOffset) + sz <= maxSize));

    // Check file size.
    off_t fileSize = lseek(fd, 0, SEEK_END);
    if (fileSize == -1) {
        throw FileDescriptorError("Could not lseek to end of buffer: " +
                                  string(strerror(errno)));
    }

    // FIXME: Unsafe cast to off_t from size_t.
    if (fileSize < static_cast<off_t>(sz) + startOffset) {
        throw FileDescriptorError(
            "The size of the buffer is smaller than requested (" +
            to_string(fileSize) + " < " +
            to_string(static_cast<off_t>(sz) + startOffset) + ")");
    }

    int mmapProt = getMaxProt(fd, reqMmapProt);

    // Since the offset argument to fd.map() must be aligned to
    // sysconf(_SC_PAGE_SIZE) we decrement offset by its difference with the
    // closest page size multiplier before creating the mapping.
    off_t mapOffset =
        static_cast<off_t>(startOffset) & ~(sysconf(_SC_PAGE_SIZE) - 1);
    long long offsetDiff = static_cast<long long>(startOffset) - mapOffset;
    size_t mapSz = sz + static_cast<size_t>(offsetDiff);

    try {
        mappedAddr = fd.map(mapOffset, mapSz, mmapProt);
    } catch (const runtime_error& e) {
        throw FileDescriptorError(e.what());
    }

    mappedAccessAddr = mappedAddr.get() + offsetDiff;
    mappedAccessSz = sz;
    mappedMmapProt = mmapProt;
}

uint8_t* Buffer::getMapping(const size_t sz, const int reqMmapProt) {
    if (maxSize && (static_cast<size_t>(startOffset) + sz > maxSize)) {
        throw FileDescriptorError(
            "Start offset (" + to_string(startOffset) + ") + size (" +
            to_string(sz) +
            ") to be mapped is larger than the size of the fd (" +
            to_string(maxSize) + ")");
    }
    if (!reqMmapProt) {
        throw FileDescriptorError("A valid prot must be provided");
    }
    if (reqMmapProt & (~(PROT_READ | PROT_WRITE))) {
        throw FileDescriptorError("Only PROT_READ and PROT_WRITE are "
                                  "valid prot. Given prot was " +
                                  to_string(reqMmapProt));
    }

    // Avoid concurrently mapping @c fd from several threads.
    lock_guard<mutex> lockGuard(mtxMapping);

    if (mappedAddr) {
        if (mappedAccessSz < sz) {
            throw FileDescriptorError(
                "Cached fd mapping is smaller than requested size (" +
                to_string(mappedAccessSz) + " < " + to_string(sz) + ")");
        }
        if ((reqMmapProt & PROT_READ) && !(mappedMmapProt & PROT_READ)) {
            throw FileDescriptorError("Cached mapping of fd has been mapped "
                                      "without read access rights");
        }
        if ((reqMmapProt & PROT_WRITE) && !(mappedMmapProt & PROT_WRITE)) {
            throw FileDescriptorError("Cached mapping of fd has been mapped "
                                      "without write access rights");
        }

        return mappedAccessAddr;
    }

    map(sz, reqMmapProt);

    return mappedAccessAddr;
}

void Buffer::addObserver(BufferContextCache* cache) {
    lock_guard<mutex> lockGuard(mtxObservers);
    const auto& [ignore, hasInserted] = bufferObservers.insert(cache);
    if (!hasInserted) {
        throw logic_error("Observer already added");
    }
}

string Buffer::getPropNames(uint32_t props) {
    string names = "";
    if (props & LAROD_FD_PROP_READWRITE) {
        names += "LAROD_FD_PROP_READWRITE";
    }
    if (props & LAROD_FD_PROP_MAP) {
        if (!names.empty()) {
            names += ", ";
        }
        names += "LAROD_FD_PROP_MAP";
    }
    if (props & LAROD_FD_PROP_DMABUF) {
        if (!names.empty()) {
            names += ", ";
        }
        names += "LAROD_FD_PROP_DMABUF";
    }

    return names;
}

} // namespace larod
