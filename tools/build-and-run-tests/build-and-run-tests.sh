#!/bin/bash

# Run a few basic functional tests against debug chip.
#
# Requirements:
#
# - dbus
#
# Example usage:
#
#   Running without arguments (latest tests against latest service):
#   dbus-run-session -- ./tools/build-and-run-tests/build-and-run-tests.sh
#
#   Running with arguments (old functional tests against newer service):
#   dbus-run-session -- ./tools/build-and-run-tests/build-and-run-tests.sh \
#       R2.2.0 R1.14.14

# Abort script if any command fails.
set -e

# Print commands.
set -x

# Initial cleanup.
rm -rf build-latest build-old

# Build larod.
if [ "$1" ]; then
    git checkout "$1"
fi
meson -Duse_session_bus=true -Db_sanitize=address,undefined build-latest
ninja -C build-latest

# Let tests use this liblarod.so.
export LD_LIBRARY_PATH=$(realpath build-latest/lib)

# Start larod.
export ASAN_OPTIONS=fast_unwind_on_malloc=0
export DBUS_STARTER_ADDRESS=$DBUS_SESSION_BUS_ADDRESS
./build-latest/src/service/larod &

# If a second version is specified, build that version.
if [ "$2" ]; then
    git checkout "$2"
fi
meson -Duse_session_bus=true -Db_sanitize=address,undefined build-old
ninja -C build-old

# Run tests. Some tests do not exist in older versions of larod, so we have to
# check if the files exist to be able to run this script in our backwards
# compatibility testing.

# larod 1.14.14
if [ -f ./build-old/test/functional/inference/larod-inf-functional-test ]; then
    # Some test cases fail because some error checks have been moved from
    # larodRunJob to larodCreateJobRequest.
    GTEST_FILTER=":*InputWithOffset"
    GTEST_FILTER+=":*InputWithTooLargeOffset"
    GTEST_FILTER+=":*OutputWithBadSize"
    ./build-old/test/functional/inference/larod-inf-functional-test \
        --gtest_filter=-$GTEST_FILTER
fi

# larod >= 2.0.0
if [ -f ./build-old/test/functional/job/larod-job-functional-test ]; then
    ./build-old/test/functional/job/larod-job-functional-test
fi

# larod >= 2.2.x
if [ -f ./build-old/test/functional/jobreq/larod-jobreq-functional-test ]; then
    ./build-old/test/functional/jobreq/larod-jobreq-functional-test
fi

# Run test binaries of potentially old repo, but against latest larod library
# and service.
./build-old/test/functional/connection/larod-conn-functional-test
./build-old/test/functional/chip/larod-chip-functional-test
./build-old/test/functional/model/larod-model-functional-test
./build-old/test/functional/tensor/larod-tensor-functional-test
./build-old/test/functional/version/larod-version-functional-test

# Run unit tests in latest repo.
./build-latest/test/functional/api-compat/v1/larod-compat-v1-functional-test
./build-latest/test/unit/lib/larod-lib-unit-test
./build-latest/test/unit/service/larod-service-unit-test
./build-latest/test/unit/service/larod-service-filedescriptor-unit-test

# Stop larod.
kill %%

# Wait for larod to terminate.
while pidof larod; do
    sleep 1
done
