/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "log.hh"

#include <iomanip>
#include <iostream>
#include <syslog.h>

using namespace std;
using namespace std::chrono;

namespace larod {

Log::Log() {
    // Connect to syslog
    int opts = LOG_NDELAY;
#ifdef DEBUG
    opts |= LOG_CONS;
#endif
    int facility = LOG_DAEMON;
    openlog(nullptr, opts, facility);
}

#ifndef DEBUG
void Log::debug(const string&) {
#else
void Log::debug(const string& msg) {
    if (printToSyslog) {
        syslog(LOG_DEBUG, "%s", msg.c_str());
    }

    if (PRINT_TO_TERMINAL) {
        print(ANSI_BOLD + "DEBUG: " + msg + ANSI_RESET, cout);
    }
#endif
}

void Log::info(const string& msg) {
    if (printToSyslog) {
        syslog(LOG_INFO, "%s", msg.c_str());
    }

    if (PRINT_TO_TERMINAL) {
        print(msg, cout);
    }
}

void Log::warning(const string& msg) {
    if (printToSyslog) {
        syslog(LOG_WARNING, "%s", msg.c_str());
    }

    if (PRINT_TO_TERMINAL) {
        print(ANSI_BOLD_COLOR_YELLOW + "WARNING: " + msg + ANSI_RESET, cerr);
    }
}

void Log::error(const string& msg) {
    if (printToSyslog) {
        syslog(LOG_ERR, "%s", msg.c_str());
    }

    if (PRINT_TO_TERMINAL) {
        print(ANSI_BOLD_COLOR_RED + "ERROR: " + msg + ANSI_RESET, cerr);
    }
}

void Log::print(const string& msg, ostream& os) {
    auto time = system_clock::now();
    auto timet = system_clock::to_time_t(time);
    auto millis =
        duration_cast<milliseconds>(time - time_point_cast<seconds>(time))
            .count();

    lock_guard<mutex> lockGuard(mtx);

    ios prevState(nullptr);
    prevState.copyfmt(os);
    os << put_time(localtime(&timet), TIME_FORMAT.c_str()) << "."
       << setfill('0') << setw(3) << millis << " " << msg << "\n";
    os.copyfmt(prevState);
}

} // namespace larod
