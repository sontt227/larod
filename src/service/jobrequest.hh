/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <variant>
#include <vector>

#include "asyncmsg.hh"
#include "buffer.hh"
#include "paramsmap.hh"
#include "tensor.hh"

namespace larod {

class JobRequest : public AsyncMsg {
public:
    JobRequest(std::weak_ptr<Session> session, const uint64_t modelId,
               std::vector<Tensor>&& inputTensors,
               std::vector<Tensor>&& outputTensors, const uint8_t priority,
               const uint64_t metaData, const ParamsMap&& params);

    void signal() override;

    bool operator<(const JobRequest& other) const {
        if (this->PRIORITY == other.PRIORITY) {
            // Compare if this is older than other.
            return this->CREATION_TIME > other.CREATION_TIME;
        }

        return this->PRIORITY < other.PRIORITY;
    }

    const uint64_t MODEL_ID;
    std::vector<Tensor> INPUT_TENSORS;
    std::vector<Tensor> OUTPUT_TENSORS;
    const uint8_t PRIORITY;
    const uint64_t META_DATA;
    const ParamsMap PARAMS;
};

} // namespace larod
