@mainpage Overview

This is the documentation of the liblarod C API.

The entire API of liblarod is contained in `larod.h`.

There are also some documents with various information (that can be found under
"Related Pages" in html docs):
* [\"Introduction for app developers\"](introduction-for-app-developers.md)
  gives a general introduction to larod.
* [\"Preprocessing\"](preprocessing.md) contains a detailed description of
  the preprocessing capabilities of larod.
* [\"Neural network inference\"](nn-inference.md) contains descriptions of the
  various neural network inference backends of larod.
* [\"Data formats in larod\"](model-formats.md) describes the data format of
  models.
* Instructions for the (DEPRECATED) larod-convert tool that converts models into
  `.larod` format can be found in
  [\"Model converter tool\"](../tools/model-converter/README.md). NOTE: It is no
  longer required to wrap models in `.larod` format and this tool will be
  removed in the future.
