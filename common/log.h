/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <stdbool.h>

#ifdef __cplusplus
#ifdef DEBUG
#include <iostream>
#define D(x)                                                                   \
    do {                                                                       \
        std::cerr << __FILE__ << "(" << __LINE__ << "): " << x << std::endl;   \
    } while (0)
#define DD(x)                                                                  \
    do {                                                                       \
        x                                                                      \
    } while (0)
#define DI(x) x
#else
#define D(x)
#define DD(x)
#define DI(x)
#endif

extern "C" {
#endif

/**
 * @brief Print debug information.
 *
 * @param debug Debug flag. If it is true, the message will print, otherwise
 *              not.
 * @param fmt printf-type formatted debug message to log.
 */
__attribute__((format(printf, 2, 3))) void logDebug(bool debug, const char* fmt,
                                                    ...);

/**
 * @brief Print information.
 *
 * @param fmt printf-type formatted information message to log.
 */
__attribute__((format(printf, 1, 2))) void logInfo(const char* fmt, ...);

/**
 * @brief Print warning.
 *
 * @param fmt printf-type formatted warning message to log.
 */
__attribute__((format(printf, 1, 2))) void logWarning(const char* fmt, ...);

/**
 * @brief Print error.
 *
 * @param fmt printf-type formatted error message to log.
 */
__attribute__((format(printf, 1, 2))) void logError(const char* fmt, ...);

#ifdef __cplusplus
}
#endif
