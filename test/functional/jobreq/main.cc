/**
 * Copyright 2021 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <gtest/gtest.h>

#include "argparse.h"
#include "jobreqfunctest.hh"
#include "larod.h"
#include "testmacros.h"
#include "testutils.hh"
#include "utils.h"

using namespace std;
using namespace larod::test;

TEST_F(JobReqFuncTest, CreateJobRequestInvalidParameter) {
    larodError* error = nullptr;
    larodJobRequest* jobReq = nullptr;

    jobReq =
        larodCreateJobRequest(nullptr, inputTensors, numInputs, outputTensors,
                              numOutputs, jobParams, &error);
    ASSERT_EQ(jobReq, nullptr);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    jobReq = larodCreateJobRequest(model, nullptr, numInputs, outputTensors,
                                   numOutputs, jobParams, &error);
    ASSERT_EQ(jobReq, nullptr);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    jobReq = larodCreateJobRequest(model, inputTensors, numInputs, nullptr,
                                   numOutputs, jobParams, &error);
    ASSERT_EQ(jobReq, nullptr);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(JobReqFuncTest, SetJobRequestOutputsInvalidParameter) {
    larodError* error = nullptr;
    larodJobRequest* jobReq = nullptr;

    jobReq =
        larodCreateJobRequest(model, inputTensors, numInputs, outputTensors,
                              numOutputs, jobParams, &error);
    ASSERT_NE(jobReq, nullptr);
    ASSERT_EQ(error, nullptr);

    bool ret =
        larodSetJobRequestOutputs(nullptr, outputTensors, numOutputs, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    ret = larodSetJobRequestOutputs(jobReq, nullptr, numOutputs, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    larodDestroyJobRequest(&jobReq);
}

TEST_F(JobReqFuncTest, SetJobRequestInputsInvalidParameter) {
    larodError* error = nullptr;
    larodJobRequest* jobReq = nullptr;

    jobReq =
        larodCreateJobRequest(model, inputTensors, numInputs, outputTensors,
                              numOutputs, jobParams, &error);
    ASSERT_NE(jobReq, nullptr);
    ASSERT_EQ(error, nullptr);

    bool ret =
        larodSetJobRequestInputs(nullptr, inputTensors, numInputs, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    ret = larodSetJobRequestInputs(jobReq, nullptr, numInputs, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    larodDestroyJobRequest(&jobReq);
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

    args_t args;
    if (parseArgs(argc, argv, &args) != 0) {
        destroyArgs(&args);
        exit(EXIT_FAILURE);
    }

    JobReqFuncTest::chip = args.chip;
    JobReqFuncTest::debugPrints = args.debugPrints;
    JobReqFuncTest::modelParams = args.modelParams;
    JobReqFuncTest::jobParams = args.jobParams;

    if (args.model) {
        JobReqFuncTest::modelFile = args.model;
    } else if (!args.hasModelParams && args.chip == LAROD_CHIP_DEBUG) {
        // If using debugchip without model and parameters, use a default dummy
        // model file. This to allow running the test without arguments.
        JobReqFuncTest::modelFile = "/etc/passwd";

        gtestPrint("No model file specified -- using /etc/passwd");
    }

    int ret = RUN_ALL_TESTS();

    destroyArgs(&args);

    return ret;
}
