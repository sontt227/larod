/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <algorithm>
#include <gtest/gtest.h>

#include "larod.h"

using namespace std;

/**
 * @brief Helper macro to verify failing API call.
 *
 * Helper macro to verify that a Larod function returns valid errors upon
 * failure: return value should be 'false', a non-null larodError pointer is
 * created and correct error code should be set in the larodError instance.
 *
 * @param retVal Return value from API call, expected to be 'false'.
 * @param errStructPtr The larodError* that was used in Larod call.
 * @param errCode Expected error code, matched towards errStructPtr->code.
 */
#define ASSERT_LAROD_ERROR(retVal, errStructPtr, errCode)                      \
    ASSERT_FALSE(retVal);                                                      \
    ASSERT_NE(errStructPtr, nullptr);                                          \
    ASSERT_EQ(errStructPtr->code, errCode);                                    \
    larodClearError(&errStructPtr);

// Verify basic functionality chip related API calls
TEST(larodFunctionalTest, Chip) {
    larodError* error = nullptr;
    larodConnection* conn = nullptr;

    bool ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    // Make sure at least one chip is listed
    larodChip* chips = nullptr;
    size_t numChips = 0;
    ret = larodListChips(conn, &chips, &numChips, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(numChips, 0);
    ASSERT_NE(chips, nullptr);
    ASSERT_EQ(error, nullptr);

    // Make sure we get a valid chip name
    const char* chipName = larodGetChipName(chips[0]);
    ASSERT_NE(chipName, "Invalid chip");
    ASSERT_NE(chipName, nullptr);
    free(chips);

    ret = larodDisconnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_EQ(error, nullptr);
}

// Check input parameter validation of larodListChips
TEST(larodFunctionalTest, LarodListChipsInvalidParameter) {
    larodError* error = nullptr;
    larodConnection* conn = nullptr;
    larodChip* chips = nullptr;
    size_t numChips = 0;

    bool ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    ret = larodListChips(nullptr, nullptr, nullptr, &error);
    ASSERT_LAROD_ERROR(ret, error, EINVAL);

    ret = larodListChips(conn, nullptr, nullptr, &error);
    ASSERT_LAROD_ERROR(ret, error, EINVAL);

    ret = larodListChips(nullptr, &chips, nullptr, &error);
    ASSERT_LAROD_ERROR(ret, error, EINVAL);

    ret = larodListChips(nullptr, nullptr, &numChips, &error);
    ASSERT_LAROD_ERROR(ret, error, EINVAL);

    ret = larodListChips(conn, &chips, nullptr, &error);
    ASSERT_LAROD_ERROR(ret, error, EINVAL);

    ret = larodListChips(conn, nullptr, &numChips, &error);
    ASSERT_LAROD_ERROR(ret, error, EINVAL);

    ret = larodDisconnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_EQ(error, nullptr);
}
