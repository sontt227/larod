/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

#include "larod.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct args_t {
    larodMap* modelParams;
    larodMap* jobParams;
    char* inputFilePath;
    char* verificationFilePath;
    larodChip chip;
    unsigned int randomSeed;
    unsigned int maxAbsDiffThreshold;
    float meanAbsDiffThreshold;
    bool debugPrints;
    bool verifyOutput;
} args_t;

int parseArgs(int argc, char** argv, args_t* args);

/**
 * @brief Destructs arguments.
 *
 * This deallocates necessary members in @p args.
 *
 * @param args Arguments to destruct.
 */
void destroyArgs(args_t* args);

#ifdef __cplusplus
}
#endif
