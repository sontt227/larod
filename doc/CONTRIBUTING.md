Contributing
============

## Code style

The code style is based on the LLVM coding standard with some minor
modifications (e.g. indent width 4, regrouping of includes and
more) and with the following rules:

1. Variable names and functions are in camelCase, i.e. starting with lower case.
   See rule 10 for an exception to this.
2. Classes are in CamelCase, i.e. starting with upper case.
3. Constants are in upper cases.
4. Namespaces are in lower cases.
5. Inline comments starts with `//`. Comments regarding documentation of
   classes, functions and such (see rule 6) have the format:
   ```c
    /**
     * ...
     * ...
     */
   ```
6. Code documentation is written in [DoxyGen][2] format.
7. In a class declaration, public access specifier is declared first, then comes
   the protected and finally the private section. There are however some
   exceptions for nested classes and such; please see existing code for
   convention.
8. Within an access specifier in a class declaration, member functions are
   declared first then the member variables. The only exception is the private
   access section where variables come before private member
   functions. Moreover, static variables are declared first among the variables.
9. Braces should always be used for if-statements, for-loops and while-loops
   (even if it is only a single line in the body).
10. As implied by rule 1, function names must not contain underscores. However
   to distinguish different versions of the same function (needed for symbol
   versioning) function names should be suffixed by `_vX`, where X is a positive
   integer dictating what version of the function it is.
11. The `__asm__` directives required for symbol versioning should be placed at
   the top of the file in which their corresponding functions are defined, as
   typically one would do with static function declarations.
12. [Experimental](#experimental) features should be marked clearly as such in
   a way similar to the following:
   ```c
   /// WARNING: This is an experimental function which is subject to change.
   void someExperimentalFunction();
   ```
   Note that it should be in Doxygen formatting when applicable.
13. Declarations for API functions whose signatures have changed between major
   versions should have their name suffixed `_API_VERSION_X` for some major
   version number X.
14. Deprecated functions and types belonging to an older major version of the
   API should be clearly documented in `larod.h` as such. Moreover, there
   should be a brief explanation of how their functionality has been replaced
   by the current API so that app developers get some pointers on how to update
   their app to a newer larod version.

Please see the code for some examples of the coding standard.

### Style checker

The tool [clang-format][1] is used as style checker and for full details please
see the file `.clang-format` in the root directory. If you want to
check the code style on your latest commit, run the following command in the
root directory (where the folder `.git` and file `.clang-format` resides):

```
git diff -U0 --no-color HEAD^ | clang-format-diff -p 1 -iregex ".*\.(c|cc|h|hh)"
```

Note that you may have to specify the installed version, for example
`clang-format-diff-7.0`.

#### Editor plugins

There are various editor plugins for clang-format to help you reformat your code
correctly according to the code style. For Vim and Emacs, please see this
[guide][1].

## Testing

All test are found in the `test` directory.

### Policy

The intention is to have both unit tests and functional tests for
functionality pertaining to both liblarod and the larod service, but without
making mocks that are hard to maintain, and to have as little redundancy in our
testing as possible, that is, not have tests overlapping in scope.

#### Library

A function in `lib` should be unit tested if the following is true:

* it's not declared in larod.h,
* it's not making any calls to sd-bus.

Otherwise it should be covered by functional tests.

#### Service

A class in `src/service` should be unit tested if it does not depend on external
libraries (such as sdbusplus or Tensorflow Lite). Otherwise it should be covered
by functional tests.

### Running unit tests

Simply run:

```
    ninja test
```

in your build catalog, or call the built binaries directly from where they
reside in the build tree (path: `build/test/unit`).

## Experimental

See `BUILD.md` for a description of what experimental entails.

### Implementing experimental features

Doing this requires one to clearly mark the feature as experimental. See point
12. of [Code style](#code-style).

### Using experimental features

As a developer working on larod one needs to take careful note about whether
an interface one is using is experimental seeing as it is as such subject to
change.

## Backward compatibility

See section \"Backward compatibility\" of
[\"Introduction for app developers\"](introduction-for-app-developers.md)
for an introduction to what kind of backward compatibility larod aims to
provide.

### ABI backward compatibility

ABI backward compatibility in liblarod is implemented using symbol versioning.
As such:

* New API functions need to be added to `larod.map` in order to be exposed in
  liblarod.
* If an API function's signature is changed, `__asm__` directives that direct an
  API function name exposed by the new liblarod to an implementation of the old
  behavior of the changed function must be added.

### API backward compatibility

API backward compatibilty is implemented in liblarod by using `#ifdef`
directives in `larod.h` that allow users to choose which version of the API to
use with `LAROD_API_VERSION_X` macros. To maintain this:

* Old versions of changed API functions are supported by typedef:ing if
  possible, but giving them a new liblarod symbol if required. This symbol is
  declared in `larod.h` along with suitable `#ifdef`s and a #define macro
  pointing the old API function name to the new symbol, and defined in the
  relevant `.c` file.
* Old versions of changed API types can hopefully be typedef:ed to the new
  version of the type, wrapped by `#ifdef`s.
* API functions and types for removed functionality can simply be kept, but with
  their declarations wrapped with `#ifdef`s in `larod.h`.

[1]: https://clang.llvm.org/docs/ClangFormat.html
[2]: http://www.doxygen.nl/
