larod machine learning service
==============================

```
 ,dPYb,                                           8I
 IP'`Yb                                           8I
 I8  8I                                           8I
 I8  8'                                           8I
 I8 dP    ,gggg,gg   ,gggggg,    ,ggggg,    ,gggg,8I
 I8dP    dP"  "Y8I   dP""""8I   dP"  "Y8gggdP"  "Y8I
 I8P    i8'    ,8I  ,8'    8I  i8'    ,8I i8'    ,8I
,d8b,_ ,d8,   ,d8b,,dP     Y8,,d8,   ,d8' d8,   ,d8b,
8P'"Y88P"Y8888P"`Y88P      `Y8P"Y8888P"   "Y8888P"`Y8
```

larod is a system service providing acceleration of machine learning
computation, more specifically accelerating inferences of artificial neural
networks.

For application developers there is a general
[introduction-to-larod](doc/introduction-for-app-developers.md) document that
covers the basics of what a user of larod ought to know. For developers of
larod, there is a document describing the [architecture](doc/architecture.md)
details of larod.

## Building

Build instructions can be found [here](doc/BUILD.md).

## Contributing

Contribution guidelines can be found [here](doc/CONTRIBUTING.md).
